import uproot
import numpy as np
np.set_printoptions(suppress=True) # print arrays without e notation
np.set_printoptions(threshold=10000)
import pandas as pd
pd.set_option('display.max_columns', None)  
import os.path
import warnings
from pandas.core.common import SettingWithCopyWarning
warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)


import argparse
parser = argparse.ArgumentParser(description='Make dataframe with tracks')
parser.add_argument('--I', type=str, default='df_N2C1m.pickle', help='inputfile')
parser.add_argument('--O', type=str, default='tracks_N2C1m.pickle', help='Output file name')
args = parser.parse_args()

inputfile = args.I
outputName = args.O

input_df = pd.read_pickle(inputfile)

features = ['Sample','DataframeEntry','LeadingJetPt','DPhiJetMET','MET','TrackPt','TrackD0','trkDeltaZ0SinTheta','TrackD0Sig','minDeltaR','DPhiTrackMET','trkOrigin','EventNumber','eventWeight'] 
dictionary = {}
output_df = pd.DataFrame(dictionary, columns=features) # columns names

i=0
for entry in input_df.index:
    if(entry%100 == 0): print('Processing entry: {}'.format(entry))
    for track in range(len(input_df['trkPt'][entry])):
        if input_df['IsMETAligned_track'][entry][track] == True:
            i += 1
            output_df.loc[i] = [input_df['Sample'][entry],
                                entry,
                                input_df['LeadingJetPt'][entry],
                                input_df['DPhiJetMET'][entry], 
                                input_df['met_Et'][entry], 
                                input_df['trkPt'][entry][track], 
                                np.fabs(input_df['trkD0'][entry][track]), 
                                np.fabs(input_df['trkZ0'][entry][track]*np.sin(input_df['trkTheta'][entry][track])), 
                                np.fabs(input_df['trkD0'][entry][track]/input_df['trkD0Err'][entry][track]), 
                                input_df['minDeltaR_BaselineAny_trk'][entry][track],
                                input_df['DPhiTrackMET'][entry][track],
                                input_df['trkOrigin'][entry][track],
                                input_df['EventNumber'][entry],
                                input_df['eventWeight'][entry]
 ]

output_df.to_pickle(outputName)
