import os

dummy_signal = ['user.jshahini.mc16_13TeV.500978.MGPy8EG_A14N23LO_N2N1_150p7_150p35_150p0_MET75.recon.DAOD_SUSY19_v0.1.e8253_a875_r10201_EXT0']

Znunu_samples = ['user.tlari.Znunu_MAXHTPTV140_280.prod9jan_EXT0',
'user.tlari.Znunu_MAXHTPTV140_280_BCFilterBVeto.prod9jan_EXT0',
'user.tlari.Znunu_MAXHTPTV140_280_BFilter.prod9jan_EXT0',
'user.tlari.Znunu_MAXHTPTV280_500.prod9jan_EXT0',
'user.tlari.Znunu_MAXHTPTV280_500_BFilter.prod9jan_EXT0',
'user.tlari.Znunu_MAXHTPTV280_500_CFilterBVeto.prod9jan_EXT0',
'user.tlari.Znunu_MAXHTPTV500_1000.prod9jan_EXT0',
'user.tlari.Znunu_MAXHTPTV1000_ECMS.prod9jan_1f_EXT0']

Wmunu_samples = ['user.ymino.364156.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364157.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364158.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364159.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364162.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364163.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364164.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_BFilter.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364165.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364166.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364167.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0',
'user.ymino.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.DAOD.e5340_s3126_r9364_r9315_2021-01-12-3-57_EXT0']

Wenu_samples = ['user.ymino.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364171.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364173.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364174.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364176.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364177.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364179.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364180.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364181.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364182.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV500_1000.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0',
'user.ymino.364183.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV1000_E_CMS.DAOD.e5340_s3126_r9364_r9315_2021-01-18-12-0_EXT0']


Wtaunu_samples = ['user.ymino.364184.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364185.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364186.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_BFilter.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364187.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364188.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364189.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_BFilter.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364190.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364191.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364192.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_BFilter.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364193.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CVetoBVeto.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364194.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CFilterBVeto.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364195.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_BFilter.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364196.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV500_1000.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0',
'user.ymino.364197.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV1000_E_CMS.DAOD.e5340_s3126_r9364_r9315_2020-11-19-1-42_EXT0']

# MakeDataframe.py
'''
for sample in dummy_signal:
    cmd = 'python3 -u MakeDataframe.py --S 500978 --I /storage/amurrone/DisplacedTrack/ntuples/' + sample + '/data-tree/' + sample + '.root --O all_df/' + sample + '.pickle'
    os.system(cmd)

'''

'''
for sample in Znunu_samples:
    cmd = 'nohup python3 -u MakeDataframe.py --S Znunu --I /storage/amurrone/DisplacedTrack/ntuples/' + sample + '/data-tree/' + sample + '.root --O all_df/' + sample + '.pickle > all_df/' + sample + '.out &'
    os.system(cmd)

for sample in Wenu_samples:
    cmd = 'nohup python3 -u MakeDataframe.py --S Wenu --I /storage/amurrone/DisplacedTrack/ntuples/' + sample + '/data-tree/' + sample + '.root --O all_df/' + sample + '.pickle > all_df/' + sample + '.out &'
    os.system(cmd)

for sample in Wmunu_samples:
    cmd = 'nohup python3 -u MakeDataframe.py --S Wmunu --I /storage/amurrone/DisplacedTrack/ntuples/' + sample + '/data-tree/' + sample + '.root --O all_df/' + sample + '.pickle > all_df/' + sample + '.out &'
    os.system(cmd)

for sample in Wtaunu_samples:
    cmd = 'nohup python3 -u MakeDataframe.py --S Wtaunu --I /storage/amurrone/DisplacedTrack/ntuples/' + sample + '/data-tree/' + sample + '.root --O all_df/' + sample + '.pickle > all_df/' + sample + '.out &'
    os.system(cmd)
'''



# SaveTracks.py
for sample in dummy_signal:
    cmd = 'nohup python3 -u SaveTracks.py --I all_df/' + sample + '.pickle --O all_df_tracks/' + sample + '.pickle > all_df_tracks/' + sample + '.out &'
    os.system(cmd)


'''
for sample in Znunu_samples:
    cmd = 'nohup python3 -u SaveTracks.py --I all_df/' + sample + '.pickle --O all_df_tracks/' + sample + '.pickle > all_df_tracks/' + sample + '.out &'
    os.system(cmd)

for sample in Wenu_samples:
    cmd = 'nohup python3 -u SaveTracks.py --I all_df/' + sample + '.pickle --O all_df_tracks/' + sample + '.pickle > all_df_tracks/' + sample + '.out &'
    os.system(cmd)

for sample in Wmunu_samples:
    cmd = 'nohup python3 -u SaveTracks.py --I all_df/' + sample + '.pickle --O all_df_tracks/' + sample + '.pickle > all_df_tracks/' + sample + '.out &'
    os.system(cmd)

for sample in Wtaunu_samples:
    cmd = 'nohup python3 -u SaveTracks.py --I all_df/' + sample + '.pickle --O all_df_tracks/' + sample + '.pickle > all_df_tracks/' + sample + '.out &'
    os.system(cmd)
'''

