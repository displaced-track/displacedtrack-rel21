//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Mar 11 12:20:13 2022 by ROOT version 6.22/00
// from TTree analysis/analysis
// found on file: user.amurrone.28353558._000001.tree.root
//////////////////////////////////////////////////////////

#ifndef analysis_h
#define analysis_h

#include <boost/algorithm/string.hpp>

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "TVector3.h"
#include "TLorentzVector.h"
#include "vector"

class analysis {

   public :

   TTree*          fChainNominal; // !pointer to the analyzed nominal/trk syst TTree or TChain 
   TTree*	   fChainSyst; // !pointer to the analyzed syst TTree or TChain
   Int_t           fCurrent; // !current Tree number in a TChain

   // Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types of nominal tree
   Double_t        trigWeight_singleElectronTrig;
   Bool_t          trigMatch_singleElectronTrig;
   Double_t        trigWeight_singlePhotonTrig;
   Bool_t          trigMatch_singlePhotonTrig;
   Double_t        trigWeight_singleMuonTrig;
   Bool_t          trigMatch_singleMuonTrig;
   Double_t        trigWeight_metTrig;
   Bool_t          trigMatch_metTrig;
   Bool_t          IsMETTrigPassed;
   Int_t           year;
   Float_t         mu;
   Float_t         actual_mu;
   Int_t           nVtx;
   TVector3        *privtx;
   TVector3        *beampos;
   Bool_t          passBadTileJetVeto;
   Bool_t          passLooseBadJetVeto;
   Bool_t          passTightBadJetVeto;
   vector<float>   *TruthJetPt;
   vector<float>   *TruthJetEta;
   vector<float>   *TruthJetPhi;
   vector<float>   *TruthJetM;
   vector<int>     *TruthJetLabel;
   Float_t         polWeight;
   Bool_t          passEmulTightJetCleaning1Jet;
   Bool_t          passEmulTightJetCleaning2Jet;
   Int_t           nLep_base;
   Int_t           nMu_base;
   Int_t           nEle_base;
   Int_t           nLep_signal;
   Int_t           nMu_signal;
   Int_t           nEle_signal;
   Int_t           lep1Flavor;
   Int_t           lep1Charge;
   Int_t           lep1Author;
   Float_t         lep1Pt;
   Float_t         lep1Eta;
   Float_t         lep1Phi;
   Float_t         lep1M;
   Float_t         lep1MT_Met;
   Float_t         lep1MT_Met_LepInvis;
   Float_t         lep1_DPhiMet;
   Float_t         lep1_DPhiMet_LepInvis;
   Float_t         lep1D0;
   Float_t         lep1D0Sig;
   Float_t         lep1Z0;
   Float_t         lep1Z0SinTheta;
   Bool_t          lep1Signal;
   Double_t        lep1Weight;
   Int_t           lep2Flavor;
   Int_t           lep2Charge;
   Int_t           lep2Author;
   Float_t         lep2Pt;
   Float_t         lep2Eta;
   Float_t         lep2Phi;
   Float_t         lep2M;
   Float_t         lep2MT_Met;
   Float_t         lep2MT_Met_LepInvis;
   Float_t         lep2_DPhiMet;
   Float_t         lep2_DPhiMet_LepInvis;
   Float_t         lep2D0;
   Float_t         lep2D0Sig;
   Float_t         lep2Z0;
   Float_t         lep2Z0SinTheta;
   Bool_t          lep2Signal;
   Double_t        lep2Weight;
   Int_t           nPhoton_base;
   Int_t           nPhoton_signal;
   Int_t           nTrkVec_base;
   Int_t           nTrkVec_signal;
   vector<bool>    *trkVecisTight;
   vector<bool>    *trkVecisLoose;
   vector<float>   *trkVecPt;
   vector<float>   *trkVecEta;
   vector<float>   *trkVecPhi;
   vector<int>     *trkVecQ;
   vector<float>   *trkVecZ0;
   vector<float>   *trkVecZ0Origin;
   vector<float>   *trkVecZ0Err;
   vector<float>   *trkVecZ0SinTheta;
   vector<float>   *trkVecD0;
   vector<float>   *trkVecD0Sig;
   vector<float>   *trkVecpixeldEdx;
   vector<float>   *trkVecpixeldEdx_Corrected;
   vector<float>   *trkVecpixeldEdx_DataDriven;
   vector<float>   *trkVecpixeldEdx_Final;
   vector<float>   *trkVecqOverP;
   vector<int>     *trkVecnIBLOFdEdx;
   vector<int>     *trkVecnUsedHitsdEdx;
   vector<float>   *trkVecdEdxMassRaw;
   vector<float>   *trkVecdEdxMassCorrected;
   vector<float>   *trkVecdEdxMassDataDriven;
   vector<float>   *trkVecdEdxMassFinal;
   vector<float>   *trkVecdEdxMass;
   vector<float>   *trkVecNNScore;
   vector<int>     *trkVecnIBLHits;
   vector<bool>    *trkVecisBaseMuon;
   vector<bool>    *trkVecisBaseElectron;
   vector<bool>    *trkVecisBasePhoton;
   vector<bool>    *trkVecisSignalMuon;
   vector<bool>    *trkVecisSignalElectron;
   vector<bool>    *trkVecisSignalPhoton;
   vector<int>     *trkVecLabel;
   vector<int>     *trkVecDef;
   vector<int>     *trkVecpdgId;
   vector<int>     *trkVecnpar;
   vector<int>     *trkVecparbarcode;
   vector<int>     *trkVecparpdgId;
   vector<int>     *trkVecparnchild;
   vector<int>     *trkVecparchildpdgId;
   vector<float>   *trkVecparchildpt;
   vector<int>     *trkVecparchildbarcode;
   vector<int>     *trkVecBarcode;
   vector<int>     *trkVecStatus;
   vector<int>     *trkVecType;
   vector<int>     *trkVecOrigin;
   vector<bool>    *trkVecIsTruthBSM;
   vector<float>   *trkVecPtcone40;
   vector<float>   *trkVecNonBaseAssocPtcone40;
   vector<float>   *trkVecNonBaseAssocPtcone30;
   vector<float>   *trkVecNonBasePhotonConvPtcone40;
   vector<float>   *trkVecNonBasePhotonConvPtcone30;
   vector<float>   *trkVecNonSignalAssocPtcone40;
   vector<float>   *trkVecNonSignalPhotonConvPtcone40;
   vector<float>   *trkVecBaseLepPhotonMinDR;
   vector<float>   *trkVecSignalLepPhotonMinDR;
   vector<float>   *trkVecTopoEtcone40;
   vector<float>   *trkVecTopoEtclus40;
   vector<float>   *trkVecBaseLepPhotonSubtractedTopoEtclus40;
   vector<float>   *trkVecBaseLepPhotonSubtractedCoreConeTopoEtclus40;
   vector<float>   *trkVecSignalLepPhotonSubtractedTopoEtclus40;
   vector<float>   *trkVecSignalLepPhotonSubtractedCoreConeTopoEtclus40;
   vector<float>   *trkVecDPhiMet;
   vector<float>   *trkVecIso;
   vector<float>   *trkVecM;
   vector<bool>    *trkVecTightPrimary;
   vector<float>   *trkVecIsoTightPrimary;
   vector<bool>    *trkVecMatchedToV0;
   vector<bool>    *trkVecMatchedToVsi;
   vector<float>   *trkVecMatchedVsiMass;
   Int_t           nTrk_base;
   Int_t           nTrk_signal;
   Float_t         trkPt;
   Float_t         trkEta;
   Float_t         trkPhi;
   Int_t           trkQ;
   Float_t         trkZ0;
   Float_t         trkZ0Err;
   Float_t         trkZ0SinTheta;
   Float_t         trkD0;
   Float_t         trkD0Err;
   Float_t         trkD0Sig;
   Bool_t          trkisSecTrk;
   Bool_t          trkisBaseMuon;
   Bool_t          trkisBaseElectron;
   Bool_t          trkisBasePhoton;
   Bool_t          trkisSignalMuon;
   Bool_t          trkisSignalElectron;
   Bool_t          trkisSignalPhoton;
   Int_t           trkLabel;
   Int_t           trkpdgId;
   Int_t           trkparpdgId;
   Int_t           trkType;
   Int_t           trkOrigin;
   Bool_t          trkIsTruthBSM;
   Float_t         trkNonSignalPhotonConvPtcone40;
   Float_t         trkSignalLepPhotonMinDR;
   Float_t         trkSignalLepPhotonSubtractedCoreConeTopoEtclus40;
   Float_t         trkIso;
   Bool_t          trkTightPrimary;
   Bool_t          trkMatchedToV0;
   Bool_t          trkMatchedToVsi;
   Float_t         trkMatchedVsiMass;
   Float_t         trkNNScore;
   Float_t         trkpixeldEdx_DataDriven;
   Float_t         trkdEdxMassRaw;
   Float_t         trkdEdxMassDataDriven;
   Int_t           nJet20;
   Int_t           nJet30;
   vector<float>   *jetPt;
   vector<float>   *jetEta;
   vector<float>   *jetPhi;
   vector<float>   *jetM;
   vector<bool>    *jetIsB;
   vector<int>     *jetTruthLabel;
   vector<int>     *jetNtrk;
   vector<float>   *jetTiming;
   vector<float>   *jetJVT;
   Int_t           nBJet20;
   Int_t           nBJet30;
   Float_t         met_Et;
   Float_t         met_Phi;
   Float_t         metTruth_Et;
   Float_t         metTruth_Phi;
   Float_t         met_track_Et;
   Float_t         met_track_Phi;
   Float_t         met_Signif;
   Float_t         metMuon_Et;
   Float_t         metMuon_Phi;
   Float_t         met_muons_invis_Et;
   Float_t         met_muons_invis_Phi;
   Float_t         met_muons_invis_Signif;
   Float_t         minDPhi_met_muons_invis_allJets20;
   Float_t         minDPhi_met_muons_invis_allJets30;
   Float_t         met_electrons_invis_Et;
   Float_t         met_electrons_invis_Phi;
   Float_t         met_electrons_invis_Signif;
   Float_t         minDPhi_met_electrons_invis_allJets20;
   Float_t         minDPhi_met_electrons_invis_allJets30;
   Float_t         met_photons_invis_Et;
   Float_t         met_photons_invis_Phi;
   Float_t         met_photons_invis_Signif;
   Float_t         minDPhi_met_photons_invis_allJets20;
   Float_t         minDPhi_met_photons_invis_allJets30;
   Float_t         met_leptons_invis_Et;
   Float_t         met_leptons_invis_Phi;
   Float_t         met_leptons_invis_Signif;
   Float_t         minDPhi_met_leptons_invis_allJets20;
   Float_t         minDPhi_met_leptons_invis_allJets30;
   Float_t         met_Et_LepInvis;
   Float_t         met_Phi_LepInvis;
   Float_t         met_Signif_LepInvis;
   Float_t         minDPhi_met_allJets20;
   Float_t         minDPhi_met_allJets30;
   Float_t         minDPhi_met_4j30;
   Float_t         minDPhi_trackMet_allJets20;
   Float_t         minDPhi_trackMet_allJets30;
   Float_t         HtJet30;
   Float_t         meffInc30;
   Float_t         mt_tau;
   Float_t         mt_taujet;
   Bool_t          hasZCand;
   Float_t         mll;
   TLorentzVector  *Photon;
   Float_t         Photon_met_Et;
   Float_t         Photon_Topoetcone40;
   Int_t           Photon_conversionType;
   Float_t         Photon_conversionRadius;
   TString         *DecayProcess;
   Double_t        pileupWeight;
   Double_t        leptonWeight;
   Double_t        eventWeight;
   Double_t        genWeight;
   Double_t        bTagWeight;
   Double_t        trigWeight;
   Double_t        jvtWeight;
   Double_t        photonWeight;
   Double_t        genWeightUp;
   Double_t        genWeightDown;
   Float_t         x1;
   Float_t         x2;
   Float_t         pdf1;
   Float_t         pdf2;
   Float_t         scalePDF;
   Int_t           id1;
   Int_t           id2;
   Float_t         leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         leptonWeight_MUON_EFF_ISO_STAT__1down;
   Float_t         leptonWeight_MUON_EFF_ISO_STAT__1up;
   Float_t         leptonWeight_MUON_EFF_ISO_SYS__1down;
   Float_t         leptonWeight_MUON_EFF_ISO_SYS__1up;
   Float_t         leptonWeight_MUON_EFF_RECO_STAT__1down;
   Float_t         leptonWeight_MUON_EFF_RECO_STAT__1up;
   Float_t         leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down;
   Float_t         leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up;
   Float_t         leptonWeight_MUON_EFF_RECO_SYS__1down;
   Float_t         leptonWeight_MUON_EFF_RECO_SYS__1up;
   Float_t         leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down;
   Float_t         leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up;
   Float_t         leptonWeight_MUON_EFF_TTVA_STAT__1down;
   Float_t         leptonWeight_MUON_EFF_TTVA_STAT__1up;
   Float_t         leptonWeight_MUON_EFF_TTVA_SYS__1down;
   Float_t         leptonWeight_MUON_EFF_TTVA_SYS__1up;
   Float_t         trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t         trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t         trigWeight_MUON_EFF_TrigStatUncertainty__1down;
   Float_t         trigWeight_MUON_EFF_TrigStatUncertainty__1up;
   Float_t         trigWeight_MUON_EFF_TrigSystUncertainty__1down;
   Float_t         trigWeight_MUON_EFF_TrigSystUncertainty__1up;
   Float_t         bTagWeight_FT_EFF_B_systematics__1down;
   Float_t         bTagWeight_FT_EFF_B_systematics__1up;
   Float_t         bTagWeight_FT_EFF_C_systematics__1down;
   Float_t         bTagWeight_FT_EFF_C_systematics__1up;
   Float_t         bTagWeight_FT_EFF_Light_systematics__1down;
   Float_t         bTagWeight_FT_EFF_Light_systematics__1up;
   Float_t         bTagWeight_FT_EFF_extrapolation__1down;
   Float_t         bTagWeight_FT_EFF_extrapolation__1up;
   Float_t         bTagWeight_FT_EFF_extrapolation_from_charm__1down;
   Float_t         bTagWeight_FT_EFF_extrapolation_from_charm__1up;
   Float_t         jvtWeight_JET_JvtEfficiency__1down;
   Float_t         jvtWeight_JET_JvtEfficiency__1up;
   Float_t         jvtWeight_JET_fJvtEfficiency__1down;
   Float_t         jvtWeight_JET_fJvtEfficiency__1up;
   Float_t         pileupWeightUp;
   Float_t         pileupWeightDown;
   ULong64_t       PRWHash;
   ULong64_t       EventNumber;
   Float_t         xsec;
   Float_t         GenHt;
   Float_t         GenMET;
   Int_t           DatasetNumber;
   Int_t           RunNumber;
   Int_t           RandomRunNumber;
   Int_t           FS;
   vector<float>   *LHE3Weights;
   vector<TString> *LHE3WeightNames;
   
   // Declaration of leaf types of syst tree. TrkVectors 
   // aren't need since they are copied from nominal
   Double_t        trigWeight_singleElectronTrig_syst;
   Bool_t          trigMatch_singleElectronTrig_syst;
   Double_t        trigWeight_singlePhotonTrig_syst;
   Bool_t          trigMatch_singlePhotonTrig_syst;
   Double_t        trigWeight_singleMuonTrig_syst;
   Bool_t          trigMatch_singleMuonTrig_syst;
   Double_t        trigWeight_metTrig_syst;
   Bool_t          trigMatch_metTrig_syst;
   Bool_t          IsMETTrigPassed_syst;
   Int_t           year_syst;
   Float_t         mu_syst;
   Float_t         actual_mu_syst;
   Int_t           nVtx_syst;
   TVector3        *privtx_syst;
   TVector3        *beampos_syst;
   Bool_t          passBadTileJetVeto_syst;
   Bool_t          passLooseBadJetVeto_syst;
   Bool_t          passTightBadJetVeto_syst;
   vector<float>   *TruthJetPt_syst;
   vector<float>   *TruthJetEta_syst;
   vector<float>   *TruthJetPhi_syst;
   vector<float>   *TruthJetM_syst;
   vector<int>     *TruthJetLabel_syst;
   Float_t         polWeight_syst;
   Bool_t          passEmulTightJetCleaning1Jet_syst;
   Bool_t          passEmulTightJetCleaning2Jet_syst;
   Int_t           nLep_base_syst;
   Int_t           nMu_base_syst;
   Int_t           nEle_base_syst;
   Int_t           nLep_signal_syst;
   Int_t           nMu_signal_syst;
   Int_t           nEle_signal_syst;
   Int_t           lep1Flavor_syst;
   Int_t           lep1Charge_syst;
   Int_t           lep1Author_syst;
   Float_t         lep1Pt_syst;
   Float_t         lep1Eta_syst;
   Float_t         lep1Phi_syst;
   Float_t         lep1M_syst;
   Float_t         lep1MT_Met_syst;
   Float_t         lep1MT_Met_LepInvis_syst;
   Float_t         lep1_DPhiMet_syst;
   Float_t         lep1_DPhiMet_LepInvis_syst;
   Float_t         lep1D0_syst;
   Float_t         lep1D0Sig_syst;
   Float_t         lep1Z0_syst;
   Float_t         lep1Z0SinTheta_syst;
   Bool_t          lep1Signal_syst;
   Double_t        lep1Weight_syst;
   Int_t           lep2Flavor_syst;
   Int_t           lep2Charge_syst;
   Int_t           lep2Author_syst;
   Float_t         lep2Pt_syst;
   Float_t         lep2Eta_syst;
   Float_t         lep2Phi_syst;
   Float_t         lep2M_syst;
   Float_t         lep2MT_Met_syst;
   Float_t         lep2MT_Met_LepInvis_syst;
   Float_t         lep2_DPhiMet_syst;
   Float_t         lep2_DPhiMet_LepInvis_syst;
   Float_t         lep2D0_syst;
   Float_t         lep2D0Sig_syst;
   Float_t         lep2Z0_syst;
   Float_t         lep2Z0SinTheta_syst;
   Bool_t          lep2Signal_syst;
   Double_t        lep2Weight_syst;
   Int_t           nPhoton_base_syst;
   Int_t           nPhoton_signal_syst;
   Int_t           nTrk_base_syst;
   Int_t           nTrk_signal_syst;
   Float_t         trkPt_syst;
   Float_t         trkEta_syst;
   Float_t         trkPhi_syst;
   Int_t           trkQ_syst;
   Float_t         trkZ0_syst;
   Float_t         trkZ0Err_syst;
   Float_t         trkZ0SinTheta_syst;
   Float_t         trkD0_syst;
   Float_t         trkD0Err_syst;
   Float_t         trkD0Sig_syst;
   Bool_t          trkisSecTrk_syst;
   Bool_t          trkisBaseMuon_syst;
   Bool_t          trkisBaseElectron_syst;
   Bool_t          trkisBasePhoton_syst;
   Bool_t          trkisSignalMuon_syst;
   Bool_t          trkisSignalElectron_syst;
   Bool_t          trkisSignalPhoton_syst;
   Int_t           trkLabel_syst;
   Int_t           trkpdgId_syst;
   Int_t           trkparpdgId_syst;
   Int_t           trkType_syst;
   Int_t           trkOrigin_syst;
   Bool_t          trkIsTruthBSM_syst;
   Float_t         trkNonSignalPhotonConvPtcone40_syst;
   Float_t         trkSignalLepPhotonMinDR_syst;
   Float_t         trkSignalLepPhotonSubtractedCoreConeTopoEtclus40_syst;
   Float_t         trkIso_syst;
   Bool_t          trkTightPrimary_syst;
   Bool_t          trkMatchedToV0_syst;
   Bool_t          trkMatchedToVsi_syst;
   Float_t         trkMatchedVsiMass_syst;
   Float_t         trkNNScore_syst;
   Float_t         trkpixeldEdx_DataDriven_syst;
   Float_t         trkdEdxMassRaw_syst;
   Float_t         trkdEdxMassDataDriven_syst;
   Int_t           nJet20_syst;
   Int_t           nJet30_syst;
   vector<float>   *jetPt_syst;
   vector<float>   *jetEta_syst;
   vector<float>   *jetPhi_syst;
   vector<float>   *jetM_syst;
   vector<bool>    *jetIsB_syst;
   vector<int>     *jetTruthLabel_syst;
   vector<int>     *jetNtrk_syst;
   vector<float>   *jetTiming_syst;
   vector<float>   *jetJVT_syst;
   Int_t           nBJet20_syst;
   Int_t           nBJet30_syst;
   Float_t         met_Et_syst;
   Float_t         met_Phi_syst;
   Float_t         metTruth_Et_syst;
   Float_t         metTruth_Phi_syst;
   Float_t         met_track_Et_syst;
   Float_t         met_track_Phi_syst;
   Float_t         met_Signif_syst;
   Float_t         metMuon_Et_syst;
   Float_t         metMuon_Phi_syst;
   Float_t         met_muons_invis_Et_syst;
   Float_t         met_muons_invis_Phi_syst;
   Float_t         met_muons_invis_Signif_syst;
   Float_t         minDPhi_met_muons_invis_allJets20_syst;
   Float_t         minDPhi_met_muons_invis_allJets30_syst;
   Float_t         met_electrons_invis_Et_syst;
   Float_t         met_electrons_invis_Phi_syst;
   Float_t         met_electrons_invis_Signif_syst;
   Float_t         minDPhi_met_electrons_invis_allJets20_syst;
   Float_t         minDPhi_met_electrons_invis_allJets30_syst;
   Float_t         met_photons_invis_Et_syst;
   Float_t         met_photons_invis_Phi_syst;
   Float_t         met_photons_invis_Signif_syst;
   Float_t         minDPhi_met_photons_invis_allJets20_syst;
   Float_t         minDPhi_met_photons_invis_allJets30_syst;
   Float_t         met_leptons_invis_Et_syst;
   Float_t         met_leptons_invis_Phi_syst;
   Float_t         met_leptons_invis_Signif_syst;
   Float_t         minDPhi_met_leptons_invis_allJets20_syst;
   Float_t         minDPhi_met_leptons_invis_allJets30_syst;
   Float_t         met_Et_LepInvis_syst;
   Float_t         met_Phi_LepInvis_syst;
   Float_t         met_Signif_LepInvis_syst;
   Float_t         minDPhi_met_allJets20_syst;
   Float_t         minDPhi_met_allJets30_syst;
   Float_t         minDPhi_met_4j30_syst;
   Float_t         minDPhi_trackMet_allJets20_syst;
   Float_t         minDPhi_trackMet_allJets30_syst;
   Float_t         HtJet30_syst;
   Float_t         meffInc30_syst;
   Float_t         mt_tau_syst;
   Float_t         mt_taujet_syst;
   Bool_t          hasZCand_syst;
   Float_t         mll_syst;
   TLorentzVector  *Photon_syst;
   Float_t         Photon_met_Et_syst;
   Float_t         Photon_Topoetcone40_syst;
   Int_t           Photon_conversionType_syst;
   Float_t         Photon_conversionRadius_syst;
   TString         *DecayProcess_syst;
   Double_t        pileupWeight_syst;
   Double_t        leptonWeight_syst;
   Double_t        eventWeight_syst;
   Double_t        genWeight_syst;
   Double_t        bTagWeight_syst;
   Double_t        trigWeight_syst;
   Double_t        jvtWeight_syst;
   Double_t        photonWeight_syst;
   Double_t        genWeightUp_syst;
   Double_t        genWeightDown_syst;
   Float_t         x1_syst;
   Float_t         x2_syst;
   Float_t         pdf1_syst;
   Float_t         pdf2_syst;
   Float_t         scalePDF_syst;
   Int_t           id1_syst;
   Int_t           id2_syst;
   Float_t         leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down_syst;
   Float_t         leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up_syst;
   Float_t         leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down_syst;
   Float_t         leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up_syst;
   Float_t         leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down_syst;
   Float_t         leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up_syst;
   Float_t         leptonWeight_MUON_EFF_ISO_STAT__1down_syst;
   Float_t         leptonWeight_MUON_EFF_ISO_STAT__1up_syst;
   Float_t         leptonWeight_MUON_EFF_ISO_SYS__1down_syst;
   Float_t         leptonWeight_MUON_EFF_ISO_SYS__1up_syst;
   Float_t         leptonWeight_MUON_EFF_RECO_STAT__1down_syst;
   Float_t         leptonWeight_MUON_EFF_RECO_STAT__1up_syst;
   Float_t         leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down_syst;
   Float_t         leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up_syst;
   Float_t         leptonWeight_MUON_EFF_RECO_SYS__1down_syst;
   Float_t         leptonWeight_MUON_EFF_RECO_SYS__1up_syst;
   Float_t         leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down_syst;
   Float_t         leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up_syst;
   Float_t         leptonWeight_MUON_EFF_TTVA_STAT__1down_syst;
   Float_t         leptonWeight_MUON_EFF_TTVA_STAT__1up_syst;
   Float_t         leptonWeight_MUON_EFF_TTVA_SYS__1down_syst;
   Float_t         leptonWeight_MUON_EFF_TTVA_SYS__1up_syst;
   Float_t         trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down_syst;
   Float_t         trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up_syst;
   Float_t         trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down_syst;
   Float_t         trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up_syst;
   Float_t         trigWeight_MUON_EFF_TrigStatUncertainty__1down_syst;
   Float_t         trigWeight_MUON_EFF_TrigStatUncertainty__1up_syst;
   Float_t         trigWeight_MUON_EFF_TrigSystUncertainty__1down_syst;
   Float_t         trigWeight_MUON_EFF_TrigSystUncertainty__1up_syst;
   Float_t         bTagWeight_FT_EFF_B_systematics__1down_syst;
   Float_t         bTagWeight_FT_EFF_B_systematics__1up_syst;
   Float_t         bTagWeight_FT_EFF_C_systematics__1down_syst;
   Float_t         bTagWeight_FT_EFF_C_systematics__1up_syst;
   Float_t         bTagWeight_FT_EFF_Light_systematics__1down_syst;
   Float_t         bTagWeight_FT_EFF_Light_systematics__1up_syst;
   Float_t         bTagWeight_FT_EFF_extrapolation__1down_syst;
   Float_t         bTagWeight_FT_EFF_extrapolation__1up_syst;
   Float_t         bTagWeight_FT_EFF_extrapolation_from_charm__1down_syst;
   Float_t         bTagWeight_FT_EFF_extrapolation_from_charm__1up_syst;
   Float_t         jvtWeight_JET_JvtEfficiency__1down_syst;
   Float_t         jvtWeight_JET_JvtEfficiency__1up_syst;
   Float_t         jvtWeight_JET_fJvtEfficiency__1down_syst;
   Float_t         jvtWeight_JET_fJvtEfficiency__1up_syst;
   Float_t         pileupWeightUp_syst;
   Float_t         pileupWeightDown_syst;
   ULong64_t       PRWHash_syst;
   ULong64_t       EventNumber_syst;
   Float_t         xsec_syst;
   Float_t         GenHt_syst;
   Float_t         GenMET_syst;
   Int_t           DatasetNumber_syst;
   Int_t           RunNumber_syst;
   Int_t           RandomRunNumber_syst;
   Int_t           FS_syst;
   vector<float>   *LHE3Weights_syst;
   vector<TString> *LHE3WeightNames_syst;

   // List of branches of nominal tree
   TBranch        *b_trigWeight_singleElectronTrig;   //!
   TBranch        *b_trigMatch_singleElectronTrig;   //!
   TBranch        *b_trigWeight_singlePhotonTrig;   //!
   TBranch        *b_trigMatch_singlePhotonTrig;   //!
   TBranch        *b_trigWeight_singleMuonTrig;   //!
   TBranch        *b_trigMatch_singleMuonTrig;   //!
   TBranch        *b_trigWeight_metTrig;   //!
   TBranch        *b_trigMatch_metTrig;   //!
   TBranch        *b_IsMETTrigPassed;   //!
   TBranch        *b_year;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_actual_mu;   //!
   TBranch        *b_nVtx;   //!
   TBranch        *b_privtx;   //!
   TBranch        *b_beampos;   //!
   TBranch        *b_passBadTileJetVeto;   //!
   TBranch        *b_passLooseBadJetVeto;   //!
   TBranch        *b_passTightBadJetVeto;   //!
   TBranch        *b_TruthJetPt;   //!
   TBranch        *b_TruthJetEta;   //!
   TBranch        *b_TruthJetPhi;   //!
   TBranch        *b_TruthJetM;   //!
   TBranch        *b_TruthJetLabel;   //!
   TBranch        *b_polWeight;   //!
   TBranch        *b_passEmulTightJetCleaning1Jet;   //!
   TBranch        *b_passEmulTightJetCleaning2Jet;   //!
   TBranch        *b_nLep_base;   //!
   TBranch        *b_nMu_base;   //!
   TBranch        *b_nEle_base;   //!
   TBranch        *b_nLep_signal;   //!
   TBranch        *b_nMu_signal;   //!
   TBranch        *b_nEle_signal;   //!
   TBranch        *b_lep1Flavor;   //!
   TBranch        *b_lep1Charge;   //!
   TBranch        *b_lep1Author;   //!
   TBranch        *b_lep1Pt;   //!
   TBranch        *b_lep1Eta;   //!
   TBranch        *b_lep1Phi;   //!
   TBranch        *b_lep1M;   //!
   TBranch        *b_lep1MT_Met;   //!
   TBranch        *b_lep1MT_Met_LepInvis;   //!
   TBranch        *b_lep1_DPhiMet;   //!
   TBranch        *b_lep1_DPhiMet_LepInvis;   //!
   TBranch        *b_lep1D0;   //!
   TBranch        *b_lep1D0Sig;   //!
   TBranch        *b_lep1Z0;   //!
   TBranch        *b_lep1Z0SinTheta;   //!
   TBranch        *b_lep1Signal;   //!
   TBranch        *b_lep1Weight;   //!
   TBranch        *b_lep2Flavor;   //!
   TBranch        *b_lep2Charge;   //!
   TBranch        *b_lep2Author;   //!
   TBranch        *b_lep2Pt;   //!
   TBranch        *b_lep2Eta;   //!
   TBranch        *b_lep2Phi;   //!
   TBranch        *b_lep2M;   //!
   TBranch        *b_lep2MT_Met;   //!
   TBranch        *b_lep2MT_Met_LepInvis;   //!
   TBranch        *b_lep2_DPhiMet;   //!
   TBranch        *b_lep2_DPhiMet_LepInvis;   //!
   TBranch        *b_lep2D0;   //!
   TBranch        *b_lep2D0Sig;   //!
   TBranch        *b_lep2Z0;   //!
   TBranch        *b_lep2Z0SinTheta;   //!
   TBranch        *b_lep2Signal;   //!
   TBranch        *b_lep2Weight;   //!
   TBranch        *b_nPhoton_base;   //!
   TBranch        *b_nPhoton_signal;   //!
   TBranch        *b_nTrkVec_base;   //!
   TBranch        *b_nTrkVec_signal;   //!
   TBranch        *b_trkVecisTight;   //!
   TBranch        *b_trkVecisLoose;   //!
   TBranch        *b_trkVecPt;   //!
   TBranch        *b_trkVecEta;   //!
   TBranch        *b_trkVecPhi;   //!
   TBranch        *b_trkVecQ;   //!
   TBranch        *b_trkVecZ0;   //!
   TBranch        *b_trkVecZ0Origin;   //!
   TBranch        *b_trkVecZ0Err;   //!
   TBranch        *b_trkVecZ0SinTheta;   //!
   TBranch        *b_trkVecD0;   //!
   TBranch        *b_trkVecD0Sig;   //!
   TBranch        *b_trkVecpixeldEdx;   //!
   TBranch        *b_trkVecpixeldEdx_Corrected;   //!
   TBranch        *b_trkVecpixeldEdx_DataDriven;   //!
   TBranch        *b_trkVecpixeldEdx_Final;   //!
   TBranch        *b_trkVecqOverP;   //!
   TBranch        *b_trkVecnIBLOFdEdx;   //!
   TBranch        *b_trkVecnUsedHitsdEdx;   //!
   TBranch        *b_trkVecdEdxMassRaw;   //!
   TBranch        *b_trkVecdEdxMassCorrected;   //!
   TBranch        *b_trkVecdEdxMassDataDriven;   //!
   TBranch        *b_trkVecdEdxMassFinal;   //!
   TBranch        *b_trkVecdEdxMass;   //!
   TBranch        *b_trkVecNNScore;   //!
   TBranch        *b_trkVecnIBLHits;   //!
   TBranch        *b_trkVecisBaseMuon;   //!
   TBranch        *b_trkVecisBaseElectron;   //!
   TBranch        *b_trkVecisBasePhoton;   //!
   TBranch        *b_trkVecisSignalMuon;   //!
   TBranch        *b_trkVecisSignalElectron;   //!
   TBranch        *b_trkVecisSignalPhoton;   //!
   TBranch        *b_trkVecLabel;   //!
   TBranch        *b_trkVecDef;   //!
   TBranch        *b_trkVecpdgId;   //!
   TBranch        *b_trkVecnpar;   //!
   TBranch        *b_trkVecparbarcode;   //!
   TBranch        *b_trkVecparpdgId;   //!
   TBranch        *b_trkVecparnchild;   //!
   TBranch        *b_trkVecparchildpdgId;   //!
   TBranch        *b_trkVecparchildpt;   //!
   TBranch        *b_trkVecparchildbarcode;   //!
   TBranch        *b_trkVecBarcode;   //!
   TBranch        *b_trkVecStatus;   //!
   TBranch        *b_trkVecType;   //!
   TBranch        *b_trkVecOrigin;   //!
   TBranch        *b_trkVecIsTruthBSM;   //!
   TBranch        *b_trkVecPtcone40;   //!
   TBranch        *b_trkVecNonBaseAssocPtcone40;   //!
   TBranch        *b_trkVecNonBaseAssocPtcone30;   //!
   TBranch        *b_trkVecNonBasePhotonConvPtcone40;   //!
   TBranch        *b_trkVecNonBasePhotonConvPtcone30;   //!
   TBranch        *b_trkVecNonSignalAssocPtcone40;   //!
   TBranch        *b_trkVecNonSignalPhotonConvPtcone40;   //!
   TBranch        *b_trkVecBaseLepPhotonMinDR;   //!
   TBranch        *b_trkVecSignalLepPhotonMinDR;   //!
   TBranch        *b_trkVecTopoEtcone40;   //!
   TBranch        *b_trkVecTopoEtclus40;   //!
   TBranch        *b_trkVecBaseLepPhotonSubtractedTopoEtclus40;   //!
   TBranch        *b_trkVecBaseLepPhotonSubtractedCoreConeTopoEtclus40;   //!
   TBranch        *b_trkVecSignalLepPhotonSubtractedTopoEtclus40;   //!
   TBranch        *b_trkVecSignalLepPhotonSubtractedCoreConeTopoEtclus40;   //!
   TBranch        *b_trkVecDPhiMet;   //!
   TBranch        *b_trkVecIso;   //!
   TBranch        *b_trkVecM;   //!
   TBranch        *b_trkVecTightPrimary;   //!
   TBranch        *b_trkVecIsoTightPrimary;   //!
   TBranch        *b_trkVecMatchedToV0;   //!
   TBranch        *b_trkVecMatchedToVsi;   //!
   TBranch        *b_trkVecMatchedVsiMass;   //!
   TBranch        *b_nTrk_base;   //!
   TBranch        *b_nTrk_signal;   //!
   TBranch        *b_trkPt;   //!
   TBranch        *b_trkEta;   //!
   TBranch        *b_trkPhi;   //!
   TBranch        *b_trkQ;   //!
   TBranch        *b_trkZ0;   //!
   TBranch        *b_trkZ0Err;   //!
   TBranch        *b_trkZ0SinTheta;   //!
   TBranch        *b_trkD0;   //!
   TBranch        *b_trkD0Err;   //!
   TBranch        *b_trkD0Sig;   //!
   TBranch        *b_trkisSecTrk;   //!
   TBranch        *b_trkisBaseMuon;   //!
   TBranch        *b_trkisBaseElectron;   //!
   TBranch        *b_trkisBasePhoton;   //!
   TBranch        *b_trkisSignalMuon;   //!
   TBranch        *b_trkisSignalElectron;   //!
   TBranch        *b_trkisSignalPhoton;   //!
   TBranch        *b_trkLabel;   //!
   TBranch        *b_trkpdgId;   //!
   TBranch        *b_trkparpdgId;   //!
   TBranch        *b_trkType;   //!
   TBranch        *b_trkOrigin;   //!
   TBranch        *b_trkIsTruthBSM;   //!
   TBranch        *b_trkNonSignalPhotonConvPtcone40;   //!
   TBranch        *b_trkSignalLepPhotonMinDR;   //!
   TBranch        *b_trkSignalLepPhotonSubtractedCoreConeTopoEtclus40;   //!
   TBranch        *b_trkIso;   //!
   TBranch        *b_trkTightPrimary;   //!
   TBranch        *b_trkMatchedToV0;   //!
   TBranch        *b_trkMatchedToVsi;   //!
   TBranch        *b_trkMatchedVsiMass;   //!
   TBranch        *b_trkNNScore;   //!
   TBranch        *b_trkpixeldEdx_DataDriven;   //!
   TBranch        *b_trkdEdxMassRaw;   //!
   TBranch        *b_trkdEdxMassDataDriven;   //!
   TBranch        *b_nJet20;   //!
   TBranch        *b_nJet30;   //!
   TBranch        *b_jetPt;   //!
   TBranch        *b_jetEta;   //!
   TBranch        *b_jetPhi;   //!
   TBranch        *b_jetM;   //!
   TBranch        *b_jetIsB;   //!
   TBranch        *b_jetTruthLabel;   //!
   TBranch        *b_jetNtrk;   //!
   TBranch        *b_jetTiming;   //!
   TBranch        *b_jetJVT;   //!
   TBranch        *b_nBJet20;   //!
   TBranch        *b_nBJet30;   //!
   TBranch        *b_met_Et;   //!
   TBranch        *b_met_Phi;   //!
   TBranch        *b_metTruth_Et;   //!
   TBranch        *b_metTruth_Phi;   //!
   TBranch        *b_met_track_Et;   //!
   TBranch        *b_met_track_Phi;   //!
   TBranch        *b_met_Signif;   //!
   TBranch        *b_metMuon_Et;   //!
   TBranch        *b_metMuon_Phi;   //!
   TBranch        *b_met_muons_invis_Et;   //!
   TBranch        *b_met_muons_invis_Phi;   //!
   TBranch        *b_met_muons_invis_Signif;   //!
   TBranch        *b_minDPhi_met_muons_invis_allJets20;   //!
   TBranch        *b_minDPhi_met_muons_invis_allJets30;   //!
   TBranch        *b_met_electrons_invis_Et;   //!
   TBranch        *b_met_electrons_invis_Phi;   //!
   TBranch        *b_met_electrons_invis_Signif;   //!
   TBranch        *b_minDPhi_met_electrons_invis_allJets20;   //!
   TBranch        *b_minDPhi_met_electrons_invis_allJets30;   //!
   TBranch        *b_met_photons_invis_Et;   //!
   TBranch        *b_met_photons_invis_Phi;   //!
   TBranch        *b_met_photons_invis_Signif;   //!
   TBranch        *b_minDPhi_met_photons_invis_allJets20;   //!
   TBranch        *b_minDPhi_met_photons_invis_allJets30;   //!
   TBranch        *b_met_leptons_invis_Et;   //!
   TBranch        *b_met_leptons_invis_Phi;   //!
   TBranch        *b_met_leptons_invis_Signif;   //!
   TBranch        *b_minDPhi_met_leptons_invis_allJets20;   //!
   TBranch        *b_minDPhi_met_leptons_invis_allJets30;   //!
   TBranch        *b_met_Et_LepInvis;   //!
   TBranch        *b_met_Phi_LepInvis;   //!
   TBranch        *b_met_Signif_LepInvis;   //!
   TBranch        *b_minDPhi_met_allJets20;   //!
   TBranch        *b_minDPhi_met_allJets30;   //!
   TBranch        *b_minDPhi_met_4j30;   //!
   TBranch        *b_minDPhi_trackMet_allJets20;   //!
   TBranch        *b_minDPhi_trackMet_allJets30;   //!
   TBranch        *b_HtJet30;   //!
   TBranch        *b_meffInc30;   //!
   TBranch        *b_mt_tau;   //!
   TBranch        *b_mt_taujet;   //!
   TBranch        *b_hasZCand;   //!
   TBranch        *b_mll;   //!
   TBranch        *b_Photon;   //!
   TBranch        *b_Photon_met_Et;   //!
   TBranch        *b_Photon_Topoetcone40;   //!
   TBranch        *b_Photon_conversionType;   //!
   TBranch        *b_Photon_conversionRadius;   //!
   TBranch        *b_DecayProcess;   //!
   TBranch        *b_pileupWeight;   //!
   TBranch        *b_leptonWeight;   //!
   TBranch        *b_eventWeight;   //!
   TBranch        *b_genWeight;   //!
   TBranch        *b_bTagWeight;   //!
   TBranch        *b_trigWeight;   //!
   TBranch        *b_jvtWeight;   //!
   TBranch        *b_photonWeight;   //!
   TBranch        *b_genWeightUp;   //!
   TBranch        *b_genWeightDown;   //!
   TBranch        *b_x1;   //!
   TBranch        *b_x2;   //!
   TBranch        *b_pdf1;   //!
   TBranch        *b_pdf2;   //!
   TBranch        *b_scalePDF;   //!
   TBranch        *b_id1;   //!
   TBranch        *b_id2;   //!
   TBranch        *b_leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_leptonWeight_MUON_EFF_ISO_STAT__1down;   //!
   TBranch        *b_leptonWeight_MUON_EFF_ISO_STAT__1up;   //!
   TBranch        *b_leptonWeight_MUON_EFF_ISO_SYS__1down;   //!
   TBranch        *b_leptonWeight_MUON_EFF_ISO_SYS__1up;   //!
   TBranch        *b_leptonWeight_MUON_EFF_RECO_STAT__1down;   //!
   TBranch        *b_leptonWeight_MUON_EFF_RECO_STAT__1up;   //!
   TBranch        *b_leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down;   //!
   TBranch        *b_leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up;   //!
   TBranch        *b_leptonWeight_MUON_EFF_RECO_SYS__1down;   //!
   TBranch        *b_leptonWeight_MUON_EFF_RECO_SYS__1up;   //!
   TBranch        *b_leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down;   //!
   TBranch        *b_leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up;   //!
   TBranch        *b_leptonWeight_MUON_EFF_TTVA_STAT__1down;   //!
   TBranch        *b_leptonWeight_MUON_EFF_TTVA_STAT__1up;   //!
   TBranch        *b_leptonWeight_MUON_EFF_TTVA_SYS__1down;   //!
   TBranch        *b_leptonWeight_MUON_EFF_TTVA_SYS__1up;   //!
   TBranch        *b_trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_trigWeight_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_trigWeight_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_trigWeight_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_trigWeight_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_bTagWeight_FT_EFF_B_systematics__1down;   //!
   TBranch        *b_bTagWeight_FT_EFF_B_systematics__1up;   //!
   TBranch        *b_bTagWeight_FT_EFF_C_systematics__1down;   //!
   TBranch        *b_bTagWeight_FT_EFF_C_systematics__1up;   //!
   TBranch        *b_bTagWeight_FT_EFF_Light_systematics__1down;   //!
   TBranch        *b_bTagWeight_FT_EFF_Light_systematics__1up;   //!
   TBranch        *b_bTagWeight_FT_EFF_extrapolation__1down;   //!
   TBranch        *b_bTagWeight_FT_EFF_extrapolation__1up;   //!
   TBranch        *b_bTagWeight_FT_EFF_extrapolation_from_charm__1down;   //!
   TBranch        *b_bTagWeight_FT_EFF_extrapolation_from_charm__1up;   //!
   TBranch        *b_jvtWeight_JET_JvtEfficiency__1down;   //!
   TBranch        *b_jvtWeight_JET_JvtEfficiency__1up;   //!
   TBranch        *b_jvtWeight_JET_fJvtEfficiency__1down;   //!
   TBranch        *b_jvtWeight_JET_fJvtEfficiency__1up;   //!
   TBranch        *b_pileupWeightUp;   //!
   TBranch        *b_pileupWeightDown;   //!
   TBranch        *b_PRWHash;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_xsec;   //!
   TBranch        *b_GenHt;   //!
   TBranch        *b_GenMET;   //!
   TBranch        *b_DatasetNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_RandomRunNumber;   //!
   TBranch        *b_FS;   //!
   TBranch        *b_LHE3Weights;   //!
   TBranch        *b_LHE3WeightNames;   //!

   // List of branches of syst tree
   TBranch        *b_trigWeight_singleElectronTrig_syst;   //!
   TBranch        *b_trigMatch_singleElectronTrig_syst;   //!
   TBranch        *b_trigWeight_singlePhotonTrig_syst;   //!
   TBranch        *b_trigMatch_singlePhotonTrig_syst;   //!
   TBranch        *b_trigWeight_singleMuonTrig_syst;   //!
   TBranch        *b_trigMatch_singleMuonTrig_syst;   //!
   TBranch        *b_trigWeight_metTrig_syst;   //!
   TBranch        *b_trigMatch_metTrig_syst;   //!
   TBranch        *b_IsMETTrigPassed_syst;   //!
   TBranch        *b_year_syst;   //!
   TBranch        *b_mu_syst;   //!
   TBranch        *b_actual_mu_syst;   //!
   TBranch        *b_nVtx_syst;   //!
   TBranch        *b_privtx_syst;   //!
   TBranch        *b_beampos_syst;   //!
   TBranch        *b_passBadTileJetVeto_syst;   //!
   TBranch        *b_passLooseBadJetVeto_syst;   //!
   TBranch        *b_passTightBadJetVeto_syst;   //!
   TBranch        *b_TruthJetPt_syst;   //!
   TBranch        *b_TruthJetEta_syst;   //!
   TBranch        *b_TruthJetPhi_syst;   //!
   TBranch        *b_TruthJetM_syst;   //!
   TBranch        *b_TruthJetLabel_syst;   //!
   TBranch        *b_polWeight_syst;   //!
   TBranch        *b_passEmulTightJetCleaning1Jet_syst;   //!
   TBranch        *b_passEmulTightJetCleaning2Jet_syst;   //!
   TBranch        *b_nLep_base_syst;   //!
   TBranch        *b_nMu_base_syst;   //!
   TBranch        *b_nEle_base_syst;   //!
   TBranch        *b_nLep_signal_syst;   //!
   TBranch        *b_nMu_signal_syst;   //!
   TBranch        *b_nEle_signal_syst;   //!
   TBranch        *b_lep1Flavor_syst;   //!
   TBranch        *b_lep1Charge_syst;   //!
   TBranch        *b_lep1Author_syst;   //!
   TBranch        *b_lep1Pt_syst;   //!
   TBranch        *b_lep1Eta_syst;   //!
   TBranch        *b_lep1Phi_syst;   //!
   TBranch        *b_lep1M_syst;   //!
   TBranch        *b_lep1MT_Met_syst;   //!
   TBranch        *b_lep1MT_Met_LepInvis_syst;   //!
   TBranch        *b_lep1_DPhiMet_syst;   //!
   TBranch        *b_lep1_DPhiMet_LepInvis_syst;   //!
   TBranch        *b_lep1D0_syst;   //!
   TBranch        *b_lep1D0Sig_syst;   //!
   TBranch        *b_lep1Z0_syst;   //!
   TBranch        *b_lep1Z0SinTheta_syst;   //!
   TBranch        *b_lep1Signal_syst;   //!
   TBranch        *b_lep1Weight_syst;   //!
   TBranch        *b_lep2Flavor_syst;   //!
   TBranch        *b_lep2Charge_syst;   //!
   TBranch        *b_lep2Author_syst;   //!
   TBranch        *b_lep2Pt_syst;   //!
   TBranch        *b_lep2Eta_syst;   //!
   TBranch        *b_lep2Phi_syst;   //!
   TBranch        *b_lep2M_syst;   //!
   TBranch        *b_lep2MT_Met_syst;   //!
   TBranch        *b_lep2MT_Met_LepInvis_syst;   //!
   TBranch        *b_lep2_DPhiMet_syst;   //!
   TBranch        *b_lep2_DPhiMet_LepInvis_syst;   //!
   TBranch        *b_lep2D0_syst;   //!
   TBranch        *b_lep2D0Sig_syst;   //!
   TBranch        *b_lep2Z0_syst;   //!
   TBranch        *b_lep2Z0SinTheta_syst;   //!
   TBranch        *b_lep2Signal_syst;   //!
   TBranch        *b_lep2Weight_syst;   //!
   TBranch        *b_nPhoton_base_syst;   //!
   TBranch        *b_nPhoton_signal_syst;   //!
   TBranch        *b_nTrk_base_syst;   //!
   TBranch        *b_nTrk_signal_syst;   //!
   TBranch        *b_trkPt_syst;   //!
   TBranch        *b_trkEta_syst;   //!
   TBranch        *b_trkPhi_syst;   //!
   TBranch        *b_trkQ_syst;   //!
   TBranch        *b_trkZ0_syst;   //!
   TBranch        *b_trkZ0Err_syst;   //!
   TBranch        *b_trkZ0SinTheta_syst;   //!
   TBranch        *b_trkD0_syst;   //!
   TBranch        *b_trkD0Err_syst;   //!
   TBranch        *b_trkD0Sig_syst;   //!
   TBranch        *b_trkisSecTrk_syst;   //!
   TBranch        *b_trkisBaseMuon_syst;   //!
   TBranch        *b_trkisBaseElectron_syst;   //!
   TBranch        *b_trkisBasePhoton_syst;   //!
   TBranch        *b_trkisSignalMuon_syst;   //!
   TBranch        *b_trkisSignalElectron_syst;   //!
   TBranch        *b_trkisSignalPhoton_syst;   //!
   TBranch        *b_trkLabel_syst;   //!
   TBranch        *b_trkpdgId_syst;   //!
   TBranch        *b_trkparpdgId_syst;   //!
   TBranch        *b_trkType_syst;   //!
   TBranch        *b_trkOrigin_syst;   //!
   TBranch        *b_trkIsTruthBSM_syst;   //!
   TBranch        *b_trkNonSignalPhotonConvPtcone40_syst;   //!
   TBranch        *b_trkSignalLepPhotonMinDR_syst;   //!
   TBranch        *b_trkSignalLepPhotonSubtractedCoreConeTopoEtclus40_syst;   //!
   TBranch        *b_trkIso_syst;   //!
   TBranch        *b_trkTightPrimary_syst;   //!
   TBranch        *b_trkMatchedToV0_syst;   //!
   TBranch        *b_trkMatchedToVsi_syst;   //!
   TBranch        *b_trkMatchedVsiMass_syst;   //!
   TBranch        *b_trkNNScore_syst;   //!
   TBranch        *b_trkpixeldEdx_DataDriven_syst;   //!
   TBranch        *b_trkdEdxMassRaw_syst;   //!
   TBranch        *b_trkdEdxMassDataDriven_syst;   //!
   TBranch        *b_nJet20_syst;   //!
   TBranch        *b_nJet30_syst;   //!
   TBranch        *b_jetPt_syst;   //!
   TBranch        *b_jetEta_syst;   //!
   TBranch        *b_jetPhi_syst;   //!
   TBranch        *b_jetM_syst;   //!
   TBranch        *b_jetIsB_syst;   //!
   TBranch        *b_jetTruthLabel_syst;   //!
   TBranch        *b_jetNtrk_syst;   //!
   TBranch        *b_jetTiming_syst;   //!
   TBranch        *b_jetJVT_syst;   //!
   TBranch        *b_nBJet20_syst;   //!
   TBranch        *b_nBJet30_syst;   //!
   TBranch        *b_met_Et_syst;   //!
   TBranch        *b_met_Phi_syst;   //!
   TBranch        *b_metTruth_Et_syst;   //!
   TBranch        *b_metTruth_Phi_syst;   //!
   TBranch        *b_met_track_Et_syst;   //!
   TBranch        *b_met_track_Phi_syst;   //!
   TBranch        *b_met_Signif_syst;   //!
   TBranch        *b_metMuon_Et_syst;   //!
   TBranch        *b_metMuon_Phi_syst;   //!
   TBranch        *b_met_muons_invis_Et_syst;   //!
   TBranch        *b_met_muons_invis_Phi_syst;   //!
   TBranch        *b_met_muons_invis_Signif_syst;   //!
   TBranch        *b_minDPhi_met_muons_invis_allJets20_syst;   //!
   TBranch        *b_minDPhi_met_muons_invis_allJets30_syst;   //!
   TBranch        *b_met_electrons_invis_Et_syst;   //!
   TBranch        *b_met_electrons_invis_Phi_syst;   //!
   TBranch        *b_met_electrons_invis_Signif_syst;   //!
   TBranch        *b_minDPhi_met_electrons_invis_allJets20_syst;   //!
   TBranch        *b_minDPhi_met_electrons_invis_allJets30_syst;   //!
   TBranch        *b_met_photons_invis_Et_syst;   //!
   TBranch        *b_met_photons_invis_Phi_syst;   //!
   TBranch        *b_met_photons_invis_Signif_syst;   //!
   TBranch        *b_minDPhi_met_photons_invis_allJets20_syst;   //!
   TBranch        *b_minDPhi_met_photons_invis_allJets30_syst;   //!
   TBranch        *b_met_leptons_invis_Et_syst;   //!
   TBranch        *b_met_leptons_invis_Phi_syst;   //!
   TBranch        *b_met_leptons_invis_Signif_syst;   //!
   TBranch        *b_minDPhi_met_leptons_invis_allJets20_syst;   //!
   TBranch        *b_minDPhi_met_leptons_invis_allJets30_syst;   //!
   TBranch        *b_met_Et_LepInvis_syst;   //!
   TBranch        *b_met_Phi_LepInvis_syst;   //!
   TBranch        *b_met_Signif_LepInvis_syst;   //!
   TBranch        *b_minDPhi_met_allJets20_syst;   //!
   TBranch        *b_minDPhi_met_allJets30_syst;   //!
   TBranch        *b_minDPhi_met_4j30_syst;   //!
   TBranch        *b_minDPhi_trackMet_allJets20_syst;   //!
   TBranch        *b_minDPhi_trackMet_allJets30_syst;   //!
   TBranch        *b_HtJet30_syst;   //!
   TBranch        *b_meffInc30_syst;   //!
   TBranch        *b_mt_tau_syst;   //!
   TBranch        *b_mt_taujet_syst;   //!
   TBranch        *b_hasZCand_syst;   //!
   TBranch        *b_mll_syst;   //!
   TBranch        *b_Photon_syst;   //!
   TBranch        *b_Photon_met_Et_syst;   //!
   TBranch        *b_Photon_Topoetcone40_syst;   //!
   TBranch        *b_Photon_conversionType_syst;   //!
   TBranch        *b_Photon_conversionRadius_syst;   //!
   TBranch        *b_DecayProcess_syst;   //!
   TBranch        *b_pileupWeight_syst;   //!
   TBranch        *b_leptonWeight_syst;   //!
   TBranch        *b_eventWeight_syst;   //!
   TBranch        *b_genWeight_syst;   //!
   TBranch        *b_bTagWeight_syst;   //!
   TBranch        *b_trigWeight_syst;   //!
   TBranch        *b_jvtWeight_syst;   //!
   TBranch        *b_photonWeight_syst;   //!
   TBranch        *b_genWeightUp_syst;   //!
   TBranch        *b_genWeightDown_syst;   //!
   TBranch        *b_x1_syst;   //!
   TBranch        *b_x2_syst;   //!
   TBranch        *b_pdf1_syst;   //!
   TBranch        *b_pdf2_syst;   //!
   TBranch        *b_scalePDF_syst;   //!
   TBranch        *b_id1_syst;   //!
   TBranch        *b_id2_syst;   //!
   TBranch        *b_pileupWeightUp_syst;   //!
   TBranch        *b_pileupWeightDown_syst;   //!
   TBranch        *b_PRWHash_syst;   //!
   TBranch        *b_EventNumber_syst;   //!
   TBranch        *b_xsec_syst;   //!
   TBranch        *b_GenHt_syst;   //!
   TBranch        *b_GenMET_syst;   //!
   TBranch        *b_DatasetNumber_syst;   //!
   TBranch        *b_RunNumber_syst;   //!
   TBranch        *b_RandomRunNumber_syst;   //!
   TBranch        *b_FS_syst;   //!
   TBranch        *b_LHE3Weights_syst;   //!
   TBranch        *b_LHE3WeightNames_syst;   //!

   // Class methods and data members
   analysis(TTree *tree=0);
   virtual ~analysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(TTree* tree, Long64_t entry);
   virtual Long64_t LoadTree(TTree* tree, Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(TTree* tree, Int_t nevents);
   virtual void     LoopOnSyst(TTree* tree, Int_t nevents);
   virtual Bool_t   Notify();
   virtual void     Show(TTree* tree, Long64_t entry = -1);
   void             SaveLHE3Weights(std::vector<float> *LHE3Weights, std::vector<TString> *LHE3WeightNames, std::string tree_name);
   void             Write(const char* filename, std::string treename);
   void             Fill(TH1D* h, Double_t entry, Double_t weight);   

   // Vector of strings used to handle name exceptions
   // when processing diboson and signal samples
   std::vector<std::string> tree_name_strs;

   // Output tree variables
   std::shared_ptr<TTree> output_tree;
   Int_t    Lep1Q;
   Int_t    nLepBase;
   Int_t    nPhotonBase;
   Int_t    nPhotonSignal;
   Int_t    nMuSig;
   Int_t    nMuBase;
   Int_t    nEleSig;
   Int_t    nEleBase;
   Int_t    nBJet;
   Float_t  MET;
   Float_t  TrackPt;
   Int_t    TrackQ;
   Float_t  TrackD0Sig;
   Int_t    TrackLabel;
   Float_t  EventWeight;
   Float_t  PileupWeight;
   Float_t  PileupWeightUp;
   Float_t  PileupWeightDown;
   Float_t  PhotonWeight;
   Float_t  LeptonWeight;
   Float_t  GenWeight;
   Float_t  BTagWeight;
   Float_t  JVTWeight;
   Float_t  TrigWeight;
   Float_t  TrigWeight_Mu;
   Float_t  TrigWeight_Ele;
   Float_t  TrigWeight_Gamma;
   Int_t    TrigMatch_met;
   Int_t    TrigMatch_Mu;
   Int_t    TrigMatch_Ele; 

   Float_t  LeptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t  LeptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t  LeptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t  LeptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t  LeptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t  LeptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t  LeptonWeight_MUON_EFF_ISO_STAT__1down;
   Float_t  LeptonWeight_MUON_EFF_ISO_STAT__1up;
   Float_t  LeptonWeight_MUON_EFF_ISO_SYS__1down;
   Float_t  LeptonWeight_MUON_EFF_ISO_SYS__1up;
   Float_t  LeptonWeight_MUON_EFF_RECO_STAT__1down;
   Float_t  LeptonWeight_MUON_EFF_RECO_STAT__1up;
   Float_t  LeptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down; 
   Float_t  LeptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up;
   Float_t  LeptonWeight_MUON_EFF_RECO_SYS__1down;
   Float_t  LeptonWeight_MUON_EFF_RECO_SYS__1up;
   Float_t  LeptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down;
   Float_t  LeptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up;
   Float_t  LeptonWeight_MUON_EFF_TTVA_STAT__1down; 
   Float_t  LeptonWeight_MUON_EFF_TTVA_STAT__1up; 
   Float_t  LeptonWeight_MUON_EFF_TTVA_SYS__1down; 
   Float_t  LeptonWeight_MUON_EFF_TTVA_SYS__1up;
   Float_t  LeptonTrigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t  LeptonTrigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Float_t  LeptonTrigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Float_t  LeptonTrigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up; 
   Float_t  LeptonTrigWeight_MUON_EFF_TrigStatUncertainty__1down;
   Float_t  LeptonTrigWeight_MUON_EFF_TrigStatUncertainty__1up;
   Float_t  LeptonTrigWeight_MUON_EFF_TrigSystUncertainty__1down;
   Float_t  LeptonTrigWeight_MUON_EFF_TrigSystUncertainty__1up;
   Float_t  JVTWeight_JET_JvtEfficiency__1down;
   Float_t  JVTWeight_JET_JvtEfficiency__1up;
   Float_t  JVTWeight_JET_fJvtEfficiency__1down;
   Float_t  JVTWeight_JET_fJvtEfficiency__1up;
   Float_t  BTagWeight_FT_EFF_B_systematics__1down;
   Float_t  BTagWeight_FT_EFF_B_systematics__1up;
   Float_t  BTagWeight_FT_EFF_C_systematics__1down;
   Float_t  BTagWeight_FT_EFF_C_systematics__1up;
   Float_t  BTagWeight_FT_EFF_Light_systematics__1down;
   Float_t  BTagWeight_FT_EFF_Light_systematics__1up;
   Float_t  BTagWeight_FT_EFF_extrapolation__1down;
   Float_t  BTagWeight_FT_EFF_extrapolation__1up;
   Float_t  BTagWeight_FT_EFF_extrapolation_from_charm__1down;
   Float_t  BTagWeight_FT_EFF_extrapolation_from_charm__1up;

   // GJets weights
   Float_t  MUR1_MUF1_PDF13000;
   Float_t  MUR1_MUF1_PDF25300;
   Float_t  MUR0p5_MUF0p5_PDF261000;
   Float_t  MUR0p5_MUF1_PDF261000;
   Float_t  MUR1_MUF0p5_PDF261000;
   Float_t  MUR1_MUF1_PDF261000;
   Float_t  MUR1_MUF1_PDF261001;
   Float_t  MUR1_MUF1_PDF261002;
   Float_t  MUR1_MUF1_PDF261003;
   Float_t  MUR1_MUF1_PDF261004;
   Float_t  MUR1_MUF1_PDF261005;
   Float_t  MUR1_MUF1_PDF261006;
   Float_t  MUR1_MUF1_PDF261007;
   Float_t  MUR1_MUF1_PDF261008;
   Float_t  MUR1_MUF1_PDF261009;
   Float_t  MUR1_MUF1_PDF261010;
   Float_t  MUR1_MUF1_PDF261011;
   Float_t  MUR1_MUF1_PDF261012;
   Float_t  MUR1_MUF1_PDF261013;
   Float_t  MUR1_MUF1_PDF261014;
   Float_t  MUR1_MUF1_PDF261015;
   Float_t  MUR1_MUF1_PDF261016;
   Float_t  MUR1_MUF1_PDF261017;
   Float_t  MUR1_MUF1_PDF261018;
   Float_t  MUR1_MUF1_PDF261019;
   Float_t  MUR1_MUF1_PDF261020;
   Float_t  MUR1_MUF1_PDF261021;
   Float_t  MUR1_MUF1_PDF261022;
   Float_t  MUR1_MUF1_PDF261023;
   Float_t  MUR1_MUF1_PDF261024;
   Float_t  MUR1_MUF1_PDF261025;
   Float_t  MUR1_MUF1_PDF261026;
   Float_t  MUR1_MUF1_PDF261027;
   Float_t  MUR1_MUF1_PDF261028;
   Float_t  MUR1_MUF1_PDF261029;
   Float_t  MUR1_MUF1_PDF261030;
   Float_t  MUR1_MUF1_PDF261031;
   Float_t  MUR1_MUF1_PDF261032;
   Float_t  MUR1_MUF1_PDF261033;
   Float_t  MUR1_MUF1_PDF261034;
   Float_t  MUR1_MUF1_PDF261035;
   Float_t  MUR1_MUF1_PDF261036;
   Float_t  MUR1_MUF1_PDF261037;
   Float_t  MUR1_MUF1_PDF261038;
   Float_t  MUR1_MUF1_PDF261039;
   Float_t  MUR1_MUF1_PDF261040;
   Float_t  MUR1_MUF1_PDF261041;
   Float_t  MUR1_MUF1_PDF261042;
   Float_t  MUR1_MUF1_PDF261043;
   Float_t  MUR1_MUF1_PDF261044;
   Float_t  MUR1_MUF1_PDF261045;
   Float_t  MUR1_MUF1_PDF261046;
   Float_t  MUR1_MUF1_PDF261047;
   Float_t  MUR1_MUF1_PDF261048;
   Float_t  MUR1_MUF1_PDF261049;
   Float_t  MUR1_MUF1_PDF261050;
   Float_t  MUR1_MUF1_PDF261051;
   Float_t  MUR1_MUF1_PDF261052;
   Float_t  MUR1_MUF1_PDF261053;
   Float_t  MUR1_MUF1_PDF261054;
   Float_t  MUR1_MUF1_PDF261055;
   Float_t  MUR1_MUF1_PDF261056;
   Float_t  MUR1_MUF1_PDF261057;
   Float_t  MUR1_MUF1_PDF261058;
   Float_t  MUR1_MUF1_PDF261059;
   Float_t  MUR1_MUF1_PDF261060;
   Float_t  MUR1_MUF1_PDF261061;
   Float_t  MUR1_MUF1_PDF261062;
   Float_t  MUR1_MUF1_PDF261063;
   Float_t  MUR1_MUF1_PDF261064;
   Float_t  MUR1_MUF1_PDF261065;
   Float_t  MUR1_MUF1_PDF261066;
   Float_t  MUR1_MUF1_PDF261067;
   Float_t  MUR1_MUF1_PDF261068;
   Float_t  MUR1_MUF1_PDF261069;
   Float_t  MUR1_MUF1_PDF261070;
   Float_t  MUR1_MUF1_PDF261071;
   Float_t  MUR1_MUF1_PDF261072;
   Float_t  MUR1_MUF1_PDF261073;
   Float_t  MUR1_MUF1_PDF261074;
   Float_t  MUR1_MUF1_PDF261075;
   Float_t  MUR1_MUF1_PDF261076;
   Float_t  MUR1_MUF1_PDF261077;
   Float_t  MUR1_MUF1_PDF261078;
   Float_t  MUR1_MUF1_PDF261079;
   Float_t  MUR1_MUF1_PDF261080;
   Float_t  MUR1_MUF1_PDF261081;
   Float_t  MUR1_MUF1_PDF261082;
   Float_t  MUR1_MUF1_PDF261083;
   Float_t  MUR1_MUF1_PDF261084;
   Float_t  MUR1_MUF1_PDF261085;
   Float_t  MUR1_MUF1_PDF261086;
   Float_t  MUR1_MUF1_PDF261087;
   Float_t  MUR1_MUF1_PDF261088;
   Float_t  MUR1_MUF1_PDF261089;
   Float_t  MUR1_MUF1_PDF261090;
   Float_t  MUR1_MUF1_PDF261091;
   Float_t  MUR1_MUF1_PDF261092;
   Float_t  MUR1_MUF1_PDF261093;
   Float_t  MUR1_MUF1_PDF261094;
   Float_t  MUR1_MUF1_PDF261095;
   Float_t  MUR1_MUF1_PDF261096;
   Float_t  MUR1_MUF1_PDF261097;
   Float_t  MUR1_MUF1_PDF261098;
   Float_t  MUR1_MUF1_PDF261099;
   Float_t  MUR1_MUF1_PDF261100;
   Float_t  MUR1_MUF2_PDF261000;
   Float_t  MUR2_MUF1_PDF261000;
   Float_t  MUR2_MUF2_PDF261000;

   // dijet weights
   Float_t  nominal;
   Float_t  hardHi; 
   Float_t  hardLo; 
   Float_t  isrmuRfac0p5_fsrmuRfac0p5;  
   Float_t  isrmuRfac0p5_fsrmuRfac1p0;  
   Float_t  isrmuRfac0p5_fsrmuRfac2p0;  
   Float_t  isrmuRfac0p625_fsrmuRfac1p0; 
   Float_t  isrmuRfac0p75_fsrmuRfac1p0;  
   Float_t  isrmuRfac0p875_fsrmuRfac1p0; 
   Float_t  isrmuRfac1p0_fsrmuRfac0p5;  
   Float_t  isrmuRfac1p0_fsrmuRfac0p625; 
   Float_t  isrmuRfac1p0_fsrmuRfac0p75;  
   Float_t  isrmuRfac1p0_fsrmuRfac0p875; 
   Float_t  isrmuRfac1p0_fsrmuRfac1p25;  
   Float_t  isrmuRfac1p0_fsrmuRfac1p5;  
   Float_t  isrmuRfac1p0_fsrmuRfac1p75;  
   Float_t  isrmuRfac1p0_fsrmuRfac2p0;  
   Float_t  isrmuRfac1p25_fsrmuRfac1p0; 
   Float_t  isrmuRfac1p5_fsrmuRfac1p0;  
   Float_t  isrmuRfac1p75_fsrmuRfac1p0;  
   Float_t  isrmuRfac2p0_fsrmuRfac0p5;  
   Float_t  isrmuRfac2p0_fsrmuRfac1p0;  
   Float_t  isrmuRfac2p0_fsrmuRfac2p0;  

   // Other samples weights
   Float_t  MUR0p5_MUF0p5_PDF303200_PSMUR0p5_PSMUF0p5;
   Float_t  MUR0p5_MUF1_PDF303200_PSMUR0p5_PSMUF1;
   Float_t  MUR1_MUF0p5_PDF303200_PSMUR1_PSMUF0p5;
   Float_t  MUR1_MUF1_PDF14068;
   Float_t  MUR1_MUF1_PDF269000;
   Float_t  MUR1_MUF1_PDF270000;
   Float_t  MUR1_MUF1_PDF27400;
   Float_t  MUR1_MUF1_PDF303200;
   Float_t  MUR1_MUF1_PDF303200_ASSEW;
   Float_t  MUR1_MUF1_PDF303200_ASSEWLO1;
   Float_t  MUR1_MUF1_PDF303200_ASSEWLO1LO2;
   Float_t  MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3;
   Float_t  MUR1_MUF1_PDF303200_EXPASSEW;
   Float_t  MUR1_MUF1_PDF303200_EXPASSEWLO1;
   Float_t  MUR1_MUF1_PDF303200_EXPASSEWLO1LO2;
   Float_t  MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3;
   Float_t  MUR1_MUF1_PDF303200_MULTIASSEW;
   Float_t  MUR1_MUF1_PDF303200_MULTIASSEWLO1;
   Float_t  MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2;
   Float_t  MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3;
   Float_t  MUR1_MUF1_PDF303201;
   Float_t  MUR1_MUF1_PDF303202;
   Float_t  MUR1_MUF1_PDF303203;
   Float_t  MUR1_MUF1_PDF303204;
   Float_t  MUR1_MUF1_PDF303205;
   Float_t  MUR1_MUF1_PDF303206;
   Float_t  MUR1_MUF1_PDF303207;
   Float_t  MUR1_MUF1_PDF303208;
   Float_t  MUR1_MUF1_PDF303209;
   Float_t  MUR1_MUF1_PDF303210;
   Float_t  MUR1_MUF1_PDF303211;
   Float_t  MUR1_MUF1_PDF303212;
   Float_t  MUR1_MUF1_PDF303213;
   Float_t  MUR1_MUF1_PDF303214;
   Float_t  MUR1_MUF1_PDF303215;
   Float_t  MUR1_MUF1_PDF303216;
   Float_t  MUR1_MUF1_PDF303217;
   Float_t  MUR1_MUF1_PDF303218;
   Float_t  MUR1_MUF1_PDF303219;
   Float_t  MUR1_MUF1_PDF303220;
   Float_t  MUR1_MUF1_PDF303221;
   Float_t  MUR1_MUF1_PDF303222;
   Float_t  MUR1_MUF1_PDF303223;
   Float_t  MUR1_MUF1_PDF303224;
   Float_t  MUR1_MUF1_PDF303225;
   Float_t  MUR1_MUF1_PDF303226;
   Float_t  MUR1_MUF1_PDF303227;
   Float_t  MUR1_MUF1_PDF303228;
   Float_t  MUR1_MUF1_PDF303229;
   Float_t  MUR1_MUF1_PDF303230;
   Float_t  MUR1_MUF1_PDF303231;
   Float_t  MUR1_MUF1_PDF303232;
   Float_t  MUR1_MUF1_PDF303233;
   Float_t  MUR1_MUF1_PDF303234;
   Float_t  MUR1_MUF1_PDF303235;
   Float_t  MUR1_MUF1_PDF303236;
   Float_t  MUR1_MUF1_PDF303237;
   Float_t  MUR1_MUF1_PDF303238;
   Float_t  MUR1_MUF1_PDF303239;
   Float_t  MUR1_MUF1_PDF303240;
   Float_t  MUR1_MUF1_PDF303241;
   Float_t  MUR1_MUF1_PDF303242;
   Float_t  MUR1_MUF1_PDF303243;
   Float_t  MUR1_MUF1_PDF303244;
   Float_t  MUR1_MUF1_PDF303245;
   Float_t  MUR1_MUF1_PDF303246;
   Float_t  MUR1_MUF1_PDF303247;
   Float_t  MUR1_MUF1_PDF303248;
   Float_t  MUR1_MUF1_PDF303249;
   Float_t  MUR1_MUF1_PDF303250;
   Float_t  MUR1_MUF1_PDF303251;
   Float_t  MUR1_MUF1_PDF303252;
   Float_t  MUR1_MUF1_PDF303253;
   Float_t  MUR1_MUF1_PDF303254;
   Float_t  MUR1_MUF1_PDF303255;
   Float_t  MUR1_MUF1_PDF303256;
   Float_t  MUR1_MUF1_PDF303257;
   Float_t  MUR1_MUF1_PDF303258;
   Float_t  MUR1_MUF1_PDF303259;
   Float_t  MUR1_MUF1_PDF303260;
   Float_t  MUR1_MUF1_PDF303261;
   Float_t  MUR1_MUF1_PDF303262;
   Float_t  MUR1_MUF1_PDF303263;
   Float_t  MUR1_MUF1_PDF303264;
   Float_t  MUR1_MUF1_PDF303265;
   Float_t  MUR1_MUF1_PDF303266;
   Float_t  MUR1_MUF1_PDF303267;
   Float_t  MUR1_MUF1_PDF303268;
   Float_t  MUR1_MUF1_PDF303269;
   Float_t  MUR1_MUF1_PDF303270;
   Float_t  MUR1_MUF1_PDF303271;
   Float_t  MUR1_MUF1_PDF303272;
   Float_t  MUR1_MUF1_PDF303273;
   Float_t  MUR1_MUF1_PDF303274;
   Float_t  MUR1_MUF1_PDF303275;
   Float_t  MUR1_MUF1_PDF303276;
   Float_t  MUR1_MUF1_PDF303277;
   Float_t  MUR1_MUF1_PDF303278;
   Float_t  MUR1_MUF1_PDF303279;
   Float_t  MUR1_MUF1_PDF303280;
   Float_t  MUR1_MUF1_PDF303281;
   Float_t  MUR1_MUF1_PDF303282;
   Float_t  MUR1_MUF1_PDF303283;
   Float_t  MUR1_MUF1_PDF303284;
   Float_t  MUR1_MUF1_PDF303285;
   Float_t  MUR1_MUF1_PDF303286;
   Float_t  MUR1_MUF1_PDF303287;
   Float_t  MUR1_MUF1_PDF303288;
   Float_t  MUR1_MUF1_PDF303289;
   Float_t  MUR1_MUF1_PDF303290;
   Float_t  MUR1_MUF1_PDF303291;
   Float_t  MUR1_MUF1_PDF303292;
   Float_t  MUR1_MUF1_PDF303293;
   Float_t  MUR1_MUF1_PDF303294;
   Float_t  MUR1_MUF1_PDF303295;
   Float_t  MUR1_MUF1_PDF303296;
   Float_t  MUR1_MUF1_PDF303297;
   Float_t  MUR1_MUF1_PDF303298;
   Float_t  MUR1_MUF1_PDF303299;
   Float_t  MUR1_MUF1_PDF303300;
   Float_t  MUR1_MUF1_PDF304400;
   Float_t  MUR1_MUF1_PDF91400;
   Float_t  MUR1_MUF1_PDF91401;
   Float_t  MUR1_MUF1_PDF91402;
   Float_t  MUR1_MUF1_PDF91403;
   Float_t  MUR1_MUF1_PDF91404;
   Float_t  MUR1_MUF1_PDF91405;
   Float_t  MUR1_MUF1_PDF91406;
   Float_t  MUR1_MUF1_PDF91407;
   Float_t  MUR1_MUF1_PDF91408;
   Float_t  MUR1_MUF1_PDF91409;
   Float_t  MUR1_MUF1_PDF91410;
   Float_t  MUR1_MUF1_PDF91411;
   Float_t  MUR1_MUF1_PDF91412;
   Float_t  MUR1_MUF1_PDF91413;
   Float_t  MUR1_MUF1_PDF91414;
   Float_t  MUR1_MUF1_PDF91415;
   Float_t  MUR1_MUF1_PDF91416;
   Float_t  MUR1_MUF1_PDF91417;
   Float_t  MUR1_MUF1_PDF91418;
   Float_t  MUR1_MUF1_PDF91419;
   Float_t  MUR1_MUF1_PDF91420;
   Float_t  MUR1_MUF1_PDF91421;
   Float_t  MUR1_MUF1_PDF91422;
   Float_t  MUR1_MUF1_PDF91423;
   Float_t  MUR1_MUF1_PDF91424;
   Float_t  MUR1_MUF1_PDF91425;
   Float_t  MUR1_MUF1_PDF91426;
   Float_t  MUR1_MUF1_PDF91427;
   Float_t  MUR1_MUF1_PDF91428;
   Float_t  MUR1_MUF1_PDF91429;
   Float_t  MUR1_MUF1_PDF91430;
   Float_t  MUR1_MUF1_PDF91431;
   Float_t  MUR1_MUF1_PDF91432;
   Float_t  MUR1_MUF2_PDF303200_PSMUR1_PSMUF2;
   Float_t  MUR2_MUF1_PDF303200_PSMUR2_PSMUF1;
   Float_t  MUR2_MUF2_PDF303200_PSMUR2_PSMUF2;
  
};
#endif

#ifdef analysis_cxx
analysis::analysis(TTree *tree) : fChainNominal(0)
{
   Init(tree);
}

analysis::~analysis()
{
   if (!fChainNominal || !fChainSyst) return;
   tree_name_strs.clear();
   //delete fChainNominal;
   //delete fChainSyst;
   //delete output_tree;
}

Int_t analysis::GetEntry(TTree* tree, Long64_t entry)
{
   // Read contents of entry.
   if (!tree) return 0;
   return tree->GetEntry(entry);
}

Long64_t analysis::LoadTree(TTree* tree, Long64_t entry)
{
   // Set the environment to read one entry
   if (!tree) return -5;
   Long64_t centry = tree->LoadTree(entry);
   if (centry < 0) return centry;
   if (tree->GetTreeNumber() != fCurrent) {
      fCurrent = tree->GetTreeNumber();
      Notify();
   }
   return centry;
}

void analysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).
   
   if (!tree) return;

   std::string tree_name = tree->GetName();

   // Set branch addresses and branch pointers for nominal tree 
   if (tree_name.find("data1") != std::string::npos || tree_name.find("NoSys") != std::string::npos){

	// Set object pointer
	jetPt = 0;
	jetEta = 0;
	Photon = 0;
	LHE3Weights = 0;
	LHE3WeightNames = 0;

   	fChainNominal = tree;
   	fCurrent = -1;
   	fChainNominal->SetMakeClass(1);

	// Activate and read only the branches we need for real, this will save a lot of time!
	fChainNominal->SetBranchStatus("*",0);
	fChainNominal->SetBranchStatus("trigWeight_singleElectronTrig",1);
	fChainNominal->SetBranchStatus("trigMatch_singleElectronTrig",1);
	fChainNominal->SetBranchStatus("trigWeight_singlePhotonTrig",1);
	fChainNominal->SetBranchStatus("trigMatch_singlePhotonTrig",1);
	fChainNominal->SetBranchStatus("trigWeight_singleMuonTrig",1);
	fChainNominal->SetBranchStatus("trigMatch_singleMuonTrig",1);
	fChainNominal->SetBranchStatus("trigWeight_metTrig",1);
	fChainNominal->SetBranchStatus("trigMatch_metTrig",1);
	fChainNominal->SetBranchStatus("nLep_base",1);
	fChainNominal->SetBranchStatus("nLep_signal",1);
	fChainNominal->SetBranchStatus("nPhoton_base",1);
	fChainNominal->SetBranchStatus("nPhoton_signal",1);
	fChainNominal->SetBranchStatus("nMu_signal",1);
	fChainNominal->SetBranchStatus("nMu_base",1);
	fChainNominal->SetBranchStatus("nEle_signal",1);
	fChainNominal->SetBranchStatus("nEle_base",1);
	fChainNominal->SetBranchStatus("lep1Charge",1);
	fChainNominal->SetBranchStatus("lep1Pt",1);
	fChainNominal->SetBranchStatus("lep1MT_Met",1);
	fChainNominal->SetBranchStatus("lep1Signal",1);
	fChainNominal->SetBranchStatus("lep2Charge",1);
	fChainNominal->SetBranchStatus("lep2Pt",1);
	fChainNominal->SetBranchStatus("lep2MT_Met",1);
	fChainNominal->SetBranchStatus("lep2Signal",1);
	fChainNominal->SetBranchStatus("passEmulTightJetCleaning1Jet",1);
	fChainNominal->SetBranchStatus("nJet30",1);
	fChainNominal->SetBranchStatus("nBJet30",1);
	fChainNominal->SetBranchStatus("jetPt",1);
	fChainNominal->SetBranchStatus("jetEta",1);
	fChainNominal->SetBranchStatus("met_Et",1);
	fChainNominal->SetBranchStatus("met_Phi",1);
	fChainNominal->SetBranchStatus("minDPhi_met_allJets30",1);
	fChainNominal->SetBranchStatus("met_electrons_invis_Et",1);
	fChainNominal->SetBranchStatus("met_electrons_invis_Phi",1);
	fChainNominal->SetBranchStatus("minDPhi_met_electrons_invis_allJets30",1);
	fChainNominal->SetBranchStatus("met_muons_invis_Et",1);
	fChainNominal->SetBranchStatus("met_muons_invis_Phi",1);
	fChainNominal->SetBranchStatus("minDPhi_met_muons_invis_allJets30",1);
	fChainNominal->SetBranchStatus("met_photons_invis_Et",1);
	fChainNominal->SetBranchStatus("met_photons_invis_Phi",1);
	fChainNominal->SetBranchStatus("minDPhi_met_photons_invis_allJets30",1);
	fChainNominal->SetBranchStatus("hasZCand",1);
	fChainNominal->SetBranchStatus("pileupWeight",1);
	fChainNominal->SetBranchStatus("leptonWeight",1);
	fChainNominal->SetBranchStatus("eventWeight",1);
	fChainNominal->SetBranchStatus("genWeight",1);
	fChainNominal->SetBranchStatus("bTagWeight",1);
	fChainNominal->SetBranchStatus("trigWeight",1);
	fChainNominal->SetBranchStatus("jvtWeight",1);
	fChainNominal->SetBranchStatus("photonWeight",1);
	fChainNominal->SetBranchStatus("Photon",1);
    fChainNominal->SetBranchStatus("pileupWeightUp",1);
    fChainNominal->SetBranchStatus("pileupWeightDown",1);
    fChainNominal->SetBranchStatus("LHE3Weights",1);
    fChainNominal->SetBranchStatus("LHE3WeightNames",1);
    fChainNominal->SetBranchStatus("leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down",1);
    fChainNominal->SetBranchStatus("leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up",1);
    fChainNominal->SetBranchStatus("leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down",1);
    fChainNominal->SetBranchStatus("leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up",1);
    fChainNominal->SetBranchStatus("leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down",1);
    fChainNominal->SetBranchStatus("leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_ISO_STAT__1down",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_ISO_STAT__1up",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_ISO_SYS__1down",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_ISO_SYS__1up",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_RECO_STAT__1down",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_RECO_STAT__1up",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_RECO_SYS__1down",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_RECO_SYS__1up",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_TTVA_STAT__1down",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_TTVA_STAT__1up",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_TTVA_SYS__1down",1);
    fChainNominal->SetBranchStatus("leptonWeight_MUON_EFF_TTVA_SYS__1up",1);
    fChainNominal->SetBranchStatus("trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down",1);
    fChainNominal->SetBranchStatus("trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up",1);
    fChainNominal->SetBranchStatus("trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down",1);
    fChainNominal->SetBranchStatus("trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up",1);
    fChainNominal->SetBranchStatus("trigWeight_MUON_EFF_TrigStatUncertainty__1down",1);
    fChainNominal->SetBranchStatus("trigWeight_MUON_EFF_TrigStatUncertainty__1up",1);
    fChainNominal->SetBranchStatus("trigWeight_MUON_EFF_TrigSystUncertainty__1down",1);
    fChainNominal->SetBranchStatus("trigWeight_MUON_EFF_TrigSystUncertainty__1up",1);
    fChainNominal->SetBranchStatus("bTagWeight_FT_EFF_B_systematics__1down",1);
    fChainNominal->SetBranchStatus("bTagWeight_FT_EFF_B_systematics__1up",1);
    fChainNominal->SetBranchStatus("bTagWeight_FT_EFF_C_systematics__1down",1);
    fChainNominal->SetBranchStatus("bTagWeight_FT_EFF_C_systematics__1up",1);
    fChainNominal->SetBranchStatus("bTagWeight_FT_EFF_Light_systematics__1down",1);
    fChainNominal->SetBranchStatus("bTagWeight_FT_EFF_Light_systematics__1up",1);
    fChainNominal->SetBranchStatus("bTagWeight_FT_EFF_extrapolation__1down",1);
    fChainNominal->SetBranchStatus("bTagWeight_FT_EFF_extrapolation__1up",1);
    fChainNominal->SetBranchStatus("bTagWeight_FT_EFF_extrapolation_from_charm__1down",1);
    fChainNominal->SetBranchStatus("bTagWeight_FT_EFF_extrapolation_from_charm__1up",1);
    fChainNominal->SetBranchStatus("jvtWeight_JET_JvtEfficiency__1down",1);
    fChainNominal->SetBranchStatus("jvtWeight_JET_JvtEfficiency__1up",1);
    fChainNominal->SetBranchStatus("jvtWeight_JET_fJvtEfficiency__1down",1);
    fChainNominal->SetBranchStatus("jvtWeight_JET_fJvtEfficiency__1up",1);

    fChainNominal->SetBranchStatus("trkPt",1);
    fChainNominal->SetBranchStatus("trkPhi",1);
    fChainNominal->SetBranchStatus("trkQ",1);
    fChainNominal->SetBranchStatus("trkD0Sig",1);
    fChainNominal->SetBranchStatus("trkisBaseMuon",1);
    fChainNominal->SetBranchStatus("trkisBaseElectron",1);
    fChainNominal->SetBranchStatus("trkisSignalMuon",1);
    fChainNominal->SetBranchStatus("trkisSignalElectron",1);
    fChainNominal->SetBranchStatus("trkLabel",1);

	fChainNominal->SetBranchAddress("trigWeight_singleElectronTrig", &trigWeight_singleElectronTrig, &b_trigWeight_singleElectronTrig);
	fChainNominal->SetBranchAddress("trigMatch_singleElectronTrig", &trigMatch_singleElectronTrig, &b_trigMatch_singleElectronTrig);
	fChainNominal->SetBranchAddress("trigWeight_singlePhotonTrig", &trigWeight_singlePhotonTrig, &b_trigWeight_singlePhotonTrig);
	fChainNominal->SetBranchAddress("trigMatch_singlePhotonTrig", &trigMatch_singlePhotonTrig, &b_trigMatch_singlePhotonTrig);
	fChainNominal->SetBranchAddress("trigWeight_singleMuonTrig", &trigWeight_singleMuonTrig, &b_trigWeight_singleMuonTrig);
	fChainNominal->SetBranchAddress("trigMatch_singleMuonTrig", &trigMatch_singleMuonTrig, &b_trigMatch_singleMuonTrig);
	fChainNominal->SetBranchAddress("trigWeight_metTrig", &trigWeight_metTrig, &b_trigWeight_metTrig);
	fChainNominal->SetBranchAddress("trigMatch_metTrig", &trigMatch_metTrig, &b_trigMatch_metTrig);
	fChainNominal->SetBranchAddress("nLep_base", &nLep_base, &b_nLep_base);
	fChainNominal->SetBranchAddress("nLep_signal", &nLep_signal, &b_nLep_signal);
	fChainNominal->SetBranchAddress("nPhoton_base", &nPhoton_base, &b_nPhoton_base);
	fChainNominal->SetBranchAddress("nPhoton_signal", &nPhoton_signal, &b_nPhoton_signal);
	fChainNominal->SetBranchAddress("nMu_signal", &nMu_signal, &b_nMu_signal);
	fChainNominal->SetBranchAddress("nMu_base", &nMu_base, &b_nMu_base);
	fChainNominal->SetBranchAddress("nEle_signal", &nEle_signal, &b_nEle_signal);
	fChainNominal->SetBranchAddress("nEle_base", &nEle_base, &b_nEle_base);
	fChainNominal->SetBranchAddress("lep1Charge", &lep1Charge, &b_lep1Charge);
	fChainNominal->SetBranchAddress("lep1Pt", &lep1Pt, &b_lep1Pt);
	fChainNominal->SetBranchAddress("lep1MT_Met", &lep1MT_Met, &b_lep1MT_Met);
	fChainNominal->SetBranchAddress("lep1Signal", &lep1Signal, &b_lep1Signal);
	fChainNominal->SetBranchAddress("lep2Charge", &lep2Charge, &b_lep2Charge);
	fChainNominal->SetBranchAddress("lep2Pt", &lep2Pt, &b_lep2Pt);
	fChainNominal->SetBranchAddress("lep2MT_Met", &lep2MT_Met, &b_lep2MT_Met);
	fChainNominal->SetBranchAddress("lep2Signal", &lep2Signal, &b_lep2Signal);
	fChainNominal->SetBranchAddress("passEmulTightJetCleaning1Jet", &passEmulTightJetCleaning1Jet, &b_passEmulTightJetCleaning1Jet);
	fChainNominal->SetBranchAddress("nJet30", &nJet30, &b_nJet30);
	fChainNominal->SetBranchAddress("nBJet30", &nBJet30, &b_nBJet30);
	fChainNominal->SetBranchAddress("jetPt", &jetPt, &b_jetPt);
	fChainNominal->SetBranchAddress("jetEta", &jetEta, &b_jetEta);
	fChainNominal->SetBranchAddress("met_Et", &met_Et, &b_met_Et);
	fChainNominal->SetBranchAddress("met_Phi", &met_Phi, &b_met_Phi);
	fChainNominal->SetBranchAddress("minDPhi_met_allJets30", &minDPhi_met_allJets30, &b_minDPhi_met_allJets30);
	fChainNominal->SetBranchAddress("met_electrons_invis_Et", &met_electrons_invis_Et, &b_met_electrons_invis_Et);
	fChainNominal->SetBranchAddress("met_electrons_invis_Phi", &met_electrons_invis_Phi, &b_met_electrons_invis_Phi);
	fChainNominal->SetBranchAddress("minDPhi_met_electrons_invis_allJets30", &minDPhi_met_electrons_invis_allJets30, &b_minDPhi_met_electrons_invis_allJets30);
	fChainNominal->SetBranchAddress("met_muons_invis_Et", &met_muons_invis_Et, &b_met_muons_invis_Et);
	fChainNominal->SetBranchAddress("met_muons_invis_Phi", &met_muons_invis_Phi, &b_met_muons_invis_Phi);
	fChainNominal->SetBranchAddress("minDPhi_met_muons_invis_allJets30", &minDPhi_met_muons_invis_allJets30, &b_minDPhi_met_muons_invis_allJets30);
	fChainNominal->SetBranchAddress("met_photons_invis_Et", &met_photons_invis_Et, &b_met_photons_invis_Et);
	fChainNominal->SetBranchAddress("met_photons_invis_Phi", &met_photons_invis_Phi, &b_met_photons_invis_Phi);
	fChainNominal->SetBranchAddress("minDPhi_met_photons_invis_allJets30", &minDPhi_met_photons_invis_allJets30, &b_minDPhi_met_photons_invis_allJets30);
	fChainNominal->SetBranchAddress("hasZCand", &hasZCand, &b_hasZCand);
	fChainNominal->SetBranchAddress("pileupWeight", &pileupWeight, &b_pileupWeight);
	fChainNominal->SetBranchAddress("pileupWeightUp", &pileupWeightUp, &b_pileupWeightUp);
    fChainNominal->SetBranchAddress("pileupWeightDown", &pileupWeightDown, &b_pileupWeightDown);
	fChainNominal->SetBranchAddress("leptonWeight", &leptonWeight, &b_leptonWeight);
	fChainNominal->SetBranchAddress("eventWeight", &eventWeight, &b_eventWeight);
	fChainNominal->SetBranchAddress("genWeight", &genWeight, &b_genWeight);
	fChainNominal->SetBranchAddress("bTagWeight", &bTagWeight, &b_bTagWeight);
	fChainNominal->SetBranchAddress("trigWeight", &trigWeight, &b_trigWeight);
	fChainNominal->SetBranchAddress("jvtWeight", &jvtWeight, &b_jvtWeight);
	fChainNominal->SetBranchAddress("photonWeight", &photonWeight, &b_photonWeight);
	fChainNominal->SetBranchAddress("Photon", &Photon, &b_Photon);	
	fChainNominal->SetBranchAddress("trkPt", &trkPt, &b_trkPt);
	fChainNominal->SetBranchAddress("trkPhi", &trkPhi, &b_trkPhi);
    fChainNominal->SetBranchAddress("trkQ", &trkQ, &b_trkQ);
    fChainNominal->SetBranchAddress("trkD0Sig", &trkD0Sig, &b_trkD0Sig);
    fChainNominal->SetBranchAddress("trkisBaseMuon", &trkisBaseMuon, &b_trkisBaseMuon);
    fChainNominal->SetBranchAddress("trkisBaseElectron", &trkisBaseElectron, &b_trkisBaseElectron);
    fChainNominal->SetBranchAddress("trkisSignalMuon", &trkisSignalMuon, &b_trkisSignalMuon);
    fChainNominal->SetBranchAddress("trkisSignalElectron", &trkisSignalElectron, &b_trkisSignalElectron);
    fChainNominal->SetBranchAddress("trkLabel", &trkLabel, &b_trkLabel);
    fChainNominal->SetBranchAddress("LHE3Weights", &LHE3Weights, &b_LHE3Weights);
    fChainNominal->SetBranchAddress("LHE3WeightNames", &LHE3WeightNames, &b_LHE3WeightNames);
    fChainNominal->SetBranchAddress("leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
    fChainNominal->SetBranchAddress("leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
    fChainNominal->SetBranchAddress("leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
    fChainNominal->SetBranchAddress("leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
    fChainNominal->SetBranchAddress("leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
    fChainNominal->SetBranchAddress("leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_ISO_STAT__1down", &leptonWeight_MUON_EFF_ISO_STAT__1down, &b_leptonWeight_MUON_EFF_ISO_STAT__1down);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_ISO_STAT__1up", &leptonWeight_MUON_EFF_ISO_STAT__1up, &b_leptonWeight_MUON_EFF_ISO_STAT__1up);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_ISO_SYS__1down", &leptonWeight_MUON_EFF_ISO_SYS__1down, &b_leptonWeight_MUON_EFF_ISO_SYS__1down);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_ISO_SYS__1up", &leptonWeight_MUON_EFF_ISO_SYS__1up, &b_leptonWeight_MUON_EFF_ISO_SYS__1up);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_RECO_STAT__1down", &leptonWeight_MUON_EFF_RECO_STAT__1down, &b_leptonWeight_MUON_EFF_RECO_STAT__1down);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_RECO_STAT__1up", &leptonWeight_MUON_EFF_RECO_STAT__1up, &b_leptonWeight_MUON_EFF_RECO_STAT__1up);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down", &leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down, &b_leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up", &leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up, &b_leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_RECO_SYS__1down", &leptonWeight_MUON_EFF_RECO_SYS__1down, &b_leptonWeight_MUON_EFF_RECO_SYS__1down);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_RECO_SYS__1up", &leptonWeight_MUON_EFF_RECO_SYS__1up, &b_leptonWeight_MUON_EFF_RECO_SYS__1up);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down", &leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down, &b_leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up", &leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up, &b_leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_TTVA_STAT__1down", &leptonWeight_MUON_EFF_TTVA_STAT__1down, &b_leptonWeight_MUON_EFF_TTVA_STAT__1down);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_TTVA_STAT__1up", &leptonWeight_MUON_EFF_TTVA_STAT__1up, &b_leptonWeight_MUON_EFF_TTVA_STAT__1up);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_TTVA_SYS__1down", &leptonWeight_MUON_EFF_TTVA_SYS__1down, &b_leptonWeight_MUON_EFF_TTVA_SYS__1down);
    fChainNominal->SetBranchAddress("leptonWeight_MUON_EFF_TTVA_SYS__1up", &leptonWeight_MUON_EFF_TTVA_SYS__1up, &b_leptonWeight_MUON_EFF_TTVA_SYS__1up);
    fChainNominal->SetBranchAddress("trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down);
    fChainNominal->SetBranchAddress("trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up);
    fChainNominal->SetBranchAddress("trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down);
    fChainNominal->SetBranchAddress("trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up);
    fChainNominal->SetBranchAddress("trigWeight_MUON_EFF_TrigStatUncertainty__1down", &trigWeight_MUON_EFF_TrigStatUncertainty__1down, &b_trigWeight_MUON_EFF_TrigStatUncertainty__1down);
    fChainNominal->SetBranchAddress("trigWeight_MUON_EFF_TrigStatUncertainty__1up", &trigWeight_MUON_EFF_TrigStatUncertainty__1up, &b_trigWeight_MUON_EFF_TrigStatUncertainty__1up);
    fChainNominal->SetBranchAddress("trigWeight_MUON_EFF_TrigSystUncertainty__1down", &trigWeight_MUON_EFF_TrigSystUncertainty__1down, &b_trigWeight_MUON_EFF_TrigSystUncertainty__1down);
    fChainNominal->SetBranchAddress("trigWeight_MUON_EFF_TrigSystUncertainty__1up", &trigWeight_MUON_EFF_TrigSystUncertainty__1up, &b_trigWeight_MUON_EFF_TrigSystUncertainty__1up);
    fChainNominal->SetBranchAddress("bTagWeight_FT_EFF_B_systematics__1down", &bTagWeight_FT_EFF_B_systematics__1down, &b_bTagWeight_FT_EFF_B_systematics__1down);
    fChainNominal->SetBranchAddress("bTagWeight_FT_EFF_B_systematics__1up", &bTagWeight_FT_EFF_B_systematics__1up, &b_bTagWeight_FT_EFF_B_systematics__1up);
    fChainNominal->SetBranchAddress("bTagWeight_FT_EFF_C_systematics__1down", &bTagWeight_FT_EFF_C_systematics__1down, &b_bTagWeight_FT_EFF_C_systematics__1down);
    fChainNominal->SetBranchAddress("bTagWeight_FT_EFF_C_systematics__1up", &bTagWeight_FT_EFF_C_systematics__1up, &b_bTagWeight_FT_EFF_C_systematics__1up);
    fChainNominal->SetBranchAddress("bTagWeight_FT_EFF_Light_systematics__1down", &bTagWeight_FT_EFF_Light_systematics__1down, &b_bTagWeight_FT_EFF_Light_systematics__1down);
    fChainNominal->SetBranchAddress("bTagWeight_FT_EFF_Light_systematics__1up", &bTagWeight_FT_EFF_Light_systematics__1up, &b_bTagWeight_FT_EFF_Light_systematics__1up);
    fChainNominal->SetBranchAddress("bTagWeight_FT_EFF_extrapolation__1down", &bTagWeight_FT_EFF_extrapolation__1down, &b_bTagWeight_FT_EFF_extrapolation__1down);
    fChainNominal->SetBranchAddress("bTagWeight_FT_EFF_extrapolation__1up", &bTagWeight_FT_EFF_extrapolation__1up, &b_bTagWeight_FT_EFF_extrapolation__1up);
    fChainNominal->SetBranchAddress("bTagWeight_FT_EFF_extrapolation_from_charm__1down", &bTagWeight_FT_EFF_extrapolation_from_charm__1down, &b_bTagWeight_FT_EFF_extrapolation_from_charm__1down);
    fChainNominal->SetBranchAddress("bTagWeight_FT_EFF_extrapolation_from_charm__1up", &bTagWeight_FT_EFF_extrapolation_from_charm__1up, &b_bTagWeight_FT_EFF_extrapolation_from_charm__1up);
    fChainNominal->SetBranchAddress("jvtWeight_JET_JvtEfficiency__1down", &jvtWeight_JET_JvtEfficiency__1down, &b_jvtWeight_JET_JvtEfficiency__1down);
    fChainNominal->SetBranchAddress("jvtWeight_JET_JvtEfficiency__1up", &jvtWeight_JET_JvtEfficiency__1up, &b_jvtWeight_JET_JvtEfficiency__1up);
    fChainNominal->SetBranchAddress("jvtWeight_JET_fJvtEfficiency__1down", &jvtWeight_JET_fJvtEfficiency__1down, &b_jvtWeight_JET_fJvtEfficiency__1down);
    fChainNominal->SetBranchAddress("jvtWeight_JET_fJvtEfficiency__1up", &jvtWeight_JET_fJvtEfficiency__1up, &b_jvtWeight_JET_fJvtEfficiency__1up);  

   }
   else{
        
	// Set object pointer
	jetPt_syst = 0;
	jetEta_syst = 0;
	Photon_syst = 0;

	fChainSyst = tree;
	fCurrent = -1;
	fChainSyst->SetMakeClass(1);

	// Activate and read only the branches we need for real, this will save a lot of time!
	fChainSyst->SetBranchStatus("*",0);
	fChainSyst->SetBranchStatus("trigWeight_singleElectronTrig",1);
	fChainSyst->SetBranchStatus("trigMatch_singleElectronTrig",1);
	fChainSyst->SetBranchStatus("trigWeight_singlePhotonTrig",1);
	fChainSyst->SetBranchStatus("trigMatch_singlePhotonTrig",1);
	fChainSyst->SetBranchStatus("trigWeight_singleMuonTrig",1);
	fChainSyst->SetBranchStatus("trigMatch_singleMuonTrig",1);
	fChainSyst->SetBranchStatus("trigWeight_metTrig",1);
	fChainSyst->SetBranchStatus("trigMatch_metTrig",1);
	fChainSyst->SetBranchStatus("nLep_base",1);
	fChainSyst->SetBranchStatus("nLep_signal",1);
	fChainSyst->SetBranchStatus("nPhoton_base",1);
	fChainSyst->SetBranchStatus("nPhoton_signal",1);
	fChainSyst->SetBranchStatus("nMu_signal",1);
	fChainSyst->SetBranchStatus("nMu_base",1);
	fChainSyst->SetBranchStatus("nEle_signal",1);
	fChainSyst->SetBranchStatus("nEle_base",1);
	fChainSyst->SetBranchStatus("lep1Charge",1);
	fChainSyst->SetBranchStatus("lep1Pt",1);
	fChainSyst->SetBranchStatus("lep1MT_Met",1);
	fChainSyst->SetBranchStatus("lep1Signal",1);
	fChainSyst->SetBranchStatus("lep2Charge",1);
	fChainSyst->SetBranchStatus("lep2Pt",1);
	fChainSyst->SetBranchStatus("lep2MT_Met",1);
	fChainSyst->SetBranchStatus("lep2Signal",1);
	fChainSyst->SetBranchStatus("passEmulTightJetCleaning1Jet",1);
	fChainSyst->SetBranchStatus("nJet30",1);
	fChainSyst->SetBranchStatus("nBJet30",1);
	fChainSyst->SetBranchStatus("jetPt",1);
	fChainSyst->SetBranchStatus("jetEta",1);
	fChainSyst->SetBranchStatus("met_Et",1);
	fChainSyst->SetBranchStatus("met_Phi",1);
	fChainSyst->SetBranchStatus("minDPhi_met_allJets30",1);
	fChainSyst->SetBranchStatus("met_electrons_invis_Et",1);
	fChainSyst->SetBranchStatus("met_electrons_invis_Phi",1);
	fChainSyst->SetBranchStatus("minDPhi_met_electrons_invis_allJets30",1);
	fChainSyst->SetBranchStatus("met_muons_invis_Et",1);
	fChainSyst->SetBranchStatus("met_muons_invis_Phi",1);
	fChainSyst->SetBranchStatus("minDPhi_met_muons_invis_allJets30",1);
	fChainSyst->SetBranchStatus("met_photons_invis_Et",1);
	fChainSyst->SetBranchStatus("met_photons_invis_Phi",1);
	fChainSyst->SetBranchStatus("minDPhi_met_photons_invis_allJets30",1);
	fChainSyst->SetBranchStatus("hasZCand",1);
	fChainSyst->SetBranchStatus("pileupWeight",1);
	fChainSyst->SetBranchStatus("leptonWeight",1);
	fChainSyst->SetBranchStatus("eventWeight",1);
	fChainSyst->SetBranchStatus("genWeight",1);
	fChainSyst->SetBranchStatus("bTagWeight",1);
	fChainSyst->SetBranchStatus("trigWeight",1);
	fChainSyst->SetBranchStatus("jvtWeight",1);
	fChainSyst->SetBranchStatus("photonWeight",1);
	fChainSyst->SetBranchStatus("Photon",1);

    fChainSyst->SetBranchStatus("trkPt",1);
    fChainSyst->SetBranchStatus("trkPhi",1);
    fChainSyst->SetBranchStatus("trkQ",1);
    fChainSyst->SetBranchStatus("trkD0Sig",1);
    fChainSyst->SetBranchStatus("trkisBaseMuon",1);
    fChainSyst->SetBranchStatus("trkisBaseElectron",1);
    fChainSyst->SetBranchStatus("trkisSignalMuon",1);
    fChainSyst->SetBranchStatus("trkisSignalElectron",1);
    fChainSyst->SetBranchStatus("trkLabel",1);

	fChainSyst->SetBranchAddress("trigWeight_singleElectronTrig", &trigWeight_singleElectronTrig_syst, &b_trigWeight_singleElectronTrig_syst);
	fChainSyst->SetBranchAddress("trigMatch_singleElectronTrig", &trigMatch_singleElectronTrig_syst, &b_trigMatch_singleElectronTrig_syst);
	fChainSyst->SetBranchAddress("trigWeight_singlePhotonTrig", &trigWeight_singlePhotonTrig_syst, &b_trigWeight_singlePhotonTrig_syst);
	fChainSyst->SetBranchAddress("trigMatch_singlePhotonTrig", &trigMatch_singlePhotonTrig_syst, &b_trigMatch_singlePhotonTrig_syst);
	fChainSyst->SetBranchAddress("trigWeight_singleMuonTrig", &trigWeight_singleMuonTrig_syst, &b_trigWeight_singleMuonTrig_syst);
	fChainSyst->SetBranchAddress("trigMatch_singleMuonTrig", &trigMatch_singleMuonTrig_syst, &b_trigMatch_singleMuonTrig_syst);
	fChainSyst->SetBranchAddress("trigWeight_metTrig", &trigWeight_metTrig_syst, &b_trigWeight_metTrig_syst);
	fChainSyst->SetBranchAddress("trigMatch_metTrig", &trigMatch_metTrig_syst, &b_trigMatch_metTrig_syst);
	fChainSyst->SetBranchAddress("nLep_base", &nLep_base_syst, &b_nLep_base_syst);
	fChainSyst->SetBranchAddress("nLep_signal", &nLep_signal_syst, &b_nLep_signal_syst);
	fChainSyst->SetBranchAddress("nPhoton_base", &nPhoton_base_syst, &b_nPhoton_base_syst);
	fChainSyst->SetBranchAddress("nPhoton_signal", &nPhoton_signal_syst, &b_nPhoton_signal_syst);
	fChainSyst->SetBranchAddress("nMu_signal", &nMu_signal_syst, &b_nMu_signal_syst);
	fChainSyst->SetBranchAddress("nMu_base", &nMu_base_syst, &b_nMu_base_syst);
	fChainSyst->SetBranchAddress("nEle_signal", &nEle_signal_syst, &b_nEle_signal_syst);
	fChainSyst->SetBranchAddress("nEle_base", &nEle_base_syst, &b_nEle_base_syst);
	fChainSyst->SetBranchAddress("lep1Charge", &lep1Charge_syst, &b_lep1Charge_syst);
	fChainSyst->SetBranchAddress("lep1Pt", &lep1Pt_syst, &b_lep1Pt_syst);
	fChainSyst->SetBranchAddress("lep1MT_Met", &lep1MT_Met_syst, &b_lep1MT_Met_syst);
	fChainSyst->SetBranchAddress("lep1Signal", &lep1Signal_syst, &b_lep1Signal_syst);
	fChainSyst->SetBranchAddress("lep2Charge", &lep2Charge_syst, &b_lep2Charge_syst);
	fChainSyst->SetBranchAddress("lep2Pt", &lep2Pt_syst, &b_lep2Pt_syst);
	fChainSyst->SetBranchAddress("lep2MT_Met", &lep2MT_Met_syst, &b_lep2MT_Met_syst);
	fChainSyst->SetBranchAddress("lep2Signal", &lep2Signal_syst, &b_lep2Signal_syst);
	fChainSyst->SetBranchAddress("passEmulTightJetCleaning1Jet", &passEmulTightJetCleaning1Jet_syst, &b_passEmulTightJetCleaning1Jet_syst);
	fChainSyst->SetBranchAddress("nJet30", &nJet30_syst, &b_nJet30_syst);
	fChainSyst->SetBranchAddress("nBJet30", &nBJet30_syst, &b_nBJet30_syst);
	fChainSyst->SetBranchAddress("jetPt", &jetPt_syst, &b_jetPt_syst);
	fChainSyst->SetBranchAddress("jetEta", &jetEta_syst, &b_jetEta_syst);
	fChainSyst->SetBranchAddress("met_Et", &met_Et_syst, &b_met_Et_syst);
	fChainSyst->SetBranchAddress("met_Phi", &met_Phi_syst, &b_met_Phi_syst);
	fChainSyst->SetBranchAddress("minDPhi_met_allJets30", &minDPhi_met_allJets30_syst, &b_minDPhi_met_allJets30_syst);
	fChainSyst->SetBranchAddress("met_electrons_invis_Et", &met_electrons_invis_Et_syst, &b_met_electrons_invis_Et_syst);
	fChainSyst->SetBranchAddress("met_electrons_invis_Phi", &met_electrons_invis_Phi_syst, &b_met_electrons_invis_Phi_syst);
	fChainSyst->SetBranchAddress("minDPhi_met_electrons_invis_allJets30", &minDPhi_met_electrons_invis_allJets30_syst, &b_minDPhi_met_electrons_invis_allJets30_syst);
	fChainSyst->SetBranchAddress("met_muons_invis_Et", &met_muons_invis_Et_syst, &b_met_muons_invis_Et_syst);
	fChainSyst->SetBranchAddress("met_muons_invis_Phi", &met_muons_invis_Phi_syst, &b_met_muons_invis_Phi_syst);
	fChainSyst->SetBranchAddress("minDPhi_met_muons_invis_allJets30", &minDPhi_met_muons_invis_allJets30_syst, &b_minDPhi_met_muons_invis_allJets30_syst);
	fChainSyst->SetBranchAddress("met_photons_invis_Et", &met_photons_invis_Et_syst, &b_met_photons_invis_Et_syst);
	fChainSyst->SetBranchAddress("met_photons_invis_Phi", &met_photons_invis_Phi_syst, &b_met_photons_invis_Phi_syst);
	fChainSyst->SetBranchAddress("minDPhi_met_photons_invis_allJets30", &minDPhi_met_photons_invis_allJets30_syst, &b_minDPhi_met_photons_invis_allJets30_syst);
	fChainSyst->SetBranchAddress("hasZCand", &hasZCand_syst, &b_hasZCand_syst);
	fChainSyst->SetBranchAddress("pileupWeight", &pileupWeight_syst, &b_pileupWeight_syst);
	fChainSyst->SetBranchAddress("leptonWeight", &leptonWeight_syst, &b_leptonWeight_syst);
	fChainSyst->SetBranchAddress("eventWeight", &eventWeight_syst, &b_eventWeight_syst);
	fChainSyst->SetBranchAddress("genWeight", &genWeight_syst, &b_genWeight_syst);
	fChainSyst->SetBranchAddress("bTagWeight", &bTagWeight_syst, &b_bTagWeight_syst);
	fChainSyst->SetBranchAddress("trigWeight", &trigWeight_syst, &b_trigWeight_syst);
	fChainSyst->SetBranchAddress("jvtWeight", &jvtWeight_syst, &b_jvtWeight_syst);
	fChainSyst->SetBranchAddress("photonWeight", &photonWeight_syst, &b_photonWeight_syst);
	fChainSyst->SetBranchAddress("Photon", &Photon_syst, &b_Photon_syst);
	
	fChainSyst->SetBranchAddress("trkPt", &trkPt_syst, &b_trkPt_syst);
	fChainSyst->SetBranchAddress("trkPhi", &trkPhi_syst, &b_trkPhi_syst);
    fChainSyst->SetBranchAddress("trkQ", &trkQ_syst, &b_trkQ_syst);
    fChainSyst->SetBranchAddress("trkD0Sig", &trkD0Sig_syst, &b_trkD0Sig_syst);
    fChainSyst->SetBranchAddress("trkisBaseMuon", &trkisBaseMuon_syst, &b_trkisBaseMuon_syst);
    fChainSyst->SetBranchAddress("trkisBaseElectron", &trkisBaseElectron_syst, &b_trkisBaseElectron_syst);
    fChainSyst->SetBranchAddress("trkisSignalMuon", &trkisSignalMuon_syst, &b_trkisSignalMuon_syst);
    fChainSyst->SetBranchAddress("trkisSignalElectron", &trkisSignalElectron_syst, &b_trkisSignalElectron_syst);
    fChainSyst->SetBranchAddress("trkLabel", &trkLabel_syst, &b_trkLabel_syst);

   }

   // Temporary hack to handle name exceptions
   tree_name_strs.clear();
   if (tree_name.find("diboson") != std::string::npos){
     boost::split(tree_name_strs, tree_name, boost::is_any_of("_"));
     tree_name = tree_name_strs[0];
     for (unsigned int i = 3; i < tree_name_strs.size(); i++) tree_name += "_" + tree_name_strs[i];
   }
   else if (tree_name.find("MGPy8EG_A14N23LO_HH") != std::string::npos){
     boost::split(tree_name_strs, tree_name, boost::is_any_of("_"));
     tree_name = "Higgsino";
     for (unsigned int i = 7; i < tree_name_strs.size(); i++) tree_name += "_" + tree_name_strs[i];
   } 
   else if (tree_name.find("Znn") != std::string::npos || tree_name.find("Zee") != std::string::npos || tree_name.find("Zmm") != std::string::npos || tree_name.find("Ztt") != std::string::npos){
     boost::split(tree_name_strs, tree_name, boost::is_any_of("_"));
     tree_name = "Zjets";
     for (unsigned int i = 1; i < tree_name_strs.size(); i++) tree_name += "_" + tree_name_strs[i];
   }
   else if (tree_name.find("Wtn") != std::string::npos || tree_name.find("Wen") != std::string::npos || tree_name.find("Wmn") != std::string::npos){
     boost::split(tree_name_strs, tree_name, boost::is_any_of("_"));
     tree_name = "Wlnjets";
     for (unsigned int i = 1; i < tree_name_strs.size(); i++) tree_name += "_" + tree_name_strs[i];
   }

   if (tree_name.find("data1") != std::string::npos) output_tree = std::shared_ptr<TTree>(new TTree("data", "data"));
   else if (tree_name.find("diboson") != std::string::npos || tree_name.find("Higgsino") != std::string::npos || tree_name.find("Zjets") != std::string::npos || tree_name.find("Wlnjets") != std::string::npos) output_tree = std::shared_ptr<TTree>(new TTree(tree_name.c_str(),tree_name.c_str()));
   else output_tree = std::shared_ptr<TTree>(new TTree(tree->GetName(), tree->GetName()));
   output_tree->SetDirectory(0);

   // Output tree branches
   output_tree->Branch("genWeight",&GenWeight,"genWeight/F");
   output_tree->Branch("eventWeight",&EventWeight,"eventWeight/F");
   output_tree->Branch("leptonWeight",&LeptonWeight,"leptonWeight/F");
   output_tree->Branch("jvtWeight",&JVTWeight,"jvtWeight/F");
   output_tree->Branch("bTagWeight",&BTagWeight,"bTagWeight/F");
   output_tree->Branch("pileupWeight",&PileupWeight,"pileupWeight/F");
   output_tree->Branch("pileupWeightUp",&PileupWeightUp,"pileupWeightUp/F");
   output_tree->Branch("pileupWeightDown",&PileupWeightDown,"pileupWeightDown/F");
   output_tree->Branch("photonWeight",&PhotonWeight,"photonWeight/F");
   output_tree->Branch("lep1Charge",&Lep1Q,"lep1Charge/I");
   output_tree->Branch("nLep_base",&nLepBase,"nLep_base/I");
   output_tree->Branch("nMu_signal",&nMuSig,"nMu_signal/I");
   output_tree->Branch("nMu_base",&nMuBase,"nMu_base/I");
   output_tree->Branch("nEle_signal",&nEleSig,"nEle_signal/I");
   output_tree->Branch("nEle_base",&nEleBase,"nEle_base/I");
   output_tree->Branch("nPhoton_signal",&nPhotonSignal,"nPhotonSignal/I");
   output_tree->Branch("nPhoton_base",&nPhotonBase,"nPhotonBase/I");
   output_tree->Branch("nBJet30",&nBJet,"nBJet30/I");
   output_tree->Branch("TrackD0Sig",&TrackD0Sig,"TrackD0Sig/F");
   output_tree->Branch("TrackLabel",&TrackLabel,"TrackLabel/I");
   output_tree->Branch("TrackPt",&TrackPt,"TrackPt/F");
   output_tree->Branch("TrackQ",&TrackQ,"TrackQ/I");
   output_tree->Branch("MET",&MET,"MET/F");
   output_tree->Branch("trigWeight",&TrigWeight,"trigWeight/F");
   output_tree->Branch("trigWeight_singleMuonTrig",&TrigWeight_Mu,"trigWeight_singleMuonTrig/F");
   output_tree->Branch("trigWeight_singleElectronTrig",&TrigWeight_Ele,"trigWeight_singleElectronTrig/F");
   output_tree->Branch("trigWeight_singlePhotonTrig",&TrigWeight_Gamma,"trigWeight_singlePhotonTrig/F");
   output_tree->Branch("trigMatch_metTrig",&TrigMatch_met,"trigMatch_metTrig/I");
   output_tree->Branch("trigMatch_singleMuonTrig",&TrigMatch_Mu,"trigMatch_singleMuonTrig/I");
   output_tree->Branch("trigMatch_singleElectronTrig",&TrigMatch_Ele,"trigMatch_singleElectronTrig/I");
   
   // Create output branch for weight systematics in nominal tree only
   if (tree_name.find("NoSys") != std::string::npos){ 
	output_tree->Branch("leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &LeptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, "LeptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down/F");
	output_tree->Branch("leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &LeptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, "LeptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up/F");
	output_tree->Branch("leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &LeptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, "LeptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down/F");
	output_tree->Branch("leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &LeptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, "LeptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up/F");
	output_tree->Branch("leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &LeptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, "LeptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down/F");
	output_tree->Branch("leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &LeptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, "LeptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up/F");
	output_tree->Branch("leptonWeight_MUON_EFF_ISO_STAT__1down", &LeptonWeight_MUON_EFF_ISO_STAT__1down, "LeptonWeight_MUON_EFF_ISO_STAT__1down/F");
	output_tree->Branch("leptonWeight_MUON_EFF_ISO_STAT__1up", &LeptonWeight_MUON_EFF_ISO_STAT__1up, "LeptonWeight_MUON_EFF_ISO_STAT__1up/F");
	output_tree->Branch("leptonWeight_MUON_EFF_ISO_SYS__1down", &LeptonWeight_MUON_EFF_ISO_SYS__1down, "LeptonWeight_MUON_EFF_ISO_SYS__1down/F");
	output_tree->Branch("leptonWeight_MUON_EFF_ISO_SYS__1up", &LeptonWeight_MUON_EFF_ISO_SYS__1up, "LeptonWeight_MUON_EFF_ISO_SYS__1up/F");
	output_tree->Branch("leptonWeight_MUON_EFF_RECO_STAT__1down", &LeptonWeight_MUON_EFF_RECO_STAT__1down, "LeptonWeight_MUON_EFF_RECO_STAT__1down/F");
	output_tree->Branch("leptonWeight_MUON_EFF_RECO_STAT__1up", &LeptonWeight_MUON_EFF_RECO_STAT__1up, "LeptonWeight_MUON_EFF_RECO_STAT__1up/F");
	output_tree->Branch("leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down", &LeptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down, "LeptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down/F");
	output_tree->Branch("leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up", &LeptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up, "LeptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up/F");
	output_tree->Branch("leptonWeight_MUON_EFF_RECO_SYS__1down", &LeptonWeight_MUON_EFF_RECO_SYS__1down, "LeptonWeight_MUON_EFF_RECO_SYS__1down/F");
	output_tree->Branch("leptonWeight_MUON_EFF_RECO_SYS__1up", &LeptonWeight_MUON_EFF_RECO_SYS__1up, "LeptonWeight_MUON_EFF_RECO_SYS__1up/F");
	output_tree->Branch("leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down", &LeptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down, "LeptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down/F");
	output_tree->Branch("leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up", &LeptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up, "LeptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up/F");
	output_tree->Branch("leptonWeight_MUON_EFF_TTVA_STAT__1down", &LeptonWeight_MUON_EFF_TTVA_STAT__1down, "LeptonWeight_MUON_EFF_TTVA_STAT__1down/F");
	output_tree->Branch("leptonWeight_MUON_EFF_TTVA_STAT__1up", &LeptonWeight_MUON_EFF_TTVA_STAT__1up, "LeptonWeight_MUON_EFF_TTVA_STAT__1up/F");
	output_tree->Branch("leptonWeight_MUON_EFF_TTVA_SYS__1down", &LeptonWeight_MUON_EFF_TTVA_SYS__1down, "LeptonWeight_MUON_EFF_TTVA_SYS__1down/F");
	output_tree->Branch("leptonWeight_MUON_EFF_TTVA_SYS__1up", &LeptonWeight_MUON_EFF_TTVA_SYS__1up, "LeptonWeight_MUON_EFF_TTVA_SYS__1up/F");
	output_tree->Branch("trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down", &LeptonTrigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down, "LeptonTrigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down/F");
	output_tree->Branch("trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up", &LeptonTrigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up, "LeptonTrigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up/F");
	output_tree->Branch("trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down", &LeptonTrigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down, "LeptonTrigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down/F");
	output_tree->Branch("trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up", &LeptonTrigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up, "LeptonTrigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up/F");
	output_tree->Branch("trigWeight_MUON_EFF_TrigStatUncertainty__1down", &LeptonTrigWeight_MUON_EFF_TrigStatUncertainty__1down, "LeptonTrigWeight_MUON_EFF_TrigStatUncertainty__1down/F");
	output_tree->Branch("trigWeight_MUON_EFF_TrigStatUncertainty__1up", &LeptonTrigWeight_MUON_EFF_TrigStatUncertainty__1up, "LeptonTrigWeight_MUON_EFF_TrigStatUncertainty__1up/F");
	output_tree->Branch("trigWeight_MUON_EFF_TrigSystUncertainty__1down", &LeptonTrigWeight_MUON_EFF_TrigSystUncertainty__1down, "LeptonTrigWeight_MUON_EFF_TrigSystUncertainty__1down/F");
	output_tree->Branch("trigWeight_MUON_EFF_TrigSystUncertainty__1up", &LeptonTrigWeight_MUON_EFF_TrigSystUncertainty__1up, "LeptonTrigWeight_MUON_EFF_TrigSystUncertainty__1up/F");
	output_tree->Branch("jvtWeight_JET_JvtEfficiency__1down", &JVTWeight_JET_JvtEfficiency__1down, "JVTWeight_JET_JvtEfficiency__1down/F");
	output_tree->Branch("jvtWeight_JET_JvtEfficiency__1up", &JVTWeight_JET_JvtEfficiency__1up, "JVTWeight_JET_JvtEfficiency__1up/F");
	output_tree->Branch("jvtWeight_JET_fJvtEfficiency__1down", &JVTWeight_JET_fJvtEfficiency__1down, "JVTWeight_JET_fJvtEfficiency__1down/F");
	output_tree->Branch("jvtWeight_JET_fJvtEfficiency__1up", &JVTWeight_JET_fJvtEfficiency__1up, "JVTWeight_JET_fJvtEfficiency__1up/F");
	output_tree->Branch("bTagWeight_FT_EFF_B_systematics__1down", &BTagWeight_FT_EFF_B_systematics__1down, "BTagWeight_FT_EFF_B_systematics__1down/F");
	output_tree->Branch("bTagWeight_FT_EFF_B_systematics__1up", &BTagWeight_FT_EFF_B_systematics__1up, "BTagWeight_FT_EFF_B_systematics__1up/F");
	output_tree->Branch("bTagWeight_FT_EFF_C_systematics__1down", &BTagWeight_FT_EFF_C_systematics__1down, "BTagWeight_FT_EFF_C_systematics__1down/F");
	output_tree->Branch("bTagWeight_FT_EFF_C_systematics__1up", &BTagWeight_FT_EFF_C_systematics__1up, "BTagWeight_FT_EFF_C_systematics__1up/F");
	output_tree->Branch("bTagWeight_FT_EFF_Light_systematics__1down", &BTagWeight_FT_EFF_Light_systematics__1down, "BTagWeight_FT_EFF_Light_systematics__1down/F");
	output_tree->Branch("bTagWeight_FT_EFF_Light_systematics__1up", &BTagWeight_FT_EFF_Light_systematics__1up, "BTagWeight_FT_EFF_Light_systematics__1up/F");
	output_tree->Branch("bTagWeight_FT_EFF_extrapolation__1down", &BTagWeight_FT_EFF_extrapolation__1down, "BTagWeight_FT_EFF_extrapolation__1down/F");
	output_tree->Branch("bTagWeight_FT_EFF_extrapolation__1up", &BTagWeight_FT_EFF_extrapolation__1up, "BTagWeight_FT_EFF_extrapolation__1up/F");
	output_tree->Branch("bTagWeight_FT_EFF_extrapolation_from_charm__1down", &BTagWeight_FT_EFF_extrapolation_from_charm__1down, "BTagWeight_FT_EFF_extrapolation_from_charm__1down/F");
	output_tree->Branch("bTagWeight_FT_EFF_extrapolation_from_charm__1up", &BTagWeight_FT_EFF_extrapolation_from_charm__1up, "BTagWeight_FT_EFF_extrapolation_from_charm__1up/F");
		
	// Weights for theoretical systematics, need to distinguish between GJets/dijet and other samples
	if (tree_name.find("Gjets") != std::string::npos) {
	  output_tree->Branch("MUR1_MUF1_PDF13000", &MUR1_MUF1_PDF13000, "MUR1_MUF1_PDF13000/F");
	  output_tree->Branch("MUR1_MUF1_PDF25300", &MUR1_MUF1_PDF25300, "MUR1_MUF1_PDF25300/F");    
	  output_tree->Branch("MUR0p5_MUF0p5_PDF261000", &MUR0p5_MUF0p5_PDF261000, "MUR0p5_MUF0p5_PDF261000/F");
   	  output_tree->Branch("MUR0p5_MUF1_PDF261000", &MUR0p5_MUF1_PDF261000, "MUR0p5_MUF1_PDF261000/F");
   	  output_tree->Branch("MUR1_MUF0p5_PDF261000", &MUR1_MUF0p5_PDF261000, "MUR1_MUF0p5_PDF261000/F");
   	  output_tree->Branch("MUR1_MUF1_PDF269000", &MUR1_MUF1_PDF269000, "MUR1_MUF1_PDF269000/F");
   	  output_tree->Branch("MUR1_MUF1_PDF270000", &MUR1_MUF1_PDF270000, "MUR1_MUF1_PDF270000/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261000", &MUR1_MUF1_PDF261000, "MUR1_MUF1_PDF261000/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261001", &MUR1_MUF1_PDF261001, "MUR1_MUF1_PDF261001/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261002", &MUR1_MUF1_PDF261002, "MUR1_MUF1_PDF261002/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261003", &MUR1_MUF1_PDF261003, "MUR1_MUF1_PDF261003/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261004", &MUR1_MUF1_PDF261004, "MUR1_MUF1_PDF261004/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261005", &MUR1_MUF1_PDF261005, "MUR1_MUF1_PDF261005/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261006", &MUR1_MUF1_PDF261006, "MUR1_MUF1_PDF261006/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261007", &MUR1_MUF1_PDF261007, "MUR1_MUF1_PDF261007/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261008", &MUR1_MUF1_PDF261008, "MUR1_MUF1_PDF261008/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261009", &MUR1_MUF1_PDF261009, "MUR1_MUF1_PDF261009/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261010", &MUR1_MUF1_PDF261010, "MUR1_MUF1_PDF261010/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261011", &MUR1_MUF1_PDF261011, "MUR1_MUF1_PDF261011/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261012", &MUR1_MUF1_PDF261012, "MUR1_MUF1_PDF261012/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261013", &MUR1_MUF1_PDF261013, "MUR1_MUF1_PDF261013/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261014", &MUR1_MUF1_PDF261014, "MUR1_MUF1_PDF261014/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261015", &MUR1_MUF1_PDF261015, "MUR1_MUF1_PDF261015/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261016", &MUR1_MUF1_PDF261016, "MUR1_MUF1_PDF261016/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261017", &MUR1_MUF1_PDF261017, "MUR1_MUF1_PDF261017/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261018", &MUR1_MUF1_PDF261018, "MUR1_MUF1_PDF261018/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261019", &MUR1_MUF1_PDF261019, "MUR1_MUF1_PDF261019/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261020", &MUR1_MUF1_PDF261020, "MUR1_MUF1_PDF261020/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261021", &MUR1_MUF1_PDF261021, "MUR1_MUF1_PDF261021/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261022", &MUR1_MUF1_PDF261022, "MUR1_MUF1_PDF261022/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261023", &MUR1_MUF1_PDF261023, "MUR1_MUF1_PDF261023/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261024", &MUR1_MUF1_PDF261024, "MUR1_MUF1_PDF261024/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261025", &MUR1_MUF1_PDF261025, "MUR1_MUF1_PDF261025/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261026", &MUR1_MUF1_PDF261026, "MUR1_MUF1_PDF261026/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261027", &MUR1_MUF1_PDF261027, "MUR1_MUF1_PDF261027/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261028", &MUR1_MUF1_PDF261028, "MUR1_MUF1_PDF261028/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261029", &MUR1_MUF1_PDF261029, "MUR1_MUF1_PDF261029/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261030", &MUR1_MUF1_PDF261030, "MUR1_MUF1_PDF261030/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261031", &MUR1_MUF1_PDF261031, "MUR1_MUF1_PDF261031/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261032", &MUR1_MUF1_PDF261032, "MUR1_MUF1_PDF261032/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261033", &MUR1_MUF1_PDF261033, "MUR1_MUF1_PDF261033/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261034", &MUR1_MUF1_PDF261034, "MUR1_MUF1_PDF261034/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261035", &MUR1_MUF1_PDF261035, "MUR1_MUF1_PDF261035/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261036", &MUR1_MUF1_PDF261036, "MUR1_MUF1_PDF261036/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261037", &MUR1_MUF1_PDF261037, "MUR1_MUF1_PDF261037/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261038", &MUR1_MUF1_PDF261038, "MUR1_MUF1_PDF261038/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261039", &MUR1_MUF1_PDF261039, "MUR1_MUF1_PDF261039/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261040", &MUR1_MUF1_PDF261040, "MUR1_MUF1_PDF261040/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261041", &MUR1_MUF1_PDF261041, "MUR1_MUF1_PDF261041/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261042", &MUR1_MUF1_PDF261042, "MUR1_MUF1_PDF261042/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261043", &MUR1_MUF1_PDF261043, "MUR1_MUF1_PDF261043/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261044", &MUR1_MUF1_PDF261044, "MUR1_MUF1_PDF261044/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261045", &MUR1_MUF1_PDF261045, "MUR1_MUF1_PDF261045/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261046", &MUR1_MUF1_PDF261046, "MUR1_MUF1_PDF261046/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261047", &MUR1_MUF1_PDF261047, "MUR1_MUF1_PDF261047/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261048", &MUR1_MUF1_PDF261048, "MUR1_MUF1_PDF261048/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261049", &MUR1_MUF1_PDF261049, "MUR1_MUF1_PDF261049/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261050", &MUR1_MUF1_PDF261050, "MUR1_MUF1_PDF261050/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261051", &MUR1_MUF1_PDF261051, "MUR1_MUF1_PDF261051/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261052", &MUR1_MUF1_PDF261052, "MUR1_MUF1_PDF261052/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261053", &MUR1_MUF1_PDF261053, "MUR1_MUF1_PDF261053/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261054", &MUR1_MUF1_PDF261054, "MUR1_MUF1_PDF261054/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261055", &MUR1_MUF1_PDF261055, "MUR1_MUF1_PDF261055/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261056", &MUR1_MUF1_PDF261056, "MUR1_MUF1_PDF261056/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261057", &MUR1_MUF1_PDF261057, "MUR1_MUF1_PDF261057/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261058", &MUR1_MUF1_PDF261058, "MUR1_MUF1_PDF261058/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261059", &MUR1_MUF1_PDF261059, "MUR1_MUF1_PDF261059/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261060", &MUR1_MUF1_PDF261060, "MUR1_MUF1_PDF261060/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261061", &MUR1_MUF1_PDF261061, "MUR1_MUF1_PDF261061/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261062", &MUR1_MUF1_PDF261062, "MUR1_MUF1_PDF261062/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261063", &MUR1_MUF1_PDF261063, "MUR1_MUF1_PDF261063/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261064", &MUR1_MUF1_PDF261064, "MUR1_MUF1_PDF261064/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261065", &MUR1_MUF1_PDF261065, "MUR1_MUF1_PDF261065/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261066", &MUR1_MUF1_PDF261066, "MUR1_MUF1_PDF261066/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261067", &MUR1_MUF1_PDF261067, "MUR1_MUF1_PDF261067/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261068", &MUR1_MUF1_PDF261068, "MUR1_MUF1_PDF261068/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261069", &MUR1_MUF1_PDF261069, "MUR1_MUF1_PDF261069/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261070", &MUR1_MUF1_PDF261070, "MUR1_MUF1_PDF261070/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261071", &MUR1_MUF1_PDF261071, "MUR1_MUF1_PDF261071/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261072", &MUR1_MUF1_PDF261072, "MUR1_MUF1_PDF261072/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261073", &MUR1_MUF1_PDF261073, "MUR1_MUF1_PDF261073/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261074", &MUR1_MUF1_PDF261074, "MUR1_MUF1_PDF261074/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261075", &MUR1_MUF1_PDF261075, "MUR1_MUF1_PDF261075/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261076", &MUR1_MUF1_PDF261076, "MUR1_MUF1_PDF261076/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261077", &MUR1_MUF1_PDF261077, "MUR1_MUF1_PDF261077/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261078", &MUR1_MUF1_PDF261078, "MUR1_MUF1_PDF261078/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261079", &MUR1_MUF1_PDF261079, "MUR1_MUF1_PDF261079/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261080", &MUR1_MUF1_PDF261080, "MUR1_MUF1_PDF261080/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261081", &MUR1_MUF1_PDF261081, "MUR1_MUF1_PDF261081/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261082", &MUR1_MUF1_PDF261082, "MUR1_MUF1_PDF261082/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261083", &MUR1_MUF1_PDF261083, "MUR1_MUF1_PDF261083/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261084", &MUR1_MUF1_PDF261084, "MUR1_MUF1_PDF261084/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261085", &MUR1_MUF1_PDF261085, "MUR1_MUF1_PDF261085/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261086", &MUR1_MUF1_PDF261086, "MUR1_MUF1_PDF261086/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261087", &MUR1_MUF1_PDF261087, "MUR1_MUF1_PDF261087/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261088", &MUR1_MUF1_PDF261088, "MUR1_MUF1_PDF261088/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261089", &MUR1_MUF1_PDF261089, "MUR1_MUF1_PDF261089/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261090", &MUR1_MUF1_PDF261090, "MUR1_MUF1_PDF261090/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261091", &MUR1_MUF1_PDF261091, "MUR1_MUF1_PDF261091/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261092", &MUR1_MUF1_PDF261092, "MUR1_MUF1_PDF261092/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261093", &MUR1_MUF1_PDF261093, "MUR1_MUF1_PDF261093/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261094", &MUR1_MUF1_PDF261094, "MUR1_MUF1_PDF261094/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261095", &MUR1_MUF1_PDF261095, "MUR1_MUF1_PDF261095/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261096", &MUR1_MUF1_PDF261096, "MUR1_MUF1_PDF261096/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261097", &MUR1_MUF1_PDF261097, "MUR1_MUF1_PDF261097/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261098", &MUR1_MUF1_PDF261098, "MUR1_MUF1_PDF261098/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261099", &MUR1_MUF1_PDF261099, "MUR1_MUF1_PDF261099/F");
   	  output_tree->Branch("MUR1_MUF1_PDF261100", &MUR1_MUF1_PDF261100, "MUR1_MUF1_PDF261100/F");
   	  output_tree->Branch("MUR1_MUF2_PDF261000", &MUR1_MUF2_PDF261000, "MUR1_MUF2_PDF261000/F");
   	  output_tree->Branch("MUR2_MUF1_PDF261000", &MUR2_MUF1_PDF261000, "MUR2_MUF1_PDF261000/F");
   	  output_tree->Branch("MUR2_MUF2_PDF261000", &MUR2_MUF2_PDF261000, "MUR2_MUF2_PDF261000/F");
	}
	else if (tree_name.find("dijet") != std::string::npos) { 
  	  output_tree->Branch("nominal", &nominal, "nominal/F"); 
  	  output_tree->Branch("hardHi", &hardHi, "hardHi/F"); 
   	  output_tree->Branch("hardLo", &hardLo, "hardLo/F");
   	  output_tree->Branch("isrmuRfac0p5_fsrmuRfac0p5", &isrmuRfac0p5_fsrmuRfac0p5, "isrmuRfac0p5_fsrmuRfac0p5/F");     
   	  output_tree->Branch("isrmuRfac0p5_fsrmuRfac1p0", &isrmuRfac0p5_fsrmuRfac1p0, "isrmuRfac0p5_fsrmuRfac1p0/F");  
   	  output_tree->Branch("isrmuRfac0p5_fsrmuRfac2p0", &isrmuRfac0p5_fsrmuRfac2p0, "isrmuRfac0p5_fsrmuRfac2p0/F");  
   	  output_tree->Branch("isrmuRfac0p625_fsrmuRfac1p0", &isrmuRfac0p625_fsrmuRfac1p0, "isrmuRfac0p625_fsrmuRfac1p0/F");  
   	  output_tree->Branch("isrmuRfac0p75_fsrmuRfac1p0", &isrmuRfac0p75_fsrmuRfac1p0, "isrmuRfac0p75_fsrmuRfac1p0/F");   
   	  output_tree->Branch("isrmuRfac0p875_fsrmuRfac1p0", &isrmuRfac0p875_fsrmuRfac1p0, "isrmuRfac0p875_fsrmuRfac1p0/F");  
   	  output_tree->Branch("isrmuRfac1p0_fsrmuRfac0p5", &isrmuRfac1p0_fsrmuRfac0p5, "isrmuRfac1p0_fsrmuRfac0p5/F");  
   	  output_tree->Branch("isrmuRfac1p0_fsrmuRfac0p625", &isrmuRfac1p0_fsrmuRfac0p625, "isrmuRfac1p0_fsrmuRfac0p625/F");  
   	  output_tree->Branch("isrmuRfac1p0_fsrmuRfac0p75", &isrmuRfac1p0_fsrmuRfac0p75, "isrmuRfac1p0_fsrmuRfac0p75/F");   
   	  output_tree->Branch("isrmuRfac1p0_fsrmuRfac0p875", &isrmuRfac1p0_fsrmuRfac0p875, "isrmuRfac1p0_fsrmuRfac0p875/F");  
   	  output_tree->Branch("isrmuRfac1p0_fsrmuRfac1p25", &isrmuRfac1p0_fsrmuRfac1p25, "isrmuRfac1p0_fsrmuRfac1p25/F");   
   	  output_tree->Branch("isrmuRfac1p0_fsrmuRfac1p5", &isrmuRfac1p0_fsrmuRfac1p5, "isrmuRfac1p0_fsrmuRfac1p5/F");  
   	  output_tree->Branch("isrmuRfac1p0_fsrmuRfac1p75", &isrmuRfac1p0_fsrmuRfac1p75, "isrmuRfac1p0_fsrmuRfac1p75/F");   
   	  output_tree->Branch("isrmuRfac1p0_fsrmuRfac2p0", &isrmuRfac1p0_fsrmuRfac2p0, "isrmuRfac1p0_fsrmuRfac2p0/F");  
   	  output_tree->Branch("isrmuRfac1p25_fsrmuRfac1p0", &isrmuRfac1p25_fsrmuRfac1p0, "isrmuRfac1p25_fsrmuRfac1p0/F"); 
   	  output_tree->Branch("isrmuRfac1p5_fsrmuRfac1p0", &isrmuRfac1p5_fsrmuRfac1p0, "isrmuRfac1p5_fsrmuRfac1p0/F");  
   	  output_tree->Branch("isrmuRfac1p75_fsrmuRfac1p0", &isrmuRfac1p75_fsrmuRfac1p0, "isrmuRfac1p75_fsrmuRfac1p0/F");   
   	  output_tree->Branch("isrmuRfac2p0_fsrmuRfac0p5", &isrmuRfac2p0_fsrmuRfac0p5, "isrmuRfac2p0_fsrmuRfac0p5/F");  
   	  output_tree->Branch("isrmuRfac2p0_fsrmuRfac1p0", &isrmuRfac2p0_fsrmuRfac1p0, "isrmuRfac2p0_fsrmuRfac1p0/F");  
   	  output_tree->Branch("isrmuRfac2p0_fsrmuRfac2p0", &isrmuRfac2p0_fsrmuRfac2p0, "isrmuRfac2p0_fsrmuRfac2p0/F");  
	}
	else {  
      output_tree->Branch("MUR1_MUF1_PDF303200_ASSEW", &MUR1_MUF1_PDF303200_ASSEW, "MUR1_MUF1_PDF303200_ASSEW/F");
      //output_tree->Branch("MUR1_MUF1_PDF303200_ASSEWLO1", &MUR1_MUF1_PDF303200_ASSEWLO1, "MUR1_MUF1_PDF303200_ASSEWLO1/F");
      //output_tree->Branch("MUR1_MUF1_PDF303200_ASSEWLO1LO2", &MUR1_MUF1_PDF303200_ASSEWLO1LO2, "MUR1_MUF1_PDF303200_ASSEWLO1LO2/F");
      //output_tree->Branch("MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3", &MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3, "MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3/F");
      output_tree->Branch("MUR1_MUF1_PDF303200_EXPASSEW", &MUR1_MUF1_PDF303200_EXPASSEW, "MUR1_MUF1_PDF303200_EXPASSEW/F");
      //output_tree->Branch("MUR1_MUF1_PDF303200_EXPASSEWLO1", &MUR1_MUF1_PDF303200_EXPASSEWLO1, "MUR1_MUF1_PDF303200_EXPASSEWLO1/F");
      //output_tree->Branch("MUR1_MUF1_PDF303200_EXPASSEWLO1LO2", &MUR1_MUF1_PDF303200_EXPASSEWLO1LO2, "MUR1_MUF1_PDF303200_EXPASSEWLO1LO2/F");
      //output_tree->Branch("MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3", &MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3, "MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3/F");
      output_tree->Branch("MUR1_MUF1_PDF303200_MULTIASSEW", &MUR1_MUF1_PDF303200_MULTIASSEW, "MUR1_MUF1_PDF303200_MULTIASSEW/F");
      //output_tree->Branch("MUR1_MUF1_PDF303200_MULTIASSEWLO1", &MUR1_MUF1_PDF303200_MULTIASSEWLO1, "MUR1_MUF1_PDF303200_MULTIASSEWLO1/F");
      //output_tree->Branch("MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2", &MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2, "MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2/F");
      //output_tree->Branch("MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3", &MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3, "MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3/F");
	  output_tree->Branch("MUR0p5_MUF0p5_PDF303200_PSMUR0p5_PSMUF0p5", &MUR0p5_MUF0p5_PDF303200_PSMUR0p5_PSMUF0p5, "MUR0p5_MUF0p5_PDF303200_PSMUR0p5_PSMUF0p5/F");
	  output_tree->Branch("MUR0p5_MUF1_PDF303200_PSMUR0p5_PSMUF1", &MUR0p5_MUF1_PDF303200_PSMUR0p5_PSMUF1, "MUR0p5_MUF1_PDF303200_PSMUR0p5_PSMUF1/F");
	  output_tree->Branch("MUR1_MUF0p5_PDF303200_PSMUR1_PSMUF0p5", &MUR1_MUF0p5_PDF303200_PSMUR1_PSMUF0p5, "MUR1_MUF0p5_PDF303200_PSMUR1_PSMUF0p5/F");
	  output_tree->Branch("MUR1_MUF1_PDF14068", &MUR1_MUF1_PDF14068, "MUR1_MUF1_PDF14068/F");
	  output_tree->Branch("MUR1_MUF1_PDF269000", &MUR1_MUF1_PDF269000, "MUR1_MUF1_PDF269000/F");
	  output_tree->Branch("MUR1_MUF1_PDF270000", &MUR1_MUF1_PDF270000, "MUR1_MUF1_PDF270000/F");
	  output_tree->Branch("MUR1_MUF1_PDF27400", &MUR1_MUF1_PDF27400, "MUR1_MUF1_PDF27400/F");
	  output_tree->Branch("MUR1_MUF1_PDF303200", &MUR1_MUF1_PDF303200, "MUR1_MUF1_PDF303200/F");
	  output_tree->Branch("MUR1_MUF1_PDF303201", &MUR1_MUF1_PDF303201, "MUR1_MUF1_PDF303201/F");
	  output_tree->Branch("MUR1_MUF1_PDF303202", &MUR1_MUF1_PDF303202, "MUR1_MUF1_PDF303202/F");
	  output_tree->Branch("MUR1_MUF1_PDF303203", &MUR1_MUF1_PDF303203, "MUR1_MUF1_PDF303203/F");
	  output_tree->Branch("MUR1_MUF1_PDF303204", &MUR1_MUF1_PDF303204, "MUR1_MUF1_PDF303204/F");
	  output_tree->Branch("MUR1_MUF1_PDF303205", &MUR1_MUF1_PDF303205, "MUR1_MUF1_PDF303205/F");
	  output_tree->Branch("MUR1_MUF1_PDF303206", &MUR1_MUF1_PDF303206, "MUR1_MUF1_PDF303206/F");
	  output_tree->Branch("MUR1_MUF1_PDF303207", &MUR1_MUF1_PDF303207, "MUR1_MUF1_PDF303207/F");
	  output_tree->Branch("MUR1_MUF1_PDF303208", &MUR1_MUF1_PDF303208, "MUR1_MUF1_PDF303208/F");
	  output_tree->Branch("MUR1_MUF1_PDF303209", &MUR1_MUF1_PDF303209, "MUR1_MUF1_PDF303209/F");
	  output_tree->Branch("MUR1_MUF1_PDF303210", &MUR1_MUF1_PDF303210, "MUR1_MUF1_PDF303210/F");
	  output_tree->Branch("MUR1_MUF1_PDF303211", &MUR1_MUF1_PDF303211, "MUR1_MUF1_PDF303211/F");
	  output_tree->Branch("MUR1_MUF1_PDF303212", &MUR1_MUF1_PDF303212, "MUR1_MUF1_PDF303212/F");
	  output_tree->Branch("MUR1_MUF1_PDF303213", &MUR1_MUF1_PDF303213, "MUR1_MUF1_PDF303213/F");
	  output_tree->Branch("MUR1_MUF1_PDF303214", &MUR1_MUF1_PDF303214, "MUR1_MUF1_PDF303214/F");
	  output_tree->Branch("MUR1_MUF1_PDF303215", &MUR1_MUF1_PDF303215, "MUR1_MUF1_PDF303215/F");
	  output_tree->Branch("MUR1_MUF1_PDF303216", &MUR1_MUF1_PDF303216, "MUR1_MUF1_PDF303216/F");
	  output_tree->Branch("MUR1_MUF1_PDF303217", &MUR1_MUF1_PDF303217, "MUR1_MUF1_PDF303217/F");
	  output_tree->Branch("MUR1_MUF1_PDF303218", &MUR1_MUF1_PDF303218, "MUR1_MUF1_PDF303218/F");
	  output_tree->Branch("MUR1_MUF1_PDF303219", &MUR1_MUF1_PDF303219, "MUR1_MUF1_PDF303219/F");
	  output_tree->Branch("MUR1_MUF1_PDF303220", &MUR1_MUF1_PDF303220, "MUR1_MUF1_PDF303220/F");
	  output_tree->Branch("MUR1_MUF1_PDF303221", &MUR1_MUF1_PDF303221, "MUR1_MUF1_PDF303221/F");
	  output_tree->Branch("MUR1_MUF1_PDF303222", &MUR1_MUF1_PDF303222, "MUR1_MUF1_PDF303222/F");
	  output_tree->Branch("MUR1_MUF1_PDF303223", &MUR1_MUF1_PDF303223, "MUR1_MUF1_PDF303223/F");
	  output_tree->Branch("MUR1_MUF1_PDF303224", &MUR1_MUF1_PDF303224, "MUR1_MUF1_PDF303224/F");
	  output_tree->Branch("MUR1_MUF1_PDF303225", &MUR1_MUF1_PDF303225, "MUR1_MUF1_PDF303225/F");
	  output_tree->Branch("MUR1_MUF1_PDF303226", &MUR1_MUF1_PDF303226, "MUR1_MUF1_PDF303226/F");
	  output_tree->Branch("MUR1_MUF1_PDF303227", &MUR1_MUF1_PDF303227, "MUR1_MUF1_PDF303227/F");
	  output_tree->Branch("MUR1_MUF1_PDF303228", &MUR1_MUF1_PDF303228, "MUR1_MUF1_PDF303228/F");
	  output_tree->Branch("MUR1_MUF1_PDF303229", &MUR1_MUF1_PDF303229, "MUR1_MUF1_PDF303229/F");
	  output_tree->Branch("MUR1_MUF1_PDF303230", &MUR1_MUF1_PDF303230, "MUR1_MUF1_PDF303230/F");
	  output_tree->Branch("MUR1_MUF1_PDF303231", &MUR1_MUF1_PDF303231, "MUR1_MUF1_PDF303231/F");
	  output_tree->Branch("MUR1_MUF1_PDF303232", &MUR1_MUF1_PDF303232, "MUR1_MUF1_PDF303232/F");
	  output_tree->Branch("MUR1_MUF1_PDF303233", &MUR1_MUF1_PDF303233, "MUR1_MUF1_PDF303233/F");
	  output_tree->Branch("MUR1_MUF1_PDF303234", &MUR1_MUF1_PDF303234, "MUR1_MUF1_PDF303234/F");
	  output_tree->Branch("MUR1_MUF1_PDF303235", &MUR1_MUF1_PDF303235, "MUR1_MUF1_PDF303235/F");
	  output_tree->Branch("MUR1_MUF1_PDF303236", &MUR1_MUF1_PDF303236, "MUR1_MUF1_PDF303236/F");
	  output_tree->Branch("MUR1_MUF1_PDF303237", &MUR1_MUF1_PDF303237, "MUR1_MUF1_PDF303237/F");
	  output_tree->Branch("MUR1_MUF1_PDF303238", &MUR1_MUF1_PDF303238, "MUR1_MUF1_PDF303238/F");
	  output_tree->Branch("MUR1_MUF1_PDF303239", &MUR1_MUF1_PDF303239, "MUR1_MUF1_PDF303239/F");
	  output_tree->Branch("MUR1_MUF1_PDF303240", &MUR1_MUF1_PDF303240, "MUR1_MUF1_PDF303240/F");
	  output_tree->Branch("MUR1_MUF1_PDF303241", &MUR1_MUF1_PDF303241, "MUR1_MUF1_PDF303241/F");
	  output_tree->Branch("MUR1_MUF1_PDF303242", &MUR1_MUF1_PDF303242, "MUR1_MUF1_PDF303242/F");
	  output_tree->Branch("MUR1_MUF1_PDF303243", &MUR1_MUF1_PDF303243, "MUR1_MUF1_PDF303243/F");
	  output_tree->Branch("MUR1_MUF1_PDF303244", &MUR1_MUF1_PDF303244, "MUR1_MUF1_PDF303244/F");
	  output_tree->Branch("MUR1_MUF1_PDF303245", &MUR1_MUF1_PDF303245, "MUR1_MUF1_PDF303245/F");
	  output_tree->Branch("MUR1_MUF1_PDF303246", &MUR1_MUF1_PDF303246, "MUR1_MUF1_PDF303246/F");
	  output_tree->Branch("MUR1_MUF1_PDF303247", &MUR1_MUF1_PDF303247, "MUR1_MUF1_PDF303247/F");
	  output_tree->Branch("MUR1_MUF1_PDF303248", &MUR1_MUF1_PDF303248, "MUR1_MUF1_PDF303248/F");
	  output_tree->Branch("MUR1_MUF1_PDF303249", &MUR1_MUF1_PDF303249, "MUR1_MUF1_PDF303249/F");
	  output_tree->Branch("MUR1_MUF1_PDF303250", &MUR1_MUF1_PDF303250, "MUR1_MUF1_PDF303250/F");
	  output_tree->Branch("MUR1_MUF1_PDF303251", &MUR1_MUF1_PDF303251, "MUR1_MUF1_PDF303251/F");
	  output_tree->Branch("MUR1_MUF1_PDF303252", &MUR1_MUF1_PDF303252, "MUR1_MUF1_PDF303252/F");
	  output_tree->Branch("MUR1_MUF1_PDF303253", &MUR1_MUF1_PDF303253, "MUR1_MUF1_PDF303253/F");
	  output_tree->Branch("MUR1_MUF1_PDF303254", &MUR1_MUF1_PDF303254, "MUR1_MUF1_PDF303254/F");
	  output_tree->Branch("MUR1_MUF1_PDF303255", &MUR1_MUF1_PDF303255, "MUR1_MUF1_PDF303255/F");
	  output_tree->Branch("MUR1_MUF1_PDF303256", &MUR1_MUF1_PDF303256, "MUR1_MUF1_PDF303256/F");
	  output_tree->Branch("MUR1_MUF1_PDF303257", &MUR1_MUF1_PDF303257, "MUR1_MUF1_PDF303257/F");
	  output_tree->Branch("MUR1_MUF1_PDF303258", &MUR1_MUF1_PDF303258, "MUR1_MUF1_PDF303258/F");
	  output_tree->Branch("MUR1_MUF1_PDF303259", &MUR1_MUF1_PDF303259, "MUR1_MUF1_PDF303259/F");
	  output_tree->Branch("MUR1_MUF1_PDF303260", &MUR1_MUF1_PDF303260, "MUR1_MUF1_PDF303260/F");
	  output_tree->Branch("MUR1_MUF1_PDF303261", &MUR1_MUF1_PDF303261, "MUR1_MUF1_PDF303261/F");
	  output_tree->Branch("MUR1_MUF1_PDF303262", &MUR1_MUF1_PDF303262, "MUR1_MUF1_PDF303262/F");
	  output_tree->Branch("MUR1_MUF1_PDF303263", &MUR1_MUF1_PDF303263, "MUR1_MUF1_PDF303263/F");
	  output_tree->Branch("MUR1_MUF1_PDF303264", &MUR1_MUF1_PDF303264, "MUR1_MUF1_PDF303264/F");
	  output_tree->Branch("MUR1_MUF1_PDF303265", &MUR1_MUF1_PDF303265, "MUR1_MUF1_PDF303265/F");
	  output_tree->Branch("MUR1_MUF1_PDF303266", &MUR1_MUF1_PDF303266, "MUR1_MUF1_PDF303266/F");
	  output_tree->Branch("MUR1_MUF1_PDF303267", &MUR1_MUF1_PDF303267, "MUR1_MUF1_PDF303267/F");
	  output_tree->Branch("MUR1_MUF1_PDF303268", &MUR1_MUF1_PDF303268, "MUR1_MUF1_PDF303268/F");
	  output_tree->Branch("MUR1_MUF1_PDF303269", &MUR1_MUF1_PDF303269, "MUR1_MUF1_PDF303269/F");
	  output_tree->Branch("MUR1_MUF1_PDF303270", &MUR1_MUF1_PDF303270, "MUR1_MUF1_PDF303270/F");
	  output_tree->Branch("MUR1_MUF1_PDF303271", &MUR1_MUF1_PDF303271, "MUR1_MUF1_PDF303271/F");
	  output_tree->Branch("MUR1_MUF1_PDF303272", &MUR1_MUF1_PDF303272, "MUR1_MUF1_PDF303272/F");
	  output_tree->Branch("MUR1_MUF1_PDF303273", &MUR1_MUF1_PDF303273, "MUR1_MUF1_PDF303273/F");
	  output_tree->Branch("MUR1_MUF1_PDF303274", &MUR1_MUF1_PDF303274, "MUR1_MUF1_PDF303274/F");
	  output_tree->Branch("MUR1_MUF1_PDF303275", &MUR1_MUF1_PDF303275, "MUR1_MUF1_PDF303275/F");
	  output_tree->Branch("MUR1_MUF1_PDF303276", &MUR1_MUF1_PDF303276, "MUR1_MUF1_PDF303276/F");
	  output_tree->Branch("MUR1_MUF1_PDF303277", &MUR1_MUF1_PDF303277, "MUR1_MUF1_PDF303277/F");
	  output_tree->Branch("MUR1_MUF1_PDF303278", &MUR1_MUF1_PDF303278, "MUR1_MUF1_PDF303278/F");
	  output_tree->Branch("MUR1_MUF1_PDF303279", &MUR1_MUF1_PDF303279, "MUR1_MUF1_PDF303279/F");
	  output_tree->Branch("MUR1_MUF1_PDF303280", &MUR1_MUF1_PDF303280, "MUR1_MUF1_PDF303280/F");
	  output_tree->Branch("MUR1_MUF1_PDF303281", &MUR1_MUF1_PDF303281, "MUR1_MUF1_PDF303281/F");
	  output_tree->Branch("MUR1_MUF1_PDF303282", &MUR1_MUF1_PDF303282, "MUR1_MUF1_PDF303282/F");
	  output_tree->Branch("MUR1_MUF1_PDF303283", &MUR1_MUF1_PDF303283, "MUR1_MUF1_PDF303283/F");
	  output_tree->Branch("MUR1_MUF1_PDF303284", &MUR1_MUF1_PDF303284, "MUR1_MUF1_PDF303284/F");
	  output_tree->Branch("MUR1_MUF1_PDF303285", &MUR1_MUF1_PDF303285, "MUR1_MUF1_PDF303285/F");
	  output_tree->Branch("MUR1_MUF1_PDF303286", &MUR1_MUF1_PDF303286, "MUR1_MUF1_PDF303286/F");
	  output_tree->Branch("MUR1_MUF1_PDF303287", &MUR1_MUF1_PDF303287, "MUR1_MUF1_PDF303287/F");
	  output_tree->Branch("MUR1_MUF1_PDF303288", &MUR1_MUF1_PDF303288, "MUR1_MUF1_PDF303288/F");
	  output_tree->Branch("MUR1_MUF1_PDF303289", &MUR1_MUF1_PDF303289, "MUR1_MUF1_PDF303289/F");
	  output_tree->Branch("MUR1_MUF1_PDF303290", &MUR1_MUF1_PDF303290, "MUR1_MUF1_PDF303290/F");
	  output_tree->Branch("MUR1_MUF1_PDF303291", &MUR1_MUF1_PDF303291, "MUR1_MUF1_PDF303291/F");
	  output_tree->Branch("MUR1_MUF1_PDF303292", &MUR1_MUF1_PDF303292, "MUR1_MUF1_PDF303292/F");
	  output_tree->Branch("MUR1_MUF1_PDF303293", &MUR1_MUF1_PDF303293, "MUR1_MUF1_PDF303293/F");
	  output_tree->Branch("MUR1_MUF1_PDF303294", &MUR1_MUF1_PDF303294, "MUR1_MUF1_PDF303294/F");
	  output_tree->Branch("MUR1_MUF1_PDF303295", &MUR1_MUF1_PDF303295, "MUR1_MUF1_PDF303295/F");
	  output_tree->Branch("MUR1_MUF1_PDF303296", &MUR1_MUF1_PDF303296, "MUR1_MUF1_PDF303296/F");
	  output_tree->Branch("MUR1_MUF1_PDF303297", &MUR1_MUF1_PDF303297, "MUR1_MUF1_PDF303297/F");
	  output_tree->Branch("MUR1_MUF1_PDF303298", &MUR1_MUF1_PDF303298, "MUR1_MUF1_PDF303298/F");
	  output_tree->Branch("MUR1_MUF1_PDF303299", &MUR1_MUF1_PDF303299, "MUR1_MUF1_PDF303299/F");
	  output_tree->Branch("MUR1_MUF1_PDF303300", &MUR1_MUF1_PDF303300, "MUR1_MUF1_PDF303300/F");
	  output_tree->Branch("MUR1_MUF1_PDF304400", &MUR1_MUF1_PDF304400, "MUR1_MUF1_PDF304400/F");
	  /*output_tree->Branch("MUR1_MUF1_PDF91400", &MUR1_MUF1_PDF91400, "MUR1_MUF1_PDF91400/F");
	  output_tree->Branch("MUR1_MUF1_PDF91401", &MUR1_MUF1_PDF91401, "MUR1_MUF1_PDF91401/F");
	  output_tree->Branch("MUR1_MUF1_PDF91402", &MUR1_MUF1_PDF91402, "MUR1_MUF1_PDF91402/F");
	  output_tree->Branch("MUR1_MUF1_PDF91403", &MUR1_MUF1_PDF91403, "MUR1_MUF1_PDF91403/F");
	  output_tree->Branch("MUR1_MUF1_PDF91404", &MUR1_MUF1_PDF91404, "MUR1_MUF1_PDF91404/F");
	  output_tree->Branch("MUR1_MUF1_PDF91405", &MUR1_MUF1_PDF91405, "MUR1_MUF1_PDF91405/F");
	  output_tree->Branch("MUR1_MUF1_PDF91406", &MUR1_MUF1_PDF91406, "MUR1_MUF1_PDF91406/F");
	  output_tree->Branch("MUR1_MUF1_PDF91407", &MUR1_MUF1_PDF91407, "MUR1_MUF1_PDF91407/F");
	  output_tree->Branch("MUR1_MUF1_PDF91408", &MUR1_MUF1_PDF91408, "MUR1_MUF1_PDF91408/F");
	  output_tree->Branch("MUR1_MUF1_PDF91409", &MUR1_MUF1_PDF91409, "MUR1_MUF1_PDF91409/F");
	  output_tree->Branch("MUR1_MUF1_PDF91410", &MUR1_MUF1_PDF91410, "MUR1_MUF1_PDF91410/F");
	  output_tree->Branch("MUR1_MUF1_PDF91411", &MUR1_MUF1_PDF91411, "MUR1_MUF1_PDF91411/F");
	  output_tree->Branch("MUR1_MUF1_PDF91412", &MUR1_MUF1_PDF91412, "MUR1_MUF1_PDF91412/F");
	  output_tree->Branch("MUR1_MUF1_PDF91413", &MUR1_MUF1_PDF91413, "MUR1_MUF1_PDF91413/F");
	  output_tree->Branch("MUR1_MUF1_PDF91414", &MUR1_MUF1_PDF91414, "MUR1_MUF1_PDF91414/F");
	  output_tree->Branch("MUR1_MUF1_PDF91415", &MUR1_MUF1_PDF91415, "MUR1_MUF1_PDF91415/F");
	  output_tree->Branch("MUR1_MUF1_PDF91416", &MUR1_MUF1_PDF91416, "MUR1_MUF1_PDF91416/F");
	  output_tree->Branch("MUR1_MUF1_PDF91417", &MUR1_MUF1_PDF91417, "MUR1_MUF1_PDF91417/F");
	  output_tree->Branch("MUR1_MUF1_PDF91418", &MUR1_MUF1_PDF91418, "MUR1_MUF1_PDF91418/F");
	  output_tree->Branch("MUR1_MUF1_PDF91419", &MUR1_MUF1_PDF91419, "MUR1_MUF1_PDF91419/F");
	  output_tree->Branch("MUR1_MUF1_PDF91420", &MUR1_MUF1_PDF91420, "MUR1_MUF1_PDF91420/F");
	  output_tree->Branch("MUR1_MUF1_PDF91421", &MUR1_MUF1_PDF91421, "MUR1_MUF1_PDF91421/F");
	  output_tree->Branch("MUR1_MUF1_PDF91422", &MUR1_MUF1_PDF91422, "MUR1_MUF1_PDF91422/F");
	  output_tree->Branch("MUR1_MUF1_PDF91423", &MUR1_MUF1_PDF91423, "MUR1_MUF1_PDF91423/F");
	  output_tree->Branch("MUR1_MUF1_PDF91424", &MUR1_MUF1_PDF91424, "MUR1_MUF1_PDF91424/F");
	  output_tree->Branch("MUR1_MUF1_PDF91425", &MUR1_MUF1_PDF91425, "MUR1_MUF1_PDF91425/F");
	  output_tree->Branch("MUR1_MUF1_PDF91426", &MUR1_MUF1_PDF91426, "MUR1_MUF1_PDF91426/F");
	  output_tree->Branch("MUR1_MUF1_PDF91427", &MUR1_MUF1_PDF91427, "MUR1_MUF1_PDF91427/F");
	  output_tree->Branch("MUR1_MUF1_PDF91428", &MUR1_MUF1_PDF91428, "MUR1_MUF1_PDF91428/F");
	  output_tree->Branch("MUR1_MUF1_PDF91429", &MUR1_MUF1_PDF91429, "MUR1_MUF1_PDF91429/F");
	  output_tree->Branch("MUR1_MUF1_PDF91430", &MUR1_MUF1_PDF91430, "MUR1_MUF1_PDF91430/F");
	  output_tree->Branch("MUR1_MUF1_PDF91431", &MUR1_MUF1_PDF91431, "MUR1_MUF1_PDF91431/F");
	  output_tree->Branch("MUR1_MUF1_PDF91432", &MUR1_MUF1_PDF91432, "MUR1_MUF1_PDF91432/F");*/
	  output_tree->Branch("MUR1_MUF2_PDF303200_PSMUR1_PSMUF2", &MUR1_MUF2_PDF303200_PSMUR1_PSMUF2, "MUR1_MUF2_PDF303200_PSMUR1_PSMUF2/F");
	  output_tree->Branch("MUR2_MUF1_PDF303200_PSMUR2_PSMUF1", &MUR2_MUF1_PDF303200_PSMUR2_PSMUF1, "MUR2_MUF1_PDF303200_PSMUR2_PSMUF1/F");
	  output_tree->Branch("MUR2_MUF2_PDF303200_PSMUR2_PSMUF2", &MUR2_MUF2_PDF303200_PSMUR2_PSMUF2, "MUR2_MUF2_PDF303200_PSMUR2_PSMUF2/F");
	}
   }

   Notify();

}

Bool_t analysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void analysis::Show(TTree* tree, Long64_t entry)
{
   // Print contents of entry.
   // If entry is not specified, print current entry
   if (!tree) return;
   tree->Show(entry);
}

Int_t analysis::Cut(Long64_t entry)
{
   // This function may be called from Loop.
   // returns  1 if entry is accepted.
   // returns -1 otherwise.
   return 1;
}

double get_dphi(float a,float b){
    double dphi = a-b;
    if(dphi<-1*M_PI) dphi= 2*M_PI+dphi;
    if(dphi>M_PI) dphi=2*M_PI-dphi;
    return dphi;
}

void analysis::SaveLHE3Weights(std::vector<float> *LHE3Weights, std::vector<TString> *LHE3WeightNames, std::string tree_name)
{
    // This is just a stupid and slow function to fill nominal output tree branches with the correct LHE3Weights.    
    // Atm, for the sake of simplicity we distinguish between dijets/Gjets samples and all the other SM processes.
    if (tree_name.find("Gjets") != std::string::npos) {
      for (unsigned int i = 0; i < LHE3Weights->size(); i++) {
		if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF13000") MUR1_MUF1_PDF13000 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF25300") MUR1_MUF1_PDF25300 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR0p5_MUF0p5_PDF261000") MUR0p5_MUF0p5_PDF261000 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR0p5_MUF1_PDF261000") MUR0p5_MUF1_PDF261000 = (*LHE3Weights)[i]; 
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF0p5_PDF261000") MUR1_MUF0p5_PDF261000 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF269000") MUR1_MUF1_PDF269000 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF270000") MUR1_MUF1_PDF270000 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261000") MUR1_MUF1_PDF261000 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261001") MUR1_MUF1_PDF261001 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261002") MUR1_MUF1_PDF261002 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261003") MUR1_MUF1_PDF261003 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261004") MUR1_MUF1_PDF261004 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261005") MUR1_MUF1_PDF261005 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261006") MUR1_MUF1_PDF261006 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261007") MUR1_MUF1_PDF261007 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261008") MUR1_MUF1_PDF261008 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261009") MUR1_MUF1_PDF261009 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261010") MUR1_MUF1_PDF261010 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261011") MUR1_MUF1_PDF261011 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261012") MUR1_MUF1_PDF261012 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261013") MUR1_MUF1_PDF261013 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261014") MUR1_MUF1_PDF261014 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261015") MUR1_MUF1_PDF261015 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261016") MUR1_MUF1_PDF261016 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261017") MUR1_MUF1_PDF261017 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261018") MUR1_MUF1_PDF261018 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261019") MUR1_MUF1_PDF261019 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261020") MUR1_MUF1_PDF261020 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261021") MUR1_MUF1_PDF261021 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261022") MUR1_MUF1_PDF261022 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261023") MUR1_MUF1_PDF261023 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261024") MUR1_MUF1_PDF261024 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261025") MUR1_MUF1_PDF261025 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261026") MUR1_MUF1_PDF261026 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261027") MUR1_MUF1_PDF261027 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261028") MUR1_MUF1_PDF261028 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261029") MUR1_MUF1_PDF261029 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261030") MUR1_MUF1_PDF261030 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261031") MUR1_MUF1_PDF261031 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261032") MUR1_MUF1_PDF261032 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261033") MUR1_MUF1_PDF261033 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261034") MUR1_MUF1_PDF261034 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261035") MUR1_MUF1_PDF261035 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261036") MUR1_MUF1_PDF261036 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261037") MUR1_MUF1_PDF261037 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261038") MUR1_MUF1_PDF261038 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261039") MUR1_MUF1_PDF261039 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261040") MUR1_MUF1_PDF261040 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261041") MUR1_MUF1_PDF261041 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261042") MUR1_MUF1_PDF261042 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261043") MUR1_MUF1_PDF261043 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261044") MUR1_MUF1_PDF261044 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261045") MUR1_MUF1_PDF261045 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261046") MUR1_MUF1_PDF261046 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261047") MUR1_MUF1_PDF261047 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261048") MUR1_MUF1_PDF261048 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261049") MUR1_MUF1_PDF261049 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261050") MUR1_MUF1_PDF261050 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261051") MUR1_MUF1_PDF261051 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261052") MUR1_MUF1_PDF261052 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261053") MUR1_MUF1_PDF261053 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261054") MUR1_MUF1_PDF261054 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261055") MUR1_MUF1_PDF261055 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261056") MUR1_MUF1_PDF261056 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261057") MUR1_MUF1_PDF261057 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261058") MUR1_MUF1_PDF261058 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261059") MUR1_MUF1_PDF261059 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261060") MUR1_MUF1_PDF261060 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261061") MUR1_MUF1_PDF261061 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261062") MUR1_MUF1_PDF261062 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261063") MUR1_MUF1_PDF261063 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261064") MUR1_MUF1_PDF261064 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261065") MUR1_MUF1_PDF261065 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261066") MUR1_MUF1_PDF261066 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261067") MUR1_MUF1_PDF261067 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261068") MUR1_MUF1_PDF261068 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261069") MUR1_MUF1_PDF261069 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261070") MUR1_MUF1_PDF261070 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261071") MUR1_MUF1_PDF261071 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261072") MUR1_MUF1_PDF261072 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261073") MUR1_MUF1_PDF261073 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261074") MUR1_MUF1_PDF261074 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261075") MUR1_MUF1_PDF261075 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261076") MUR1_MUF1_PDF261076 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261077") MUR1_MUF1_PDF261077 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261078") MUR1_MUF1_PDF261078 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261079") MUR1_MUF1_PDF261079 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261080") MUR1_MUF1_PDF261080 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261081") MUR1_MUF1_PDF261081 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261082") MUR1_MUF1_PDF261082 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261083") MUR1_MUF1_PDF261083 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261084") MUR1_MUF1_PDF261084 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261085") MUR1_MUF1_PDF261085 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261086") MUR1_MUF1_PDF261086 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261087") MUR1_MUF1_PDF261087 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261088") MUR1_MUF1_PDF261088 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261089") MUR1_MUF1_PDF261089 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261090") MUR1_MUF1_PDF261090 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261091") MUR1_MUF1_PDF261091 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261092") MUR1_MUF1_PDF261092 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261093") MUR1_MUF1_PDF261093 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261094") MUR1_MUF1_PDF261094 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261095") MUR1_MUF1_PDF261095 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261096") MUR1_MUF1_PDF261096 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261097") MUR1_MUF1_PDF261097 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261098") MUR1_MUF1_PDF261098 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261099") MUR1_MUF1_PDF261099 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF261100") MUR1_MUF1_PDF261100 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR1_MUF2_PDF261000") MUR1_MUF2_PDF261000 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR2_MUF1_PDF261000") MUR2_MUF1_PDF261000 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "MUR2_MUF2_PDF261000") MUR2_MUF2_PDF261000 = (*LHE3Weights)[i];
      }
    }
    else if (tree_name.find("dijet") != std::string::npos) {
      for (unsigned int i = 0; i < LHE3Weights->size(); i++) {
		if ((*LHE3WeightNames)[i] == "nominal") nominal = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "hardHi") hardHi = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "hardLo") hardLo = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac0p5_fsrmuRfac0p5") isrmuRfac0p5_fsrmuRfac0p5 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac0p5_fsrmuRfac1p0") isrmuRfac0p5_fsrmuRfac1p0 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac0p5_fsrmuRfac2p0") isrmuRfac0p5_fsrmuRfac2p0 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac0p625_fsrmuRfac1p0") isrmuRfac0p625_fsrmuRfac1p0 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac0p75_fsrmuRfac1p0") isrmuRfac0p75_fsrmuRfac1p0 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac0p875_fsrmuRfac1p0") isrmuRfac0p875_fsrmuRfac1p0 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac1p0_fsrmuRfac0p5") isrmuRfac1p0_fsrmuRfac0p5 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac1p0_fsrmuRfac0p625") isrmuRfac1p0_fsrmuRfac0p625 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac1p0_fsrmuRfac0p75") isrmuRfac1p0_fsrmuRfac0p75 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac1p0_fsrmuRfac0p875") isrmuRfac1p0_fsrmuRfac0p875 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac1p0_fsrmuRfac1p25") isrmuRfac1p0_fsrmuRfac1p25 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac1p0_fsrmuRfac1p5") isrmuRfac1p0_fsrmuRfac1p5 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac1p0_fsrmuRfac1p75") isrmuRfac1p0_fsrmuRfac1p75 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac1p0_fsrmuRfac2p0") isrmuRfac1p0_fsrmuRfac2p0 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac1p25_fsrmuRfac1p0") isrmuRfac1p25_fsrmuRfac1p0 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac1p5_fsrmuRfac1p0") isrmuRfac1p5_fsrmuRfac1p0 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac1p75_fsrmuRfac1p0") isrmuRfac1p75_fsrmuRfac1p0 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac2p0_fsrmuRfac0p5") isrmuRfac2p0_fsrmuRfac0p5 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac2p0_fsrmuRfac1p0") isrmuRfac2p0_fsrmuRfac1p0 = (*LHE3Weights)[i];
		else if ((*LHE3WeightNames)[i] == "isrmuRfac2p0_fsrmuRfac2p0") isrmuRfac2p0_fsrmuRfac2p0 = (*LHE3Weights)[i];
      }
    }
    else {
      for (unsigned int i = 0; i < LHE3Weights->size(); i++) {
		if ((*LHE3WeightNames)[i] == "MUR0p5_MUF0p5_PDF303200_PSMUR0p5_PSMUF0p5") MUR0p5_MUF0p5_PDF303200_PSMUR0p5_PSMUF0p5 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR0p5_MUF1_PDF303200_PSMUR0p5_PSMUF1") MUR0p5_MUF1_PDF303200_PSMUR0p5_PSMUF1 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF0p5_PDF303200_PSMUR1_PSMUF0p5") MUR1_MUF0p5_PDF303200_PSMUR1_PSMUF0p5 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF14068") MUR1_MUF1_PDF14068 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF269000") MUR1_MUF1_PDF269000 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF270000") MUR1_MUF1_PDF270000 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF27400") MUR1_MUF1_PDF27400 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200") MUR1_MUF1_PDF303200 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_ASSEW") MUR1_MUF1_PDF303200_ASSEW = (*LHE3Weights)[i];
        //else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_ASSEWLO1") MUR1_MUF1_PDF303200_ASSEWLO1 = (*LHE3Weights)[i];
        //else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_ASSEWLO1LO2") MUR1_MUF1_PDF303200_ASSEWLO1LO2 = (*LHE3Weights)[i];
        //else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3") MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_EXPASSEW") MUR1_MUF1_PDF303200_EXPASSEW = (*LHE3Weights)[i];
        //else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_EXPASSEWLO1") MUR1_MUF1_PDF303200_EXPASSEWLO1 = (*LHE3Weights)[i];
        //else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_EXPASSEWLO1LO2") MUR1_MUF1_PDF303200_EXPASSEWLO1LO2 = (*LHE3Weights)[i];
        //else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3") MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_MULTIASSEW") MUR1_MUF1_PDF303200_MULTIASSEW = (*LHE3Weights)[i];
        //else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_MULTIASSEWLO1") MUR1_MUF1_PDF303200_MULTIASSEWLO1 = (*LHE3Weights)[i];
        //else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2") MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2 = (*LHE3Weights)[i];
        //else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3") MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303201") MUR1_MUF1_PDF303201 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303202") MUR1_MUF1_PDF303202 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303203") MUR1_MUF1_PDF303203 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303204") MUR1_MUF1_PDF303204 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303205") MUR1_MUF1_PDF303205 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303206") MUR1_MUF1_PDF303206 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303207") MUR1_MUF1_PDF303207 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303208") MUR1_MUF1_PDF303208 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303209") MUR1_MUF1_PDF303209 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303210") MUR1_MUF1_PDF303210 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303211") MUR1_MUF1_PDF303211 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303212") MUR1_MUF1_PDF303212 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303213") MUR1_MUF1_PDF303213 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303214") MUR1_MUF1_PDF303214 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303215") MUR1_MUF1_PDF303215 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303216") MUR1_MUF1_PDF303216 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303217") MUR1_MUF1_PDF303217 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303218") MUR1_MUF1_PDF303218 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303219") MUR1_MUF1_PDF303219 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303220") MUR1_MUF1_PDF303220 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303221") MUR1_MUF1_PDF303221 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303222") MUR1_MUF1_PDF303222 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303223") MUR1_MUF1_PDF303223 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303224") MUR1_MUF1_PDF303224 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303225") MUR1_MUF1_PDF303225 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303226") MUR1_MUF1_PDF303226 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303227") MUR1_MUF1_PDF303227 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303228") MUR1_MUF1_PDF303228 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303229") MUR1_MUF1_PDF303229 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303230") MUR1_MUF1_PDF303230 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303231") MUR1_MUF1_PDF303231 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303232") MUR1_MUF1_PDF303232 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303233") MUR1_MUF1_PDF303233 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303234") MUR1_MUF1_PDF303234 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303235") MUR1_MUF1_PDF303235 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303236") MUR1_MUF1_PDF303236 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303237") MUR1_MUF1_PDF303237 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303238") MUR1_MUF1_PDF303238 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303239") MUR1_MUF1_PDF303239 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303240") MUR1_MUF1_PDF303240 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303241") MUR1_MUF1_PDF303241 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303242") MUR1_MUF1_PDF303242 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303243") MUR1_MUF1_PDF303243 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303244") MUR1_MUF1_PDF303244 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303245") MUR1_MUF1_PDF303245 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303246") MUR1_MUF1_PDF303246 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303247") MUR1_MUF1_PDF303247 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303248") MUR1_MUF1_PDF303248 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303249") MUR1_MUF1_PDF303249 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303250") MUR1_MUF1_PDF303250 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303251") MUR1_MUF1_PDF303251 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303252") MUR1_MUF1_PDF303252 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303253") MUR1_MUF1_PDF303253 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303254") MUR1_MUF1_PDF303254 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303255") MUR1_MUF1_PDF303255 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303256") MUR1_MUF1_PDF303256 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303257") MUR1_MUF1_PDF303257 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303258") MUR1_MUF1_PDF303258 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303259") MUR1_MUF1_PDF303259 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303260") MUR1_MUF1_PDF303260 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303261") MUR1_MUF1_PDF303261 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303262") MUR1_MUF1_PDF303262 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303263") MUR1_MUF1_PDF303263 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303264") MUR1_MUF1_PDF303264 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303265") MUR1_MUF1_PDF303265 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303266") MUR1_MUF1_PDF303266 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303267") MUR1_MUF1_PDF303267 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303268") MUR1_MUF1_PDF303268 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303269") MUR1_MUF1_PDF303269 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303270") MUR1_MUF1_PDF303270 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303271") MUR1_MUF1_PDF303271 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303272") MUR1_MUF1_PDF303272 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303273") MUR1_MUF1_PDF303273 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303274") MUR1_MUF1_PDF303274 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303275") MUR1_MUF1_PDF303275 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303276") MUR1_MUF1_PDF303276 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303277") MUR1_MUF1_PDF303277 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303278") MUR1_MUF1_PDF303278 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303279") MUR1_MUF1_PDF303279 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303280") MUR1_MUF1_PDF303280 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303281") MUR1_MUF1_PDF303281 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303282") MUR1_MUF1_PDF303282 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303283") MUR1_MUF1_PDF303283 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303284") MUR1_MUF1_PDF303284 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303285") MUR1_MUF1_PDF303285 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303286") MUR1_MUF1_PDF303286 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303287") MUR1_MUF1_PDF303287 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303288") MUR1_MUF1_PDF303288 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303289") MUR1_MUF1_PDF303289 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303290") MUR1_MUF1_PDF303290 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303291") MUR1_MUF1_PDF303291 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303292") MUR1_MUF1_PDF303292 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303293") MUR1_MUF1_PDF303293 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303294") MUR1_MUF1_PDF303294 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303295") MUR1_MUF1_PDF303295 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303296") MUR1_MUF1_PDF303296 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303297") MUR1_MUF1_PDF303297 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303298") MUR1_MUF1_PDF303298 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303299") MUR1_MUF1_PDF303299 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF303300") MUR1_MUF1_PDF303300 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF304400") MUR1_MUF1_PDF304400 = (*LHE3Weights)[i];
        /*else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91400") MUR1_MUF1_PDF91400 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91401") MUR1_MUF1_PDF91401 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91402") MUR1_MUF1_PDF91402 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91403") MUR1_MUF1_PDF91403 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91404") MUR1_MUF1_PDF91404 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91405") MUR1_MUF1_PDF91405 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91406") MUR1_MUF1_PDF91406 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91407") MUR1_MUF1_PDF91407 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91408") MUR1_MUF1_PDF91408 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91409") MUR1_MUF1_PDF91409 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91410") MUR1_MUF1_PDF91410 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91411") MUR1_MUF1_PDF91411 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91412") MUR1_MUF1_PDF91412 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91413") MUR1_MUF1_PDF91413 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91414") MUR1_MUF1_PDF91414 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91415") MUR1_MUF1_PDF91415 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91416") MUR1_MUF1_PDF91416 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91417") MUR1_MUF1_PDF91417 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91418") MUR1_MUF1_PDF91418 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91419") MUR1_MUF1_PDF91419 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91420") MUR1_MUF1_PDF91420 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91421") MUR1_MUF1_PDF91421 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91422") MUR1_MUF1_PDF91422 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91423") MUR1_MUF1_PDF91423 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91424") MUR1_MUF1_PDF91424 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91425") MUR1_MUF1_PDF91425 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91426") MUR1_MUF1_PDF91426 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91427") MUR1_MUF1_PDF91427 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91428") MUR1_MUF1_PDF91428 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91429") MUR1_MUF1_PDF91429 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91430") MUR1_MUF1_PDF91430 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91431") MUR1_MUF1_PDF91431 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF1_PDF91432") MUR1_MUF1_PDF91432 = (*LHE3Weights)[i];*/
        else if ((*LHE3WeightNames)[i] == "MUR1_MUF2_PDF303200_PSMUR1_PSMUF2") MUR1_MUF2_PDF303200_PSMUR1_PSMUF2 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR2_MUF1_PDF303200_PSMUR2_PSMUF1") MUR2_MUF1_PDF303200_PSMUR2_PSMUF1 = (*LHE3Weights)[i];
        else if ((*LHE3WeightNames)[i] == "MUR2_MUF2_PDF303200_PSMUR2_PSMUF2") MUR2_MUF2_PDF303200_PSMUR2_PSMUF2 = (*LHE3Weights)[i];
      } 
    }  
}

#endif // #ifdef analysis_cxx
