import uproot
import tensorflow as tf
tf.random.set_seed(123)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os.path

print('Using tf version {}'.format(tf.__version__))

saved_model_A = '/storage/ballaben/samples_DisplacedTrack/DNN_A.h5'
saved_model_B = '/storage/ballaben/samples_DisplacedTrack/DNN_B.h5'

features = ["DPhiJetMET","TrackPt","TrackD0","TrackD0Sig","TrackZ0SinThetha","TrackDeltaR","DPhiTrackMET","TrackCharge","nPixHits","RatioJetPtMET"]
# No LeadingJetPt to avoid DNN/MET correlation for ABCD-like background estimation

weighted_training = False # True or False

train_over_signal_not_truth_matched = True # True or False
label_signal_not_truth_matched = '0'  # if train_over_signal_not_truth_matched = True, '0' or '1'



def read_samples(samples_to_read = 'training', remove_signal_not_truth_matched = False, label_signal_not_truth_matched = '0'):

    path = '/storage/ballaben/samples_DisplacedTrack/c++/hadded_May2021_HitInfo/'
    print('Using samples in {} : {}'.format(path, os.listdir(path)))

    ####################################################
    if samples_to_read == 'training':
        print("Reading training samples")
        higgsino_151_150p5_150_file = uproot.open(path+'higgsino_151_150p5_150.root') 
        higgsino_150p5_150p5_150_file = uproot.open(path+'higgsino_150p5_150p5_150.root')
        higgsino_150p7_150p35_150_file = uproot.open(path+'higgsino_150p7_150p35_150.root') 
        Wenu_file = uproot.open(path+'Wenu.root') 
        Wmunu_file = uproot.open(path+'Wmunu.root') 
        Wtaunu_file = uproot.open(path+'Wtaunu.root') 
        Znunu_file = uproot.open(path+'Znunu.root') 
        ttbar_file = uproot.open(path+'ttbar.root') 

        higgsino_151_150p5_150_file = higgsino_151_150p5_150_file['Tree']
        higgsino_150p5_150p5_150_file = higgsino_150p5_150p5_150_file['Tree']
        higgsino_150p7_150p35_150_file = higgsino_150p7_150p35_150_file['Tree']

        Wenu_file = Wenu_file['Tree']
        Wmunu_file = Wmunu_file['Tree']
        Wtaunu_file = Wtaunu_file['Tree']
        Znunu_file = Znunu_file['Tree']
        ttbar_file = ttbar_file['Tree']

        branches = higgsino_151_150p5_150_file.keys()

        dictionary_151_150p5_150 = {}
        dictionary_150p5_150p5_150 = {}
        dictionary_150p7_150p35_150 = {}
        dictionary_Wenu = {}
        dictionary_Wmunu = {}
        dictionary_Wtaunu = {}
        dictionary_Znunu = {}
        dictionary_ttbar = {}

        dictionary_151_150p5_150['Sample'] = '151_150p5_150'
        dictionary_150p5_150p5_150['Sample'] = '150p5_150p5_150'
        dictionary_150p7_150p35_150['Sample'] = '150p7_150p35_150'
        dictionary_Wenu['Sample'] = 'Wenu'
        dictionary_Wmunu['Sample'] = 'Wmunu'
        dictionary_Wtaunu['Sample'] = 'Wtaunu'
        dictionary_Znunu['Sample'] = 'Znunu'
        dictionary_ttbar['Sample'] = 'ttbar'

        for branch in branches:
            dictionary_151_150p5_150[branch] = np.array(higgsino_151_150p5_150_file[branch])
            dictionary_150p5_150p5_150[branch] = np.array(higgsino_150p5_150p5_150_file[branch])
            dictionary_150p7_150p35_150[branch] = np.array(higgsino_150p7_150p35_150_file[branch])
            dictionary_Wenu[branch] = np.array(Wenu_file[branch])
            dictionary_Wmunu[branch] = np.array(Wmunu_file[branch])
            dictionary_Wtaunu[branch] = np.array(Wtaunu_file[branch])
            dictionary_Znunu[branch] = np.array(Znunu_file[branch])
            dictionary_ttbar[branch] = np.array(ttbar_file[branch])

        df_151_150p5_150 = pd.DataFrame(dictionary_151_150p5_150)
        df_150p5_150p5_150 = pd.DataFrame(dictionary_150p5_150p5_150)
        df_150p7_150p35_150 = pd.DataFrame(dictionary_150p7_150p35_150)

        df_Wenu = pd.DataFrame(dictionary_Wenu)
        df_Wmunu = pd.DataFrame(dictionary_Wmunu)
        df_Wtaunu = pd.DataFrame(dictionary_Wtaunu)
        df_Znunu = pd.DataFrame(dictionary_Znunu)
        df_ttbar = pd.DataFrame(dictionary_ttbar)

        if remove_signal_not_truth_matched == True:
            print("remove_signal_not_truth_matched == True. Removing signal not truth matched tracks.")
            df_151_150p5_150.drop(df_151_150p5_150[df_151_150p5_150.TrackOrigin==-1].index, inplace=True)
            df_150p5_150p5_150.drop(df_150p5_150p5_150[df_150p5_150p5_150.TrackOrigin==-1].index, inplace=True)
            df_150p7_150p35_150.drop(df_150p7_150p35_150[df_150p7_150p35_150.TrackOrigin==-1].index, inplace=True)

        frames = [df_151_150p5_150, df_150p5_150p5_150, df_150p7_150p35_150, df_Wenu, df_Wmunu, df_Wtaunu, df_Znunu, df_ttbar]

        df = pd.concat(frames)

    ####################################################

    if samples_to_read == 'other':
        print("Reading other samples")
        higgsino_151_151_150_file = uproot.open(path+'higgsino_151_151_150.root') 
        higgsino_151_151_150_file = higgsino_151_151_150_file['Tree']

        branches = higgsino_151_151_150_file.keys()

        dictionary_151_151_150 = {}
        dictionary_151_151_150['Sample'] = '151_151_150'

        for branch in branches:
            dictionary_151_151_150[branch] = np.array(higgsino_151_151_150_file[branch])

        df = pd.DataFrame(dictionary_151_151_150)

    ####################################################

    # Adding branch weight for weighted training

    if weighted_training == True:
        print("weighted_training set to True. Adding the \"weight\" column with \"weight\"=eventWeight*xsec*138950.")
        df["weight"]= df["eventWeight"]*df["xsec"]*138950.
    else:
        print("weighted_training set to False. Not considering weights in the training.")

   # Defining Ratio

    df['RatioJetPtMET']= np.full( len(df.index), -1)
    df['RatioJetPtMET']= df['LeadingJetPt']/df['MET']


    # Track labelling

    df['Label'] = np.full( len(df.index), -1)
    if train_over_signal_not_truth_matched == True and label_signal_not_truth_matched =='0':
        df['Label'][  df['TrackOrigin'] == -1 ] = 0  
        df['Label'][ (df['TrackOrigin'] == 1)  | (df['TrackOrigin'] == 2) ] = 1

    elif train_over_signal_not_truth_matched == True and label_signal_not_truth_matched =='1':
        df['Label'][  (df['Sample'] != '151_150p5_150') & (df['Sample'] != '150p5_150p5_150') & (df['Sample'] != '150p7_150p35_150') ] = 0  
        df['Label'][  (df['Sample'] == '151_150p5_150') | (df['Sample'] == '150p5_150p5_150') | (df['Sample'] == '150p7_150p35_150') ] = 1  

    elif train_over_signal_not_truth_matched == False:
        df['Label'][  df['TrackOrigin'] == -1 ] = 0  
        df['Label'][ (df['TrackOrigin'] == 1)  | (df['TrackOrigin'] == 2) ] = 1


    if (  len(df[df['Label'] == -1].index)  != 0 ):
        raise Exception('Errors in associating track labels')
    elif (  len(df[df['Label'] == -1].index)  == 0 ):
        print('All tracks have been labeled')
        print('Labelled 1 tracks = {}, Labelled 0 tracks = {}'.format(len(df[df['Label'] == 1].index), len(df[df['Label'] == 0].index)))

    # Dataframe splitting according to EventNumber

    A = df[df['EventNumber']%2 == 0]
    B = df[df['EventNumber']%2 == 1]

    print('Labelled 1 tracks in A = {}, Labelled 0 tracks in A = {}'.format(len(A[A['Label'] == 1].index), len(A[A['Label'] == 0].index)))
    print('Labelled 1 tracks in B = {}, Labelled 0 tracks in B = {}'.format(len(B[B['Label'] == 1].index), len(B[B['Label'] == 0].index)))

    print('Dataframe')
    print(df)
    return(df,A,B)


def train_A(df,A):

    import datetime
    if os.path.isfile(saved_model_A) is True:
        print ('model already exists')
        model_A = tf.keras.models.load_model(saved_model_A)

    if os.path.isfile(saved_model_A) is False:

        model_A = tf.keras.Sequential([
        
            tf.keras.layers.Flatten(),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.Dense(1, activation='sigmoid')
        ])

        model_A.compile(optimizer='adam', 
                  #optimizer='sgd',
                  loss='binary_crossentropy',
                  #loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

        log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
        if weighted_training == True:
            history = model_A.fit(A[features], A[['Label']], epochs=5, sample_weight=A[['weight']], callbacks=[tensorboard_callback])
        else:
            history = model_A.fit(A[features], A[['Label']], epochs=5, callbacks=[tensorboard_callback])
        model_A.save(saved_model_A)

    print(model_A.summary())
    print(model_A.history)
    return model_A


def train_B(df,B):

    import datetime
    if os.path.isfile(saved_model_B) is True:
        print ('model already exists')
        model_B = tf.keras.models.load_model(saved_model_B)

    if os.path.isfile(saved_model_B) is False:

        model_B = tf.keras.Sequential([
        
            tf.keras.layers.Flatten(),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.Dense(1, activation='sigmoid')
        ])

        model_B.compile(optimizer='adam', 
                  #optimizer='sgd',
                  loss='binary_crossentropy',
                  #loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

        log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
        if weighted_training == True:
            history = model_B.fit(B[features], B[['Label']], epochs=5, sample_weight=B[['weight']], callbacks=[tensorboard_callback])
        else:
            history = model_B.fit(B[features], B[['Label']], epochs=5, callbacks=[tensorboard_callback])
        model_B.save(saved_model_B)

    print(model_B.summary())
    print(model_B.history)
    return model_B


def test(df,A,B):

    model_A = tf.keras.models.load_model(saved_model_A)
    model_B = tf.keras.models.load_model(saved_model_B)

    print("Proceeding to predict A")
    predictions_A = model_B.predict(A[features])

    print("Proceeding to predict B")
    predictions_B = model_A.predict(B[features])

    A['DNN'] = predictions_A
    B['DNN'] = predictions_B

    tested_df = pd.concat([A,B])
    #tested_df.to_pickle('tested_df.pickle')
    return tested_df


def save_tree(tested_df):
    import uproot3
    import os
    #test_set = pd.read_pickle('tested_df.pickle')

    outputdir_name = 'output_withDNN'
    os.system('mkdir {}'.format(outputdir_name))

    samples = tested_df['Sample'].unique()

    branches = tested_df.keys().to_list()
    branches.remove('Sample') # Sample branch should not be added on output trees

    print("Dataframe contaning: {} samples".format(samples))
    print("Dataframe contaning: {} branches".format(branches))

    for sample in samples:
        print("Processing: {} sample".format(sample))
        datatype_dict={}
        distributions_dict={}

        for branch in branches:
            if (branch == "EventNumber") or (branch == "TrackOrigin") or (branch == "Label"):
                datatype_dict[branch] = "int32"
            else:
                datatype_dict[branch] = "float32"
            distributions_dict[branch] = np.array(tested_df[tested_df['Sample']==sample][branch])

        with uproot3.recreate("{}/{}_withDNN.root".format(outputdir_name,sample)) as f:
            f[sample] = uproot3.newtree(datatype_dict)
            f[sample].extend(distributions_dict)


def main():

    if train_over_signal_not_truth_matched == True:
        print("train_over_signal_not_truth_matched set to True.")
        if label_signal_not_truth_matched == '0':
            print("Signal not truth matched tracks will be considered as background tracks having TrackOrigin = -1. A Label 0 will be associated to them.")
            df,A,B = read_samples('training' , False, '0')
        elif label_signal_not_truth_matched == '1':
            print("Signal not truth matched tracks will be considered as signal tracks. A Label 1 will be associated to them as for the signal truth matched tracks")
            df,A,B = read_samples('training' , False, '1')
    elif train_over_signal_not_truth_matched == False:
        print("train_over_signal_not_truth_matched set to False. Removing Signal not truth matched tracks from the training.")
        df,A,B = read_samples('training' , True)

    print("Proceeding to Train A")
    train_A(df,A)
    print("Proceeding to Train B")
    train_B(df,B)
    print("Proceeding to test trained samples")

    if train_over_signal_not_truth_matched == False:
        print("train_over_signal_not_truth_matched set to False. Re-reading all training samples to include in the test set the signal not truth matched tracks removed before.")
        df,A,B = read_samples('training')

    tested_df = test(df,A,B)
    print("Proceeding to save trees from tested samples")
    save_tree(tested_df)

    df,A,B = read_samples('other')
    print("Proceeding to test the other samples")
    tested_df = test(df,A,B)
    print("Proceeding to save trees from the other samples")
    save_tree(tested_df)


if __name__ == "__main__":
    main()

