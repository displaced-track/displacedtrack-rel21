import uproot
import tensorflow as tf
tf.random.set_seed(123)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os.path

print('Using tf version {}'.format(tf.__version__))

saved_model_A = '/storage/ballaben/samples_DisplacedTrack/DNN_weighted/DNN_A.h5'
saved_model_B = '/storage/ballaben/samples_DisplacedTrack/DNN_weighted/DNN_B.h5'

features = ["LeadingJetPt","DPhiJetMET","TrackPt","TrackD0","TrackD0Sig","TrackZ0SinThetha","TrackDeltaR","DPhiTrackMET"]

def read_samples():

    path = '/storage/ballaben/samples_DisplacedTrack/c++/hadded_output/'
    print('Using samples in {} : {}'.format(path, os.listdir(path)))

    higgsino_151_150p5_150_file = uproot.open(path+'higgsino_151_150p5_150.root') 
    Wenu_file = uproot.open(path+'Wenu.root') 
    Wmunu_file = uproot.open(path+'Wmunu.root') 
    Wtaunu_file = uproot.open(path+'Wtaunu.root') 
    Znunu_file = uproot.open(path+'Znunu.root') 
    ttbar_file = uproot.open(path+'ttbar.root') 

    higgsino_151_150p5_150_file = higgsino_151_150p5_150_file['Tree']
    Wenu_file = Wenu_file['Tree']
    Wmunu_file = Wmunu_file['Tree']
    Wtaunu_file = Wtaunu_file['Tree']
    Znunu_file = Znunu_file['Tree']
    ttbar_file = ttbar_file['Tree']

    branches = higgsino_151_150p5_150_file.keys()


    print('Reading Samples')

    dictionary_151_150p5_150 = {}
    dictionary_Wenu = {}
    dictionary_Wmunu = {}
    dictionary_Wtaunu = {}
    dictionary_Znunu = {}
    dictionary_ttbar = {}

    dictionary_151_150p5_150['Sample'] = '151_150p5_150'
    dictionary_Wenu['Sample'] = 'Wenu'
    dictionary_Wmunu['Sample'] = 'Wmunu'
    dictionary_Wtaunu['Sample'] = 'Wtaunu'
    dictionary_Znunu['Sample'] = 'Znunu'
    dictionary_ttbar['Sample'] = 'ttbar'

    for branch in branches:
        dictionary_151_150p5_150[branch] = np.array(higgsino_151_150p5_150_file[branch])
        dictionary_Wenu[branch] = np.array(Wenu_file[branch])
        dictionary_Wmunu[branch] = np.array(Wmunu_file[branch])
        dictionary_Wtaunu[branch] = np.array(Wtaunu_file[branch])
        dictionary_Znunu[branch] = np.array(Znunu_file[branch])
        dictionary_ttbar[branch] = np.array(ttbar_file[branch])

    df_151_150p5_150 = pd.DataFrame(dictionary_151_150p5_150)
    df_Wenu = pd.DataFrame(dictionary_Wenu)
    df_Wmunu = pd.DataFrame(dictionary_Wmunu)
    df_Wtaunu = pd.DataFrame(dictionary_Wtaunu)
    df_Znunu = pd.DataFrame(dictionary_Znunu)
    df_ttbar = pd.DataFrame(dictionary_ttbar)

    frames = [df_151_150p5_150, df_Wenu, df_Wmunu, df_Wtaunu, df_Znunu, df_ttbar]
    df = pd.concat(frames)

    df["weight"]= df["eventWeight"]*df["xsec"]*138950.

    df['Label'] = np.full( len(df.index), -1)
    df['Label'][  df['TrackOrigin'] == -1 ] = 0  
    df['Label'][ (df['TrackOrigin'] == 1)  | (df['TrackOrigin'] == 2) ] = 1
    if (  len(df[df['Label'] == -1].index)  != 0 ):
            raise Exception('Errors in associating track labels')
    elif (  len(df[df['Label'] == -1].index)  == 0 ):
            print('All tracks have been labeled')
            print('Displaced tracks from C1/N2 decays = {}, Background tracks = {}'.format(len(df[df['Label'] == 1].index), len(df[df['Label'] == 0].index)))

    A = df[df['EventNumber']%2 == 0]
    B = df[df['EventNumber']%2 == 1]

    print('Displaced tracks from C1/N2 decays in A = {}, Background tracks in A = {}'.format(len(A[A['Label'] == 1].index), len(A[A['Label'] == 0].index)))
    print('Displaced tracks from C1/N2 decays in B = {}, Background tracks in B = {}'.format(len(B[B['Label'] == 1].index), len(B[B['Label'] == 0].index)))

    print('Dataframe')
    print(df)
    return(df,A,B)


def train_A(df,A):

    import datetime
    if os.path.isfile(saved_model_A) is True:
        print ('model already exists')
        model_A = tf.keras.models.load_model(saved_model_A)

    if os.path.isfile(saved_model_A) is False:

        model_A = tf.keras.Sequential([
        
            tf.keras.layers.Flatten(),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.Dense(1, activation='sigmoid')
        ])

        model_A.compile(optimizer='adam', 
                  #optimizer='sgd',
                  loss='binary_crossentropy',
                  #loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

        log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

        history = model_A.fit(A[features], A[['Label']], epochs=5, sample_weight=A[['weight']], callbacks=[tensorboard_callback])
        model_A.save(saved_model_A)

    print(model_A.summary())
    print(model_A.history)
    return model_A


def train_B(df,B):

    import datetime
    if os.path.isfile(saved_model_B) is True:
        print ('model already exists')
        model_B = tf.keras.models.load_model(saved_model_B)

    if os.path.isfile(saved_model_B) is False:

        model_B = tf.keras.Sequential([
        
            tf.keras.layers.Flatten(),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Dropout(rate=0.2), 
            tf.keras.layers.Dense(100, activation='relu'),
        
            tf.keras.layers.Dense(1, activation='sigmoid')
        ])

        model_B.compile(optimizer='adam', 
                  #optimizer='sgd',
                  loss='binary_crossentropy',
                  #loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])

        log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

        history = model_B.fit(B[features], B[['Label']], epochs=5, sample_weight=B[['weight']], callbacks=[tensorboard_callback])
        model_B.save(saved_model_B)

    print(model_B.summary())
    print(model_B.history)
    return model_B


def test(df,A,B):

    model_A = tf.keras.models.load_model(saved_model_A)
    model_B = tf.keras.models.load_model(saved_model_B)

    print("Proceeding to predict A")
    predictions_A = model_B.predict(A[features])

    print("Proceeding to predict B")
    predictions_B = model_A.predict(B[features])

    A['DNN'] = predictions_A
    B['DNN'] = predictions_B

    tested_df = pd.concat([A,B])
    tested_df.to_pickle('tested_df.pickle')
    return tested_df


def save_tree(tested_df):
    import uproot3
    import os
    #test_set = pd.read_pickle('tested_df.pickle')

    outputdir_name = 'output_withDNN'
    os.system('mkdir {}'.format(outputdir_name))

    samples = tested_df['Sample'].unique()

    branches = tested_df.keys().to_list()
    branches.remove('Sample') # Sample branch should not be added on output trees

    print("Dataframe contaning: {} samples".format(samples))
    print("Dataframe contaning: {} branches".format(branches))

    for sample in samples:
        print("Processing: {} sample".format(sample))
        datatype_dict={}
        distributions_dict={}

        for branch in branches:
            if (branch == "EventNumber") or (branch == "TrackOrigin") or (branch == "Label"):
                datatype_dict[branch] = "int32"
            else:
                datatype_dict[branch] = "float32"
            distributions_dict[branch] = np.array(tested_df[tested_df['Sample']==sample][branch])

        with uproot3.recreate("{}/{}_withDNN.root".format(outputdir_name,sample)) as f:
            f[sample] = uproot3.newtree(datatype_dict)
            f[sample].extend(distributions_dict)


def main():
    df,A,B = read_samples()
    print("Proceeding to Train A")
    train_A(df,A)
    print("Proceeding to Train B")
    train_B(df,B)
    print("Proceeding to test")
    tested_df = test(df,A,B)
    print("Proceeding to save trees")
    save_tree(tested_df)

if __name__ == "__main__":
    main()

