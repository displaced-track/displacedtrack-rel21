#!/usr/bin/env python
'''

ATLAS Higgsino dark matter summary plot

Originally by Jesse Liu

Versions:
* May 2021: switch compressed contour w/ combination and fix bug in LEP contour (LJ)
* March 2021: add disappearing track 136 result (LJ)
* May 2020: update to 1/2L paper (FM)
* Jun 2019: updated 1/2L to 139/fb CONF
* Mar 2018
* Dec 2017

Limits:
* ATLAS disappearing track search
* ATLAS leptonic searches (originally 2L)
* LEP chargino

This requires matplotlib and a LaTeX environment configured.

Run as
./summary_higgsino.py

'''

import matplotlib as mplt
mplt.use('Agg') # So we can use without X forwarding

import numpy as np
import os, json, math, csv, argparse
import matplotlib.pyplot  as plt
import matplotlib.lines   as mlines
import matplotlib.patches as mpatches
import matplotlib.ticker  as ticker

# So we can produce PDFs
from matplotlib.backends.backend_pdf import PdfPages

from ROOT import *
from scipy.integrate import quad

# Book-keeping annotation
#ATLAS_status = 'Internal'
ATLAS_status = 'Preliminary'
COM_LUMI     = r'$\sqrt{s} = 13$ TeV, 136 - 139 fb$^{-1}$' 
DATE         = 'June 2021'

doLogY = True # Draw the y-axis on log scale
do_dM_yaxis = True # Print the y-axis label as deltaM(N2, N1)

mZ = 91.1876

#__________________________________________
def main():
  '''
  * Obtain contours from input files
  * Axis properties
  * Annotating text
  * Construct legends
  * Finalise and save
  '''

  print('Making summary plot for Higgsino dark matter')
  
  mkdir('figs') # For the figures
 
  # Define colours
  myBlue        = '#064987'
  myMediumBlue  = '#a3cfed'
  myOrange      = '#d57117'
  myLightOrange = '#fec44f'
  myYellow      = '#ffcd38'
  myDarkGrey    = '#555555'
  myGrey        = '#cccccc'
  myLightGrey   = '#dddddd'
  myLightRed  = '#ffab70'

  # Configure figure 
  fig, ax = plt.subplots()
  fig.set_size_inches(11, 8)
  
  # ------------------------------------------------------- 
  # Obtain contours from input files
  # ------------------------------------------------------- 

  # 1L1T/2L ISR
  # CONF-2019-014
#  compr_2Lexp = csv_to_lists_toC1N1(
#  'existing_limits/HEPData-ins1767649-v2-Figure_14a_Expected.csv')
#  compr_2Lobs = csv_to_lists_toC1N1(
#      'existing_limits/HEPData-ins1767649-v2-Figure_14a_Observed.csv')
#  exp_cont = plt.plot(compr_2Lexp['mSUSY'], compr_2Lexp['deltaM'], c=myBlue,     linewidth=1, linestyle='--', zorder=-1) 
#  obs_cont = plt.fill(compr_2Lobs['mSUSY'], compr_2Lobs['deltaM'], myMediumBlue, linewidth=0, zorder=-1)
 

  # 2L compressed + 3L combination
  comb_2L_3L_file = 'existing_limits/ATL13TeV_139ifb_Higgsino_2L_3L_comb.root'
  comb_2L_3L_exp, comb_2L_3L_obs = root_to_list_N2_to_C1(comb_2L_3L_file)
  comb_2L_3L_exp = plt.plot(comb_2L_3L_exp['x_val'], comb_2L_3L_exp['y_val'], c=myBlue, linewidth=1, linestyle='--', zorder=-1)
  comb_2L_3L_obs = plt.fill(comb_2L_3L_obs['x_val'], comb_2L_3L_obs['y_val'], myMediumBlue, linewidth=0, zorder=-1)

  # Disappearing track 
  # PHYS-PUB-2017-019
#  disappTrk_file = 'existing_limits/disappearing_track_higgsino.root'
#  disappTrk_exp, disappTrk_obs = root_tg_to_list(disappTrk_file, 0.001, False) # if disapp track y-axis given in MeV, convert to GeV
#  disappTrk_exp = plt.plot(disappTrk_exp['x_val'], disappTrk_exp['y_val'], c=myOrange,    linewidth=1, linestyle='--', zorder=-1) 
#  disappTrk_obs = plt.fill(disappTrk_obs['x_val'], disappTrk_obs['y_val'], myLightOrange, linewidth=0, zorder=-1)

  # Disappearing track
  # CONF 2021
  disappTrk_file = 'existing_limits/DT_Paper2021_pixelOnly_higgsino_deltaM.root'
  disappTrk_exp, disappTrk_obs = root_tg_to_list(disappTrk_file, 1, True) # if disapp track y-axis given in MeV, convert to GeV, and close the conf contour
  disappTrk_exp = plt.plot(disappTrk_exp['x_val'], disappTrk_exp['y_val'], c=myOrange,    linewidth=1, linestyle='--', zorder=-1) 
  disappTrk_obs = plt.fill(disappTrk_obs['x_val'], disappTrk_obs['y_val'], myLightOrange, linewidth=0, zorder=-1)

  # LEP chargino limit
  d_lep = csv_to_lists('existing_limits/lepHiggsinoC1.csv')
  lep_higgsino = plt.fill(d_lep['mC1'], d_lep['dMC1N1'], myLightGrey, linewidth=0) 

  # Soft Displaced Track
  soft_displ_trk_file = csv_to_lists('existing_limits/Yuya_limits.csv')
  soft_displ_trk = plt.fill(soft_displ_trk_file['mC1'], soft_displ_trk_file['dMC1N1'], myLightRed, linewidth=0) 

  # Pure higgsino theory prediction
  #d_lep = csv_to_lists('existing_limits/pure_higgsino_theory.csv')
  #x_theo = d_lep['mC1']
  #y_theo = [ float(y) * 1E-3 for y in d_lep['dMC1N1'] ] # Convert MeV to GeV
  
  x_th = [ x for x in range(80, 270) ]
  y_th = []
 
  for mC1 in x_th:
    y_th.append( pure_hino_theory(mC1) )
  pure_higgsino_theory = plt.plot(x_th, y_th, c=myDarkGrey, linewidth=3, linestyle='-.', zorder=-1)
   
  # ------------------------------------------------------- 
  # Axis properties
  # ------------------------------------------------------- 

  # Axis limits
  plt.xlim(90, 270)
  plt.ylim(0.2, 40)

  # Axes titles
  x_txt = r'$m(\tilde{\chi}^\pm_1)$ [GeV]'
  if do_dM_yaxis:
    y_txt = r'$\sf{\Delta}$$m(\tilde{\chi}^\pm_1, \tilde{\chi}^0_1)$ [GeV]'
  else:
    y_txt = r'$m(\tilde{\chi}^\pm_1) - m(\tilde{\chi}^0_1)$ [GeV]'
  
  proc_txt = r'$pp \to \tilde{\chi}^0_2\tilde{\chi}^\pm_1, \tilde{\chi}^0_2\tilde{\chi}^0_1, \tilde{\chi}^+_1\tilde{\chi}^-_1, \tilde{\chi}^\pm_1\tilde{\chi}^0_1$ (Higgsino)'

  # Align axis titles to the right as ATLAS style
  plt.xlabel(x_txt, labelpad=20, ha='right', x=1.0) 
  plt.ylabel(y_txt, labelpad=20, ha='right', y=1.0)

  if doLogY:
    # To enable 0.1, 1, 10 notation on log scale tick marks
    # From https://stackoverflow.com/questions/21920233/matplotlib-log-scale-tick-label-number-formatting
    # Add additional labels to log-scale axis
    ax.set_yscale("log")
    ax.set_yticks([0.2, 0.5, 1, 2, 5, 10, 20, 40])
    #ax.set_yticks([0.5, 1, 2, 5, 10, 20, 50])
    ax.get_yaxis().set_major_formatter(ticker.FormatStrFormatter('%g'))
   
  # ------------------------------------------------------- 
  # Annotating text
  # ------------------------------------------------------- 
  dy=0
      
  txt_xmin = 0.65
  txt_ymax = 0.90
 
  if doLogY:
    txt_ymax = 0.85
 
  fig.text(txt_xmin, txt_ymax, r'\textbf{\textit{ATLAS}} ' + ATLAS_status, color='Black', size=21)
  fig.text(txt_xmin, txt_ymax-0.045, COM_LUMI, color='Black', size=18)
  fig.text(txt_xmin, txt_ymax-0.085, r'{0}'.format(proc_txt), color='Black', size=15)
  fig.text(0.86, 0.95, DATE, color='Black', size=16)

  # ------------------------------------------------------- 
  # Construct legends
  # ------------------------------------------------------- 
  
  # Construct legend for the different excluded regions
  leg_handles = [] 
  leg_labels  = []
  
  leg_handles.append( mpatches.Patch(color=myMediumBlue) )
  #leg_labels.append( r'$1\ell$ 1 track + $2\ell$, 139 fb$^{-1}$, CONF-2019-014, $m(\tilde{\chi}^0_2) = m(\tilde{\chi}^0_1) + 2 \sf{\Delta}$$m(\tilde{\chi}^\pm_1, \tilde{\chi}^0_1)$' )
  #leg_labels.append( r'Soft $2\ell$, 139 fb$^{-1}$, CONF-2019-014, $m(\tilde{\chi}^0_2) = m(\tilde{\chi}^0_1) + 2 \sf{\Delta}$$m(\tilde{\chi}^\pm_1, \tilde{\chi}^0_1)$' )
  leg_labels.append( r'$3\ell$ + Soft $2\ell$, arXiv:2106.01676, 1911.12606, $m(\tilde{\chi}^0_2) = m(\tilde{\chi}^0_1) + 2 \sf{\Delta}$$m(\tilde{\chi}^\pm_1, \tilde{\chi}^0_1)$')
  leg_handles.append(mpatches.Patch(color=myLightOrange))
  leg_labels.append( r'Disappearing track, ATLAS-CONF-2021-015, $m(\tilde{\chi}^0_2) = m(\tilde{\chi}^0_1)$' )
  leg_handles.append( mpatches.Patch(color=myLightGrey, lw=0) )
  leg_labels.append(r'LEP2 $\tilde{\chi}^\pm_1$ excluded')
  leg_labels.append( r'Soft Displaced Track' )
  leg_handles.append(mpatches.Patch(color=myLightRed))
  leg_handles.append( mlines.Line2D([], [], color=myDarkGrey, linewidth=3, linestyle='-.'))
  leg_labels.append( r'Theoretical prediction for pure Higgsino' )

  
  leg = plt.legend(leg_handles, 
                   leg_labels,
                   frameon=False, 
                   loc='upper left',
                   bbox_to_anchor=(0.38, 0.49),
                   prop={'size':11}, 
                   borderpad=0.9, 
                   labelspacing=0.3, 
                   handlelength=1.3,
                   handleheight=1.1,
                   scatterpoints=1)
 
  # Construct the legend for observed/expected limits 
  leg1_handles = [] 
  leg1_labels  = []
  leg1_handles.append( mlines.Line2D([], [], color=myDarkGrey, linewidth=2, linestyle='-'))
  leg1_labels.append( r'Observed limits' )
  leg1_handles.append( mlines.Line2D([], [], color=myDarkGrey, linewidth=2, linestyle='--'))
  leg1_labels.append( r'Expected limits' )
  
  leg1 = plt.legend(leg1_handles, 
                   leg1_labels,
                   frameon=False, 
                   loc='lower right',
                   bbox_to_anchor=(0.9, 0.56),
                   prop={'size':14}, 
                   title=r'All limits at 95\% CL',
                   borderpad=1.2, 
                   labelspacing=0.3, 
                   handlelength=2.1,
                   handleheight=1.1,
                   ncol=1,
                   columnspacing=0.7,
                   handletextpad=0.5,
                   scatterpoints=1)
  leg1._legend_box.align = "left"
  leg1.get_title().set_fontsize(14) 
  ax.add_artist(leg)
  ax.add_artist(leg1)
  
  # ------------------------------------------------------- 
  # Finalise and save
  # ------------------------------------------------------- 
  
  atlas_style(plt, ax)
  plt.tight_layout(pad=0.3)
  plt.subplots_adjust( top=0.93 )
 
  # Save plot 
  save_name = 'figs/ATLAS_SUSY_EWSummary_higgsino'
  print('Saving as {0}'.format(save_name))
  plt.savefig(save_name + '.pdf', format='pdf', dpi=50)
#  plt.savefig(save_name + '.png', format='png', dpi=400)
#  plt.savefig(save_name + '.eps', format='eps', dpi=150)

#__________________________________________
def integrand(x, r): 
  return (2 - x) * np.log( 1 + x / ( r * (1 - x)**2 ) )

#__________________________________________
def pure_hino_theory(mC1):
  # From Eq 2 & 3 of https://arxiv.org/abs/hep-ph/9804359
  r = mC1**2 / mZ**2
  # Perform numerical integral from Eq 3
  integral = quad(integrand, 0, 1, args=(r))
  # Calculate the mass splitting from Eq 2
  dm = 0.356 * ( math.sqrt(r) / math.pi ) * integral[0]
  return dm

#__________________________________________
def root_tg_to_list(in_file, rescale_y=1., conf=False):
  '''
  Converts a root file containing 
  "Expected" and "Observed" limits
  into four lists for the contours
  for disappearing track root file
  '''
  tfile_in = TFile( in_file )
  
  cont_exp = tfile_in.Get('Expected')
  cont_obs = tfile_in.Get('Observed')
  
  n_pts_exp = cont_exp.GetN()
  n_pts_obs = cont_obs.GetN()
  
  d_exp = {'x_val' : [], 'y_val' : [] }
  d_obs = {'x_val' : [], 'y_val' : [] }

  # Extract the TGraph points into lists for plotting
  #print('Expected')
  if (conf) :

      for i in range(-20, n_pts_exp):
        x_exp = Double()
        y_exp = Double()
        x_exp_min = Double()
        y_exp_min = Double()
        cont_exp.GetPoint(0, x_exp_min, y_exp_min)

        if (i < 0):
#          d_exp['x_val'].append(x_exp_min) # vertical line 
#          d_exp['y_val'].append(y_exp_min+i/.1) # vertical line
          d_exp['x_val'].append(94.0) # close contour
          d_exp['y_val'].append(y_exp_min+i/.1) # close contour
          #d_exp['x_val'].append(94.0 + (x_exp_min-94)/20.*i) # horizontal line
          #d_exp['y_val'].append(y_exp_min) # horizontal line

        else:
          cont_exp.GetPoint(i, x_exp, y_exp)
          if float(x_exp) < 95.1:
            d_exp['x_val'].append(94.0)
          else:
            d_exp['x_val'].append(x_exp)
          d_exp['y_val'].append(y_exp * float(rescale_y) )

      for i in range(-20, n_pts_obs):
        x_obs = Double()
        y_obs = Double()
        x_obs_min = Double()
        y_obs_min = Double()
        cont_obs.GetPoint(0, x_obs_min, y_obs_min)

        if (i < 0):
          d_obs['x_val'].append(94.0) # close contour
          d_obs['y_val'].append(y_obs_min+i/.1) # close contour
          #d_obs['x_val'].append(94.0 + (x_obs_min-94)/20.*i) # horizontal line
          #d_obs['y_val'].append(y_obs_min) # horizontal line
#          d_obs['x_val'].append(x_obs_min) # vertical line
#          d_obs['y_val'].append(y_obs_min+i/.1) # vertical line

        else:
          cont_obs.GetPoint(i, x_obs, y_obs)
          if float(x_obs) < 95.1:
            d_obs['x_val'].append(94.0) 
          else: 
            d_obs['x_val'].append(x_obs)
          d_obs['y_val'].append(y_obs * float(rescale_y) )
      
  else :

      for i in range(0, n_pts_exp):
        x_exp = Double()
        y_exp = Double()
        cont_exp.GetPoint(i, x_exp, y_exp)
        # Push the left-most disappearing track points beneath LEP contour
        if float(x_exp) < 95.1:
          d_exp['x_val'].append(94.0) 
        else:
          d_exp['x_val'].append(x_exp) 
        d_exp['y_val'].append(y_exp * float(rescale_y) ) 
        #print('{0}, {1}'.format(x_exp, y_exp) )
        #print('Observed')
        for i in range(0, n_pts_obs):
          x_obs = Double()
          y_obs = Double()
          cont_obs.GetPoint(i, x_obs, y_obs) 
          if float(x_obs) < 95.1:
            d_obs['x_val'].append(94.0) 
          else:
            d_obs['x_val'].append(x_obs) 
          d_obs['y_val'].append(y_obs * float(rescale_y) ) 
    #print('{0}, {1}'.format(x_obs, y_obs) )

  return d_exp, d_obs

#__________________________________________
def root_to_list_N2_to_C1(in_file):
  '''
  Converts a root file containing 
  "Exp_0" and "Obs_0" limits
  into four lists for the contours
  for 2L and 3L combination
  convert x-axis from N2 to C1
  convert y-axis from N2-N1 to C1-N1
  '''
  tfile_in = TFile( in_file )
  
  cont_exp = tfile_in.Get('Exp_0')
  cont_obs = tfile_in.Get('Obs_0')
  
  n_pts_exp = cont_exp.GetN()
  n_pts_obs = cont_obs.GetN()
  
  d_exp = {'x_val' : [], 'y_val' : [] }
  d_obs = {'x_val' : [], 'y_val' : [] }

  # Extract the TGraph points into lists for plotting

  for i in range(0, n_pts_exp):
    x_exp = Double()
    y_exp = Double()
    cont_exp.GetPoint(i, x_exp, y_exp)
    d_exp['x_val'].append((2*x_exp-y_exp)/2.)
    d_exp['y_val'].append(y_exp/2.)
        
  x_final = 0

  for i in range(0, n_pts_obs): 
    x_obs = Double()
    y_obs = Double()
    cont_obs.GetPoint(i, x_obs, y_obs)
    d_obs['x_val'].append((2*x_obs-y_obs)/2.)
    d_obs['y_val'].append(y_obs/2.)

    x_final = y_obs/2.

  # close the contour
  d_obs['x_val'].append(100)
  d_obs['y_val'].append(x_final)
    

#    print('{0}, {1}'.format((2*x_obs-y_obs)/2., y_obs/2.))
      
  return d_exp, d_obs

#__________________________________________
def csv_to_lists(csv_file):
  '''
  Converts csv to dictionary of lists containing columns
  the dictionary keys is the header
  '''
  with open(csv_file) as input_file:
    reader = csv.reader(input_file)
    col_names = next(reader)
    data = {name: [] for name in col_names}
    for line in reader:
      for pos, name in enumerate(col_names):
        data[name].append(line[pos])
  return data

#__________________________________________
def csv_to_lists_toC1N1(csv_file):
  '''
  Converts csv to dictionary of lists containing columns
  the dictionary keys is the header
  '''
  with open(csv_file) as input_file:
    reader = csv.reader(input_file)
    col_names = next(reader)
    data = {name: [] for name in col_names}
    for line in reader:
      line[1] = str(float(line[1])/2)
      for pos, name in enumerate(col_names):
        data[name].append(line[pos])
  return data

#__________________________________________
def atlas_style(plt, ax):
  '''
  Call this function at the end atlas_style(plt, ax)
  to impose a reasonable approximation of ATLAS style
  '''
  
  plt.rcParams.update({'font.size': 32})
  plt.rcParams['text.usetex'] = True
  plt.rcParams['text.latex.preview'] = True # needed for vertical alignment of TeX in legend
  plt.rcParams['text.dvipnghack'] = True 
  plt.rcParams['text.antialiased'] = True 
  plt.rcParams['text.latex.preamble'] = [
     r'\usepackage{siunitx}',   # i need upright \micro symbols, but you need...
     r'\sisetup{detect-all}',   # ...this to force siunitx to actually use your fonts
     r'\usepackage{helvet}',    # ATLAS requires Helvetica typeface
     r'\usepackage{sansmath}',  # Load sansmath so math -> helvet(ica) mostly
     r'\sansmath',              # <- tricky! -- gotta actually tell tex to use!
     r'\usepackage[utf8]{inputenc}',
     r'\DeclareUnicodeCharacter{2212}{$-$}', # Use the typographically correct minus sign
  ]  
   
  # Adjust axis ticks
  ax.minorticks_on()
  ax.tick_params('x', length=12, width=1, which='major', labelsize='28', pad=10)
  ax.tick_params('x', length=6,  width=1, which='minor') 
  ax.tick_params('y', length=12, width=1, which='major', labelsize='28', pad=10)
  ax.tick_params('y', length=6,  width=1, which='minor') 

#_________________________________________________________________________
def mkdir(dirPath):
  '''
  make directory for given input path
  '''
  try:
    os.makedirs(dirPath)
    print('Successfully made new directory ' + dirPath)
  except OSError:
    pass

#__________________________________________
if __name__ == "__main__":
    main()
