'''

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

HistFitter configuration file for the Compressed Electroweak SUSY DT Analysis ANA-SUSY-2020-04.

Current configuration is based on a full data-driven background estimation:
	-- V+jets QCD tracks from PU and PV nf : 0L-D region 
	-- V+jets QCD tracks from PU and PV tf : 1L regions
	-- W+jets had : TauHad region
	-- W+jets lep : TauLep region

--------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------

'''

from configManager import configMgr
from systematic import Systematic
from configWriter import fitConfig,Measurement,Channel,Sample
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange
from ROOT import TCanvas
from ROOT import TColor
from ROOT import TFile
from math import sqrt
from copy import deepcopy
from optparse import OptionParser 
import pprint
import numpy as np

myUserArgs = configMgr.userArg.split(' ')
myInputParser = OptionParser()
myInputParser.add_option("--doABCDBkg",         action = 'store_true',                           default = False,  help = 'ABCD bkg-only fit')
myInputParser.add_option("--doExclusion",       action = 'store_true',                           default = False,  help = 'Exclusion model-dependent fit - set automatically by default')
myInputParser.add_option("--doDiscovery",       action = 'store_true',                           default = False,  help = 'Discovery model-independent fit')
myInputParser.add_option("--doBkgValidation",   action = 'store_true',                           default = False,  help = 'Add VRs to bkg-only fit')
myInputParser.add_option("--doSyst",            action = 'store_true',                           default = False,  help = 'Do systematics')
myInputParser.add_option("--doBackupCacheFile", action = 'store_true',                           default = False,  help = 'BackupCacheFile')
myInputParser.add_option("--doValidationPlots", action = 'store_true',                           default = False,  help = 'Validation plots')
myInputParser.add_option("--VariableToPlot",                                                     default = 'MET',  help = 'Variable to plot')
myInputParser.add_option("--ChannelToPlot",                                                      default = 'SR_A', help = 'Channel to plot')
myInputParser.add_option("--doTestSystematics", action = 'store_true',                           default = False,  help = 'Apply test systematics')
myInputParser.add_option("--point",                                    dest = 'point',           default = '',     help = 'Specify a signal point')
myInputParser.add_option("--discoveryRegion",                          dest = 'discoveryRegion', default = 'SR_A', help = 'Discovery region where the discovery fit is run')
myInputParser.add_option("--tag",                                                                default = '',     help ='Name of the tag to add to the output directory and workspace name')

(options, args)   = myInputParser.parse_args(myUserArgs)
doABCDBkg         = options.doABCDBkg
doExclusion       = options.doExclusion
doDiscovery       = options.doDiscovery
doBkgValidation   = options.doBkgValidation
doSyst            = options.doSyst
doValidationPlots = options.doValidationPlots
VariableToPlot    = []
VariableToPlot    = options.VariableToPlot.split(",")
ChannelToPlot     = []
ChannelToPlot     = options.ChannelToPlot.split(",")
doTestSystematics = options.doTestSystematics
whichPoint        = options.point
discoveryRegion   = []
doBackupCacheFile = options.doBackupCacheFile
tag = options.tag
if options.discoveryRegion != "":
	discoveryRegion=options.discoveryRegion.split(",")

print(options,args)


# If no fit is selected as additional argument, an exclusion fit runs by default
if doABCDBkg == False and doDiscovery == False and doValidationPlots == False:
    doExclusion = True


if doABCDBkg == True:
    fit_type = 'ABCD'
    print("******** ABCD Bkg Fit ********")
elif doExclusion == True:
    fit_type = 'Exclusion'
    print("******** Exclusion Fit ********")
elif doDiscovery == True:
    fit_type = 'Discovery'
    print("******** Discovery Fit ********")
elif doValidationPlots == True and doABCDBkg == False:
    fit_type = 'Validation'
    print("******** Validation Fit (just to draw some plots) ********")


if doSyst == True:
    syst_type = 'withSyst'
    print("******** Including Systematics ********")
elif doTestSystematics:
	syst_type = 'testSyst'
	print("******** Using Tes Systematics ********")
elif doSyst == False:
    syst_type = 'noSyst'
    print("******** Excluding Systematics ********")


if doBackupCacheFile == True:
    print("***** Using BackupCacheFile *******")


# Define HistFactory attributes
if tag == '':
	configMgr.analysisName = fit_type + "_" + syst_type 
else:
	configMgr.analysisName = fit_type + "_" + syst_type + "_" + tag

if whichPoint != "":
	configMgr.analysisName += "_" + whichPoint

configMgr.outputFileName = "results/" + configMgr.analysisName + ".root"


## Setting HistFitter internal parameters

# From HF tag v.0.65 - normalized histos from the cachefile are rebuilt, unless forceNorm is set to False
configMgr.forceNorm = False

useStat = True
configMgr.statErrThreshold = 0.05

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi  = 1.                                     # Luminosity of input TTree after weighting
configMgr.outputLumi = 3244.54 + 33402.2 + 44630.6 + 58791.6  # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")

configMgr.blindSR = True
configMgr.blindCR = False
configMgr.blindVR = False

configMgr.ReduceCorrMatrix = True

# Setting the parameters of the hypothesis test
configMgr.nTOYs          = 20000 
configMgr.calculatorType = 2     # 2 == asymptotic calculator, 0 == frequentist calculator using toys
configMgr.testStatType   = 3     # 3 == one-sided profile likelihood test statistic (LHC default)
configMgr.nPoints        = 100   # number of values scanned of signal-strength for upper-limit determination of signal strength.

# Set scan range for the upper limit
#configMgr.scanRange = (0., 5.)

# Suffix of nominal tree
configMgr.nomName = "_NoSys"

#configMgr.prun = True
#configMgr.prunThreshold = 0.01

if doBkgValidation == True:
	weights = ["eventWeight", "genWeight", "jvtWeight", "pileupWeight", "leptonWeight", "photonWeight", #"bTagWeight",
			   "(trigMatch_metTrig ? 1 : (trigMatch_singleMuonTrig ? (trigWeight * trigWeight_singleMuonTrig) : (trigMatch_singleElectronTrig ? (trigWeight * trigWeight_singleElectronTrig) : trigWeight * trigWeight_singlePhotonTrig)))"
			  ]
else:
	weights = ["eventWeight", "genWeight", "jvtWeight", "pileupWeight", "leptonWeight",
			   "(trigMatch_metTrig ? 1 : (trigMatch_singleMuonTrig ? (trigWeight * trigWeight_singleMuonTrig) : (trigWeight * trigWeight_singleElectronTrig)))"
			  ]

configMgr.weights = (weights)

# All grid points 
sigSamples_grid = []

if doBackupCacheFile == True:
    configMgr.useCacheToTreeFallback = True # enable the fallback to trees
    configMgr.useHistBackupCacheFile = True # enable the use of an alternate data file
	# The data file of your previous fit (= the backup cache file)
	if doDiscovery:
        configMgr.histBackupCacheFile = "/users2/alesala/DisplacedTrack/HistFitter/data/Discovery.root"  
	elif doExclusion:
        configMgr.histBackupCacheFile = "/users2/alesala/DisplacedTrack/HistFitter/data/" + configMgr.analysisName + ".root"
    else:
        configMgr.histBackupCacheFile = "/users2/alesala/DisplacedTrack/HistFitter/data/ABCD_withSyst.root"  


############################################################################
#     SRs
############################################################################

configMgr.cutsDict['SR_A_0L_Sd0_20plus'] = '(MET > 600.) && (TrackD0Sig > 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0)' #&& (nBJet30 == 0)'
configMgr.cutsDict['SR_A_0L_Sd0_8_20']   = '(MET > 600.) && (TrackD0Sig > 8. && TrackD0Sig < 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0)' #&& (nBJet30 == 0)'


############################################################################
#      CRs
############################################################################

if doBkgValidation:
	configMgr.cutsDict['CR_D_0L']             = '(MET > 600.) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 0)' #&& (nBJet30 == 0)'
	configMgr.cutsDict['CR_B_1mu_Sd0_20plus'] = '(MET > 300 && MET < 600) && (TrackD0Sig > 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 1 && nPhoton_base == 0 && nMu_signal == 1)' #&& (nBJet30 == 0)'
	configMgr.cutsDict['CR_B_1mu_Sd0_8_20']   = '(MET > 300 && MET < 600) && (TrackD0Sig > 8. && TrackD0Sig < 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 1 && nPhoton_base == 0 && nMu_signal == 1)' #&& (nBJet30 == 0)'
	configMgr.cutsDict['CR_C_1mu']            = '(MET > 300 && MET < 600) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 1 && nPhoton_base == 0 && nMu_signal == 1)' #&& (nBJet30 == 0)'
	configMgr.cutsDict['CR_tau_had_high']     = '(MET > 600) && (TrackD0Sig > 3.) && (TrackPt > 8. && TrackPt < 20.) && (nLep_base == 0 && nPhoton_base == 0)' #&& (nBJet30 == 0)'
	configMgr.cutsDict['CR_tau_lep_high']     = '(MET > 600) && (TrackD0Sig > 3.) && (TrackPt > 8. && TrackPt < 20.) && (nLep_base == 1 && nPhoton_base == 0 && nMu_base == 1 && nMu_signal == 0)' #&& (nBJet30 == 0)'
else:
	configMgr.cutsDict['CR_D_0L']             = '(MET > 600.) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0)' #&& (nBJet30 == 0)'
	configMgr.cutsDict['CR_B_1mu_Sd0_20plus'] = '(MET > 300 && MET < 600) && (TrackD0Sig > 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 1 && nMu_signal == 1)' #&& (nBJet30 == 0)'
	configMgr.cutsDict['CR_B_1mu_Sd0_8_20']   = '(MET > 300 && MET < 600) && (TrackD0Sig > 8. && TrackD0Sig < 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 1 && nMu_signal == 1)' #&& (nBJet30 == 0)'
	configMgr.cutsDict['CR_C_1mu']            = '(MET > 300 && MET < 600) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 1 && nMu_signal == 1)' #&& (nBJet30 == 0)'
	configMgr.cutsDict['CR_tau_had_high']     = '(MET > 600) && (TrackD0Sig > 3.) && (TrackPt > 8. && TrackPt < 20.) && (nLep_base == 0)' #&& (nBJet30 == 0)'
	configMgr.cutsDict['CR_tau_lep_high']     = '(MET > 600) && (TrackD0Sig > 3.) && (TrackPt > 8. && TrackPt < 20.) && (nLep_base == 1 && nMu_base == 1 && nMu_signal == 0)' #&& (nBJet30 == 0)'

configMgr.cutsDict['CR_C_0L']             = '(MET > 300. && MET < 400.) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 0)' #&& (nBJet30 == 0)'
configMgr.cutsDict['CR_C_1ele']           = '(MET > 300 && MET < 600) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 1 && nPhoton_base == 0 && nEle_signal == 1)' #&& (nBJet30 == 0)'

configMgr.cutsDict['CR_C_ll']             = '(MET > 300 && MET < 600) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 2 && nPhoton_base == 0 && (nEle_signal == 2 || nMu_signal == 2))' #&& (nBJet30 == 0)'
configMgr.cutsDict['CR_D_ll']             = '(MET > 600.) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 2 && nPhoton_base == 0 && (nEle_signal == 2 || nMu_signal == 2))' #&& (nBJet30 == 0)'

configMgr.cutsDict['CR_C_1Y']             = '(MET > 300 && MET < 600) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 1)' #&& (nBJet30 == 0)'
configMgr.cutsDict['CR_D_1Y']             = '(MET > 600.) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 1)' #&& (nBJet30 == 0)'

configMgr.cutsDict['CR_C_jj']             = '(MET > 300 && MET < 600) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 0)' #&& (nBJet30 == 0)'
configMgr.cutsDict['CR_D_jj']             = '(MET > 600.) && (TrackD0Sig > 0. && TrackD0Sig < 8.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 0)' #&& (nBJet30 == 0)'

configMgr.cutsDict['CR_tau_had_low']      = '(MET > 300 && MET < 400) && (TrackD0Sig > 3.) && (TrackPt > 8. && TrackPt < 20.) && (nLep_base == 0 && nPhoton_base == 0)' #&& (nBJet30 == 0)'
configMgr.cutsDict['CR_tau_lep_low']      = '(MET > 300 && MET < 400) && (TrackD0Sig > 3.) && (TrackPt > 8. && TrackPt < 20.) && (nLep_base == 1 && nPhoton_base == 0 && nMu_base == 1 && nMu_signal == 0)' #&& (nBJet30 == 0)'

############################################################################
#      VRs
############################################################################

configMgr.cutsDict['VR_B_0L_Sd0_20plus']    = '(MET > 300 && MET < 400) && (TrackD0Sig > 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 0)' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_B_0L_Sd0_8_20']      = '(MET > 300 && MET < 400) && (TrackD0Sig > 8. && TrackD0Sig < 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 0)' #&& (nBJet30 == 0)'

configMgr.cutsDict['VR_B_ll_Sd0_20plus']    = '(MET > 300 && MET < 600) && (TrackD0Sig > 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 2 && nPhoton_base == 0 && (nEle_signal == 2 || nMu_signal == 2))' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_B_ll_Sd0_8_20']      = '(MET > 300 && MET < 600) && (TrackD0Sig > 8. && TrackD0Sig < 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 2 && nPhoton_base == 0 && (nEle_signal == 2 || nMu_signal == 2))' #&& (nBJet30 == 0)'

configMgr.cutsDict['VR_B_1ele_Sd0_20plus']  = '(MET > 300 && MET < 600) && (TrackD0Sig > 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 1 && nPhoton_base == 0 && nEle_signal == 1)' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_B_1ele_Sd0_8_20']    = '(MET > 300 && MET < 600) && (TrackD0Sig > 8. && TrackD0Sig < 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 1 && nPhoton_base == 0 && nEle_signal == 1)' #&& (nBJet30 == 0)'

configMgr.cutsDict['VR_A_1Y_Sd0_20plus']    = '(MET > 600.) && (TrackD0Sig > 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 1)' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_A_1Y_Sd0_8_20']      = '(MET > 600.) && (TrackD0Sig > 8. && TrackD0Sig < 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 1)' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_B_1Y_Sd0_20plus']    = '(MET > 300 && MET < 600) && (TrackD0Sig > 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 1)' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_B_1Y_Sd0_8_20']      = '(MET > 300 && MET < 600) && (TrackD0Sig > 8. && TrackD0Sig < 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 1)' #&& (nBJet30 == 0)'

configMgr.cutsDict['VR_A_jj_Sd0_20plus']    = '(MET > 600.) && (TrackD0Sig > 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 0)' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_A_jj_Sd0_8_20']      = '(MET > 600.) && (TrackD0Sig > 8. && TrackD0Sig < 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 0)' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_B_jj_Sd0_20plus']    = '(MET > 300 && MET < 600) && (TrackD0Sig > 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 0)' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_B_jj_Sd0_8_20']      = '(MET > 300 && MET < 600) && (TrackD0Sig > 8. && TrackD0Sig < 20.) && (TrackPt > 2. && TrackPt < 5.) && (nLep_base == 0 && nPhoton_base == 1 && nPhoton_signal == 0)' #&& (nBJet30 == 0)'

configMgr.cutsDict['VR_tau_had_high']       = '(MET > 600) && (TrackD0Sig > 3.) && (TrackPt > 5. && TrackPt < 8.) && (nLep_base == 0 && nPhoton_base == 0)' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_tau_lep_high']       = '(MET > 600) && (TrackD0Sig > 3.) && (TrackPt > 5. && TrackPt < 8.) && (nLep_base == 1 && nPhoton_base == 0 && nMu_base == 1 && nMu_signal == 0)' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_tau_had_low']        = '(MET > 300 && MET < 400) && (TrackD0Sig > 3.) && (TrackPt > 5. && TrackPt < 8.) && (nLep_base == 0 && nPhoton_base == 0)' #&& (nBJet30 == 0)'
configMgr.cutsDict['VR_tau_lep_low']        = '(MET > 300 && MET < 400) && (TrackD0Sig > 3.) && (TrackPt > 5. && TrackPt < 8.) && (nLep_base == 1 && nPhoton_base == 0 && nMu_base == 1 && nMu_signal == 0)' #&& (nBJet30 == 0)'

configMgr.cutsDict['VR_all'] = '(nLep_base == 0)'

############################################################################
#      SYSTEMATICS
############################################################################

systype             = "overallSys"
systype_oneSide     = "histoSysOneSide"
systype_norm        = "overallNormSys"
systype_normOneSide = "overallNormHistoSysOneSide"

######################### Test systematics #######################

TestSystematic_10corr   = Systematic("TestSystematic_10corr",   None, 1.10, 0.90, "user", "histoSys")  #fake syst 
TestSystematic_10uncorr = Systematic("TestSystematic_10uncorr", None, 1.10, 0.90, "user", "overallSys") #fake syst 

######################### Theoretical systematics ################

# PMG SystematicsTool creates up/down variation histograms/TGraphs. In order to retrieve the correct theoretical systematics, 
# loop on regions, bkg. samples and systematics names and fill a dictionary with the corresponding variations. Special treatment
# is needed for the V+jets EW corrections and for the single top s/t channels

inputPath = "/users2/alesala/PMGTools/SysHistogramMaker/Combined/"  # Path to variations files produced by the PMGSystematicsTool
TheoSysList = {}

if doSyst:

	for region in os.listdir(inputPath): 
		TheoSysList[region] = {}
		for sample in os.listdir(inputPath + region):  

			# Catch bkgs. variations used in all fits
			if "higgsino" not in sample:
				
				# Get the QCD-only nominal variation file name
				for f in os.listdir(inputPath + region + "/" + sample):
					if "Nominal" in f: 
						NominalFile = f
						break
			
				ROOTFileQCDNominal  = TFile(inputPath + region + "/" + sample +"/" + NominalFile)
				ROOTFileCombination = TFile(inputPath + region + "/" + sample + "/all.root")
				
				if "diboson" in sample: sample = "VV"
				TheoSysList[region][sample] = {} 

				# Get the nominal and the overall up/down variations and fill the dictionary
				for key in ROOTFileQCDNominal.GetListOfKeys():
					if key.GetClassName() != "TGraphAsymmErrors" or "__" in key.GetName(): continue
					graphNominal = ROOTFileQCDNominal.Get(key.GetName())
				for key in ROOTFileCombination.GetListOfKeys():
					if key.GetClassName() != "TGraphAsymmErrors" or "__" in key.GetName(): continue
					graphCombination = ROOTFileCombination.Get(key.GetName())

				if graphNominal.GetPointY(0) == 0:
					TheoSysList[region][sample]['Nominal']    = 0.
					TheoSysList[region][sample]['NewNominal'] = 0.
					TheoSysList[region][sample]['up']         = 0. 
					TheoSysList[region][sample]['down']       = 0.
				else:
					TheoSysList[region][sample]['Nominal']    = graphNominal.GetPointY(0)
					TheoSysList[region][sample]['NewNominal'] = graphCombination.GetPointY(0)
					TheoSysList[region][sample]['up']         = 1. + (graphCombination.GetErrorYhigh(0) / graphCombination.GetPointY(0)) 
					TheoSysList[region][sample]['down']       = 1. - (graphCombination.GetErrorYlow(0) / graphCombination.GetPointY(0))

			# Catch signal variations too used in exclusion fits
			if doExclusion and whichPoint in sample:

				TheoSysList[region][sample] = {}
				ROOTFileCombination = TFile(inputPath + region + "/" + sample + "/all.root")
				
				# Get the overall up/down variations and fill the dictionary
				for key in ROOTFileCombination.GetListOfKeys():
					if key.GetClassName() != "TGraphAsymmErrors" or "__" in key.GetName(): continue
					graphCombination = ROOTFileCombination.Get(key.GetName())

				if graphCombination.GetPointY(0) == 0:
					TheoSysList[region][sample]['Nominal']    = 0.
					TheoSysList[region][sample]['NewNominal'] = 0.
					TheoSysList[region][sample]['up']         = 0.  
					TheoSysList[region][sample]['down']       = 0.
				else:
					TheoSysList[region][sample]['Nominal']    = graphCombination.GetPointY(0)
					TheoSysList[region][sample]['NewNominal'] = graphCombination.GetPointY(0)
					TheoSysList[region][sample]['up']         = 1. + (graphCombination.GetErrorYhigh(0) / graphCombination.GetPointY(0)) 
					TheoSysList[region][sample]['down']       = 1. - (graphCombination.GetErrorYlow(0) / graphCombination.GetPointY(0))
		
		# As sanity check remove any negative down variation 	
		for region in TheoSysList:
			for sample in TheoSysList[region]:
				if TheoSysList[region][sample]['down'] < 0: TheoSysList[region][sample]['down'] = 0.

		# Finally compute single top uncertainties. Sum s and t channel and add in quadrature their errors
		for region in TheoSysList:
			TheoSysList[region]['top'] = {}
			TheoSysList[region]['top']['Nominal']    = TheoSysList[region]['singletop_schannel']['Nominal'] + TheoSysList[region]['singletop_tchannel']['Nominal']
			TheoSysList[region]['top']['NewNominal'] = TheoSysList[region]['singletop_schannel']['NewNominal'] + TheoSysList[region]['singletop_tchannel']['NewNominal']
			TheoSysList[region]['top']['up']         = sqrt(TheoSysList[region]['singletop_schannel']['up']**2 + TheoSysList[region]['singletop_tchannel']['up']**2)
			TheoSysList[region]['top']['down']       = sqrt(TheoSysList[region]['singletop_schannel']['down']**2 + TheoSysList[region]['singletop_tchannel']['down']**2)

	# Print the formatted dictionaries when running with systematics
	print("Dictionary storing theoretical systematics")
	pprint.pprint(TheoSysList)


######################### Experimental systematics ################

def replaceWeight(oldList,oldWeight,newWeight):   
	newList = deepcopy(oldList)
	for i in range(len(newList)):
		if oldWeight in newList[i]:
			newList[i] = newList[i].replace(oldWeight, newWeight)
	#newList[oldList.index(oldWeight)] = newWeight
	return newList

# Nominal weights replacement
pileupWeight_UP 			                    = replaceWeight(weights,"pileupWeight","pileupWeightUp")
pileupWeight_DOWN 			        	        = replaceWeight(weights,"pileupWeight","pileupWeightDown")
jvtWeight_JET_JvtEfficiency_UP 		            = replaceWeight(weights,"jvtWeight","jvtWeight_JET_JvtEfficiency__1up")
jvtWeight_JET_JvtEfficiency_DOWN 	            = replaceWeight(weights,"jvtWeight","jvtWeight_JET_JvtEfficiency__1down")
jvtWeight_JET_fJvtEfficiency_UP 	            = replaceWeight(weights,"jvtWeight","jvtWeight_JET_fJvtEfficiency__1up")
jvtWeight_JET_fJvtEfficiency_DOWN 	            = replaceWeight(weights,"jvtWeight","jvtWeight_JET_fJvtEfficiency__1down")
leptonWeight_EL_EFF_ID_UP 		   	            = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up")
leptonWeight_EL_EFF_ID_DOWN 		            = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down")
leptonWeight_EL_EFF_ISO_UP 		                = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up")
leptonWeight_EL_EFF_ISO_DOWN                    = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down")
leptonWeight_EL_EFF_RECO_UP                     = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up")
leptonWeight_EL_EFF_RECO_DOWN                   = replaceWeight(weights,"leptonWeight","leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down")
leptonWeight_MUON_EFF_ISO_STAT_UP               = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_ISO_STAT__1up")
leptonWeight_MUON_EFF_ISO_STAT_DOWN             = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_ISO_STAT__1down")
leptonWeight_MUON_EFF_ISO_SYS_UP                = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_ISO_SYS__1up")
leptonWeight_MUON_EFF_ISO_SYS_DOWN              = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_ISO_SYS__1down")
leptonWeight_MUON_EFF_RECO_STAT_UP              = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_RECO_STAT__1up")
leptonWeight_MUON_EFF_RECO_STAT_DOWN            = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_RECO_STAT__1down")
leptonWeight_MUON_EFF_RECO_STAT_LOWPT_UP        = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up")
leptonWeight_MUON_EFF_RECO_STAT_LOWPT_DOWN      = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down")
leptonWeight_MUON_EFF_RECO_SYS_UP               = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_RECO_SYS__1up")
leptonWeight_MUON_EFF_RECO_SYS_DOWN             = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_RECO_SYS__1down")
leptonWeight_MUON_EFF_RECO_SYS_LOWPT_UP         = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up")
leptonWeight_MUON_EFF_RECO_SYS_LOWPT_DOWN       = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down")
leptonWeight_MUON_EFF_TTVA_STAT_UP              = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_TTVA_STAT__1up")
leptonWeight_MUON_EFF_TTVA_STAT_DOWN            = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_TTVA_STAT__1down")
leptonWeight_MUON_EFF_TTVA_SYS_UP               = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_TTVA_SYS__1up")
leptonWeight_MUON_EFF_TTVA_SYS_DOWN             = replaceWeight(weights,"leptonWeight","leptonWeight_MUON_EFF_TTVA_SYS__1down")
trigWeight_EL_EFF_TriggerEff_UP                 = replaceWeight(weights,"trigWeight_singleElectronTrig","trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1up")
trigWeight_EL_EFF_TriggerEff_DOWN               = replaceWeight(weights,"trigWeight_singleElectronTrig","trigWeight_EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR__1down")
trigWeight_EL_EFF_Trigger_UP                    = replaceWeight(weights,"trigWeight_singleElectronTrig","trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1up")
trigWeight_EL_EFF_Trigger_DOWN                  = replaceWeight(weights,"trigWeight_singleElectronTrig","trigWeight_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR__1down")
trigWeight_MUON_EFF_TrigStat_UP					= replaceWeight(weights,"trigWeight_singleMuonTrig","trigWeight_MUON_EFF_TrigStatUncertainty__1up")
trigWeight_MUON_EFF_TrigStat_DOWN				= replaceWeight(weights,"trigWeight_singleMuonTrig","trigWeight_MUON_EFF_TrigStatUncertainty__1down")
trigWeight_MUON_EFF_TrigSyst_UP					= replaceWeight(weights,"trigWeight_singleMuonTrig","trigWeight_MUON_EFF_TrigSystUncertainty__1up")
trigWeight_MUON_EFF_TrigSyst_DOWN				= replaceWeight(weights,"trigWeight_singleMuonTrig","trigWeight_MUON_EFF_TrigSystUncertainty__1down")
'''bTagWeight_FT_EFF_B_systematics_UP				= replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1up")
bTagWeight_FT_EFF_B_systematics_DOWN            = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_B_systematics__1down")
bTagWeight_FT_EFF_C_systematics_UP              = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1up")
bTagWeight_FT_EFF_C_systematics_DOWN            = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_C_systematics__1down")
bTagWeight_FT_EFF_Light_systematics_UP          = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1up")
bTagWeight_FT_EFF_Light_systematics_DOWN        = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_Light_systematics__1down")
bTagWeight_FT_EFF_extrapolation_UP              = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1up")
bTagWeight_FT_EFF_extrapolation_DOWN            = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation__1down")
bTagWeight_FT_EFF_extrapolation_from_charm_UP   = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1up")
bTagWeight_FT_EFF_extrapolation_from_charm_DOWN = replaceWeight(weights,"bTagWeight","bTagWeight_FT_EFF_extrapolation_from_charm__1down")'''

##################### Systematics for NON-NORMALIZED backgrounds ##################
EG_Resolution = Systematic("EG_RES",configMgr.nomName,"_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree",systype)
EG_Scale      = Systematic("EG_SCALE",configMgr.nomName,"_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree",systype)

JET_BJES_Response 			               = Systematic("JET_BJES_RESPONSE",configMgr.nomName,"_JET_BJES_Response__1up","_JET_BJES_Response__1down","tree",systype)
JET_EffectiveNP_1 			               = Systematic("JET_EFFECTIVENP_1",configMgr.nomName,"_JET_EffectiveNP_1__1up","_JET_EffectiveNP_1__1down","tree",systype)
JET_EffectiveNP_2 			               = Systematic("JET_EFFECTIVENP_2",configMgr.nomName,"_JET_EffectiveNP_2__1up","_JET_EffectiveNP_2__1down","tree",systype)
JET_EffectiveNP_3 			               = Systematic("JET_EFFECTIVENP_3",configMgr.nomName,"_JET_EffectiveNP_3__1up","_JET_EffectiveNP_3__1down","tree",systype)
JET_EffectiveNP_4 			               = Systematic("JET_EFFECTIVENP_4",configMgr.nomName,"_JET_EffectiveNP_4__1up","_JET_EffectiveNP_4__1down","tree",systype)
JET_EffectiveNP_5 			               = Systematic("JET_EFFECTIVENP_5",configMgr.nomName,"_JET_EffectiveNP_5__1up","_JET_EffectiveNP_5__1down","tree",systype)
JET_EffectiveNP_6 			               = Systematic("JET_EFFECTIVENP_6",configMgr.nomName,"_JET_EffectiveNP_6__1up","_JET_EffectiveNP_6__1down","tree",systype)
JET_EffectiveNP_7 			               = Systematic("JET_EFFECTIVENP_7",configMgr.nomName,"_JET_EffectiveNP_7__1up","_JET_EffectiveNP_7__1down","tree",systype)
JET_EffectiveNP_8RestTerm                  = Systematic("JET_EFFECTIVENP_8RestTerm",configMgr.nomName,"_JET_EffectiveNP_8restTerm__1up","_JET_EffectiveNP_8restTerm__1down","tree",systype)
JET_EtaIntercalibration_Modelling          = Systematic("JET_ETAINTERCALIBRATION_MODELLING",configMgr.nomName,"_JET_EtaIntercalibration_Modelling__1up","_JET_EtaIntercalibration_Modelling__1down","tree",systype)
JET_EtaIntercalibration_NonClosure2018Data = Systematic("JET_ETAINTERCALIBRATION_NONCLOSURE2018DATA",configMgr.nomName,"_JET_EtaIntercalibration_NonClosure_2018data__1up","_JET_EtaIntercalibration_NonClosure_2018data__1down","tree",systype)
JET_EtaIntercalibration_NonClosureHighE    = Systematic("JET_ETAINTERCALIBRATION_NONCLOSUREHIGHE",configMgr.nomName,"_JET_EtaIntercalibration_NonClosure_highE__1up","_JET_EtaIntercalibration_NonClosure_highE__1down","tree",systype)
JET_EtaIntercalibration_NonClosureNegEta   = Systematic("JET_ETAINTERCALIBRATION_NONCLOSURENEGETA",configMgr.nomName,"_JET_EtaIntercalibration_NonClosure_negEta__1up","_JET_EtaIntercalibration_NonClosure_negEta__1down","tree",systype)
JET_EtaIntercalibration_NonClosurePosEta   = Systematic("JET_ETAINTERCALIBRATION_NONCLOSUREPOSETA",configMgr.nomName,"_JET_EtaIntercalibration_NonClosure_posEta__1up","_JET_EtaIntercalibration_NonClosure_posEta__1down","tree",systype)
JET_EtaIntercalibration_TotalStat          = Systematic("JET_ETAINTERCALIBRATION_TOTALSTAT",configMgr.nomName,"_JET_EtaIntercalibration_TotalStat__1up","_JET_EtaIntercalibration_TotalStat__1down","tree",systype)
JET_FlavorComposition = Systematic("JET_FLAVOR_COMPOSITION",configMgr.nomName,"_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree",systype)
JET_FlavorResponse 	  = Systematic("JET_FLAVOR_RESPONSE",configMgr.nomName,"_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree",systype)

JER_DataVsMC16            = Systematic("JET_JER_DATAVSMC_MC16",configMgr.nomName,"_JET_JER_DataVsMC_MC16__1up","_JET_JER_DataVsMC_MC16__1down","tree",systype_oneSide)
JER_EffectiveNP_1 	      = Systematic("JET_JER_EFFECTIVENP_1",configMgr.nomName,"_JET_JER_EffectiveNP_1__1up","_JET_JER_EffectiveNP_1__1down","tree",systype_oneSide)
JER_EffectiveNP_2 	      = Systematic("JET_JER_EFFECTIVENP_2",configMgr.nomName,"_JET_JER_EffectiveNP_2__1up","_JET_JER_EffectiveNP_2__1down","tree",systype_oneSide)
JER_EffectiveNP_3 	      = Systematic("JET_JER_EFFECTIVENP_3",configMgr.nomName,"_JET_JER_EffectiveNP_3__1up","_JET_JER_EffectiveNP_3__1down","tree",systype_oneSide)
JER_EffectiveNP_4 	      = Systematic("JET_JER_EFFECTIVENP_4",configMgr.nomName,"_JET_JER_EffectiveNP_4__1up","_JET_JER_EffectiveNP_4__1down","tree",systype_oneSide)
JER_EffectiveNP_5 	      = Systematic("JET_JER_EFFECTIVENP_5",configMgr.nomName,"_JET_JER_EffectiveNP_5__1up","_JET_JER_EffectiveNP_5__1down","tree",systype_oneSide)
JER_EffectiveNP_6 	      = Systematic("JET_JER_EFFECTIVENP_6",configMgr.nomName,"_JET_JER_EffectiveNP_6__1up","_JET_JER_EffectiveNP_6__1down","tree",systype_oneSide)
JER_EffectiveNP_7RestTerm = Systematic("JET_JER_EFFECTIVENP_7RestTerm",configMgr.nomName,"_JET_JER_EffectiveNP_7restTerm__1up","_JET_JER_EffectiveNP_7restTerm__1down","tree",systype_oneSide)

JET_Pileup_OffsetMu      = Systematic("JET_PILEUP_OFFSETMU",configMgr.nomName,"_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree",systype)
JET_Pileup_OffsetNPV     = Systematic("JET_PILEUP_OFFSETNPV",configMgr.nomName,"_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree",systype)
JET_Pileup_PtTerm        = Systematic("JET_PILEUP_PTTERM",configMgr.nomName,"_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree",systype)
JET_Pileup_RhoTopology   = Systematic("JET_PILEUP_RHOTOPOLGY",configMgr.nomName,"_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree",systype)
JET_PunchThrough_MC16    = Systematic("JET_PUNCHTHROUGH_MC16",configMgr.nomName,"_JET_PunchThrough_MC16__1up","_JET_PunchThrough_MC16__1down","tree",systype)
JET_SingleParticleHighPt = Systematic("JET_SINGLEPARTICLE_HIGHPT",configMgr.nomName,"_JET_SingleParticle_HighPt__1up","_JET_SingleParticle_HighPt__1down","tree",systype)

MET_Para  = Systematic("MET_SOFTTRK_RESOPARA",configMgr.nomName,"_MET_SoftTrk_ResoPara","_NoSys","tree",systype_oneSide)
MET_Perp  = Systematic("MET_SOFTTRK_RESOPERP",configMgr.nomName,"_MET_SoftTrk_ResoPerp","_NoSys","tree",systype_oneSide)
MET_Scale = Systematic("MET_SOFTTRK_SCALE",configMgr.nomName,"_MET_SoftTrk_Scale__1up","_MET_SoftTrk_Scale__1down","tree",systype)

MUON_ID           = Systematic("MUON_ID",configMgr.nomName,"_MUON_ID__1up","_MUON_ID__1down","tree",systype)
MUON_MS           = Systematic("MUON_MS",configMgr.nomName,"_MUON_MS__1up","_MUON_MS__1down","tree",systype)
MUON_SAGITTA_STAT = Systematic("MUON_SAGITTA_DATASTAT",configMgr.nomName,"_MUON_SAGITTA_DATASTAT__1up","_MUON_SAGITTA_DATASTAT__1down","tree",systype)
MUON_SAGITTA_RES  = Systematic("MUON_SAGITTA_RESBIAS",configMgr.nomName,"_MUON_SAGITTA_RESBIAS__1up","_MUON_SAGITTA_RESBIAS__1down","tree",systype)
MUON_SCALE        = Systematic("MUON_SCALE",configMgr.nomName,"_MUON_SCALE__1up","_MUON_SCALE__1down","tree",systype)

TRK_RES_D0 = Systematic("TRK_RES_D0_MEAS",configMgr.nomName,"_TRK_RES_D0_MEAS","_NoSys","tree",systype_oneSide)
TRK_RES_Z0 = Systematic("TRK_RES_Z0_MEAS",configMgr.nomName,"_TRK_RES_Z0_MEAS","_NoSys","tree",systype_oneSide)

pileup 			         = Systematic("PILEUP",weights,pileupWeight_UP,pileupWeight_DOWN,"weight",systype)
jvt 			         = Systematic("JVT",weights,jvtWeight_JET_JvtEfficiency_UP,jvtWeight_JET_JvtEfficiency_DOWN,"weight",systype)
fjvt 			         = Systematic("fJVT",weights,jvtWeight_JET_fJvtEfficiency_UP,jvtWeight_JET_fJvtEfficiency_DOWN,"weight",systype)
EL_EFF_ID 		         = Systematic("EL_EFF_ID",weights,leptonWeight_EL_EFF_ID_UP,leptonWeight_EL_EFF_ID_DOWN,"weight",systype)
EL_EFF_ISO 		         = Systematic("EL_EFF_ISO",weights,leptonWeight_EL_EFF_ISO_UP,leptonWeight_EL_EFF_ISO_DOWN,"weight",systype)
EL_EFF_RECO              = Systematic("EL_EFF_RECO",weights,leptonWeight_EL_EFF_RECO_UP,leptonWeight_EL_EFF_RECO_DOWN,"weight",systype)
MUON_EFF_ISO 		     = Systematic("MUON_EFF_ISO",weights,leptonWeight_MUON_EFF_ISO_STAT_UP,leptonWeight_MUON_EFF_ISO_STAT_DOWN,"weight",systype)
MUON_EFF_ISO_SYS 	     = Systematic("MUON_EFF_ISO_SYS",weights,leptonWeight_MUON_EFF_ISO_SYS_UP,leptonWeight_MUON_EFF_ISO_SYS_DOWN,"weight",systype)
MUON_EFF_RECO_STAT       = Systematic("MUON_EFF_RECO_STAT",weights,leptonWeight_MUON_EFF_RECO_STAT_UP,leptonWeight_MUON_EFF_RECO_STAT_DOWN,"weight",systype)
MUON_EFF_RECO_STAT_LOWPT = Systematic("MUON_EFF_RECO_STAT_LOWPT",weights,leptonWeight_MUON_EFF_RECO_STAT_LOWPT_UP,leptonWeight_MUON_EFF_RECO_STAT_LOWPT_DOWN,"weight",systype) 
MUON_EFF_RECO_SYS        = Systematic("MUON_EFF_RECO_SYS",weights,leptonWeight_MUON_EFF_RECO_SYS_UP,leptonWeight_MUON_EFF_RECO_SYS_DOWN,"weight",systype)
MUON_EFF_RECO_SYS_LOWPT  = Systematic("MUON_EFF_RECO_SYS_LOWPT",weights,leptonWeight_MUON_EFF_RECO_SYS_LOWPT_UP,leptonWeight_MUON_EFF_RECO_SYS_LOWPT_DOWN,"weight",systype)
MUON_EFF_TTVA_STAT       = Systematic("MUON_EFF_TTVA_STAT",weights,leptonWeight_MUON_EFF_TTVA_STAT_UP,leptonWeight_MUON_EFF_TTVA_STAT_DOWN,"weight",systype)
MUON_EFF_TTVA_SYS        = Systematic("MUON_EFF_TTVA_SYS",weights,leptonWeight_MUON_EFF_TTVA_SYS_UP,leptonWeight_MUON_EFF_TTVA_SYS_DOWN,"weight",systype)
EL_EFF_TriggerEff        = Systematic("EL_EFF_TriggerEff",weights,trigWeight_EL_EFF_TriggerEff_UP,trigWeight_EL_EFF_TriggerEff_DOWN,"weight",systype)
EL_EFF_Trigger           = Systematic("EL_EFF_Trigger",weights,trigWeight_EL_EFF_Trigger_UP,trigWeight_EL_EFF_Trigger_DOWN,"weight",systype)
MUON_EFF_TriggerStat     = Systematic("MU_EFF_TriggerStat",weights,trigWeight_MUON_EFF_TrigStat_UP,trigWeight_MUON_EFF_TrigStat_DOWN,"weight",systype)
MUON_EFF_TriggerSyst     = Systematic("MU_EFF_TriggerSyst",weights,trigWeight_MUON_EFF_TrigSyst_UP,trigWeight_MUON_EFF_TrigSyst_DOWN,"weight",systype)
#bTAG_EFF_B               = Systematic("bTAG_EFF_B",weights,bTagWeight_FT_EFF_B_systematics_UP,bTagWeight_FT_EFF_B_systematics_DOWN,"weight",systype)
#bTAG_EFF_C               = Systematic("bTAG_EFF_C",weights,bTagWeight_FT_EFF_C_systematics_UP,bTagWeight_FT_EFF_C_systematics_DOWN,"weight",systype)
#bTAG_EFF_Light           = Systematic("bTAG_EFF_Light",weights,bTagWeight_FT_EFF_Light_systematics_UP,bTagWeight_FT_EFF_Light_systematics_DOWN,"weight",systype)
#bTAG_EFF_Extrapolation   = Systematic("bTAG_EFF_Extrapolation",weights,bTagWeight_FT_EFF_extrapolation_UP,bTagWeight_FT_EFF_extrapolation_DOWN,"weight",systype)
#bTAG_EFF_Extrapolation_C = Systematic("bTAG_EFF_Extrapolation_from_charm",weights,bTagWeight_FT_EFF_extrapolation_from_charm_UP,bTagWeight_FT_EFF_extrapolation_from_charm_DOWN,"weight",systype)

# Background syst. lists
SystList = [EG_Resolution, EG_Scale, JET_BJES_Response, JET_EffectiveNP_1, JET_EffectiveNP_2, JET_EffectiveNP_3, JET_EffectiveNP_4, JET_EffectiveNP_5, JET_EffectiveNP_6, JET_EffectiveNP_7, JET_EffectiveNP_8RestTerm, JET_EtaIntercalibration_Modelling, JET_EtaIntercalibration_NonClosure2018Data, JET_EtaIntercalibration_NonClosureHighE, JET_EtaIntercalibration_NonClosureNegEta, JET_EtaIntercalibration_NonClosurePosEta, JET_EtaIntercalibration_TotalStat, JET_FlavorComposition, JET_FlavorResponse, JER_DataVsMC16, JER_EffectiveNP_1, JER_EffectiveNP_2, JER_EffectiveNP_3, JER_EffectiveNP_4, JER_EffectiveNP_5, JER_EffectiveNP_6, JER_EffectiveNP_7RestTerm, JET_Pileup_OffsetMu, JET_Pileup_OffsetNPV, JET_Pileup_PtTerm, JET_Pileup_RhoTopology, JET_PunchThrough_MC16, JET_SingleParticleHighPt, MET_Para, MET_Perp, MET_Scale, MUON_ID, MUON_MS, MUON_SAGITTA_STAT, MUON_SAGITTA_RES, MUON_SCALE, TRK_RES_D0, TRK_RES_Z0, pileup, jvt, fjvt, EL_EFF_ID, EL_EFF_ISO, EL_EFF_RECO, MUON_EFF_ISO, MUON_EFF_ISO_SYS, MUON_EFF_RECO_STAT, MUON_EFF_RECO_STAT_LOWPT, MUON_EFF_RECO_SYS, MUON_EFF_RECO_SYS_LOWPT, MUON_EFF_TTVA_STAT, MUON_EFF_TTVA_SYS, EL_EFF_TriggerEff, EL_EFF_Trigger, MUON_EFF_TriggerStat, MUON_EFF_TriggerSyst]
#SystList = [EG_Resolution, EG_Scale, JET_BJES_Response, JET_EffectiveNP_1, JET_EffectiveNP_2, JET_EffectiveNP_3, JET_EffectiveNP_4, JET_EffectiveNP_5, JET_EffectiveNP_6, JET_EffectiveNP_7, JET_EffectiveNP_8RestTerm, JET_EtaIntercalibration_Modelling, JET_EtaIntercalibration_NonClosure2018Data, JET_EtaIntercalibration_NonClosureHighE, JET_EtaIntercalibration_NonClosureNegEta, JET_EtaIntercalibration_NonClosurePosEta, JET_EtaIntercalibration_TotalStat, JET_FlavorComposition, JET_FlavorResponse, JER_DataVsMC16, JER_EffectiveNP_1, JER_EffectiveNP_2, JER_EffectiveNP_3, JER_EffectiveNP_4, JER_EffectiveNP_5, JER_EffectiveNP_6, JER_EffectiveNP_7RestTerm, JET_Pileup_OffsetMu, JET_Pileup_OffsetNPV, JET_Pileup_PtTerm, JET_Pileup_RhoTopology, JET_PunchThrough_MC16, JET_SingleParticleHighPt, MET_Para, MET_Perp, MET_Scale, MUON_ID, MUON_MS, MUON_SAGITTA_STAT, MUON_SAGITTA_RES, MUON_SCALE, TRK_RES_D0, TRK_RES_Z0, pileup, jvt, fjvt, EL_EFF_ID, EL_EFF_ISO, EL_EFF_RECO, MUON_EFF_ISO, MUON_EFF_ISO_SYS, MUON_EFF_RECO_STAT, MUON_EFF_RECO_STAT_LOWPT, MUON_EFF_RECO_SYS, MUON_EFF_RECO_SYS_LOWPT, MUON_EFF_TTVA_STAT, MUON_EFF_TTVA_SYS, EL_EFF_TriggerEff, EL_EFF_Trigger, MUON_EFF_TriggerStat, MUON_EFF_TriggerSyst, bTAG_EFF_B, bTAG_EFF_C, bTAG_EFF_Light, bTAG_EFF_Extrapolation, bTAG_EFF_Extrapolation_C]

# Remember to add DataVsMC16_AFII and JET_PunchThrough_MC16_AFII
SignalSystList = [EG_Resolution, EG_Scale, JET_BJES_Response, JET_EffectiveNP_1, JET_EffectiveNP_2, JET_EffectiveNP_3, JET_EffectiveNP_4, JET_EffectiveNP_5, JET_EffectiveNP_6, JET_EffectiveNP_7, JET_EffectiveNP_8RestTerm, JET_EtaIntercalibration_Modelling, JET_EtaIntercalibration_NonClosure2018Data, JET_EtaIntercalibration_NonClosureHighE, JET_EtaIntercalibration_NonClosureNegEta, JET_EtaIntercalibration_NonClosurePosEta, JET_EtaIntercalibration_TotalStat, JET_FlavorComposition, JET_FlavorResponse, JER_EffectiveNP_1, JER_EffectiveNP_2, JER_EffectiveNP_3, JER_EffectiveNP_4, JER_EffectiveNP_5, JER_EffectiveNP_6, JER_EffectiveNP_7RestTerm, JET_Pileup_OffsetMu, JET_Pileup_OffsetNPV, JET_Pileup_PtTerm, JET_Pileup_RhoTopology, JET_SingleParticleHighPt, MET_Para, MET_Perp, MET_Scale, MUON_ID, MUON_MS, MUON_SAGITTA_STAT, MUON_SAGITTA_RES, MUON_SCALE, TRK_RES_D0, TRK_RES_Z0, pileup, jvt, fjvt, EL_EFF_ID, EL_EFF_ISO, EL_EFF_RECO, MUON_EFF_ISO, MUON_EFF_ISO_SYS, MUON_EFF_RECO_STAT, MUON_EFF_RECO_STAT_LOWPT, MUON_EFF_RECO_SYS, MUON_EFF_RECO_SYS_LOWPT, MUON_EFF_TTVA_STAT, MUON_EFF_TTVA_SYS, EL_EFF_TriggerEff, EL_EFF_Trigger, MUON_EFF_TriggerStat, MUON_EFF_TriggerSyst]
#SignalSystList = [EG_Resolution, EG_Scale, JET_BJES_Response, JET_EffectiveNP_1, JET_EffectiveNP_2, JET_EffectiveNP_3, JET_EffectiveNP_4, JET_EffectiveNP_5, JET_EffectiveNP_6, JET_EffectiveNP_7, JET_EffectiveNP_8RestTerm, JET_EtaIntercalibration_Modelling, JET_EtaIntercalibration_NonClosure2018Data, JET_EtaIntercalibration_NonClosureHighE, JET_EtaIntercalibration_NonClosureNegEta, JET_EtaIntercalibration_NonClosurePosEta, JET_EtaIntercalibration_TotalStat, JET_FlavorComposition, JET_FlavorResponse, JER_EffectiveNP_1, JER_EffectiveNP_2, JER_EffectiveNP_3, JER_EffectiveNP_4, JER_EffectiveNP_5, JER_EffectiveNP_6, JER_EffectiveNP_7RestTerm, JET_Pileup_OffsetMu, JET_Pileup_OffsetNPV, JET_Pileup_PtTerm, JET_Pileup_RhoTopology, JET_SingleParticleHighPt, MET_Para, MET_Perp, MET_Scale, MUON_ID, MUON_MS, MUON_SAGITTA_STAT, MUON_SAGITTA_RES, MUON_SCALE, TRK_RES_D0, TRK_RES_Z0, pileup, jvt, fjvt, EL_EFF_ID, EL_EFF_ISO, EL_EFF_RECO, MUON_EFF_ISO, MUON_EFF_ISO_SYS, MUON_EFF_RECO_STAT, MUON_EFF_RECO_STAT_LOWPT, MUON_EFF_RECO_SYS, MUON_EFF_RECO_SYS_LOWPT, MUON_EFF_TTVA_STAT, MUON_EFF_TTVA_SYS, EL_EFF_TriggerEff, EL_EFF_Trigger, MUON_EFF_TriggerStat, MUON_EFF_TriggerSyst, bTAG_EFF_B, bTAG_EFF_C, bTAG_EFF_Light, bTAG_EFF_Extrapolation, bTAG_EFF_Extrapolation_C]

##################### Systematics for NORMALIZED backgrounds ####################
EG_Resolution_norm = Systematic("EG_RES",configMgr.nomName,"_EG_RESOLUTION_ALL__1up","_EG_RESOLUTION_ALL__1down","tree",systype_norm)
EG_Scale_norm      = Systematic("EG_SCALE",configMgr.nomName,"_EG_SCALE_ALL__1up","_EG_SCALE_ALL__1down","tree",systype_norm)

JET_BJES_Response_norm 				= Systematic("JET_BJES_RESPONSE",configMgr.nomName,"_JET_BJES_Response__1up","_JET_BJES_Response__1down","tree",systype_norm)
JET_EffectiveNP_1_norm 				= Systematic("JET_EFFECTIVENP_1",configMgr.nomName,"_JET_EffectiveNP_1__1up","_JET_EffectiveNP_1__1down","tree",systype_norm)
JET_EffectiveNP_2_norm 				= Systematic("JET_EFFECTIVENP_2",configMgr.nomName,"_JET_EffectiveNP_2__1up","_JET_EffectiveNP_2__1down","tree",systype_norm)
JET_EffectiveNP_3_norm 				= Systematic("JET_EFFECTIVENP_3",configMgr.nomName,"_JET_EffectiveNP_3__1up","_JET_EffectiveNP_3__1down","tree",systype_norm)
JET_EffectiveNP_4_norm 				= Systematic("JET_EFFECTIVENP_4",configMgr.nomName,"_JET_EffectiveNP_4__1up","_JET_EffectiveNP_4__1down","tree",systype_norm)
JET_EffectiveNP_5_norm 				= Systematic("JET_EFFECTIVENP_5",configMgr.nomName,"_JET_EffectiveNP_5__1up","_JET_EffectiveNP_5__1down","tree",systype_norm)
JET_EffectiveNP_6_norm 				= Systematic("JET_EFFECTIVENP_6",configMgr.nomName,"_JET_EffectiveNP_6__1up","_JET_EffectiveNP_6__1down","tree",systype_norm)
JET_EffectiveNP_7_norm 				= Systematic("JET_EFFECTIVENP_7",configMgr.nomName,"_JET_EffectiveNP_7__1up","_JET_EffectiveNP_7__1down","tree",systype_norm)
JET_EffectiveNP_8RestTerm_norm                  = Systematic("JET_EFFECTIVENP_8RestTerm",configMgr.nomName,"_JET_EffectiveNP_8restTerm__1up","_JET_EffectiveNP_8restTerm__1down","tree",systype_norm)
JET_EtaIntercalibration_Modelling_norm          = Systematic("JET_ETAINTERCALIBRATION_MODELLING",configMgr.nomName,"_JET_EtaIntercalibration_Modelling__1up","_JET_EtaIntercalibration_Modelling__1down","tree",systype_norm)
JET_EtaIntercalibration_NonClosure2018Data_norm = Systematic("JET_ETAINTERCALIBRATION_NONCLOSURE2018DATA",configMgr.nomName,"_JET_EtaIntercalibration_NonClosure_2018data__1up","_JET_EtaIntercalibration_NonClosure_2018data__1down","tree",systype_norm)
JET_EtaIntercalibration_NonClosureHighE_norm    = Systematic("JET_ETAINTERCALIBRATION_NONCLOSUREHIGHE",configMgr.nomName,"_JET_EtaIntercalibration_NonClosure_highE__1up","_JET_EtaIntercalibration_NonClosure_highE__1down","tree",systype_norm)
JET_EtaIntercalibration_NonClosureNegEta_norm   = Systematic("JET_ETAINTERCALIBRATION_NONCLOSURENEGETA",configMgr.nomName,"_JET_EtaIntercalibration_NonClosure_negEta__1up","_JET_EtaIntercalibration_NonClosure_negEta__1down","tree",systype_norm)
JET_EtaIntercalibration_NonClosurePosEta_norm   = Systematic("JET_ETAINTERCALIBRATION_NONCLOSUREPOSETA",configMgr.nomName,"_JET_EtaIntercalibration_NonClosure_posEta__1up","_JET_EtaIntercalibration_NonClosure_posEta__1down","tree",systype_norm)
JET_EtaIntercalibration_TotalStat_norm          = Systematic("JET_ETAINTERCALIBRATION_TOTALSTAT",configMgr.nomName,"_JET_EtaIntercalibration_TotalStat__1up","_JET_EtaIntercalibration_TotalStat__1down","tree",systype_norm)
JET_FlavorComposition_norm                      = Systematic("JET_FLAVOR_COMPOSITION",configMgr.nomName,"_JET_Flavor_Composition__1up","_JET_Flavor_Composition__1down","tree",systype_norm)
JET_FlavorResponse_norm                         = Systematic("JET_FLAVOR_RESPONSE",configMgr.nomName,"_JET_Flavor_Response__1up","_JET_Flavor_Response__1down","tree",systype_norm)

JER_DataVsMC16_norm            = Systematic("JET_JER_DATAVSMC_MC16",configMgr.nomName,"_JET_JER_DataVsMC_MC16__1up","_JET_JER_DataVsMC_MC16__1down","tree",systype_normOneSide)
JER_EffectiveNP_1_norm 	       = Systematic("JET_JER_EFFECTIVENP_1",configMgr.nomName,"_JET_JER_EffectiveNP_1__1up","_JET_JER_EffectiveNP_1__1down","tree",systype_normOneSide)
JER_EffectiveNP_2_norm 	       = Systematic("JET_JER_EFFECTIVENP_2",configMgr.nomName,"_JET_JER_EffectiveNP_2__1up","_JET_JER_EffectiveNP_2__1down","tree",systype_normOneSide)
JER_EffectiveNP_3_norm 	       = Systematic("JET_JER_EFFECTIVENP_3",configMgr.nomName,"_JET_JER_EffectiveNP_3__1up","_JET_JER_EffectiveNP_3__1down","tree",systype_normOneSide)
JER_EffectiveNP_4_norm 	       = Systematic("JET_JER_EFFECTIVENP_4",configMgr.nomName,"_JET_JER_EffectiveNP_4__1up","_JET_JER_EffectiveNP_4__1down","tree",systype_normOneSide)
JER_EffectiveNP_5_norm 	       = Systematic("JET_JER_EFFECTIVENP_5",configMgr.nomName,"_JET_JER_EffectiveNP_5__1up","_JET_JER_EffectiveNP_5__1down","tree",systype_normOneSide)
JER_EffectiveNP_6_norm 	       = Systematic("JET_JER_EFFECTIVENP_6",configMgr.nomName,"_JET_JER_EffectiveNP_6__1up","_JET_JER_EffectiveNP_6__1down","tree",systype_normOneSide)
JER_EffectiveNP_7RestTerm_norm = Systematic("JET_JER_EFFECTIVENP_7RestTerm",configMgr.nomName,"_JET_JER_EffectiveNP_7restTerm__1up","_JET_JER_EffectiveNP_7restTerm__1down","tree",systype_normOneSide)

JET_Pileup_OffsetMu_norm      = Systematic("JET_PILEUP_OFFSETMU",configMgr.nomName,"_JET_Pileup_OffsetMu__1up","_JET_Pileup_OffsetMu__1down","tree",systype_norm)
JET_Pileup_OffsetNPV_norm     = Systematic("JET_PILEUP_OFFSETNPV",configMgr.nomName,"_JET_Pileup_OffsetNPV__1up","_JET_Pileup_OffsetNPV__1down","tree",systype_norm)
JET_Pileup_PtTerm_norm        = Systematic("JET_PILEUP_PTTERM",configMgr.nomName,"_JET_Pileup_PtTerm__1up","_JET_Pileup_PtTerm__1down","tree",systype_norm)
JET_Pileup_RhoTopology_norm   = Systematic("JET_PILEUP_RHOTOPOLGY",configMgr.nomName,"_JET_Pileup_RhoTopology__1up","_JET_Pileup_RhoTopology__1down","tree",systype_norm)
JET_PunchThrough_MC16_norm    = Systematic("JET_PUNCHTHROUGH_MC16",configMgr.nomName,"_JET_PunchThrough_MC16__1up","_JET_PunchThrough_MC16__1down","tree",systype_norm)
JET_SingleParticleHighPt_norm = Systematic("JET_SINGLEPARTICLE_HIGHPT",configMgr.nomName,"_JET_SingleParticle_HighPt__1up","_JET_SingleParticle_HighPt__1down","tree",systype_norm)

MET_Para_norm  = Systematic("MET_SOFTTRK_RESOPARA",configMgr.nomName,"_MET_SoftTrk_ResoPara","_NoSys","tree",systype_normOneSide)
MET_Perp_norm  = Systematic("MET_SOFTTRK_RESOPERP",configMgr.nomName,"_MET_SoftTrk_ResoPerp","_NoSys","tree",systype_normOneSide)
MET_Scale_norm = Systematic("MET_SOFTTRK_SCALE",configMgr.nomName,"_MET_SoftTrk_Scale__1up","_MET_SoftTrk_Scale__1down","tree",systype_norm)

MUON_ID_norm           = Systematic("MUON_ID",configMgr.nomName,"_MUON_ID__1up","_MUON_ID__1down","tree",systype_norm)
MUON_MS_norm           = Systematic("MUON_MS",configMgr.nomName,"_MUON_MS__1up","_MUON_MS__1down","tree",systype_norm)
MUON_SAGITTA_STAT_norm = Systematic("MUON_SAGITTA_DATASTAT",configMgr.nomName,"_MUON_SAGITTA_DATASTAT__1up","_MUON_SAGITTA_DATASTAT__1down","tree",systype_norm)
MUON_SAGITTA_RES_norm  = Systematic("MUON_SAGITTA_RESBIAS",configMgr.nomName,"_MUON_SAGITTA_RESBIAS__1up","_MUON_SAGITTA_RESBIAS__1down","tree",systype_norm)
MUON_SCALE_norm        = Systematic("MUON_SCALE",configMgr.nomName,"_MUON_SCALE__1up","_MUON_SCALE__1down","tree",systype_norm)

TRK_RES_D0_norm = Systematic("TRK_RES_D0_MEAS",configMgr.nomName,"_TRK_RES_D0_MEAS","_NoSys","tree",systype_normOneSide)
TRK_RES_Z0_norm = Systematic("TRK_RES_Z0_MEAS",configMgr.nomName,"_TRK_RES_Z0_MEAS","_NoSys","tree",systype_normOneSide)

pileup_norm                   = Systematic("PILEUP",weights,pileupWeight_UP,pileupWeight_DOWN,"weight",systype_norm)
jvt_norm                      = Systematic("JVT",weights,jvtWeight_JET_JvtEfficiency_UP,jvtWeight_JET_JvtEfficiency_DOWN,"weight",systype_norm)
fjvt_norm                     = Systematic("fJVT",weights,jvtWeight_JET_fJvtEfficiency_UP,jvtWeight_JET_fJvtEfficiency_DOWN,"weight",systype_norm)
EL_EFF_ID_norm                = Systematic("EL_EFF_ID",weights,leptonWeight_EL_EFF_ID_UP,leptonWeight_EL_EFF_ID_DOWN,"weight",systype_norm)
EL_EFF_ISO_norm               = Systematic("EL_EFF_ISO",weights,leptonWeight_EL_EFF_ISO_UP,leptonWeight_EL_EFF_ISO_DOWN,"weight",systype_norm)
EL_EFF_RECO_norm              = Systematic("EL_EFF_RECO",weights,leptonWeight_EL_EFF_RECO_UP,leptonWeight_EL_EFF_RECO_DOWN,"weight",systype_norm)
MUON_EFF_ISO_norm             = Systematic("MUON_EFF_ISO",weights,leptonWeight_MUON_EFF_ISO_STAT_UP,leptonWeight_MUON_EFF_ISO_STAT_DOWN,"weight",systype_norm)
MUON_EFF_ISO_SYS_norm         = Systematic("MUON_EFF_ISO_SYS",weights,leptonWeight_MUON_EFF_ISO_SYS_UP,leptonWeight_MUON_EFF_ISO_SYS_DOWN,"weight",systype_norm)
MUON_EFF_RECO_STAT_norm       = Systematic("MUON_EFF_RECO_STAT",weights,leptonWeight_MUON_EFF_RECO_STAT_UP,leptonWeight_MUON_EFF_RECO_STAT_DOWN,"weight",systype_norm)
MUON_EFF_RECO_STAT_LOWPT_norm = Systematic("MUON_EFF_RECO_STAT_LOWPT",weights,leptonWeight_MUON_EFF_RECO_STAT_LOWPT_UP,leptonWeight_MUON_EFF_RECO_STAT_LOWPT_DOWN,"weight",systype_norm) 
MUON_EFF_RECO_SYS_norm        = Systematic("MUON_EFF_RECO_SYS",weights,leptonWeight_MUON_EFF_RECO_SYS_UP,leptonWeight_MUON_EFF_RECO_SYS_DOWN,"weight",systype_norm)
MUON_EFF_RECO_SYS_LOWPT_norm  = Systematic("MUON_EFF_RECO_SYS_LOWPT",weights,leptonWeight_MUON_EFF_RECO_SYS_LOWPT_UP,leptonWeight_MUON_EFF_RECO_SYS_LOWPT_DOWN,"weight",systype_norm)
MUON_EFF_TTVA_STAT_norm       = Systematic("MUON_EFF_TTVA_STAT",weights,leptonWeight_MUON_EFF_TTVA_STAT_UP,leptonWeight_MUON_EFF_TTVA_STAT_DOWN,"weight",systype_norm)
MUON_EFF_TTVA_SYS_norm        = Systematic("MUON_EFF_TTVA_SYS",weights,leptonWeight_MUON_EFF_TTVA_SYS_UP,leptonWeight_MUON_EFF_TTVA_SYS_DOWN,"weight",systype_norm)
EL_EFF_TriggerEff_norm        = Systematic("EL_EFF_TriggerEff",weights,trigWeight_EL_EFF_TriggerEff_UP,trigWeight_EL_EFF_TriggerEff_DOWN,"weight",systype_norm)
EL_EFF_Trigger_norm           = Systematic("EL_EFF_Trigger",weights,trigWeight_EL_EFF_Trigger_UP,trigWeight_EL_EFF_Trigger_DOWN,"weight",systype_norm)
MUON_EFF_TriggerStat_norm     = Systematic("MU_EFF_TriggerStat",weights,trigWeight_MUON_EFF_TrigStat_UP,trigWeight_MUON_EFF_TrigStat_DOWN,"weight",systype_norm)
MUON_EFF_TriggerSyst_norm     = Systematic("MU_EFF_TriggerSyst",weights,trigWeight_MUON_EFF_TrigSyst_UP,trigWeight_MUON_EFF_TrigSyst_DOWN,"weight",systype_norm)
#bTAG_EFF_B_norm               = Systematic("bTAG_EFF_B",weights,bTagWeight_FT_EFF_B_systematics_UP,bTagWeight_FT_EFF_B_systematics_DOWN,"weight",systype_norm)
#bTAG_EFF_C_norm               = Systematic("bTAG_EFF_C",weights,bTagWeight_FT_EFF_C_systematics_UP,bTagWeight_FT_EFF_C_systematics_DOWN,"weight",systype_norm)
#bTAG_EFF_Light_norm           = Systematic("bTAG_EFF_Light",weights,bTagWeight_FT_EFF_Light_systematics_UP,bTagWeight_FT_EFF_Light_systematics_DOWN,"weight",systype_norm)
#bTAG_EFF_Extrapolation_norm   = Systematic("bTAG_EFF_Extrapolation",weights,bTagWeight_FT_EFF_extrapolation_UP,bTagWeight_FT_EFF_extrapolation_DOWN,"weight",systype_norm)
#bTAG_EFF_Extrapolation_C_norm = Systematic("bTAG_EFF_Extrapolation_from_charm",weights,bTagWeight_FT_EFF_extrapolation_from_charm_UP,bTagWeight_FT_EFF_extrapolation_from_charm_DOWN,"weight",systype_norm)

# Normalized background syst. list
NormSystList = [EG_Resolution_norm, EG_Scale_norm, JET_BJES_Response_norm, JET_EffectiveNP_1_norm, JET_EffectiveNP_2_norm, JET_EffectiveNP_3_norm, JET_EffectiveNP_4_norm, JET_EffectiveNP_5_norm, JET_EffectiveNP_6_norm, JET_EffectiveNP_7_norm, JET_EffectiveNP_8RestTerm_norm, JET_EtaIntercalibration_Modelling_norm, JET_EtaIntercalibration_NonClosure2018Data_norm, JET_EtaIntercalibration_NonClosureHighE_norm, JET_EtaIntercalibration_NonClosureNegEta_norm, JET_EtaIntercalibration_NonClosurePosEta_norm, JET_EtaIntercalibration_TotalStat_norm, JET_FlavorComposition_norm, JET_FlavorResponse_norm, JER_DataVsMC16_norm, JER_EffectiveNP_1_norm, JER_EffectiveNP_2_norm, JER_EffectiveNP_3_norm, JER_EffectiveNP_4_norm, JER_EffectiveNP_5_norm, JER_EffectiveNP_6_norm, JER_EffectiveNP_7RestTerm_norm, JET_Pileup_OffsetMu_norm, JET_Pileup_OffsetNPV_norm, JET_Pileup_PtTerm_norm, JET_Pileup_RhoTopology_norm, JET_PunchThrough_MC16_norm, JET_SingleParticleHighPt_norm, MET_Para_norm, MET_Perp_norm, MET_Scale_norm, MUON_ID_norm, MUON_MS_norm, MUON_SAGITTA_STAT_norm, MUON_SAGITTA_RES_norm, MUON_SCALE_norm, TRK_RES_D0_norm, TRK_RES_Z0_norm, pileup_norm, jvt_norm, fjvt_norm, EL_EFF_ID_norm, EL_EFF_ISO_norm, EL_EFF_RECO_norm, MUON_EFF_ISO_norm, MUON_EFF_ISO_SYS_norm, MUON_EFF_RECO_STAT_norm, MUON_EFF_RECO_STAT_LOWPT_norm, MUON_EFF_RECO_SYS_norm, MUON_EFF_RECO_SYS_LOWPT_norm, MUON_EFF_TTVA_STAT_norm, MUON_EFF_TTVA_SYS_norm, EL_EFF_TriggerEff_norm, EL_EFF_Trigger_norm, MUON_EFF_TriggerStat_norm, MUON_EFF_TriggerSyst_norm]
#NormSystList = [EG_Resolution_norm, EG_Scale_norm, JET_BJES_Response_norm, JET_EffectiveNP_1_norm, JET_EffectiveNP_2_norm, JET_EffectiveNP_3_norm, JET_EffectiveNP_4_norm, JET_EffectiveNP_5_norm, JET_EffectiveNP_6_norm, JET_EffectiveNP_7_norm, JET_EffectiveNP_8RestTerm_norm, JET_EtaIntercalibration_Modelling_norm, JET_EtaIntercalibration_NonClosure2018Data_norm, JET_EtaIntercalibration_NonClosureHighE_norm, JET_EtaIntercalibration_NonClosureNegEta_norm, JET_EtaIntercalibration_NonClosurePosEta_norm, JET_EtaIntercalibration_TotalStat_norm, JET_FlavorComposition_norm, JET_FlavorResponse_norm, JER_DataVsMC16_norm, JER_EffectiveNP_1_norm, JER_EffectiveNP_2_norm, JER_EffectiveNP_3_norm, JER_EffectiveNP_4_norm, JER_EffectiveNP_5_norm, JER_EffectiveNP_6_norm, JER_EffectiveNP_7RestTerm_norm, JET_Pileup_OffsetMu_norm, JET_Pileup_OffsetNPV_norm, JET_Pileup_PtTerm_norm, JET_Pileup_RhoTopology_norm, JET_PunchThrough_MC16_norm, JET_SingleParticleHighPt_norm, MET_Para_norm, MET_Perp_norm, MET_Scale_norm, MUON_ID_norm, MUON_MS_norm, MUON_SAGITTA_STAT_norm, MUON_SAGITTA_RES_norm, MUON_SCALE_norm, TRK_RES_D0_norm, TRK_RES_Z0_norm, pileup_norm, jvt_norm, fjvt_norm, EL_EFF_ID_norm, EL_EFF_ISO_norm, EL_EFF_RECO_norm, MUON_EFF_ISO_norm, MUON_EFF_ISO_SYS_norm, MUON_EFF_RECO_STAT_norm, MUON_EFF_RECO_STAT_LOWPT_norm, MUON_EFF_RECO_SYS_norm, MUON_EFF_RECO_SYS_LOWPT_norm, MUON_EFF_TTVA_STAT_norm, MUON_EFF_TTVA_SYS_norm, EL_EFF_TriggerEff_norm, EL_EFF_Trigger_norm, MUON_EFF_TriggerStat_norm, MUON_EFF_TriggerSyst_norm, bTAG_EFF_B_norm, bTAG_EFF_C_norm, bTAG_EFF_Light_norm, bTAG_EFF_Extrapolation_norm, bTAG_EFF_Extrapolation_C_norm]


#################################################################
#      Samples
#################################################################
if doABCDBkg or doExclusion or doDiscovery:

	# Samples used in all regions  
	data = Sample("Data",kBlack)
	data.setPrefixTreeName("data")
	data.setData()

	ttbar = Sample("ttbar",kGray+1)
	ttbar.setPrefixTreeName("ttbar")
	ttbar.setNormByTheory()

	top = Sample("top",kGray+2)	
	top.setPrefixTreeName("singletop")
	top.setNormByTheory()
	
	VV = Sample("VV",TColor.GetColor("#ffca99"))	
	VV.setPrefixTreeName("diboson")
	VV.setNormByTheory()
	
	Ztautau = Sample("Ztautau",TColor.GetColor("#cdffff"))	
	Ztautau.setPrefixTreeName("Zjets")
	Ztautau.setNormByTheory()

	WjetsTauHad = Sample("WjetsTauHad",kRed+2)
	WjetsTauHad.setPrefixTreeName("Wlnjets")
	WjetsTauHad.addSampleSpecificWeight("(TrackLabel == 4)")
	if doBkgValidation == False:
		WjetsTauHad.setNormRegions([('CR_tau_had_high','cuts')])
	else:
		WjetsTauHad.setNormRegions([('CR_tau_had_high','cuts'), ('CR_tau_had_low','cuts')])

	WjetsTauLep = Sample("WjetsTauLep",kRed+2)
	WjetsTauLep.setPrefixTreeName("Wlnjets")
	WjetsTauLep.addSampleSpecificWeight("(TrackLabel == 2 || TrackLabel == 3)")
	if doBkgValidation == False:
		WjetsTauLep.setNormRegions([('CR_tau_lep_high','cuts')])
	else:
		WjetsTauLep.setNormRegions([('CR_tau_lep_high','cuts'), ('CR_tau_lep_low','cuts')])
	
	WjetsQCD = Sample("WjetsQCD",kRed+2)
	WjetsQCD.setPrefixTreeName("Wlnjets")
	WjetsQCD.addSampleSpecificWeight("(TrackLabel == 0 || TrackLabel == 1 || TrackLabel == 5)")
	WjetsQCD.setNormByTheory()

	Zjets = Sample("Zjets",kRed)
	Zjets.setPrefixTreeName("Zjets")
	Zjets.setNormByTheory()
		
	dijet = Sample("dijet",kBlue+2)
	dijet.setPrefixTreeName("dijet")
	dijet.setNormByTheory()

	samples = [ [ttbar, top, Ztautau, VV, dijet, Zjets, WjetsQCD],
				[WjetsTauHad, WjetsTauLep]
			  ]
	
	for i in range(len(samples)):		
		for sp in samples[i]:
			sp.setStatConfig(useStat)

			if doSyst and i == 0:
				for syst in SystList: sp.addSystematic(syst)
			elif doSyst and i == 1:
				for syst in NormSystList: sp.addSystematic(syst)

			if doTestSystematics:
				sp.addSystematic(TestSystematic_10corr)
				sp.addSystematic(TestSystematic_10uncorr)

if doBackupCacheFile == False or (doBackupCacheFile and configMgr.forceNorm): 

	if doBkgValidation == False:
		data.addInputs(        ["/storage/alesala/ntuples_processed/ntuples_v5/NoGammaVeto/data.root"]      )			
		ttbar.addInputs(       ["/storage/alesala/ntuples_processed/ntuples_v5/NoGammaVeto/ttbar.root"]     )
		top.addInputs(         ["/storage/alesala/ntuples_processed/ntuples_v5/NoGammaVeto/singletop.root"] )
		VV.addInputs(	       ["/storage/alesala/ntuples_processed/ntuples_v5/NoGammaVeto/diboson.root"]   )
		Ztautau.addInputs(     ["/storage/alesala/ntuples_processed/ntuples_v5/NoGammaVeto/Ztautau.root"]   )
		WjetsTauHad.addInputs( ["/storage/alesala/ntuples_processed/ntuples_v5/NoGammaVeto/Wlnu.root"]      )
		WjetsTauLep.addInputs( ["/storage/alesala/ntuples_processed/ntuples_v5/NoGammaVeto/Wlnu.root"]      )
		WjetsQCD.addInputs(    ["/storage/alesala/ntuples_processed/ntuples_v5/NoGammaVeto/Wlnu.root"]      )
		Zjets.addInputs(       ["/storage/alesala/ntuples_processed/ntuples_v5/NoGammaVeto/Zjets.root"]     )
		dijet.addInputs(   	   ["/storage/alesala/ntuples_processed/ntuples_v5/NoGammaVeto/dijets.root"]    )
	else:
		data.addInputs(        ["/storage/alesala/ntuples_processed/ntuples_v5/GammaVeto/data.root"]      )			
		ttbar.addInputs(       ["/storage/alesala/ntuples_processed/ntuples_v5/GammaVeto/ttbar.root"]     )
		top.addInputs(         ["/storage/alesala/ntuples_processed/ntuples_v5/GammaVeto/singletop.root"] )
		VV.addInputs(	       ["/storage/alesala/ntuples_processed/ntuples_v5/GammaVeto/diboson.root"]   )
		Ztautau.addInputs(     ["/storage/alesala/ntuples_processed/ntuples_v5/GammaVeto/Ztautau.root"]   )
		WjetsTauHad.addInputs( ["/storage/alesala/ntuples_processed/ntuples_v5/GammaVeto/Wlnu.root"]      )
		WjetsTauLep.addInputs( ["/storage/alesala/ntuples_processed/ntuples_v5/GammaVeto/Wlnu.root"]      )
		WjetsQCD.addInputs(    ["/storage/alesala/ntuples_processed/ntuples_v5/GammaVeto/Wlnu.root"]      )
		Zjets.addInputs(       ["/storage/alesala/ntuples_processed/ntuples_v5/GammaVeto/Zjets.root"]     )
		dijet.addInputs(   	   ["/storage/alesala/ntuples_processed/ntuples_v5/GammaVeto/dijets.root"]    )

	commonSamples = [data, ttbar, top, WjetsTauHad, WjetsTauLep, WjetsQCD, Zjets, Ztautau, VV, dijet]


## Measurement parameters
measName = "BasicMeasurement"
measLumi = 1.
measLumiError = 0.017

## Cut channels parameters
cutsNBins = 1
cutsBinLow = 0.0
cutsBinHigh = 1.0

# Dictionary for plotting cosmetics
Plots = { }


############################################################################
#      ABCD Bkg Fit
############################################################################

if doABCDBkg:
	configMgr.doExclusion=False
	myTopLvl = configMgr.addFitConfig("ABCD")
	
	meas = myTopLvl.addMeasurement(measName, measLumi, measLumiError)
	meas.addPOI("mu_SIG")

	# SRs
	SR_A_0L_Sd0_20plus = myTopLvl.addChannel("cuts", ['SR_A_0L_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
	SR_A_0L_Sd0_8_20   = myTopLvl.addChannel("cuts", ['SR_A_0L_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
		
	myTopLvl.addSignalChannels([SR_A_0L_Sd0_20plus, SR_A_0L_Sd0_8_20])

	# CRs
	CR_D_0L             = myTopLvl.addChannel("cuts", ['CR_D_0L'], cutsNBins, cutsBinLow, cutsBinHigh)
	CR_B_1mu_Sd0_20plus = myTopLvl.addChannel("cuts", ['CR_B_1mu_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
	CR_B_1mu_Sd0_8_20   = myTopLvl.addChannel("cuts", ['CR_B_1mu_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
	CR_C_1mu            = myTopLvl.addChannel("cuts", ['CR_C_1mu'], cutsNBins, cutsBinLow, cutsBinHigh)
	CR_tau_had_high     = myTopLvl.addChannel("cuts", ['CR_tau_had_high'], cutsNBins, cutsBinLow, cutsBinHigh)
	CR_tau_lep_high     = myTopLvl.addChannel("cuts", ['CR_tau_lep_high'], cutsNBins, cutsBinLow, cutsBinHigh)

	myTopLvl.addBkgConstrainChannels([CR_D_0L, CR_B_1mu_Sd0_20plus, CR_B_1mu_Sd0_8_20, CR_C_1mu, CR_tau_had_high, CR_tau_lep_high])

	# CRs for the VRs
	if doBkgValidation:
		CR_C_0L         = myTopLvl.addChannel("cuts", ['CR_C_0L'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_C_1ele       = myTopLvl.addChannel("cuts", ['CR_C_1ele'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_C_ll         = myTopLvl.addChannel("cuts", ['CR_C_ll'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_C_1Y         = myTopLvl.addChannel("cuts", ['CR_C_1Y'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_D_1Y         = myTopLvl.addChannel("cuts", ['CR_D_1Y'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_C_jj         = myTopLvl.addChannel("cuts", ['CR_C_jj'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_D_jj         = myTopLvl.addChannel("cuts", ['CR_D_jj'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_tau_had_low  = myTopLvl.addChannel("cuts", ['CR_tau_had_low'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_tau_lep_low  = myTopLvl.addChannel("cuts", ['CR_tau_lep_low'], cutsNBins, cutsBinLow, cutsBinHigh)

		myTopLvl.addBkgConstrainChannels([CR_C_0L, CR_C_1ele, CR_C_ll, CR_C_1Y, CR_D_1Y, CR_C_jj, CR_D_jj, CR_tau_had_low, CR_tau_lep_low])

		# VRs
		VR_B_0L_Sd0_20plus   = myTopLvl.addChannel("cuts", ['VR_B_0L_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_B_0L_Sd0_8_20     = myTopLvl.addChannel("cuts", ['VR_B_0L_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_B_ll_Sd0_20plus   = myTopLvl.addChannel("cuts", ['VR_B_ll_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_B_ll_Sd0_8_20     = myTopLvl.addChannel("cuts", ['VR_B_ll_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_B_1ele_Sd0_20plus = myTopLvl.addChannel("cuts", ['VR_B_1ele_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_B_1ele_Sd0_8_20   = myTopLvl.addChannel("cuts", ['VR_B_1ele_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_A_1Y_Sd0_20plus   = myTopLvl.addChannel("cuts", ['VR_A_1Y_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_A_1Y_Sd0_8_20     = myTopLvl.addChannel("cuts", ['VR_A_1Y_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_B_1Y_Sd0_20plus   = myTopLvl.addChannel("cuts", ['VR_B_1Y_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_B_1Y_Sd0_8_20     = myTopLvl.addChannel("cuts", ['VR_B_1Y_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_A_jj_Sd0_20plus   = myTopLvl.addChannel("cuts", ['VR_A_jj_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_A_jj_Sd0_8_20     = myTopLvl.addChannel("cuts", ['VR_A_jj_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_B_jj_Sd0_20plus   = myTopLvl.addChannel("cuts", ['VR_B_jj_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_B_jj_Sd0_8_20     = myTopLvl.addChannel("cuts", ['VR_B_jj_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_tau_had_low       = myTopLvl.addChannel("cuts", ['VR_tau_had_low'], cutsNBins, cutsBinLow, cutsBinHigh)
		VR_tau_lep_low       = myTopLvl.addChannel("cuts", ['VR_tau_lep_low'], cutsNBins, cutsBinLow, cutsBinHigh)

		myTopLvl.addValidationChannels([VR_B_0L_Sd0_20plus, VR_B_0L_Sd0_8_20, VR_B_ll_Sd0_20plus, VR_B_ll_Sd0_8_20, VR_B_1ele_Sd0_20plus, VR_B_1ele_Sd0_8_20, VR_A_1Y_Sd0_20plus, VR_A_1Y_Sd0_8_20, VR_B_1Y_Sd0_20plus, VR_B_1Y_Sd0_8_20, VR_A_jj_Sd0_20plus, VR_A_jj_Sd0_8_20, VR_B_jj_Sd0_20plus, VR_B_jj_Sd0_8_20, VR_tau_had_low, VR_tau_lep_low])

	# Add all samples to their regions
	for sp in commonSamples:

		if "Zjets" not in sp.name and "QCD" not in sp.name:
			SR_A_0L_Sd0_20plus.addSample(sp)
			SR_A_0L_Sd0_8_20.addSample(sp)
			CR_D_0L.addSample(sp)
			CR_B_1mu_Sd0_20plus.addSample(sp)
			CR_B_1mu_Sd0_8_20.addSample(sp)
			CR_C_1mu.addSample(sp)
			
			if doBkgValidation:
				CR_C_0L.addSample(sp)
				CR_C_1ele.addSample(sp)
				CR_C_ll.addSample(sp)

				VR_B_0L_Sd0_20plus.addSample(sp)
				VR_B_0L_Sd0_8_20.addSample(sp)
				VR_B_ll_Sd0_20plus.addSample(sp)
				VR_B_ll_Sd0_8_20.addSample(sp)
				VR_B_1ele_Sd0_20plus.addSample(sp)
				VR_B_1ele_Sd0_8_20.addSample(sp)

				if "Zjets" not in sp.name and "dijet" not in sp.name:
					CR_C_1Y.addSample(sp)
					CR_D_1Y.addSample(sp)
					CR_C_jj.addSample(sp)
					CR_D_jj.addSample(sp)

					VR_A_1Y_Sd0_20plus.addSample(sp)
					VR_A_1Y_Sd0_8_20.addSample(sp)
					VR_B_1Y_Sd0_20plus.addSample(sp)
					VR_B_1Y_Sd0_8_20.addSample(sp)
					VR_A_jj_Sd0_20plus.addSample(sp)
					VR_A_jj_Sd0_8_20.addSample(sp)
					VR_B_jj_Sd0_20plus.addSample(sp)
					VR_B_jj_Sd0_8_20.addSample(sp)

		CR_tau_had_high.addSample(sp)
		CR_tau_lep_high.addSample(sp)

		if doBkgValidation:
			CR_tau_had_low.addSample(sp)
			CR_tau_lep_low.addSample(sp)
			
			VR_tau_had_low.addSample(sp)
			VR_tau_lep_low.addSample(sp)
		
	# Load theory systematics
	if doSyst:
		for ch in myTopLvl.channels:
			ChannelName = ch.name.split('cuts_')[1]
			for sp in TheoSysList[ChannelName]:
				if "Tau" in sp or "Lep" in sp:
					if TheoSysList[ChannelName][sp]['NewNominal'] == 0: continue
					ch.getSample(sp).addSampleSpecificWeight(str(TheoSysList[ChannelName][sp]['NewNominal']))
					ch.getSample(sp).addSampleSpecificWeight(str(1. / TheoSysList[ChannelName][sp]['Nominal']))
					ch.getSample(sp).addSystematic(Systematic("WjetsTau_TheoryUnc", configMgr.weights, TheoSysList[ChannelName][sp]['up'], TheoSysList[ChannelName][sp]['down'], 'user', 'userNormHistoSys'))
				elif "WjetsQCD" in sp or "Zjets" in sp or "Ztautau" in sp and TheoSysList[ChannelName][sp]['NewNominal'] != 0:
					if TheoSysList[ChannelName][sp]['NewNominal'] == 0: continue
					ch.getSample(sp).addSampleSpecificWeight(str(TheoSysList[ChannelName][sp]['NewNominal']))
					ch.getSample(sp).addSampleSpecificWeight(str(1. / TheoSysList[ChannelName][sp]['Nominal']))
					ch.getSample(sp).addSystematic(Systematic(sp + "_TheoryUnc", configMgr.weights, TheoSysList[ChannelName][sp]['up'], TheoSysList[ChannelName][sp]['down'], 'user', 'userHistoSys'))
				elif "schannel" not in sp and "tchannel" not in sp:
					ch.getSample(sp).addSystematic(Systematic(sp + "_TheoryUnc", configMgr.weights, TheoSysList[ChannelName][sp]['up'], TheoSysList[ChannelName][sp]['down'], 'user', 'userHistoSys'))

	# Dummy V+jets sample used for the ABCD bkg estimation 
	Vjets = Sample("Vjets_dummy", kRed)

	# Main ABCD method, SRs + CRs
	SR_A_0L_Sd0_20plus.addSample(Vjets)
	SR_A_0L_Sd0_20plus.getSample("Vjets_dummy").buildHisto([1000.], "SR_A_0L_Sd0_20plus", "cuts", 0.)
	SR_A_0L_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_D", 1., 1., 2.)
	SR_A_0L_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)
	SR_A_0L_Sd0_20plus.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad_high", 1., 0.8, 1.6)
	SR_A_0L_Sd0_20plus.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_high", 1., 0.8, 1.6)

	SR_A_0L_Sd0_8_20.addSample(Vjets)
	SR_A_0L_Sd0_8_20.getSample("Vjets_dummy").buildHisto([1000.], "SR_A_0L_Sd0_8_20", "cuts", 0.)
	SR_A_0L_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_D", 1., 1., 2.)
	SR_A_0L_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)
	SR_A_0L_Sd0_8_20.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad_high", 1., 0.8, 1.6)
	SR_A_0L_Sd0_8_20.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_high", 1., 0.8, 1.6)

	# Non-closure systematic
	SR_A_0L_Sd0_20plus.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_SR_A_0L_Sd0_20plus', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
	SR_A_0L_Sd0_8_20.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_SR_A_0L_Sd0_8_20', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))

	# CKKW & QSF systematics
	SR_A_0L_Sd0_20plus.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
	SR_A_0L_Sd0_20plus.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
	SR_A_0L_Sd0_20plus.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
	SR_A_0L_Sd0_20plus.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
	SR_A_0L_Sd0_8_20.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
	SR_A_0L_Sd0_8_20.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
	SR_A_0L_Sd0_8_20.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
	SR_A_0L_Sd0_8_20.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))

	CR_B_1mu_Sd0_20plus.addSample(Vjets)
	CR_B_1mu_Sd0_20plus.getSample("Vjets_dummy").buildHisto([1000.], "CR_B_1mu_Sd0_20plus", "cuts", 0.)
	CR_B_1mu_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)
	CR_B_1mu_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1mu_C", 1., 1., 15.)

	CR_B_1mu_Sd0_8_20.addSample(Vjets)
	CR_B_1mu_Sd0_8_20.getSample("Vjets_dummy").buildHisto([1000.], "CR_B_1mu_Sd0_8_20", "cuts", 0.)
	CR_B_1mu_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)
	CR_B_1mu_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1mu_C", 1., 1., 15.)

	CR_C_1mu.addSample(Vjets)
	CR_C_1mu.getSample("Vjets_dummy").buildHisto([1000.], "CR_C_1mu", "cuts", 0.)
	CR_C_1mu.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1mu_C", 1., 1., 15.)

	CR_D_0L.addSample(Vjets)
	CR_D_0L.getSample("Vjets_dummy").buildHisto([1000.], "CR_D_0L", "cuts", 0.)
	CR_D_0L.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_D", 1., 1., 2.)	
	CR_D_0L.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad_high", 1., 0.8, 1.6)	
	CR_D_0L.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_high", 1., 0.8, 1.6)
	
	# CKKW & QSF systematics
	CR_D_0L.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
	CR_D_0L.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
	CR_D_0L.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
	CR_D_0L.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))

	CR_tau_had_high.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad_high", 1., 0.8, 1.6)
	CR_tau_had_high.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_high", 1., 0.8, 1.6)
	CR_tau_lep_high.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_high", 1., 0.8, 1.6)
	
	# CKKW & QSF systematics
	CR_tau_had_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.22, 1. - 0.22, 'user', 'userNormHistoSys'))
	CR_tau_had_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.22, 1. - 0.22, 'user', 'userNormHistoSys'))
	CR_tau_had_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
	CR_tau_had_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
	CR_tau_lep_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.21, 1. - 0.21, 'user', 'userNormHistoSys'))
	CR_tau_lep_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.21, 1. - 0.21, 'user', 'userNormHistoSys'))
	CR_tau_lep_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.15, 1. - 0.15, 'user', 'userNormHistoSys'))
	CR_tau_lep_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.15, 1. - 0.15, 'user', 'userNormHistoSys'))

	if doBkgValidation:
		# Dummy Gjets, dijet samples used for the ABCD bkg validation
		Gjets = Sample("Gjets_dummy", kBlue)
		dijet = Sample("dijet_dummy", kBlue)
	
		# ABCD validation, VRs + CRs for the VRs
		# 0L CRs + VRs
		CR_tau_had_low.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad_low", 1., 0.8, 1.6)
		CR_tau_had_low.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_low", 1., 0.8, 1.6)
		CR_tau_lep_low.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_low", 1., 0.8, 1.6)
		
		CR_C_0L.addSample(Vjets)
		CR_C_0L.getSample("Vjets_dummy").buildHisto([1000.], "CR_C_0L", "cuts", 0.)
		CR_C_0L.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_C", 1., 25., 30.)
		CR_C_0L.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad_low", 1., 0.8, 1.6)	
		CR_C_0L.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_low", 1., 0.8, 1.6)

		VR_B_0L_Sd0_20plus.addSample(Vjets)
		VR_B_0L_Sd0_20plus.getSample("Vjets_dummy").buildHisto([1000.], "VR_B_0L_Sd0_20plus", "cuts", 0.)
		VR_B_0L_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_C", 1., 25., 30.)
		VR_B_0L_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)
		VR_B_0L_Sd0_20plus.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad_low", 1., 0.8, 1.6)
		VR_B_0L_Sd0_20plus.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_low", 1., 0.8, 1.6)
			
		VR_B_0L_Sd0_8_20.addSample(Vjets)
		VR_B_0L_Sd0_8_20.getSample("Vjets_dummy").buildHisto([1000.], "VR_B_0L_Sd0_8_20", "cuts", 0.)
		VR_B_0L_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_C", 1., 25., 30.)
		VR_B_0L_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)
		VR_B_0L_Sd0_8_20.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad_low", 1., 0.8, 1.6)
		VR_B_0L_Sd0_8_20.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_low", 1., 0.8, 1.6)

		# Non-closure systematic
		VR_B_0L_Sd0_20plus.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_VR_B_0L_Sd0_20plus', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
		VR_B_0L_Sd0_8_20.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_VR_B_0L_Sd0_8_20', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))

		# 1ele CRs + VRs
		CR_C_1ele.addSample(Vjets)
		CR_C_1ele.getSample("Vjets_dummy").buildHisto([1000.], "CR_C_1ele", "cuts", 0.)
		CR_C_1ele.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1ele_C", 1., 5., 10.)
		
		VR_B_1ele_Sd0_20plus.addSample(Vjets)
		VR_B_1ele_Sd0_20plus.getSample("Vjets_dummy").buildHisto([1000.], "VR_B_1ele_Sd0_20plus", "cuts", 0.)
		VR_B_1ele_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1ele_C", 1., 5., 10.)
		VR_B_1ele_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)
		
		VR_B_1ele_Sd0_8_20.addSample(Vjets)
		VR_B_1ele_Sd0_8_20.getSample("Vjets_dummy").buildHisto([1000.], "VR_B_1ele_Sd0_8_20", "cuts", 0.)
		VR_B_1ele_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1ele_C", 1., 5., 10.)
		VR_B_1ele_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)

		# Non-closure systematic
		VR_B_1ele_Sd0_20plus.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_VR_B_1e_Sd0_20plus', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
		VR_B_1ele_Sd0_8_20.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_VR_B_1e_Sd0_8_20', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))

		# 2L CRs + VRs
		CR_C_ll.addSample(Vjets)
		CR_C_ll.getSample("Vjets_dummy").buildHisto([1000.], "CR_C_ll", "cuts", 0.)
		CR_C_ll.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_ll_C", 1., 4., 7.)

		VR_B_ll_Sd0_20plus.addSample(Vjets)
		VR_B_ll_Sd0_20plus.getSample("Vjets_dummy").buildHisto([1000.], "VR_B_ll_Sd0_20plus", "cuts", 0.)
		VR_B_ll_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_ll_C", 1., 4., 7.)
		VR_B_ll_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)

		VR_B_ll_Sd0_8_20.addSample(Vjets)
		VR_B_ll_Sd0_8_20.getSample("Vjets_dummy").buildHisto([1000.], "VR_B_ll_Sd0_8_20", "cuts", 0.)
		VR_B_ll_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_ll_C", 1., 4., 7.)
		VR_B_ll_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)
		
		# Non-closure systematic
		VR_B_ll_Sd0_20plus.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_VR_B_ll_Sd0_20plus', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
		VR_B_ll_Sd0_8_20.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_VR_B_ll_Sd0_8_20', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))

		# 1Y CRs + VRs
		CR_C_1Y.addSample(Gjets)
		CR_C_1Y.getSample("Gjets_dummy").buildHisto([1000.], "CR_C_1Y", "cuts", 0.)
		CR_C_1Y.getSample("Gjets_dummy").addNormFactor("mu_Gjets_nf_1Y_C", 1., 85., 90.)

		CR_D_1Y.addSample(Gjets)
		CR_D_1Y.getSample("Gjets_dummy").buildHisto([1000.], "CR_D_1Y", "cuts", 0.)
		CR_D_1Y.getSample("Gjets_dummy").addNormFactor("mu_Gjets_nf_1Y_D", 1., 1., 2.)	

		VR_A_1Y_Sd0_20plus.addSample(Gjets)
		VR_A_1Y_Sd0_20plus.getSample("Gjets_dummy").buildHisto([1000.], "VR_A_1Y_Sd0_20plus", "cuts", 0.)
		VR_A_1Y_Sd0_20plus.getSample("Gjets_dummy").addNormFactor("mu_Gjets_nf_1Y_D", 1., 1., 2.)
		VR_A_1Y_Sd0_20plus.getSample("Gjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)

		VR_A_1Y_Sd0_8_20.addSample(Gjets)
		VR_A_1Y_Sd0_8_20.getSample("Gjets_dummy").buildHisto([1000.], "VR_A_1Y_Sd0_8_20", "cuts", 0.)
		VR_A_1Y_Sd0_8_20.getSample("Gjets_dummy").addNormFactor("mu_Gjets_nf_1Y_D", 1., 1., 2.)
		VR_A_1Y_Sd0_8_20.getSample("Gjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)

		VR_B_1Y_Sd0_20plus.addSample(Gjets)
		VR_B_1Y_Sd0_20plus.addSample(dijet)
		VR_B_1Y_Sd0_20plus.getSample("Gjets_dummy").buildHisto([1000.], "VR_B_1Y_Sd0_20plus", "cuts", 0.)
		VR_B_1Y_Sd0_20plus.getSample("Gjets_dummy").addNormFactor("mu_Gjets_nf_1Y_C", 1., 85., 90.)
		VR_B_1Y_Sd0_20plus.getSample("Gjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)

		VR_B_1Y_Sd0_8_20.addSample(Gjets)
		VR_B_1Y_Sd0_8_20.addSample(dijet)
		VR_B_1Y_Sd0_8_20.getSample("Gjets_dummy").buildHisto([1000.], "VR_B_1Y_Sd0_8_20", "cuts", 0.)
		VR_B_1Y_Sd0_8_20.getSample("Gjets_dummy").addNormFactor("mu_Gjets_nf_1Y_C", 1., 85., 90.)
		VR_B_1Y_Sd0_8_20.getSample("Gjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)
		
		# Non-closure systematic
		VR_A_1Y_Sd0_20plus.getSample("Gjets_dummy").addSystematic(Systematic('TF_Envelope_VR_A_1Y_Sd0_20plus', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
		VR_A_1Y_Sd0_8_20.getSample("Gjets_dummy").addSystematic(Systematic('TF_Envelope_VR_A_1Y_Sd0_8_20', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
		VR_B_1Y_Sd0_20plus.getSample("Gjets_dummy").addSystematic(Systematic('TF_Envelope_VR_B_1Y_Sd0_20plus', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
		VR_B_1Y_Sd0_8_20.getSample("Gjets_dummy").addSystematic(Systematic('TF_Envelope_VR_B_1Y_Sd0_8_20', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))

		# 1Y base CRs + VRs
		CR_C_jj.addSample(dijet)
		CR_C_jj.getSample("dijet_dummy").buildHisto([1000.], "CR_C_jj", "cuts", 0.)
		CR_C_jj.getSample("dijet_dummy").addNormFactor("mu_dijet_nf_C", 1., 65., 70.)

		CR_D_jj.addSample(dijet)
		CR_D_jj.getSample("dijet_dummy").buildHisto([1000.], "CR_D_jj", "cuts", 0.)
		CR_D_jj.getSample("dijet_dummy").addNormFactor("mu_dijet_nf_D", 1., 1., 3.)

		VR_A_jj_Sd0_20plus.addSample(dijet)
		VR_A_jj_Sd0_20plus.getSample("dijet_dummy").buildHisto([1000.], "VR_A_jj_Sd0_20plus", "cuts", 0.)
		VR_A_jj_Sd0_20plus.getSample("dijet_dummy").addNormFactor("mu_dijet_nf_D", 1., 1., 3.)
		VR_A_jj_Sd0_20plus.getSample("dijet_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)

		VR_A_jj_Sd0_8_20.addSample(dijet)
		VR_A_jj_Sd0_8_20.getSample("dijet_dummy").buildHisto([1000.], "VR_A_jj_Sd0_8_20", "cuts", 0.)
		VR_A_jj_Sd0_8_20.getSample("dijet_dummy").addNormFactor("mu_dijet_nf_D", 1., 1., 3.)
		VR_A_jj_Sd0_8_20.getSample("dijet_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)

		VR_B_jj_Sd0_20plus.addSample(dijet)
		VR_B_jj_Sd0_20plus.getSample("dijet_dummy").buildHisto([1000.], "VR_B_jj_Sd0_20plus", "cuts", 0.)
		VR_B_jj_Sd0_20plus.getSample("dijet_dummy").addNormFactor("mu_dijet_nf_C", 1., 65., 70.)
		VR_B_jj_Sd0_20plus.getSample("dijet_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)

		VR_B_jj_Sd0_8_20.addSample(dijet)
		VR_B_jj_Sd0_8_20.getSample("dijet_dummy").buildHisto([1000.], "VR_B_jj_Sd0_8_20", "cuts", 0.)
		VR_B_jj_Sd0_8_20.getSample("dijet_dummy").addNormFactor("mu_dijet_nf_C", 1., 65., 70.)
		VR_B_jj_Sd0_8_20.getSample("dijet_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)
		
		# Non-closure systematic
		VR_A_jj_Sd0_20plus.getSample("dijet_dummy").addSystematic(Systematic('TF_Envelope_VR_A_jj_Sd0_20plus', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
		VR_A_jj_Sd0_8_20.getSample("dijet_dummy").addSystematic(Systematic('TF_Envelope_VR_A_jj_Sd0_8_20', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
		VR_B_jj_Sd0_20plus.getSample("dijet_dummy").addSystematic(Systematic('TF_Envelope_VR_B_jj_Sd0_20plus', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
		VR_B_jj_Sd0_8_20.getSample("dijet_dummy").addSystematic(Systematic('TF_Envelope_VR_B_jj_Sd0_8_20', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))

		# Tau validation
		VR_tau_had_low.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad_low", 1., 0.8, 1.6)
		VR_tau_had_low.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_low", 1., 0.8, 1.6)
		VR_tau_lep_low.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep_low", 1., 0.8, 1.6)


#####################################################################
#         Exclusion fit
#####################################################################

if whichPoint != '':
    sigSamples_to_run = [whichPoint]
else: # no --point provided 
    sigSamples_to_run = sigSamples_grid

if doExclusion:
	configMgr.doExclusion = True
	for sig in sigSamples_to_run:
		
		myTopLvl = configMgr.addFitConfig("higgsino_%s"%sig)
		meas = myTopLvl.addMeasurement(measName, measLumi, measLumiError)
		meas.addPOI("mu_SIG")
				
		sigSample = Sample("higgsino_" + sig,kPink)
		sigSample.setPrefixTreeName("Higgsino")
		sigSample.addInput("/storage/alesala/ntuples_processed/ntuples_v5/NoGammaVeto/higgsino_" + sig + ".root")
		sigSample.setStatConfig(useStat)
		sigSample.setNormFactor("mu_SIG", 1., 0., 2.)
		sigSample.addSampleSpecificWeight("((1000))") # Temporary fix to xsec wrong units
		if doSyst:
			for syst in SignalSystList:
				sigSample.addSystematic(syst)
		myTopLvl.addSamples(sigSample)
		myTopLvl.setSignalSample(sigSample)
	
		# SRs
		SR_A_0L_Sd0_20plus = myTopLvl.addChannel("cuts", ['SR_A_0L_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
		SR_A_0L_Sd0_8_20   = myTopLvl.addChannel("cuts", ['SR_A_0L_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
			
		myTopLvl.addSignalChannels([SR_A_0L_Sd0_20plus, SR_A_0L_Sd0_8_20])

		# CRs
		CR_D_0L             = myTopLvl.addChannel("cuts", ['CR_D_0L'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_B_1mu_Sd0_20plus = myTopLvl.addChannel("cuts", ['CR_B_1mu_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_B_1mu_Sd0_8_20   = myTopLvl.addChannel("cuts", ['CR_B_1mu_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_C_1mu            = myTopLvl.addChannel("cuts", ['CR_C_1mu'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_tau_had_high     = myTopLvl.addChannel("cuts", ['CR_tau_had_high'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_tau_lep_high     = myTopLvl.addChannel("cuts", ['CR_tau_lep_high'], cutsNBins, cutsBinLow, cutsBinHigh)

		myTopLvl.addBkgConstrainChannels([CR_D_0L, CR_B_1mu_Sd0_20plus, CR_B_1mu_Sd0_8_20, CR_C_1mu, CR_tau_had_high, CR_tau_lep_high])

		# Add all samples to their regions
		for sp in commonSamples:
			
			if "Zjets" not in sp.name and "QCD" not in sp.name:
				SR_A_0L_Sd0_20plus.addSample(sp)
				SR_A_0L_Sd0_8_20.addSample(sp)
				CR_D_0L.addSample(sp)
				CR_B_1mu_Sd0_20plus.addSample(sp)
				CR_B_1mu_Sd0_8_20.addSample(sp)
				CR_C_1mu.addSample(sp)
	
			CR_tau_had_high.addSample(sp)
			CR_tau_lep_high.addSample(sp)
	
		# Load theory systematics
		if doSyst:
			for ch in myTopLvl.channels:
				ChannelName = ch.name.split('cuts_')[1]
				for sp in TheoSysList[ChannelName]:
					if "Tau" in sp or "Lep" in sp:
						if TheoSysList[ChannelName][sp]['NewNominal'] == 0: continue
						ch.getSample(sp).addSampleSpecificWeight(str(TheoSysList[ChannelName][sp]['NewNominal']))
						ch.getSample(sp).addSampleSpecificWeight(str(1. / TheoSysList[ChannelName][sp]['Nominal']))
						ch.getSample(sp).addSystematic(Systematic("WjetsTau_TheoryUnc", configMgr.weights, TheoSysList[ChannelName][sp]['up'], TheoSysList[ChannelName][sp]['down'], 'user', 'userNormHistoSys'))
					elif "WjetsQCD" in sp or "Zjets" in sp or "Ztautau" in sp and TheoSysList[ChannelName][sp]['NewNominal'] != 0:
						if TheoSysList[ChannelName][sp]['NewNominal'] == 0: continue
						ch.getSample(sp).addSampleSpecificWeight(str(TheoSysList[ChannelName][sp]['NewNominal']))
						ch.getSample(sp).addSampleSpecificWeight(str(1. / TheoSysList[ChannelName][sp]['Nominal']))
						ch.getSample(sp).addSystematic(Systematic(sp + "_TheoryUnc", configMgr.weights, TheoSysList[ChannelName][sp]['up'], TheoSysList[ChannelName][sp]['down'], 'user', 'userHistoSys'))
					elif "schannel" not in sp and "tchannel" not in sp:
						ch.getSample(sp).addSystematic(Systematic(sp + "_TheoryUnc", configMgr.weights, TheoSysList[ChannelName][sp]['up'], TheoSysList[ChannelName][sp]['down'], 'user', 'userHistoSys'))

		# Dummy V+jets sample used for the ABCD bkg estimation and dummy 
		Vjets = Sample("Vjets_dummy", kRed)
		
		# Main ABCD method, SRs + CRs
		SR_A_0L_Sd0_20plus.addSample(Vjets)
		SR_A_0L_Sd0_20plus.getSample("Vjets_dummy").buildHisto([10.89], "SR_A_0L_Sd0_20plus", "cuts", 0.)
		SR_A_0L_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_D", 1., 1., 2.)
		SR_A_0L_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)
		SR_A_0L_Sd0_20plus.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad", 1., 0.8, 1.6)
		SR_A_0L_Sd0_20plus.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep", 1., 0.8, 1.6)

		SR_A_0L_Sd0_8_20.addSample(Vjets)
		SR_A_0L_Sd0_8_20.getSample("Vjets_dummy").buildHisto([17.83], "SR_A_0L_Sd0_8_20", "cuts", 0.)
		SR_A_0L_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_D", 1., 1., 2.)
		SR_A_0L_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)
		SR_A_0L_Sd0_8_20.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad", 1., 0.8, 1.6)
		SR_A_0L_Sd0_8_20.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep", 1., 0.8, 1.6)
		
		# Non-closure systematic
		SR_A_0L_Sd0_20plus.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_SR_A_0L_Sd0_20plus', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
		SR_A_0L_Sd0_8_20.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_SR_A_0L_Sd0_8_20', configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
	
		# CKKW & QSF systematics
		SR_A_0L_Sd0_20plus.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
		SR_A_0L_Sd0_20plus.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
		SR_A_0L_Sd0_20plus.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
		SR_A_0L_Sd0_20plus.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
		SR_A_0L_Sd0_8_20.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
		SR_A_0L_Sd0_8_20.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
		SR_A_0L_Sd0_8_20.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
		SR_A_0L_Sd0_8_20.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))

		CR_B_1mu_Sd0_20plus.addSample(Vjets)
		CR_B_1mu_Sd0_20plus.getSample("Vjets_dummy").buildHisto([10.89], "CR_B_1mu_Sd0_20plus", "cuts", 0.)
		CR_B_1mu_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)
		CR_B_1mu_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1mu_C", 1., 1., 15.)

		CR_B_1mu_Sd0_8_20.addSample(Vjets)
		CR_B_1mu_Sd0_8_20.getSample("Vjets_dummy").buildHisto([17.83], "CR_B_1mu_Sd0_8_20", "cuts", 0.)
		CR_B_1mu_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)
		CR_B_1mu_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1mu_C", 1., 1., 15.)

		CR_C_1mu.addSample(Vjets)
		CR_C_1mu.getSample("Vjets_dummy").buildHisto([1000.], "CR_C_1mu", "cuts", 0.)
		CR_C_1mu.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1mu_C", 1., 1., 15.)

		CR_D_0L.addSample(Vjets)
		CR_D_0L.getSample("Vjets_dummy").buildHisto([1000.], "CR_D_0L", "cuts", 0.)
		CR_D_0L.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_D", 1., 1., 2.)	
		CR_D_0L.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad", 1., 0.8, 1.6)	
		CR_D_0L.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep", 1., 0.8, 1.6)
		
		# CKKW & QSF systematics
		CR_D_0L.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
		CR_D_0L.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
		CR_D_0L.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
		CR_D_0L.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))

		CR_tau_had_high.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad", 1., 0.8, 1.6)
		CR_tau_had_high.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep", 1., 0.8, 1.6)
		CR_tau_lep_high.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep", 1., 0.8, 1.6)
		
		# CKKW & QSF systematics
		CR_tau_had_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.22, 1. - 0.22, 'user', 'userNormHistoSys'))
		CR_tau_had_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.22, 1. - 0.22, 'user', 'userNormHistoSys'))
		CR_tau_had_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
		CR_tau_had_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
		CR_tau_lep_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.21, 1. - 0.21, 'user', 'userNormHistoSys'))
		CR_tau_lep_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.21, 1. - 0.21, 'user', 'userNormHistoSys'))
		CR_tau_lep_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.15, 1. - 0.15, 'user', 'userNormHistoSys'))
		CR_tau_lep_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.15, 1. - 0.15, 'user', 'userNormHistoSys'))


############################################################
#           Discovery fit
############################################################

if doDiscovery:
	configMgr.doExclusion = False
	configMgr.blindSR = False
	channels_to_run_Discovery = ['SR_A_0L_Sd0_20plus', 'SR_A_0L_Sd0_8_20']

	for ch in channels_to_run_Discovery:
		myTopLvl = configMgr.addFitConfig("Discovery_%s"%ch)
		meas = myTopLvl.addMeasurement(measName, measLumi, measLumiError)
		meas.addPOI("mu_SIG_%s"%ch)

		# SR needs to use the [0.5, 1.5] binning to have correct behaviour 
		# with 'cuts'. Same thing with all dummy samples created below
		SR = myTopLvl.addChannel("cuts", [ch], 1, 0.5, 1.5)
		
		myTopLvl.addSignalChannels([SR])
	
		# Dummy signal sample needed to run discovery fit
		SR.addDiscoverySamples(["SIG_%s"%ch], [1.], [0.], [100.], [kMagenta])
			
		# CRs
		if ch == 'SR_A_0L_Sd0_20plus':
			CR_B_1mu_Sd0_20plus = myTopLvl.addChannel("cuts", ['CR_B_1mu_Sd0_20plus'], cutsNBins, cutsBinLow, cutsBinHigh)
		else:
			CR_B_1mu_Sd0_8_20   = myTopLvl.addChannel("cuts", ['CR_B_1mu_Sd0_8_20'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_D_0L             = myTopLvl.addChannel("cuts", ['CR_D_0L'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_C_1mu            = myTopLvl.addChannel("cuts", ['CR_C_1mu'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_tau_had_high     = myTopLvl.addChannel("cuts", ['CR_tau_had_high'], cutsNBins, cutsBinLow, cutsBinHigh)
		CR_tau_lep_high     = myTopLvl.addChannel("cuts", ['CR_tau_lep_high'], cutsNBins, cutsBinLow, cutsBinHigh)

		if ch == 'SR_A_0L_Sd0_20plus':
			myTopLvl.addBkgConstrainChannels([CR_D_0L, CR_B_1mu_Sd0_20plus, CR_C_1mu, CR_tau_had_high, CR_tau_lep_high])
		else:
			myTopLvl.addBkgConstrainChannels([CR_D_0L, CR_B_1mu_Sd0_8_20, CR_C_1mu, CR_tau_had_high, CR_tau_lep_high])

		# Add all samples to their regions
		for sp in commonSamples:
			
			if "Zjets" not in sp.name and "QCD" not in sp.name:
				SR.addSample(sp)
				CR_D_0L.addSample(sp)
				CR_C_1mu.addSample(sp)
				if ch == 'SR_A_0L_Sd0_20plus':
					CR_B_1mu_Sd0_20plus.addSample(sp)
				else:
					CR_B_1mu_Sd0_8_20.addSample(sp)
	
			CR_tau_had_high.addSample(sp)
			CR_tau_lep_high.addSample(sp)

		# Load theory systematics
		if doSyst:
			for ch in myTopLvl.channels:
				ChannelName = ch.name.split('cuts_')[1]
				for sp in TheoSysList[ChannelName]:
					if "Tau" in sp or "Lep" in sp:
						if TheoSysList[ChannelName][sp]['NewNominal'] == 0: continue
						ch.getSample(sp).addSampleSpecificWeight(str(TheoSysList[ChannelName][sp]['NewNominal']))
						ch.getSample(sp).addSampleSpecificWeight(str(1. / TheoSysList[ChannelName][sp]['Nominal']))
						ch.getSample(sp).addSystematic(Systematic("WjetsTau_TheoryUnc", configMgr.weights, TheoSysList[ChannelName][sp]['up'], TheoSysList[ChannelName][sp]['down'], 'user', 'userNormHistoSys'))
					elif "WjetsQCD" in sp or "Zjets" in sp or "Ztautau" in sp and TheoSysList[ChannelName][sp]['NewNominal'] != 0:
						if TheoSysList[ChannelName][sp]['NewNominal'] == 0: continue
						ch.getSample(sp).addSampleSpecificWeight(str(TheoSysList[ChannelName][sp]['NewNominal']))
						ch.getSample(sp).addSampleSpecificWeight(str(1. / TheoSysList[ChannelName][sp]['Nominal']))
						ch.getSample(sp).addSystematic(Systematic(sp + "_TheoryUnc", configMgr.weights, TheoSysList[ChannelName][sp]['up'], TheoSysList[ChannelName][sp]['down'], 'user', 'userHistoSys'))
					elif "schannel" not in sp and "tchannel" not in sp:
						ch.getSample(sp).addSystematic(Systematic(sp + "_TheoryUnc", configMgr.weights, TheoSysList[ChannelName][sp]['up'], TheoSysList[ChannelName][sp]['down'], 'user', 'userHistoSys'))
		
		# Dummy V+jets sample used for the ABCD bkg estimation and dummy 
		Vjets = Sample("Vjets_dummy", kRed)
		
		if ch == 'SR_A_0L_Sd0_20plus':
			SR.addSample(Vjets)
			SR.getSample("Vjets_dummy").buildHisto([1000.], [ch] , "cuts", 0.5)
			SR.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_D", 1., 1., 2.)
			SR.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)
			SR.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad", 1., 0.8, 1.6)
			SR.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep", 1., 0.8, 1.6)
		
			# Non-closure systematic
			SR.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_%s'%ch, configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
			
			# CKKW & QSF systematics
			SR.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
			SR.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
			SR.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
			SR.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
		
			CR_B_1mu_Sd0_20plus.addSample(Vjets)
			CR_B_1mu_Sd0_20plus.getSample("Vjets_dummy").buildHisto([1000.], "CR_B_1mu_Sd0_20plus", "cuts", 0.5)
			CR_B_1mu_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_20plus", 1., 0., 1.)
			CR_B_1mu_Sd0_20plus.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1mu_C", 1., 1., 15.)
		else:
			SR.addSample(Vjets)
			SR.getSample("Vjets_dummy").buildHisto([1000.], [ch], "cuts", 0.5)
			SR.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_D", 1., 1., 2.)
			SR.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)
			SR.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad", 1., 0.8, 1.6)
			SR.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep", 1., 0.8, 1.6)
		
			# Non-closure systematic
			SR.getSample("Vjets_dummy").addSystematic(Systematic('TF_Envelope_%s'%ch, configMgr.weights, 1. + 0.1, 1. - 0.1, 'user', 'userHistoSys'))
			
			# CKKW & QSF systematics
			SR.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
			SR.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
			SR.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
			SR.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
		
			CR_B_1mu_Sd0_8_20.addSample(Vjets)
			CR_B_1mu_Sd0_8_20.getSample("Vjets_dummy").buildHisto([1000.], "CR_B_1mu_Sd0_8_20", "cuts", 0.5)
			CR_B_1mu_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_tf_Sd0_8_20", 1., 0., 1.)
			CR_B_1mu_Sd0_8_20.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1mu_C", 1., 1., 15.)
		
		CR_C_1mu.addSample(Vjets)
		CR_C_1mu.getSample("Vjets_dummy").buildHisto([1000.], "CR_C_1mu", "cuts", 0.5)
		CR_C_1mu.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_1mu_C", 1., 1., 15.)

		CR_D_0L.addSample(Vjets)
		CR_D_0L.getSample("Vjets_dummy").buildHisto([1000.], "CR_D_0L", "cuts", 0.5)
		CR_D_0L.getSample("Vjets_dummy").addNormFactor("mu_Vjets_nf_0L_D", 1., 1., 2.)	
		CR_D_0L.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad", 1., 0.8, 1.6)	
		CR_D_0L.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep", 1., 0.8, 1.6)
		
		# CKKW & QSF systematics
		CR_D_0L.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
		CR_D_0L.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.01, 1. - 0.01, 'user', 'userNormHistoSys'))
		CR_D_0L.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
		CR_D_0L.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))

		CR_tau_had_high.getSample("WjetsTauHad").addNormFactor("mu_WjetsTauHad", 1., 0.8, 1.6)
		CR_tau_had_high.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep", 1., 0.8, 1.6)
		CR_tau_lep_high.getSample("WjetsTauLep").addNormFactor("mu_WjetsTauLep", 1., 0.8, 1.6)
		
		# CKKW & QSF systematics
		CR_tau_had_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.22, 1. - 0.22, 'user', 'userNormHistoSys'))
		CR_tau_had_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.22, 1. - 0.22, 'user', 'userNormHistoSys'))
		CR_tau_had_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
		CR_tau_had_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.06, 1. - 0.06, 'user', 'userNormHistoSys'))
		CR_tau_lep_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.21, 1. - 0.21, 'user', 'userNormHistoSys'))
		CR_tau_lep_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_CKKW_TheoryUnc', configMgr.weights, 1 + 0.21, 1. - 0.21, 'user', 'userNormHistoSys'))
		CR_tau_lep_high.getSample("WjetsTauHad").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.15, 1. - 0.15, 'user', 'userNormHistoSys'))
		CR_tau_lep_high.getSample("WjetsTauLep").addSystematic(Systematic('WjetsTau_QSF_TheoryUnc', configMgr.weights, 1 + 0.15, 1. - 0.15, 'user', 'userNormHistoSys'))

