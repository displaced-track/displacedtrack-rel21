import ROOT
from ROOT import *
import os


ROOT.gROOT.LoadMacro("analysis.C")

#path = "/storage/amurrone/DisplacedTrack/ntuples/"
path = "/storage/amurrone/DisplacedTrack/ntuples_Sept2021_TightEvtCleaning/"
dir_list = os.listdir(path)


for sample in dir_list:
    if "user.tlari" and "Znunu" in sample:
        dir = sample+"/data-tree/"
        infile = path+dir+sample+".root"
        chain = TChain("tree_NoSys")
        chain.Add(infile)
        print ("Read tree with ", chain.GetEntries(), " entries")
        ana = ROOT.analysis()
        ana.Loop(chain, -1, True)
        #ana.Write(sample+".root")
        ana.Write("JetTightCleaning/"+sample+".root")
