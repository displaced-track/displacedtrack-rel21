//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Apr 27 15:18:15 2020 by ROOT version 6.14/06
// from TTree analysis/My analysis ntuple
// found on file: higgsino_101p0_100p0.root
//////////////////////////////////////////////////////////

#ifndef analysis_h
#define analysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>
#include <TProfile.h>
#include <math.h>
#include <TLorentzVector.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class analysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types

  Int_t           year;
  Float_t         mu;
  Float_t         actual_mu;
  Int_t           nVtx;
  Bool_t          passEmulTightJetCleaning1Jet;
  Bool_t          passEmulTightJetCleaning2Jet;
  vector<float>   *elePt;
  vector<float>   *eleEta;
  vector<float>   *elePhi;
  vector<float>   *muonPt;
  vector<float>   *muonEta;
  vector<float>   *muonPhi;
  vector<float>   *muonM;
  vector<int>     *muonCharge;
  vector<bool>    *trkisTight;
  vector<float>   *trkPt;
  vector<float>   *trkEta;
  vector<float>   *trkTheta;
  vector<float>   *trkPhi;
  vector<int>     *trkQ;
  vector<float>   *trkZ0;
  vector<float>   *trkZ0Err;
  vector<float>   *trkZ0sinTheta;
  vector<float>   *trkD0;
  vector<float>   *trkD0Err;
  vector<float>   *trkD0Sig;
  vector<int>     *trkOrigin;
  vector<float>   *trkFitQuality;
  vector<int>     *nIBLHits;
  vector<int>     *nB0LayerHits;
  vector<int>     *nPixLayers;
  vector<int>     *nExpIBLHits;
  vector<int>     *nExpB0LayerHits;
  vector<int>     *nPixHits;
  vector<int>     *nPixHoles;
  vector<int>     *nPixSharedHits;
  vector<int>     *nSCTHits;
  vector<int>     *nSCTHoles;
  vector<int>     *nSCTSharedHits;
  vector<float>   *jetPt;
  vector<float>   *jetEta;
  vector<float>   *jetPhi;
  vector<float>   *jetM;
  vector<float>   *jetJVT;
  Float_t         met_Et;
  Float_t         met_Phi;
  Float_t         metTruth_Et;
  Float_t         metTruth_Phi;
  Float_t         met_Signif;
  Float_t         pileupWeight;
  Float_t         leptonWeight;
  Float_t         eventWeight;
  Float_t         genWeight;
  Float_t         bTagWeight;
  Float_t         jvtWeight;
  Float_t         genWeightUp;
  Float_t         genWeightDown;
  ULong64_t       PRWHash;
  ULong64_t       EventNumber;
  Float_t         xsec;
  Float_t         GenHt;
  Float_t         GenMET;
  Int_t           DatasetNumber;
  Int_t           RunNumber;
  Int_t           RandomRunNumber;
  Int_t           FS;

   double          m_mass;
   double          m_Et_met_muons;
   double          m_Phi_met_muons;


   // List of branches
   TBranch        *b_year;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_actual_mu;   //!
   TBranch        *b_nVtx;   //!
   TBranch        *b_passEmulTightJetCleaning1Jet;   //!
   TBranch        *b_passEmulTightJetCleaning2Jet;   //!
   TBranch        *b_elePt;   //!
   TBranch        *b_eleEta;   //!
   TBranch        *b_elePhi;   //!
   TBranch        *b_muonPt;   //!
   TBranch        *b_muonEta;   //!
   TBranch        *b_muonPhi;   //!
   TBranch        *b_muonM;   //!
   TBranch        *b_muonCharge;   //!
   TBranch        *b_trkisTight;   //!
   TBranch        *b_trkPt;   //!
   TBranch        *b_trkEta;   //!
   TBranch        *b_trkTheta;   //!
   TBranch        *b_trkPhi;   //!
   TBranch        *b_trkQ;   //!
   TBranch        *b_trkZ0;   //!
   TBranch        *b_trkZ0Err;   //!
   TBranch        *b_trkZ0sinTheta;   //!
   TBranch        *b_trkD0;   //!
   TBranch        *b_trkD0Err;   //!
   TBranch        *b_trkD0Sig;   //!
   TBranch        *b_trkOrigin;   //!
   TBranch        *b_trkFitQuality;   //!
   TBranch        *b_nIBLHits;   //!
   TBranch        *b_nB0LayerHits;   //!
   TBranch        *b_nPixLayers;   //!
   TBranch        *b_nExpIBLHits;   //!
   TBranch        *b_nExpB0LayerHits;   //!
   TBranch        *b_nPixHits;   //!
   TBranch        *b_nPixHoles;   //!
   TBranch        *b_nPixSharedHits;   //!
   TBranch        *b_nSCTHits;   //!
   TBranch        *b_nSCTHoles;   //!
   TBranch        *b_nSCTSharedHits;   //!
   TBranch        *b_jetPt;   //!
   TBranch        *b_jetEta;   //!
   TBranch        *b_jetPhi;   //!
   TBranch        *b_jetM;   //!
   TBranch        *b_jetJVT;   //!
   TBranch        *b_met_Et;   //!
   TBranch        *b_met_Phi;   //!
   TBranch        *b_metTruth_Et;   //!
   TBranch        *b_metTruth_Phi;   //!
   TBranch        *b_met_Signif;   //!
   TBranch        *b_pileupWeight;   //!
   TBranch        *b_leptonWeight;   //!
   TBranch        *b_eventWeight;   //!
   TBranch        *b_genWeight;   //!
   TBranch        *b_bTagWeight;   //!
   TBranch        *b_jvtWeight;   //!
   TBranch        *b_genWeightUp;   //!
   TBranch        *b_genWeightDown;   //!
   TBranch        *b_PRWHash;   //!
   TBranch        *b_EventNumber;   //!
   TBranch        *b_xsec;   //!
   TBranch        *b_GenHt;   //!
   TBranch        *b_GenMET;   //!
   TBranch        *b_DatasetNumber;   //!
   TBranch        *b_RunNumber;   //!
   TBranch        *b_RandomRunNumber;   //!
   TBranch        *b_FS;   //!

   analysis(TTree *tree=0);
   virtual ~analysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop(TTree* tree, Int_t nevents, Bool_t iszerolep);
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);

   void Write(const char* nomefile);


   TH1F* h_met;
   TH1F* h_leadjet;
   TH1F* h_leadjet_phi;
   TH1F* h_TrackPt;
   TH1F* h_d0;
   TH1F* h_sd0;
   TH1F* h_z0;
   TH1F* h_dphi;
   TH1F* h_iso;
   TH1F* h_dphi_muon1_track;
   TH1F* h_deta_muon1_track;
   TH1F* h_dR_muon1_track;
   TH1F* h_dR_muon2_track;


};

#endif

#ifdef analysis_cxx
analysis::analysis(TTree *tree) : fChain(0){

   Init(tree);

 h_met = new TH1F("met","Met",80,200.,1000.);
 h_leadjet = new TH1F("leadjet","Leading jet pT",85,150.,1000.);
 h_leadjet_phi = new TH1F("leadjetPhi", "Leading jet phi",60,-3.14,3.14);
 h_TrackPt = new TH1F("TrackPt","TrackPt",40,1.,5.);
 h_d0 = new TH1F("d0","|D0|",20,0.,10.);
 h_sd0 = new TH1F("sd0","|S(D0)|",80,0.,40.);
 h_z0 = new TH1F("z0","z0sen(theta)",12,0.,3.);
 h_dphi = new TH1F("dphi","DPhi",30,0.,3.);
 h_iso = new TH1F("iso", "Isolation R>1", 40,0.,20.);
 h_dphi_muon1_track = new TH1F("dphimuon1", "dPhi muon1 track",30,0.,3.14);
 h_deta_muon1_track = new TH1F("detamuon1", "dEta muon1 track",40,0.,4.);
 h_dR_muon1_track = new TH1F("dRmuon1", "dR muon1 track",10,0.,0.05);
 h_dR_muon2_track = new TH1F("dRmuon2", "dR muon2 track",10,0.,0.05);
}

analysis::~analysis()
{

  delete h_met;
  delete h_leadjet;
  delete h_leadjet_phi;
  delete h_TrackPt;
  delete h_d0;
  delete h_sd0;
  delete h_z0;
  delete h_dphi;
  delete h_iso;
  delete h_dphi_muon1_track;
  delete h_deta_muon1_track;
  delete h_dR_muon1_track;
  delete h_dR_muon2_track;



   if (!fChain) return;
   delete fChain->GetCurrentFile();

}

Int_t analysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t analysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void analysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   elePt = 0;
   eleEta = 0;
   elePhi = 0;
   muonPt = 0;
   muonEta = 0;
   muonPhi = 0;
   muonM = 0;
   muonCharge = 0;
   trkisTight = 0;
   trkPt = 0;
   trkEta = 0;
   trkTheta = 0;
   trkPhi = 0;
   trkQ = 0;
   trkZ0 = 0;
   trkZ0Err = 0;
   trkZ0sinTheta = 0;
   trkD0 = 0;
   trkD0Err = 0;
   trkD0Sig = 0;
   trkOrigin = 0;
   trkFitQuality = 0;
   nIBLHits = 0;
   nB0LayerHits = 0;
   nPixLayers = 0;
   nExpIBLHits = 0;
   nExpB0LayerHits = 0;
   nPixHits = 0;
   nPixHoles = 0;
   nPixSharedHits = 0;
   nSCTHits = 0;
   nSCTHoles = 0;
   nSCTSharedHits = 0;
   jetPt = 0;
   jetEta = 0;
   jetPhi = 0;
   jetM = 0;
   jetJVT = 0;

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("year", &year, &b_year);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("actual_mu", &actual_mu, &b_actual_mu);
   fChain->SetBranchAddress("nVtx", &nVtx, &b_nVtx);
   fChain->SetBranchAddress("passEmulTightJetCleaning1Jet", &passEmulTightJetCleaning1Jet, &b_passEmulTightJetCleaning1Jet);
   fChain->SetBranchAddress("passEmulTightJetCleaning2Jet", &passEmulTightJetCleaning2Jet, &b_passEmulTightJetCleaning2Jet);
   fChain->SetBranchAddress("elePt", &elePt, &b_elePt);
   fChain->SetBranchAddress("eleEta", &eleEta, &b_eleEta);
   fChain->SetBranchAddress("elePhi", &elePhi, &b_elePhi);
   fChain->SetBranchAddress("muonPt", &muonPt, &b_muonPt);
   fChain->SetBranchAddress("muonEta", &muonEta, &b_muonEta);
   fChain->SetBranchAddress("muonPhi", &muonPhi, &b_muonPhi);
   fChain->SetBranchAddress("muonM", &muonM, &b_muonM);
   fChain->SetBranchAddress("muonCharge", &muonCharge, &b_muonCharge);
   fChain->SetBranchAddress("trkisTight", &trkisTight, &b_trkisTight);
   fChain->SetBranchAddress("trkPt", &trkPt, &b_trkPt);
   fChain->SetBranchAddress("trkEta", &trkEta, &b_trkEta);
   fChain->SetBranchAddress("trkTheta", &trkTheta, &b_trkTheta);
   fChain->SetBranchAddress("trkPhi", &trkPhi, &b_trkPhi);
   fChain->SetBranchAddress("trkQ", &trkQ, &b_trkQ);
   fChain->SetBranchAddress("trkZ0", &trkZ0, &b_trkZ0);
   fChain->SetBranchAddress("trkZ0Err", &trkZ0Err, &b_trkZ0Err);
   fChain->SetBranchAddress("trkZ0sinTheta", &trkZ0sinTheta, &b_trkZ0sinTheta);
   fChain->SetBranchAddress("trkD0", &trkD0, &b_trkD0);
   fChain->SetBranchAddress("trkD0Err", &trkD0Err, &b_trkD0Err);
   fChain->SetBranchAddress("trkD0Sig", &trkD0Sig, &b_trkD0Sig);
   fChain->SetBranchAddress("trkOrigin", &trkOrigin, &b_trkOrigin);
   fChain->SetBranchAddress("trkFitQuality", &trkFitQuality, &b_trkFitQuality);
   fChain->SetBranchAddress("nIBLHits", &nIBLHits, &b_nIBLHits);
   fChain->SetBranchAddress("nB0LayerHits", &nB0LayerHits, &b_nB0LayerHits);
   fChain->SetBranchAddress("nPixLayers", &nPixLayers, &b_nPixLayers);
   fChain->SetBranchAddress("nExpIBLHits", &nExpIBLHits, &b_nExpIBLHits);
   fChain->SetBranchAddress("nExpB0LayerHits", &nExpB0LayerHits, &b_nExpB0LayerHits);
   fChain->SetBranchAddress("nPixHits", &nPixHits, &b_nPixHits);
   fChain->SetBranchAddress("nPixHoles", &nPixHoles, &b_nPixHoles);
   fChain->SetBranchAddress("nPixSharedHits", &nPixSharedHits, &b_nPixSharedHits);
   fChain->SetBranchAddress("nSCTHits", &nSCTHits, &b_nSCTHits);
   fChain->SetBranchAddress("nSCTHoles", &nSCTHoles, &b_nSCTHoles);
   fChain->SetBranchAddress("nSCTSharedHits", &nSCTSharedHits, &b_nSCTSharedHits);
   fChain->SetBranchAddress("jetPt", &jetPt, &b_jetPt);
   fChain->SetBranchAddress("jetEta", &jetEta, &b_jetEta);
   fChain->SetBranchAddress("jetPhi", &jetPhi, &b_jetPhi);
   fChain->SetBranchAddress("jetM", &jetM, &b_jetM);
   fChain->SetBranchAddress("jetJVT", &jetJVT, &b_jetJVT);
   fChain->SetBranchAddress("met_Et", &met_Et, &b_met_Et);
   fChain->SetBranchAddress("met_Phi", &met_Phi, &b_met_Phi);
   fChain->SetBranchAddress("metTruth_Et", &metTruth_Et, &b_metTruth_Et);
   fChain->SetBranchAddress("metTruth_Phi", &metTruth_Phi, &b_metTruth_Phi);
   fChain->SetBranchAddress("met_Signif", &met_Signif, &b_met_Signif);
   fChain->SetBranchAddress("pileupWeight", &pileupWeight, &b_pileupWeight);
   fChain->SetBranchAddress("leptonWeight", &leptonWeight, &b_leptonWeight);
   fChain->SetBranchAddress("eventWeight", &eventWeight, &b_eventWeight);
   fChain->SetBranchAddress("genWeight", &genWeight, &b_genWeight);
   fChain->SetBranchAddress("bTagWeight", &bTagWeight, &b_bTagWeight);
   fChain->SetBranchAddress("jvtWeight", &jvtWeight, &b_jvtWeight);
   fChain->SetBranchAddress("genWeightUp", &genWeightUp, &b_genWeightUp);
   fChain->SetBranchAddress("genWeightDown", &genWeightDown, &b_genWeightDown);
   fChain->SetBranchAddress("PRWHash", &PRWHash, &b_PRWHash);
   fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
   fChain->SetBranchAddress("xsec", &xsec, &b_xsec);
   fChain->SetBranchAddress("GenHt", &GenHt, &b_GenHt);
   fChain->SetBranchAddress("GenMET", &GenMET, &b_GenMET);
   fChain->SetBranchAddress("DatasetNumber", &DatasetNumber, &b_DatasetNumber);
   fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
   fChain->SetBranchAddress("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
   fChain->SetBranchAddress("FS", &FS, &b_FS);


   Notify();
}

Bool_t analysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void analysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t analysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef analysis_cxx







bool passlep(vector<float> *elePt, vector<float> *muonPt){

int n = 0;
int m = 0;


      int nEle = elePt->size();
      for(int iEle=0; iEle < nEle; iEle++){
        if((*elePt)[iEle]>7) n=n+1;
      }

      int nMuon = muonPt->size();
      for(int iMuon=0; iMuon < nMuon; iMuon++){
        if((*muonPt)[iMuon]>7) m=m+1;
      }

return (n+m)==0;
}

bool OSmuons(vector<int> *muonCharge, vector<float> *muonPt){                      // Exactly two opposite sign muons
  int nMuon = muonPt->size();
  if ((nMuon==2) && ((*muonPt)[0]>7 && (*muonPt)[1]>7) && ((*muonCharge)[0]*(*muonCharge)[1]==-1)){
    return true;
  }
  else{
    return false;
  }
}

bool pass_30(vector<double> v){

int dim=v.size();
int n=0;

for(int i=0; i<dim; i++){

	if(v[i]>30) n=n+1;

	}

return n<4;
}



double get_dphi(double a,double b){

	double dphi = a-b;

		if(dphi<-1*M_PI) dphi= 2*M_PI+dphi;
		if(dphi>M_PI) dphi=2*M_PI-dphi;

return dphi;
}

double get_dphi(float a,double b){

	double dphi = a-b;

		if(dphi<-1*M_PI) dphi= 2*M_PI+dphi;
		if(dphi>M_PI) dphi=2*M_PI-dphi;

return dphi;
}

double get_d0_sign(float d0, float d0err){
  return d0/d0err;
}

double get_z0sintheta(float z0, float theta) {
  return z0*sin(theta);
}


bool MuonTrack (TLorentzVector muon, float trkEta, float trkPhi) {
  float dR_muon_track;

  if (fabs(muon.Eta())<2.5) {
    dR_muon_track = sqrt(pow(get_dphi(muon.Phi(), (trkPhi)),2.) + pow(muon.Eta()-(trkEta),2.));
  }
  if (dR_muon_track<0.01) {
    return true;
  }
  else {
    return false;
  }
}
