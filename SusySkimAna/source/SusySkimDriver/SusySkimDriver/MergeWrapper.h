#ifndef SUSYSKIMDRIVER_MERGEWRAPPER_H_
#define SUSYSKIMDRIVER_MERGEWRAPPER_H_

// ROOT includes
#include "TString.h"

// Framework includes
#include "SusySkimMaker/SampleSet.h"
#include "SusySkimMaker/SampleHandler.h"

class MergeWrapper
{

 public:
  MergeWrapper();
  ~MergeWrapper(){};

  // Initialize running
  bool init(TString mergeDir,TString selector="",bool resubmit=false);

  // User call to merge
  void merge(TString process,TString treeName="",SampleHandler::BATCH_TYPE=SampleHandler::BATCH_TYPE::NONE);

  // Flag setters
  void noSubmit(bool noSubmit){ m_noSubmit=noSubmit; }
  void disableMetaDataCheck(bool disableMetaDataCheck){m_disableMetaDataCheck=disableMetaDataCheck;}
  void setEventRange(int minEvent,int maxEvent){ m_minEvent=minEvent; m_maxEvent=maxEvent; }

 private:

  // Master method
  void merge(SampleSet* sampleSet,TString treeName="");

  // Batch methods
  void merge_submitToLSF(SampleSet* sampleSet,TString treeName);

  // Check jobs
  bool checkMerge(const SampleSet* sampleSet, TTree*& tree, TChain* cbkChain);

  SampleSet* getSampleSet(TString process);
  bool checkJob(SampleSet* sampleSet,TString sys,int minEvent,int maxEvent);

 private:
  TString m_mergeDir;
  TString m_selector;

  int m_minEvent;
  int m_maxEvent;
  int m_nEventsPerMergeJob;
  bool m_disableMetaDataCheck;
  bool m_resubmitMergeJobs;
  bool m_noSubmit;

};

#endif
