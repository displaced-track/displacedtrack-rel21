#include <cstdlib>
#include <string>

// ROOT
#include "TChain.h"

// RootCore
#include "SusySkimMaker/SampleHandler.h"
#include "SusySkimMaker/ArgParser.h"
#include "SusySkimMaker/PkgDiscovery.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimDriver/MergeWrapper.h"

using namespace std;

int main(int argc, char** argv)
{

  const char* APP_NAME = "run_merge";

  TString baseDir           = "";
  TString user              = gSystem->ExpandPathName("$USER");
  TString tag               = "";
  TString process           = "";
  TString deepConfig        = "";
  TString priorityTags      = "";
  TString groupSet          = "";
  TString productionGroup   = "";
  TString prefix            = "";
  TString includeFiles      = "";
  TString excludeFiles      = "";
  TString sumwHist          = "";
  TString xsec              = "";
  TString selector          = "";
  TString treeName          = "";

  bool disableMetaDataCheck=false;
  bool skipSampleInfoDump=false;
  bool submitToTorque=false;
  bool submitToCondor=false;
  bool submitToLRZ=false;
  bool submitToLSF=false;
  bool noSubmit=false;
  bool mergeByProcessName=false;
  bool resubmitMergeJobs=false;
  bool mergeOutputs=true;
  bool skipMergePrep=false;

  float lumi = 1.00;
  int priority = -1;
  int minEvent = -1.0;
  int maxEvent = -1.0;

  ArgParser arg;

  /** Read inputs to program */
  for(int i = 1; i < argc; i++) {
    if( arg.parse(argv[i],"-d","Path to base directory. Will create three directories inside: trees, skims, and merged") )
      baseDir = argv[++i];
    else if (arg.parse(argv[i],"-u","Grid user name, default is taken as $USER") )
      user = argv[++i];
    else if (arg.parse(argv[i],"-groupSet","Group set name" ) )
      groupSet = argv[++i];
    else if (arg.parse(argv[i],"-tag","Tag for production you wish to process" ))
      tag = argv[++i];
    else if (arg.parse(argv[i],"-process","Name of the process you wish to call the output trees" ))
      process = argv[++i];
    else if (arg.parse(argv[i],"-selector","Specific a selector (to run init_merge and execute_merge methods of BaseUser" ) )
      selector = argv[++i];
    else if (arg.parse(argv[i],"-treeName","Specific treeName you want to process out of the inclusive file" ) )
      treeName = argv[++i];
    else if (arg.parse(argv[i],"-deepConfig","Deep configuration file" ))
      deepConfig = argv[++i];
    else if (arg.parse(argv[i],"-priorityTags","Ordered comma separated list of tags. If a sample exists with the any of these tags, taken over primary tag" ))
      priorityTags = argv[++i];
    else if (arg.parse(argv[i],"-minEvent","Min event to process") )
      minEvent = atoi(argv[++i]);
    else if (arg.parse(argv[i],"-maxEvent","Max event to process") )
      maxEvent = atoi(argv[++i]);
    else if (arg.parse(argv[i],"-productionGroup","For official group production (e.g. phys-susy), changes prefix of output containers to group.<productionGroup>" ) )
      productionGroup = argv[++i];
    else if (arg.parse(argv[i],"-includeFiles","Specific files to include" ) )
      includeFiles = argv[++i];
    else if (arg.parse(argv[i],"-excludeFiles","Specific files to exclude" ) )
      excludeFiles = argv[++i];
    else if (arg.parse(argv[i], "-priority","Select samples only with this priority, specified as a property in sample text files"))
      priority=atoi(argv[++i]);
    else if (arg.parse(argv[i],"--disableMetaDataCheck","Disable meta data checks; i.e. AMI checks") )
      disableMetaDataCheck = true;
    else if (arg.parse(argv[i],"-lumi","Luminosity to reweight the trees (default 1.00)") )
      lumi = atof(argv[++i]);
    else if (arg.parse(argv[i],"--skipSampleInfoDump","Skip the dump of sample info. By default done in merge") )
      skipSampleInfoDump = true;
    else if (arg.parse(argv[i],"--lsf","Submit the merge to the LSF batch system") )
      submitToLSF = true;
    else if (arg.parse(argv[i],"--noSubmit","Don't actually submit the merge to the batch (obviously only used when a batch submission is used") )
      noSubmit = true;
    else if (arg.parse(argv[i],"--mergeByProcess","When samples are submitted and grouped by processed name (i.e. local batch methods), use this flag" ) )
      mergeByProcessName = true;
    else if (arg.parse(argv[i],"--skipMergePrep","Only to be used by batch scripts, or your SampleSets are already prepared and you want to skip [not recommended]" ) )
      skipMergePrep = true;
    else if (arg.parse(argv[i],"--resubmit","Resubmit merge jobs. Only implemented for LSF batch systems!" ) )
      resubmitMergeJobs = true;
    else
    {
      std::cout << "Unknown argument " << argv[i] << std::endl;
      return 0;
    }
  }

  // MetaData checks make use of AMI, so better make sure to set it up!
  if(!disableMetaDataCheck){
    char* amiPath = getenv("PYAMI_VERSION");
    if(amiPath == NULL){
      std::cerr << "Need to run 'lsetup pyami' in order to proceed! Aborting." << std::endl;
      abort();
    }
  }

  // Absoutely required for all executables
  CHECK( PkgDiscovery::Init( deepConfig ) );

  SampleHandler* sh = new SampleHandler();
  sh->mergeOutputs(mergeOutputs);
  sh->includeFiles(includeFiles);
  sh->excludeFiles(excludeFiles);
  sh->disableMetaDataCheck(disableMetaDataCheck);
  sh->setLumi(lumi);
  sh->skipSampleInfoDump(skipSampleInfoDump);
  sh->mergeByProcessName(mergeByProcessName);
  sh->setSamplePriority(priority);
  sh->setEventRange(minEvent, maxEvent);

  //Construct the prefix, e.g. user.<username> or group.<groupName>. If productionGroup is given, use group.<productionGroup>, otherwise use user.<username>.
  if ( !productionGroup.IsWhitespace() ) prefix = "group." + productionGroup;
  else prefix = "user." + user;

  sh->initialize(baseDir,tag,groupSet,prefix);
  sh->setPriorityTags(priorityTags);

  SampleHandler::BATCH_TYPE batchType = SampleHandler::BATCH_TYPE::NONE;
  if( submitToTorque ) batchType = SampleHandler::BATCH_TYPE::TORQUE;
  if( submitToCondor ) batchType = SampleHandler::BATCH_TYPE::CONDOR;
  if( submitToLRZ    ) batchType = SampleHandler::BATCH_TYPE::LRZ;
  if( submitToLSF    ) batchType = SampleHandler::BATCH_TYPE::LSF;

  // Run merging
  // This now only prepares the inputs
  if( !skipMergePrep ){
    sh->prepareMergeGroupSet();
  }

  // Initalize wrapping tool
  MergeWrapper* mergeWrapper = new MergeWrapper();
  mergeWrapper->init( sh->getMergeDir(), selector, resubmitMergeJobs );
  mergeWrapper->setEventRange(minEvent,maxEvent);
  mergeWrapper->disableMetaDataCheck(disableMetaDataCheck);
  mergeWrapper->noSubmit(noSubmit);

  // Run merge for each process
  SampleHandler::SampleInfo* set = sh->getSampleInfo( sh->groupName() );
  for( auto& sampleConfig : set->sampleConfig ){
    // Select a specific process (usually batch jobs)
    if( process!="" && sampleConfig->process!=process ) continue;
    // Run merge!
    mergeWrapper->merge( sampleConfig->process,treeName,batchType );
  }

  return 0;

}
