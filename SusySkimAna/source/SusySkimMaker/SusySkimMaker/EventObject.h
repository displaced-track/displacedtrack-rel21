#ifndef SusySkimMaker_EventObject_h
#define SusySkimMaker_EventObject_h

// Rootcore
#include "SusySkimMaker/BaseHeader.h"
#include "SusySkimMaker/EventVariable.h"
#include "SUSYTools/SUSYCrossSection.h"
#include "PhotonVertexSelection/PhotonVertexSelectionTool.h"
#include "PATInterfaces/SystematicRegistry.h"

// EDM includes:
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"

// PMG Tools
#include "PMGTools/PMGTruthWeightTool.h"
#include "GammaORTools/VGammaORTool.h"

// Root
#include "TSystem.h"
#include "TH1F.h"

// C++
#include <fstream>
#include <iostream>

// Forward declarations
class GoodRunsListSelectionTool;
class CentralDB;
class TreeMaker;
class StatusCode;


namespace CP
{
  class PileupReweightingTool;
}

//
class EventObject
{

 public:
  EventObject();
  ~EventObject();

  // Global init
  StatusCode init(TreeMaker*& treeMaker,float nEvents,bool isData,bool isAf2);

  // Initialization methods
  StatusCode init_tools(bool isData,bool isAf2, bool disablePRW=false);
  StatusCode init_GRL();
  StatusCode init_pileUpReweightTool();
  StatusCode init_PMGTruthWeightTool();
  StatusCode init_Xsec();
  StatusCode init_numEvts( TString histName="weighted__AOD" );

  //
  // Return the list of PRW profiles,
  // Relies on the m_runNumber being set
  // and if it's not, uses the default field
  // Fills the global m_prwFiles field
  std::vector<std::string> getPRWProfileList();

  //
  // Return the list of lumi-calc files
  // Fills the global m_lumiCalcFiles field
  std::vector<std::string> getLumiCalcList(bool isData);

  //
  // Return the actual mu files from CentralDB
  std::vector<std::string> getActualMuList();

  // Always call
  void Reset();

  //
  void Log(MetaData* metaData);

  ///
  /// Get systematic variations and store into m_systSet
  ///
  StatusCode fillSysVector();

  ///
  /// Fill the EventVariable object.
  ///
  void fillEventContainer(const xAOD::EventInfo* eventInfo,
                          const xAOD::JetContainer* jets,
                          const xAOD::MuonContainer* muons,
                          const xAOD::TruthEventContainer* truthEvents,
                          const xAOD::VertexContainer* primVertex,
                          const xAOD::PhotonContainer* photons,
                          std::map<TString,bool> evtL1TriggerMap,
                          std::map<TString,bool> evtHLTTriggerMap,
              std::map<TString,float> evtTriggerPrescaleMap,
                          float met_L1,
                          float met_HLT_pufit,
                          float met_HLT_cell,
                          float leadJetPt_L1,
                          float leadJetPt_HLT,
                          float sherpaVjetsWeight,
			  const std::map<TString,float> &globalDiLepTrigSF,
			  const std::map<TString,float> &globalMultiLepTrigSF,
                          const std::map<TString,float> &globalPhotonTrigSF,
                          int pdgid1, int pdgid2,
                          std::string sys_name
                          );

  void fillEventContainerTruth(const xAOD::EventInfo* eventInfo,
      const xAOD::TruthParticleContainer* truthParticles,
      const xAOD::TruthEventContainer* truthEvents,
      std::string sys_name="");

  void fillPDFInfo(const xAOD::TruthEventContainer* truthEvents,
                   EventVariable* const event,
                   bool fillDefault=false);

  enum XSEC_INFO {
    ALL=0,
    XSEC=1,
    KFAC=2,
    GEFF=3,
    UNCERT=4
  };

  void fillPileUpWeights(const xAOD::EventInfo* eventInfo, EventVariable*& event);
  float getXsec(unsigned int channelNumber,unsigned int finalState=0,XSEC_INFO xsec_info=ALL);
  float getPileUpWeight(const xAOD::EventInfo* eventInfo,  CP::SystematicSet variation );
  bool checkPRWInputHistogram(unsigned int channelNumber, unsigned int periodNumber);
  float getNumEvents(unsigned int channelNumber,bool cache=true);
  unsigned long long getPileupWeightHash(const xAOD::EventInfo* eventInfo);
  TString makeWeightTree(TString outputDir, TString channelNumber,TString hashName, TString pileupName, int var=0);
  float GetCorrectedAverageInteractionsPerCrossing(const xAOD::EventInfo* eventInfo);
  float GetAvgInteractionsPerCrossing(const xAOD::EventInfo* eventInfo);
  float GetActualInteractionsPerCrossing(const xAOD::EventInfo* eventInfo);
  float GetRunLumi(const xAOD::EventInfo* eventInfo);
  bool passGoodVtx(const xAOD::VertexContainer* vertices);
  int getNVtx(const xAOD::VertexContainer* vertices, unsigned int nTracks);
  bool passGRL(unsigned int runN, unsigned int lumiB);
  unsigned int getRandomRunNumber(const xAOD::EventInfo* eventInfo);
  unsigned int getRandomRunNumber(unsigned int DSID,int runNumber,unsigned long long evtNumber);
  int getNumHSJets(const xAOD::JetContainer* jets);

  bool passBadJet(const xAOD::JetContainer* jets);
  bool passEvtBadJet(const xAOD::EventInfo* eventInfo, bool useLoose);
  bool passBadMuon(const xAOD::MuonContainer* muons);
  bool passCosmicMuon(const xAOD::MuonContainer* muons);
  bool passEmulEvtBadNJet(const xAOD::EventInfo* eventInfo, int n_jet);
  void emulateTightEvtCleaningPF(const xAOD::JetContainer* jets_PF, const xAOD::JetContainer* jets_EMTopo, const xAOD::EventInfo* eventInfo);

  // Helper functions
  const EventVariable* getEvt(TString sysName);
  unsigned int getDSID(){return m_dsid;}
  unsigned int getRunNumber(){return m_runNumber;}
  bool         getUsePMGXsecTool(){ return m_usePMGXsecTool; }

  void setSOWPerEvent(float sumOfWeightsPerEvent) {m_sumOfWeightsPerEvent=sumOfWeightsPerEvent;}
  void setDSID(int dsid) {m_dsid = dsid;}
  void setRunNumber(int runNumber) {m_runNumber = runNumber;}
  void setWriteLHEWeights(bool writeLHE3){
    std::cout << "EventObject::setWriteLHEWeights Set to " << writeLHE3 << std::endl;
    m_writeLHE3 = writeLHE3;
  }

 private:

  ///
  /// Fill photon MVA classified vertex
  ///
  void fillPhotonVtxMVA(EventVariable*& event,const xAOD::PhotonContainer* photonCont);

  ///
  /// Fill primary vertex information
  ///
  void fillPrimaryVertex(EventVariable*& event,const xAOD::VertexContainer* primVertex);

  ///
  /// Fill pileup vertex information
  ///
  void fillPileupVertex(EventVariable*& event,const xAOD::VertexContainer* primVertex);

 protected:

   GoodRunsListSelectionTool*       m_grl;
   CP::PileupReweightingTool*       m_pileupReweightingTool;
   CP::PhotonVertexSelectionTool*   m_photonVtxSel;
   VGammaORTool *m_vgammaOR;
   SUSY::CrossSectionDB*            m_susyCrossSection;
   SUSY::CrossSectionDB::Process    m_crossSectionDB;
   TH1F*                            m_numEvtHist;
   float                            m_nEvents;
   bool                             m_isData;
   bool                             m_xAODAccess;

   ///
   /// Store DSID
   ///
   int m_dsid;

   ///
   /// Store RunNumber
   ///
   int m_runNumber;

   ///
   TString m_numEvtPath;

   ///
   /// Where the cross sections are located
   ///
   TString m_xsecPathName;

   ///
   /// Global EventVariable map
   ///
   std::map<TString,EventVariable*> m_eventMap;

   ///
   /// Vector containing all GRL XML files. NO default provides, the user MUST configure this.
   ///
   std::vector<std::string> m_grlVec;

   ///
   /// Vector containing luminosity calc files for PRW tool.  NO default provides, the user MUST configure this.
   ///
   std::vector<std::string> m_lumiCalcFiles;

   ///
   /// Vector containing PRW files. NO default provides, the user MUST configure this.
   ///
   std::vector<std::string> m_prwFiles;

   ///
   /// Actual mu list
   ///
   std::vector<std::string> m_actualMuList;

   ///
   /// Systematic variations for tools affected by this class
   ///
   std::vector<CP::SystematicSet> m_systSet;

   ///
   /// SumOfWeights(originial file before derivation)/nEvents(this file)
   /// To be determined in xAODEventLooper::fileExecute()
   ///
   float m_sumOfWeightsPerEvent;

   ///
   /// When true, don't try to get the pileup weights. Default false
   ///
   bool m_disablePRW;

   ///
   /// When true, don't require the GRL to be passed. Steered in deep.conf
   ///
   bool m_ignoreGRL;

   ///
   /// When true, try to retrieve/store LHE3 event weights. Steered in deep.conf
   ///
   bool m_writeLHE3;

   ///
   /// Write out photon vertices, Steered in deep.conf
   ///
   bool m_writeMVAPhotonVtx;

   ///
   /// Switch back to using ST cross section DB
   /// Defasult is true (i.e. use PMG tool) as in the ST header
   /// Steered in deep.config
   ///
   bool m_usePMGXsecTool;

   ///
   /// Driver to retrieve LHE3 weights
   ///
   PMGTools::PMGTruthWeightTool *m_PMGTruthWeightTool;

   ///
   /// Members to steer Tight event cleaning emulation
   ///
   int m_tightEvtCleaningNJets;

   ///
   /// Run the VGammaORTool on MC (requires TruthParticles container in input)
   ///
   int m_doVGammaOR;
   bool m_xsecExists = true;

};

#endif
