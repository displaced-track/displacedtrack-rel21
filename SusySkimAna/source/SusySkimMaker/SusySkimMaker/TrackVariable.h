#ifndef SusySkimMaker_TrackVariable_h
#define SusySkimMaker_TrackVariable_h

// Rootcore
#include "SusySkimMaker/TruthVariable.h"
#include "SusySkimMaker/MCCorrections.h"

// ROOT
#include "TVector3.h"

/*
  Generic container to hold track objects
  Primary intended use will be for special tracks
  but in principle could be used for anything
*/

class TrackVariable : public ObjectVariable
{

 public:

  ///
  /// Constructor
  ///
  TrackVariable();

  //
  virtual ~TrackVariable();

  TrackVariable(const TrackVariable&);
  TrackVariable& operator=(const TrackVariable&);

  // What type of Track is
  // filled in this class?
  // Currently filled by matching to the primary track container name
  // Need to make more robust,based on author? Available in derivations?
  enum TrackType{
    UNKNOWN=0,         // Unknown
    STD=1,             // Standard tracks
    PIXELPRD=2,        // 4-layer tracklets run in default reco
    PIXELTHREELAYER=3, // Three-layer tracklets
    MSONLY=4,          // MS only tracks
    ELECTRONLINK=5,
    MUONLINK=6,
    PHOTONLINK=7
  };

  TrackType trackType;

  ///
  /// Track selection
  ///
  bool isTight;
  bool isLoose;

  ///
  /// Track charge
  ///
  int q;
  float d0;
  float z0;
  float z0Origin;
  float d0Err;
  float z0Err;
  float z0SinTheta; // JEFF
  float m; // JEFF
  int TightPrimary; // JEFF

  float TRTdEdx; // SICONG
  float pixeldEdx; // SICONG
  int index; // SICONG

  float ptcone20;
  float ptcone30;
  float ptcone40;

  ///
  /// Parent Info
  ///
  int npar;
  int parbarcode;
  int parpdgId;
  int parnchild;
  int parchildpdgId;
  float parchildpt;
  int parchildbarcode;

  ///
  /// ProdVertex
  ///
  TVector3 prodvtx;
  int prodvtxtype;
  int ishardvtx;

  ///
  /// DecayVertex
  ///
  TVector3 decayvtx;
  int decayvtxtype;

  ///
  /// Secondary Vertex Information
  ///
  int isSecTrk;

  ///
  /// Track Quality
  ///
  float chiSquared;
  float numberDoF;

  ///
  /// Track hits
  ///
  uint8_t nIBLHits;
  uint8_t nBLayerHits;
  uint8_t nPixLayers;
  uint8_t nExpIBLHits;
  uint8_t nExpBLayerHits;
  uint8_t nPixHits;
  uint8_t nPixDeadSensors;
  uint8_t nPixHoles;
  uint8_t nPixSharedHits;
  uint8_t nPixSplitHits;
  uint8_t nSCTHits;
  uint8_t nSCTDeadSensors;
  uint8_t nSCTHoles;
  uint8_t nSCTSharedHits;
  uint8_t nPixOutliers;
  uint8_t nSCTOutliers;
  uint8_t nTRTHits;
  uint8_t nPixSpoiltHits;
  uint8_t nGangedFlaggedFakes;

  uint32_t hitPattern;

  ///
  /// Truth classification
  ///
  int status;
  int type;
  int origin;
  int pdgId;
  float truthMatchProb;

  ///
  /// Isolation variables
  ///
  float etcone20Topo;
  float etcone30Topo;
  float etcone40Topo;
  float etclus20Topo;
  float etclus30Topo;
  float etclus40Topo;

  ///
  /// Fit quality chi2/nDOF
  float fitQuality;
  float truthProVtx;

  // Stores the vertex position
  // Typically only set when an associated
  // track is created
  TVector3 vtx;

  // Vertex fit quality
  // Similar to vtx above, typically only
  // filled when an associated track is formed
  float vtxQuality;

  float d0SV;
  float z0SV;
  float ptSV;
  float etaSV;
  float phiSV;
  float d0ErrSV;
  float z0ErrSV;
  float pErrSV;

  float truth_d0SV;
  float truth_z0SV;

  ///
  /// Associated calorimeter information
  /// Obviously this is still work in progress,
  /// but eventually in the skim making process
  /// we should do some matching to calorimeter
  /// topoclusters, save its TLV, and some other
  /// properties
  ///
  /// Areas to study:
  ///  - In the matching, how does the number of pixel hits
  ///    determine our matching criteria.
  ///    This is something that should be optimized to reject backgrounds
  ///
  //TLorentzVector& getCaloClusterTLV(){ return *caloCluster; }

  TLorentzVector caloCluster;

/*
 numberOfPixelHits               = 2,  //!< these are the pixel hits, including the b-layer [unit8_t].
    numberOfPixelOutliers           =41,  //!< these are the pixel outliers, including the b-layer [unit8_t].
    numberOfPixelHoles              = 1,  //!< number of pixel layers on track with absence of hits [unit8_t].
    numberOfPixelSharedHits         =17,  //!< number of Pixel all-layer hits shared by several tracks [unit8_t].
    numberOfPixelSplitHits          =44,  //!< number of Pixel all-layer hits split by cluster splitting [unit8_t].
    numberOfGangedPixels            =14,  //!< number of pixels which have a ganged ambiguity [unit8_t].
    numberOfGangedFlaggedFakes      =32,  //!< number of Ganged Pixels flagged as fakes [unit8_t].
    numberOfPixelDeadSensors        =33,  //!< number of dead pixel sensors crossed [unit8_t].
    numberOfPixelSpoiltHits         =35,  //!< number of pixel hits with broad errors (width/sqrt(12)) [unit8_t].
    numberOfSCTHits                 = 3,  //!< number of hits in SCT [unit8_t].
    numberOfSCTOutliers             =39,  //!< number of SCT outliers [unit8_t].
    numberOfSCTHoles                = 4,  //!< number of SCT holes [unit8_t].
    numberOfSCTDoubleHoles          =28,  //!< number of Holes in both sides of a SCT module [unit8_t].
    numberOfSCTSharedHits           =18,  //!< number of SCT hits shared by several tracks [unit8_t].
    numberOfSCTDeadSensors          =34,  //!< number of dead SCT sensors crossed [unit8_t].
    numberOfSCTSpoiltHits           =36,  //!< number of SCT hits with broad errors (width/sqrt(12)) [unit8_t].
    numberOfTRTHits                 = 5,  //!< number of TRT hits [unit8_t].
    numberOfTRTOutliers             =19,  //!< number of TRT outliers [unit8_t].
    numberOfTRTHoles                =40,  //!< number of TRT holes [unit8_t].
    numberOfTRTHighThresholdHits    = 6,  //!< number of TRT hits which pass the high threshold (only xenon counted) [unit8_t].
    numberOfTRTHighThresholdHitsTotal= 64,  //!< total number of TRT hits which pass the high threshold  [unit8_t].
*/

  TrackVariable* makeAssociatedTrack(); //!
  TrackVariable* getAssociatedTrack(int idx); //!

  ///
  /// Return the numbers of associated tracks to this track
  ///
  int nAssociatedTracks(){ return m_associatedTrack.size(); } //!

  // Print out
  void print() const; //!

  // Truth link
  mutable TruthVariable* truthLink;


 protected:
   std::vector<TrackVariable*> m_associatedTrack;

  ClassDef(TrackVariable, 3);

};

#endif
