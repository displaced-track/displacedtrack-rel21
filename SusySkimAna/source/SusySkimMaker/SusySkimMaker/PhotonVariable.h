#ifndef SusySkimMaker_PhotonVariable_h
#define SusySkimMaker_PhotonVariable_h

#include "SusySkimMaker/ObjectVariable.h"
#include "SusySkimMaker/MCCorrections.h"
#include "SusySkimMaker/TrackVariable.h"

class PhotonVariable : public ObjectVariable
{

 public:
 
  ///
  /// Default constructor, required
  ///
  PhotonVariable();

  ///
  /// Destructor
  ///
  virtual ~PhotonVariable();

  ///
  /// Copy constructor
  ///
  PhotonVariable(const PhotonVariable&);

  ///
  /// Assignment operator
  ///
  PhotonVariable& operator=(const PhotonVariable&);

  // Isolation variables
  //  => https://twiki.cern.ch/twiki/bin/view/AtlasProtected/IsolationSelectionTool
  bool IsoFCTightCaloOnly;
  bool IsoFixedCutLoose;
  bool IsoFixedCutTight;

  ///
  /// Isolation variable: Sum of the topological cluster ET within a cone of a radius dR = 0.2
  ///
  float topoetcone20;
  
  ///
  /// Isolation variable: Sum of the topological cluster ET within a cone of a radius dR = 0.3
  ///
  float topoetcone30;
  
  ///
  /// Isolation variable:Sum of the topological cluster ET within a cone of a radius dR = 0.4;
  ///
  float topoetcone40;

  ///
  /// Isolation variable:Sum of the topological cluster ET within a cone of a radius dR = 0.2, 0.3, 0.4;
  ///
  float SUSY20_topoetcone20;
  float SUSY20_topoetcone30;
  float SUSY20_topoetcone40;
  float SUSY20_topoetcone20NonCoreCone;
  float SUSY20_topoetcone30NonCoreCone;
  float SUSY20_topoetcone40NonCoreCone;

  ///
  /// SUSYTools decision for signal 
  ///
  bool signal;

  bool tight;
  bool passOR;

  bool isConverted;
  float conversionRadius;
  int   conversionType;

  ///
  /// Track links
  ///
  std::vector<TrackVariable*> trkLinks;

  ///
  /// Reconstruction efficiency scale factor, key=systematic name
  ///
  std::map<TString,float> recoSF;

  ///
  /// Trigger efficiencies
  ///
  MCCorrContainer trigEff;


 // Methods
 public:

   // Retrieve ID/iso SFs
   float getRecoSF(const TString sys);
   
   ///
   /// Method to get the trigger eff and SF for a given systematic
   ///
   double getTrigSF(const TString instance, const TString sys);

   // Print out characteristics of this photon
   void print() const;

 public:
  ClassDef(PhotonVariable, 3);


};

#endif
