#ifndef SusySkimMaker_Systematics_h
#define SusySkimMaker_Systematics_h

/*
 * Class is very much in development, but will port over the code we have
 * in rather random places and also feature some upgrades 
 */

// C++
#include <map>
#include <vector>

// ROOT
#include "TString.h"

// RootCore
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SusySkimMaker/ObjectTools.h"

// Forward declarations
class TreeMaker;
class StatusCode;

class Systematics
{

 public:
  Systematics();
  ~Systematics();

  //
  StatusCode init(ST::SUSYObjDef_xAOD& susyObj,int writeTrees, int writeSkims, TString sysSetName, bool isData);
  StatusCode initDefaultSet(int writeTrees, int writeSkims);
  
  //
  StatusCode OpenFile(TString sysSetName);

  //
  void addSysBranches(TreeMaker*& treeMaker,TString nomTree="");

  //
  void fillSysBranches(TreeMaker*& treeMaker,Objects* obj,ObjectTools* objTools, bool useTrackJets=false, TString trigChains="");

  //
  static TString getBranchPrefix(ObjectTools::WeightType weightType, TString unique="");

  // Print out classifications
  void print();

  ///
  /// Return a vector of the missing systematics. This will occur when a user selects a subset 
  /// of the full systematic list provided by SUSYTools (i.e. some are irrelevent, or you missed some)
  ///
  const std::vector<ST::SystInfo> getExcludedSystematics(){ return m_excludedSystematics; }

  ///
  /// Weight based systematics
  ///
  const std::vector<ST::SystInfo> getWeightSystematics(){ return m_sysInfoWeights; }

  ///
  /// Kinematic based systematics
  ///
  const std::vector<ST::SystInfo> getKinematicSystematics(){ return m_sysInfoKinematics; }

  ///
  /// Kinematic lists, separately for what is written out into tree/skim steams
  ///
  const std::vector<TString> getKinematicSkimList(){ return m_sysKinematics_skims; };
  const std::vector<TString> getKinematicTreeList(){ return m_sysKinematics_trees; };

  /// Misc methods
  bool isTriggerSystematic(TString sys);
  bool consistentTriggerAndSystematic(TString trigger, TString systematic);
  void addTriggerChainSF(TString chain){ m_triggerSFChains.push_back(chain); }

 protected:

   // These two are just copies of the ST list, seperated into kinematics/weights 
   // and filtered through what the user wants. Anything exlcluded by the user is saved
   // into m_excludedSystematics;
   std::vector<ST::SystInfo> m_sysInfoKinematics;
   std::vector<ST::SystInfo> m_sysInfoWeights;
   std::vector<ST::SystInfo> m_excludedSystematics;

   std::vector<TString> m_sysKinematics_trees;
   std::vector<TString> m_sysKinematics_skims;
 
   std::vector<TString> m_states;

   // Filled in classify, using sysInfoKinematics
   std::map<ObjectTools::WeightType,std::vector<TString>> m_weightSysMap;

 private:

  ///
  /// Directory where the users systematic text files are located
  /// In the future, this will be looked up by CentralDB
  ///
  TString m_sysFileDir;

  std::ifstream m_file;

  ///
  /// Triggers
  ///
  std::vector<TString> m_triggerSFChains;

};

#endif
