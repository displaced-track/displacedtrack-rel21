#ifndef SusySkimMaker_ArgParser_h
#define SusySkimMaker_ArgParser_h

#include "TString.h"

class ArgParser
{

 public:
  ArgParser();
  virtual ~ArgParser(){};


  ///
  /// Two main purposes for this method:
  ///  1) Compare inputFlag to flag, returns true if inputFlag==flag,
  ///     otherwise returns false
  ///  
  ///  2) If inputFlag==m_help, prints out message and returns false,
  ///     this is used for printing the help menu    
  ///
  bool parse(const char* inputFlag, const char* flag, const char* message);

  ///
  /// Sets m_help to help
  ///
  void setHelpFlag(TString help){m_help=help;}

 protected:

   ///
   /// Name of argument which toggles method parse to print out the help menu messages
   ///
   TString m_help;




};

#endif
