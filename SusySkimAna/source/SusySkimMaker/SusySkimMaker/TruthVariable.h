#ifndef SusySkimMaker_TruthVariable_h
#define SusySkimMaker_TruthVariable_h

#include "SusySkimMaker/ObjectVariable.h"

class TruthVariable : public ObjectVariable
{

 public:
  TruthVariable();
  virtual ~TruthVariable(){};

  TruthVariable(const TruthVariable&);
  TruthVariable& operator=(const TruthVariable&);

  int q; // charge*3
  float charge;
  int pdgId;
  int status;
  int barcode;
  int parentPdgId;
  int parentBarcode;
  int parentStatus;
  bool isTruthBSM;

  TVector3 decayVtx;
  TVector3 prodVtx;

  float decayVtxPerp;
  float decayVtxX;
  float decayVtxY;
  float decayVtxZ;
  float prodVtxPerp;
  float prodVtxX;
  float prodVtxY;
  float prodVtxZ;

  bool isAntiKt4Jet;
  bool bjet;

  TLorentzVector bareTLV;

  void print(); //!

  ClassDef(TruthVariable, 2);

};

#endif
