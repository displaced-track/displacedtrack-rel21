#ifndef SusySkimMaker_CentralDB_h
#define SusySkimMaker_CentralDB_h

// ROOT
#include "TString.h"
#include "TObject.h"
#include <iostream>
#include <map>

class StatusCode;

//
// Used to collect configurations 
// Note that its important to load the configuration in
// the constructor, since this class is used in many places
//

class CentralDB : public TObject
{

 public:
  CentralDB();
  ~CentralDB(){};

  //
  // Get a field
  // Update the second argument
  //
  static void retrieve(const TString field, float& value);
  static void retrieve(const TString field, double& value);
  static void retrieve(const TString field, int& value);
  static void retrieve(const TString field, bool& value);
  static void retrieve(const TString field, TString& value);
  static void retrieve(const TString field, std::string& value);
  static void retrieve(const TString field, std::vector<TString>& value);
  static void retrieve(const TString field, std::vector<std::string>& value);

  //
  // Get a field
  // Return value
  // 
  static bool retrieve(const TString field);


 private:

  static void check();

  ///
  /// Called by the constructor. As instance of this class will provide 
  /// the configuration.
  ///
  static void LoadConfig();

  ///
  /// Reads the configuration file and searches for any line containing 'field'
  /// If the line contains 'field', tokenize wrt ":" and add the second field to m_fields
  ///
  static void ReadConfig();

  ///
  /// Look up function for m_fields. Checks that the size of the entry == 1 (as m_fields stored vectors to be general)
  /// Aborts if it cannot find field (a mis configuration)
  ///
  static TString find( const TString field );

  ///
  /// Similar to find, except returns the full vector which can in general be larger than 1. 
  /// Useful for fields occuring more than once in the configuration file: i.e. PRW files 
  ///
  static std::vector<TString> findVec( const TString field );

};


#endif
