#!/bin/bash

if [[ $# > 0 ]]; then
        input=$1
else
        echo "Usage ./getNEvents.sh <file>" 
        exit
fi

TOTAL=0

for ds in `cat $input | awk '{print $1}'`; do

       # AMI doesn't like the scope that ruico and DQ2 have upgraded to
        if [[ $ds == *:* ]]; then
          ds=`echo $ds | awk -F ":" '{print $2}' `
        fi

        sumw=`ami show dataset info $ds | grep totalEvents | awk '{print $3}'`
        dsid=`ami show dataset info $ds | grep datasetNumber | awk '{print $3}'`

        echo -e "$ds\t$sumw"

        TOTAL=$(($TOTAL + $sumw))

        echo $TOTAL
done


echo ""
echo $TOTAL
