#!/bin/bash

#
# CHECK THAT THE USER PASSED A FILE TO THIS SCRIPT
#
if [[ $# > 0 ]]; then
        input=$1
else
        echo "Usage ./getSampleStatus.sh <file>" 
        exit
fi

#
# MAKE SURE AMI IS SETUP, 
#
if ! type "ami" > /dev/null; then
  echo "Please setup AMI (localSetupPyAMI) and be sure that you have a valid grid proxy (voms-proxy-init -voms atlas)"
  exit
fi

ALL_SAMPLES_OK=1

for ds in `cat $input | awk '{print $1}'`; do

        if [[ $ds == *#* ]]; then
          continue
        fi

        # AMI doesn't like the scope that ruico and DQ2 have upgraded to
        if [[ $ds == *:* ]]; then
          ds=`echo $ds | awk -F ":" '{print $2}' `
        fi

        status=`ami show dataset info $ds | grep prodsysStatus | awk -F ":" '{print $2}'i | sed 's/ //g'`
        dsid=`ami show dataset info $ds | grep datasetNumber | awk -F ":" '{print $2}'`

       echo $status

        if [[ $status != "ALLEVENTSAVAILABLE" ]]; then
          echo "Sample is NOT ready yet "$ds 
          echo "  >>>> Status "$status
          ALL_SAMPLES_OK=0
        fi
done

if [[ "$ALL_SAMPLES_OK" -eq "1" ]]; then
  echo "All samples are ready!" 
fi

