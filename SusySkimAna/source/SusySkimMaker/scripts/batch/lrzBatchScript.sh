#!/bin/bash
##
##
#SBATCH --clusters=lcg
#SBATCH --partition=lcg_serial
#SBATCH --qos=lcg_add
#SBATCH --export=NONE
#SBATCH --get-user-env
#SBATCH --mem=1000mb
#SBATCH --time=02:00:00


echo Running on host `hostname`
echo Time is `date`
echo Directory is `pwd`

echo "Selector: " $selector
echo "Input:    " $input
echo "Output:   " $output
echo "nEvt:     " $nEvt
echo "nSkip:    " $nSkip

shopt -s expand_aliases

source /home/grid/lcg/sw/setup_atlas_cvmfs.sh.rod

pushd $rootcorebin/../

set -- ""
source rcSetup.sh

popd

run_SkimNtRun -F $input -s $output -selector $selector -k $nSkip -n $nEvt
