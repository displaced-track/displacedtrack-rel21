#!/bin/bash

# $analysisExecutablesDir is set to either:
# - ${ROOTCOREBIN}/bin/x86_64-slc6-gcc49-opt/
# - ${WorkDir_DIR}/bin/
# depending on R21 vs. R20.7

executable       = $ENV(analysisExecutablesDir)/run_merge_standalone
universe         = vanilla
log              = $(Process).log
output           = $(Process).out
error            = $(Process).err
#arguments        = "-sys $sys -d $path -f $outFileName -tag $mytag -groupSet $groupset -p $process -isData $isData -isTruthOnly $isTruth"
getenv           = True
accounting_group = $ENV(CONDOR_ACCOUNTING_GROUP)
queue 
