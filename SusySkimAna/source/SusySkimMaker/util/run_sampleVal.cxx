
#include "SusySkimMaker/ArgParser.h"
#include "SusySkimMaker/SampleHandler.h"
#include "SusySkimMaker/MsgLog.h"

int main(int argc, char** argv)
{

  MsgLog::WARNING("run_sampleVal","THIS IS WORK IN PROGRESS, DON'T USE YET! IT WILL PROBABLY BREAK!");

  TString selector = "";
  
  ArgParser arg;

  /** Read inputs to program */
  for(int i = 1; i < argc; i++) {
    if( arg.parse(argv[i],"-selector","Selector name") )
      selector = argv[++i];
  }

  /*
    Run some basic tests to ensure we can run
    over our derivations without any problems!
  */

  Info("run_sampleVal","Running pre-grid submission tests....");
  Info("run_sampleVal","This is still work in progress, so I don't recommmend its usage!!!");

  // Look for samples

  // Run a test job for each
  //  -> Include systematics
  //  -> Specify a selector?
  
  TString samples = "/home/mgignac/data/downloads/SUSY5/unit_test_207_samples/ttbar ";
  TString outputDir = "sampleVal_ttbar";

  TString cmd = "xAODNtMaker ";
  cmd += " -d "+outputDir;
  cmd += " -s "+samples;
  cmd += " -selector "+selector;
  cmd += " -writeTrees 2 ";
  cmd += " -sysSet allSys ";
  cmd += " -MaxEvents 10";

  Info("run_sampleVal","Running command: %s",cmd.Data() );
     
  // Run the command!
  gSystem->Exec( cmd.Data() );
  

  // TODO : Check the output


  Info("run_sampleVal","Validation successful!!!" );

  return 0;

}
