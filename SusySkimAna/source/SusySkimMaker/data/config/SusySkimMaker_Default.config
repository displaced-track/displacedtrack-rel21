
########################## READ BEFORE EDITING #########################
# Take note of the following:
#  1) Any line containing a '#' will be ignored
#     - Note that this means you cannot put comments at the end of the line!
#  2) Separate a field and its value by ":"
#  3) For booleans, use an interger: false==0, true==1
#  4) For paths, they will be expanded, so please always specify with respect to $ROOTCOREBIN env
########################################################################

#
# DxAOD Kernel name: This specifies which Bookkeeper data to extract
# and ultimately will be used to normalize the samples to the total sum of weights
#
DxAODKernel : SUSY5KernelSkim

# 
# Important directories which define where to look 
# for text files needed by the framework (if running in that mode)
#
#
SysFileDir         : $ROOTCOREBIN/data/SusySkim1LInclusive/systematics/
GroupSetDir        : $ROOTCOREBIN/data/SusySkim1LInclusive/samples/

#
SusyToolsConfigFile : $ROOTCOREBIN/data/SusySkimMaker/SUSYTools/SUSYTools_SusySkimMaker_Default.conf


#
# Pile-up reweighting information
#
LumiCalc     : $ROOTCOREBIN/data/SusySkimMaker/PRW/ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-005.root
LumiCalc     : $ROOTCOREBIN/data/SusySkimMaker/PRW/ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-008.root

# These are the files used when a particular sample cannot
# be associated to a particular MC campaign (matching done by Run Number)
PRWGeneral   : $ROOTCOREBIN/data/SusySkimMaker/PRW/merged_prw.root
PRWGeneral   : $ROOTCOREBIN/data/SusySkimMaker/PRW/410500.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_nonallhad.root
PRWGeneral   : $ROOTCOREBIN/data/SusySkimMaker/PRW/MGPy8EG_A14N23LO_SS_onestepCC_PRW.root
PRWGeneral   : $ROOTCOREBIN/data/SusySkimMaker/PRW/WZll_3topSM_ttH125_PRW.root
PRWGeneral   : $ROOTCOREBIN/data/SusySkimMaker/PRW/MGPy8EG_A14N_GG_onestepCC_FullSim_PRW.root
PRWGeneral   : $ROOTCOREBIN/data/SusySkimMaker/PRW/pMSSM_PRW.root
PRWGeneral   : $ROOTCOREBIN/data/SusySkimMaker/PRW/MGPy8EG_A14N_GG_onestepCC_FastSim_PRW.root

# Profiles for MC15A
PRWMC15A   : $ROOTCOREBIN/data/SusySkimMaker/PRW/merged_prw.root
PRWMC15A   : $ROOTCOREBIN/data/SusySkimMaker/PRW/MGPy8EG_A14N23LO_SS_onestepCC_PRW.root
PRWMC15A   : $ROOTCOREBIN/data/SusySkimMaker/PRW/WZll_3topSM_ttH125_PRW.root
PRWMC15A   : $ROOTCOREBIN/data/SusySkimMaker/PRW/MGPy8EG_A14N_GG_onestepCC_FullSim_PRW.root
PRWMC15A   : $ROOTCOREBIN/data/SusySkimMaker/PRW/pMSSM_PRW.root
PRWMC15A   : $ROOTCOREBIN/data/SusySkimMaker/PRW/MGPy8EG_A14N_GG_onestepCC_FastSim_PRW.root
PRWMC15A   : $ROOTCOREBIN/data/SusySkimMaker/PRW/MadGraphPythia8EvtGen_A14N_GG_2stepWZ_PRW_2.4.3.root
PRWMC15A   : $ROOTCOREBIN/data/SusySkimMaker/PRW/mc_missingPRW_mc15a_2.4.3.root

# Profiles for MC15B
PRWMC15B   : $ROOTCOREBIN/data/SusySkimMaker/PRW/merged_prw_mc15b.root
PRWMC15B   : $ROOTCOREBIN/data/SusySkimMaker/PRW/mc_missingPRW_mc15b_2.4.3.root
PRWMC15B   : $ROOTCOREBIN/data/SusySkimMaker/PRW/PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_nonallhad_410500.root
PRWMC15B   : $ROOTCOREBIN/data/SusySkimMaker/PRW/Sherpa_NNPDF30NNLO_Wlnu.PRW_2.4.3.root
PRWMC15B   : $ROOTCOREBIN/data/SusySkimMaker/PRW/PRW_Sherpa22_Zjets.root
PRWMC15B   : $ROOTCOREBIN/data/SusySkimMaker/PRW/Alpgen_PRW.root
PRWMC15A   : $ROOTCOREBIN/data/SusySkimMaker/PRW/PRW_gl_WZh.root
PRWMC15B   : $ROOTCOREBIN/data/SusySkimMaker/PRW/PRW_diboson.root

# Profiles for MC15C
PRWMC15C   : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/merged_prw_mc15c_latest.root
#PRWMC15C   : $ROOTCOREBIN/data/SusySkimMaker/PRW/PRW_WZh_all.root
#PRWMC15C   : $ROOTCOREBIN/data/SusySkimMaker/PRW/SSonestep_FS_2.4.15.root
#PRWMC15C   : $ROOTCOREBIN/data/SusySkimMaker/PRW/SSonestep_AF_2.4.15.root
#PRWMC15C   : $ROOTCOREBIN/data/SusySkimMaker/PRW/GGonestep_FS_2.4.15.root
#PRWMC15C   : $ROOTCOREBIN/data/SusySkimMaker/PRW/GGonestep_AF_2.4.15.root
#PRWMC15C   : $ROOTCOREBIN/data/SusySkimMaker/PRW/GG2stepWZGGpMSSM_AF_2.4.18.root
#PRWMC15C   : $ROOTCOREBIN/data/SusySkimMaker/PRW/top_reco_sysVariations.root
#PRWMC15C   : $ROOTCOREBIN/data/SusySkimMaker/PRW/Signal_missing_AF_2.5.0_Jan22.root
#PRWMC15C   : $ROOTCOREBIN/data/SusySkimMaker/PRW/GGonestep_gridx_extension_AF_NTUP_PILEUP_p2761.root
# Default config file for samples that don't have a valid profile#
#PRWMC15C   : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root

# Shut off PRW
DisablePRW : 0

# Ignore the GRL
IgnoreGRL : 0

#
# Number of events from event counter histograms
# Note really used much anymore, but here for completeness and flexiability
#

NumEvtPath : $ROOTCOREBIN/data/SusySkimMaker/evtCounters/T_02_11trunk/nEvents.root

#
# Good run list information
#
Grl : $ROOTCOREBIN/data/SusySkimMaker/GRLs/data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml
Grl : $ROOTCOREBIN/data/SusySkimMaker/GRLs/data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml

#
# Cross section path
# Alternatively you can read them from $ROOTCOREBIN/data/SUSYTools/mc15_13TeV/ if your local
# SUSYTools version is more up to date then the cvmfs version
#
CrossSection : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/SUSYTools/xsdb/mc15_13TeV/

#
# MET Rebuilding, true or false
#
ExcludeAdditionalLeptons : 0

#
# Filtering options
#
FilterBaseline     : 1
FilterOR           : 1
SkimNLeptons       : 1
DoSampleOR         : 0
DoSampleORZjetZgam : 0

# Filter out the decays that shouldn't be in the Sherpa 2.2.1 WplvWmqq sample (363360)
DoSampleORWplvWmqq : 1

# Data specific filtering
RequireTrigInData : 1
RequireGRL : 1

# Misc configurations
#
UnitsFromMEV        : 0.001
SaveTruthInfo       : 1
WriteDetailedSkim   : 0

#
# Scale factor control
DoTriggerSFs : 1


# Truth level configurations
#
EmulateTruthBtagging : 0
SaveTruthEvtInfo: 0
SaveTruthEvtInfoFull: 0
