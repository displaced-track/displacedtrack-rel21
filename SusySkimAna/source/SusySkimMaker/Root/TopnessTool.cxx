#include "SusySkimMaker/TopnessTool.h"

// implementation of Nelder Mead (or downhill simplex) algorithm
#include "SusySkimMaker/amoeba.h"

//#include "SUSYTools/SUSYObjDef.h"

#include "TRandom3.h"
#include <algorithm>
#include <cassert>
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"

// top and W mass in MeV
const double m_top = 173.5e3;
const double m_W = 80.4e3;

// resolution parameters, also in MeV
const double a_W = 5e3;
const double a_t = 15e3;
const double a_CM = 1e6;

// squared constants
const double m_top_sqr = m_top * m_top;
const double m_W_sqr = m_W * m_W;
const double a_W_sqr = a_W * a_W;
const double a_t_sqr = a_t * a_t;
const double a_CM_sqr = a_CM * a_CM;

// Initialize object using measured kinematic variables.  two jets are
// required. if there is ==1 b-tagged jet in the event, three jets should be
// provided. the combinations (b,j1) and (b,j2) are evaluated in that case.
// units are assumed to be either MeV or GeV, depending on the useGeV flag.
TopnessTool::TopnessTool(const TLorentzVector& p_l_,
                         const TVector2& p_miss_xy_,
                         const TLorentzVector& p_j1_,
                         const TLorentzVector& p_j2_,
                         bool use_GeV) :
  p_nu_x(0), p_nu_y(0), p_nu_z(0), p_W_z(0),
  p_l(use_GeV ? p_l_ * 1e3 : p_l_),
  p_miss_xy(use_GeV ? p_miss_xy_ * 1e3 : p_miss_xy_),
  p_j1(use_GeV ? p_j1_ * 1e3 : p_j1_),
  p_j2(use_GeV ? p_j2_ * 1e3 : p_j2_)
{ }

TopnessTool::TopnessTool(const TLorentzVector& p_l_,
                         const TVector2& p_miss_xy_,
                         const TLorentzVector& p_j1_,
                         const TLorentzVector& p_j2_,
                         const TLorentzVector& p_j3_,
                         bool use_GeV) :
  p_nu_x(0), p_nu_y(0), p_nu_z(0), p_W_z(0),
  p_l(use_GeV ? p_l_ * 1e3 : p_l_),
  p_miss_xy(use_GeV ? p_miss_xy_ * 1e3 : p_miss_xy_),
  p_j1(use_GeV ? p_j1_ * 1e3 : p_j1_),
  p_j2(use_GeV ? p_j2_ * 1e3 : p_j2_),
  p_j3(use_GeV ? p_j3_ * 1e3 : p_j3_)
{ }

// constructor for use with SUSYTools
TopnessTool::TopnessTool(TLorentzVector Lepton,
                         const xAOD::JetContainer* jets,
                         const xAOD::MissingET* MET) :
  p_nu_x(0), p_nu_y(0), p_nu_z(0), p_W_z(0) // p_miss_xy(E_T_miss)
{
        p_miss_xy = TVector2(MET->mpx(),MET->mpy());
        p_l = Lepton;

  // loop over the jets in the container
  // FIXME: use a View container instead, to save memory
  /*xAOD::JetContainer* goodJets = new xAOD::JetContainer();
  xAOD::JetAuxContainer* goodJetsAux = new xAOD::JetAuxContainer();
  goodJets->setStore( goodJetsAux ); //< Connect the two
  xAOD::JetContainer::const_iterator j_itr = jets->begin();
  xAOD::JetContainer::const_iterator j_end = jets->end();
  for (;j_itr != j_end; j_itr++){
      xAOD::Jet* jet = new xAOD::Jet();
      jet->makePrivateStore( **j_itr );
      goodJets->push_back( jet );
  } */
 
  //std::sort(goodJets->begin(), goodJets->end(), [](xAOD::Jet* a, xAOD::Jet* b){return (a->btagging()->SV1plusIP3D_discriminant() > b->btagging()->SV1plusIP3D_discriminant() ) ;});
       
        int nBjet = 0;
        int index[5]={-1,-1,-1,-1,-1};
        for(unsigned int iJet =0; iJet < jets->size(); ++iJet){
                if (nBjet > 4) break;
                if (jets->at(iJet)->auxdata<char>("bjet") ){
                        index[nBjet] = iJet;
                        nBjet++;                       
                }              
        }
        //std::cout<<" Number of b-tag jets: "<< nBjet << std::endl;
        //std::cout<<" Number of jets: "<< jets->size() << std::endl;
       
        if (nBjet == 0){
          p_j1 = jets->at(0)->p4();
          p_j2 = jets->at(1)->p4();
        }
        else {
                p_j1 = jets->at(index[0])->p4();
               
                if (nBjet >=2){
                        p_j2 = jets->at(index[1])->p4();
                }
                else {
                        int ind1 = -1;
                        int ind2 = -1;
                        for(unsigned int iJet =0; iJet < jets->size(); ++iJet){
                                if (ind1 > -1 && ind2 > -1) break;
                                if (iJet != abs(index[0]) ){
                                        if (ind1 > -1){
                                                ind2 = iJet;
                                        }
                                        else {
                                                ind1 = iJet;
                                        }
                                                       
                                }              
                        }
                        //std::cout<<"ind1, ind2: "<<ind1<<" "<<ind2 << std::endl;
                  p_j2 = jets->at(ind1)->p4();
                  p_j3 = jets->at(ind2)->p4();
                       
                }
        }

  //delete goodJets;
  //delete goodJetsAux;
}


// adapter for TopnessTool::topness function, also converting parameters to MeV.
// (amoeba internally uses GeV).
struct bound_topness {
  bound_topness(TopnessTool* t_): t(t_) { }
  TopnessTool* t;
  double operator()(std::vector<double> x) {
    x[0] *= 1e3; x[1] *= 1e3; x[2] *= 1e3; x[3] *= 1e3;
    return t->topness(x);
  }
};

double TopnessTool::minimize(int n_iterations, double tolerance, double step) {
  static TRandom3 r;

  // using GeV inside amoeba, this seems to improve results.
        Amoeba am(tolerance * 1e3);
  step *= 1e3;

  // for 2 b-jets, there are two possible assignments.  if 3 jets were provided,
  // both combinations (j1,j2) and (j1,j3) will be tested, in both orders.
  int n_assignments = p_j3.E() > 0 ? 4 : 2;

  bound_topness func(this);

  // run several minimizations, starting from different parameter values.
  // keep overall best result.
  double fmin_best = 999;
        for (int i = 0; i < n_iterations; ++i) {
    // initial parameters are chosen random between +-4 TeV (parameters given to
    // amoeba are are in GeV to reduce numerical imprecision). eveywhere else
    // units are MeV.  the same value is used for all 4 parameters.
    std::vector<double> start(4, 4e3 * (2 * r.Rndm() - 1));

    for (int j = 0; j < n_assignments; ++j) {
      // run simplex minimization
      am.minimize(start, step, func);
      // keep track of overall best result
      if (am.fmin < fmin_best || i+j == 0) {
        fmin_best = am.fmin;
        p_nu_x = am.p[0][0] * 1e3;
        p_nu_y = am.p[0][1] * 1e3;
        p_nu_z = am.p[0][2] * 1e3;
        p_W_z = am.p[0][3] * 1e3;
      }

      // next time, run minimization using different b-jet assignment
      std::swap(p_j1, p_j2);
      // if three jets are used, switch j2 and j3 every other time
      if (p_j3.E() > 0 && j % 2 == 1)
        std::swap(p_j2, p_j3);
    }
  }
       
        return fmin_best;
}

double TopnessTool::topness() const {
  std::vector<double> params(4);
  params[0] = p_nu_x;
  params[1] = p_nu_y;
  params[2] = p_nu_z;
  params[3] = p_W_z;
  return topness(params);
}

static inline double sqr(double x) { return x * x; }

double TopnessTool::topness(const std::vector<double>& params) const {
  //// rewritten without TLorentzVector ////
  double E_nu = sqrt(params[0]*params[0] + params[1]*params[1] + params[2]*params[2]);
  double p1[4] = { p_l[0] + params[0], p_l[1] + params[1], p_l[2] + params[2], p_l[3] + E_nu };
  double delta_m_W1_sqr = p1[0]*p1[0] + p1[1]*p1[1] + p1[2]*p1[2] - p1[3]*p1[3] + m_W_sqr;
  p1[0] += p_j1[0]; p1[1] += p_j1[1]; p1[2] += p_j1[2]; p1[3] += p_j1[3];

  double p2[4] = { -params[0] + p_miss_xy.Px(),
                   -params[1] + p_miss_xy.Py(),
                   params[3], 0 };
  p2[3] = p_j2[3] + sqrt(m_W_sqr + p2[0]*p2[0] + p2[1]*p2[1] + p2[2]*p2[2]);
  p2[0] += p_j2[0]; p2[1] += p_j2[1]; p2[2] += p_j2[2];

  double delta_m_side1_sqr = p1[0]*p1[0] + p1[1]*p1[1] + p1[2]*p1[2] - p1[3]*p1[3] + m_top_sqr;
  double delta_m_side2_sqr = p2[0]*p2[0] + p2[1]*p2[1] + p2[2]*p2[2] - p2[3]*p2[3] + m_top_sqr;
  double delta_m_ttbar_sqr = sqr(p1[0] + p2[0]) + sqr(p1[1] + p2[1]) + sqr(p1[2] + p2[2]) - sqr(p1[3] + p2[3]) + 4 * m_top_sqr;

  double result2 = log(sqr(delta_m_W1_sqr / a_W_sqr) +
                       sqr(delta_m_side1_sqr / a_t_sqr) +
                       sqr(delta_m_side2_sqr / a_t_sqr) +
                       sqr(delta_m_ttbar_sqr / a_CM_sqr));

  return result2;
}
