
#include <vector>
#include <map>
#include <string>
#include <utility>

#include "SusySkimMaker/ObjectVariable.h"
#include "SusySkimMaker/LeptonVariable.h"
#include "SusySkimMaker/TrackVariable.h"
#include "SusySkimMaker/TruthVariable.h"
#include "SusySkimMaker/TruthEvent.h"
#include "SusySkimMaker/MuonVariable.h"
#include "SusySkimMaker/MuonRawVariable.h"
#include "SusySkimMaker/ElectronVariable.h"
#include "SusySkimMaker/JetVariable.h"
#include "SusySkimMaker/EventVariable.h"
#include "SusySkimMaker/MetVariable.h"
#include "SusySkimMaker/TauVariable.h"
#include "SusySkimMaker/PhotonVariable.h"
#include "SusySkimMaker/MetaData.h"
#include "SusySkimMaker/MCCorrections.h"
#include "SusySkimMaker/SampleSet.h"
#include "SusySkimMaker/V0Variable.h"
#include "SusySkimMaker/VSIVariable.h" //SICONG
#ifdef __CINT__
#pragma link C++ class V0Variable+;
#pragma link C++ class std::vector< V0Variable* >+;

#pragma link C++ class VSIVariable+;
#pragma link C++ class std::vector< VSIVariable* >+;

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class SampleSet+;
#pragma link C++ class vector<int>+;
#pragma link C++ class vector<TLorentzVector>+;
#pragma link C++ class vector<TVector3>+;

#pragma link C++ class ObjectVariable+;
#pragma link C++ class std::vector< ObjectVariable* >+;

#pragma link C++ class LeptonVariable+;
#pragma link C++ class std::vector< LeptonVariable* >+;

#pragma link C++ class TrackVariable+;
#pragma link C++ class std::vector< TrackVariable* >+;

#pragma link C++ class MuonVariable+;
#pragma link C++ class std::vector< MuonVariable* >+;

#pragma link C++ class MuonRawVariable+;
#pragma link C++ class std::vector< MuonRawVariable* >+;

#pragma link C++ class ElectronVariable+;
#pragma link C++ class std::vector< ElectronVariable* >+;

#pragma link C++ class JetVariable+;
#pragma link C++ class std::vector< JetVariable* >+;

#pragma link C++ class TauVariable+;
#pragma link C++ class std::vector< TauVariable* >+;

#pragma link C++ class PhotonVariable+;
#pragma link C++ class std::vector< PhotonVariable* >+;

#pragma link C++ class EventVariable+;
#pragma link C++ class MetVariable+;
#pragma link C++ class std::vector< MetVariable* >+;

#pragma link C++ class map<TString,bool>+;
#pragma link C++ class map<TString,float>+;
#pragma link C++ class map<TString,double>+;
#pragma link C++ class pair<TString,float>+;
#pragma link C++ class map<TString,std::map<TString,float>>+;
#pragma link C++ class pair<TString,bool>+;
//#pragma link C++ class pair<double,double>+;
#pragma link C++ class map<TString,std::pair<double,double>>+;
#pragma link C++ class map<TString,double>+;

#pragma link C++ class MetaData+;
#pragma link C++ class map<TString,TH1F*>+;
#pragma link C++ class map<TString,std::pair<TString,float>>+;
#pragma link C++ class map<TString,std::pair<TString,int>>+;
#pragma link C++ class map<TString,std::pair<TString,bool>>+;
#pragma link C++ class map<TString,std::pair<TString,TString>>+;
#pragma link C++ class vector< std::pair<TString,float> >+;
#pragma link C++ class map<int,float>+;
#pragma link C++ class map<EventVariable::EventCleaning,bool>+;

#pragma link C++ class TruthVariable+;
#pragma link C++ class std::vector< TruthVariable* >+;

#pragma link C++ class Stream;
#pragma link C++ class MetFlavor;
#pragma link C++ class TruthEvent+;

#pragma link C++ class MCCorrElement+;
#pragma link C++ class MCCorrContainer+;
#pragma link C++ class map<TString,MCCorrElement*>+;

#endif


