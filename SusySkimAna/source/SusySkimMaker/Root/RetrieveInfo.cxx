
// RootCore
#include "SusySkimMaker/RetrieveInfo.h"
#include "AsgTools/StatusCode.h"

// ROOT
#include "TFile.h"

// C++
#include <iomanip>
#include <sstream>

RetrieveInfo::RetrieveInfo() :
  m_cutFlowFormat("txt"),
  m_cutFlowScaleFactor(1),
  m_cutFlowPrefix("")
{

}
// ---------------------------------------------------------------------- //
void RetrieveInfo::Process()
{

  //
  // This method is only called from command line options
  //

  // Interface on the command line is with a text file
  // Convert to a vector, so we can call getXsec in other methods with a vector of samples
  if( !m_xSecDSIDs.IsWhitespace() ){

    Warning("Process","This method is outdated. Now done through SampleHandler!");
    return;

    std::vector<TString> samples;
    samples.clear();

    ifstream myFile ( m_xSecDSIDs.Data() );
    TString name;
    if ( myFile.is_open() ){
      while ( myFile.good() ){
    name.ReadLine(myFile);
    samples.push_back(name);
      }
    }
    myFile.close();

    // Do it!
    //getXsec(samples,"");

  }

  // Cut-flows
  if( !m_cutFlowFiles.IsWhitespace()  or !m_cutFlowProcesses.IsWhitespace() ) {
    outputCutflows();
  }


}
// ---------------------------------------------------------------------- //
void RetrieveInfo::getXsec(TString processName,std::map<int,double> norm,std::vector<TString> samples, EventObject* evt)
{

  // Checks
  if( !evt ){
    Error("RetrieveInfo::getXsec","Null EventObject pointer, cannot use it to extract cross sections!!");
    return;
  }


  std::string LatexString = "";
  unsigned int finalState = 0;

  LatexString = LatexString + "%<*table> \n";
  LatexString = LatexString + "\\begin{table}[h] \n";
  LatexString = LatexString + "\\begin{center} \n";
  LatexString = LatexString + "\\tiny \n";
  LatexString = LatexString + "%<*tabular> \n";
  LatexString = LatexString + "\\begin{tabular}{ccccccc} \n";
  LatexString = LatexString + "\\toprule \n";
  LatexString = LatexString + "Dataset ID & Process & Tags & $ \\sigma \\times \\epsilon$ [pb] & $k$-factor & Generator efficiency & $ \\mathcal{L}_{int} [\\mathrm{fb}^{-1}]$ \\\\ \n";
  LatexString = LatexString + "\\midrule \n";

  //
  for( auto& name : samples ){

    std::string theDatasetName = name.Data();

    // Data or MC?
    bool isData = !name.BeginsWith("mc");

    name.ReplaceAll("_","\\_");

    // Get DSID
    unsigned int theDSID = atoi((theDatasetName.substr(theDatasetName.find(".")+1,6)).c_str());

    if( theDSID == 0 ){
      Warning("RetrieveInfo::getXsec","Could not extract dataset ID, ignoring this dataset... ");
      continue;
    }

    float theXsec = -1;
    float theKfac = -1;
    float theGeff = -1;
    float theTotal = -1;
    float theIntLumi = -1;

    // Exact number of processed events
    double theNevents = 0.0;
    auto n = norm.find( theDSID );
    if( n==norm.end() ){
      Warning("RetrieveInfo::getXsec","Could not find number of events for sample %s",name.Data() );
    }
    else{
      theNevents = n->second;
    }

    if (!isData){

      theXsec = evt->getXsec(theDSID,finalState,EventObject::XSEC_INFO::XSEC);

      // TODO: Make more robust
      if( theXsec<=0 ){
    finalState = 2; // gluinos
    theXsec = evt->getXsec(theDSID,finalState,EventObject::XSEC_INFO::XSEC);
    if( theXsec<=0 ){
      finalState = 4; // squarks
      theXsec = evt->getXsec(theDSID,finalState,EventObject::XSEC_INFO::XSEC);
      if( theXsec<=0 ){
        Error("RetrieveInfo::getXsec","Cannot determine cross section!!");
        return;
      }
    }
      }

      //
      theKfac   = evt->getXsec(theDSID,finalState,EventObject::XSEC_INFO::KFAC);
      theGeff   = evt->getXsec(theDSID,finalState,EventObject::XSEC_INFO::GEFF);
      theTotal  = evt->getXsec(theDSID,finalState,EventObject::XSEC_INFO::ALL);
      theIntLumi = theNevents / theTotal / 1000.0;

      //std::cout << "-------------------------------- " <<  std::endl;
      //std::cout << "Cross-section: " << theXsec << std::endl;
      //std::cout << "Number of events : " << theNevents << std::endl;
      //std::cout << "K-factor: " << theKfac << std::endl;
      //std::cout << "Generator efficiency: " << theGeff << std::endl;
      //std::cout << "Total: " << theTotal << std::endl;
      //std::cout << "Integrated Lumi: " << theIntLumi << std::endl;
      //std::cout << "-------------------------------- " <<  std::endl;

    }

    char line[200];

    /*
    sprintf(line, "%i & %1.0f & %1.0f & %1.0f & %1.3f & %1.2f & %1.3f & %1.3f & %1.3f \\\\ \n", theDSID,
        qMass.Atof(),C1.Atof(),N1.Atof(),
        theXsec, theKfac, theGeff,theXsec*theKfac*theGeff,theNevents);
    TString qMass = ((TObjString*)name.Tokenize("_")->At(5))->String();
    TString C1    = ((TObjString*)name.Tokenize("_")->At(6))->String();
    TString N1    = ((TObjString*)name.Tokenize("_")->At(7))->String();
    N1 = ((TObjString*)N1.Tokenize(".")->At(0))->String();

    // mc15_13TeV.372600.MGPy8EG_A14N23LO_SS_onestepCC_225_125_25

    std::cout << "Nevents: " << theNevents << std::endl;
    std::cout << "q,C1,N1: " << qMass << "  " << C1 << "  " << N1 << std::endl;
    */


    TString datasetID = ((TObjString*)name.Tokenize(".")->At(1))->String();
    TString process   = ((TObjString*)name.Tokenize(".")->At(2))->String();
    TString tags      = ((TObjString*)name.Tokenize(".")->At(5))->String();

    if( tags.Contains("/") ) tags.ReplaceAll("/","");

    // Background only strings
    sprintf(line, "%i & %s & %s & %1.4f & %1.2f & %1.3f & %1.3f \\\\ \n", theDSID,
        process.Data(), tags.Data(),theXsec, theKfac, theGeff, theIntLumi);

    LatexString = LatexString + line;

  } // Loop over samples


  LatexString = LatexString + "\\bottomrule \n";
  LatexString = LatexString + "\\end{tabular} \n";
  LatexString = LatexString + "%</tabular> \n";
  LatexString = LatexString + "\\end{center} \n";
  LatexString = LatexString + "\\end{table} \n";
  LatexString = LatexString + "%</table> \n";

  // Output naming
  TString outFileName = "";
  if( !m_outputDirname.IsWhitespace() ){
    outFileName = m_outputDirname + "/";
  }
  outFileName += "INFO_" + processName + ".tex";

  if( outFileName.Contains("%") ){
    outFileName.ReplaceAll("%","");
  }


  // Write it out!!
  std::ofstream outFile( outFileName.Data() );
  if( !outFile.is_open() ){
    Error("RetrieveInfo::getXsec","Error, cannot open %s to write out cross section information!!",outFileName.Data() );
    return;
  }

  outFile << LatexString;
  outFile.close();

  Info("RetrieveInfo::getXsec","Created file %s with dataset %s",outFileName.Data(),processName.Data() );


}
// ---------------------------------------------------------------------- //
void RetrieveInfo::outputXsec()
{


}
// ---------------------------------------------------------------------- //
void RetrieveInfo::outputCutflows()
{

  if (!(m_cutFlowScaleFactor == 1)) {
    std::cout << "cutflows will be scaled by a factor of " << m_cutFlowScaleFactor << std::endl;
  }

  // Output cutflows for one process
  if (!m_cutFlowFiles.IsWhitespace()) {
    std::vector<TH1F*> hists = getAddedCutFlows(m_cutFlowFiles);
    for (unsigned int i=0; i < hists.size(); i++) {
      if (!(m_cutFlowScaleFactor == 1)) {
    hists[i]->Scale(m_cutFlowScaleFactor);
      }
      formatCutflow(hists[i]);
    }
  }

  // Output cutflows with different processes as columns
  if (!m_cutFlowProcesses.IsWhitespace()) {

    std::vector<std::pair<TString, TString> > titlesAndFiles = parseProcessFile(m_cutFlowProcesses);

    std::map<TString, TString> cutNameSubstitutions;
    if (!m_cutFlowCutNames.IsWhitespace()) {
      cutNameSubstitutions = parseCutNameFile(m_cutFlowCutNames);
    }

    std::vector<std::vector<TH1F*> > vectors;
    vectors.clear();

    // first process
    vectors.push_back(getAddedCutFlows(titlesAndFiles[0].second));

    // the other processes
    for (unsigned int i=1; i < titlesAndFiles.size(); i++) {
      vectors.push_back(getAdditionalProcessCutFlows(titlesAndFiles[i].second, vectors[0]));
    }

    for(unsigned int i=0; i<vectors[0].size(); i++){
      std::vector<TH1F*> processes;
      processes.clear();
      std::vector<TString> titles;
      for (unsigned int j=0; j < vectors.size(); j++) {
    TString histName = vectors[j][i]->GetName();
    if (vectors[j][i]->GetSumw2N() == 0 && !(histName.Contains("unweighted"))) {
      std::cout << "Sum of weights not stored for histogram " << histName;
      std::cout << " (" << titlesAndFiles[j].first << ")";
      std::cout << " - the errors won't be displayed correctly!" << std::endl;
    }
    if (!(m_cutFlowScaleFactor == 1)) {
      vectors[j][i]->Scale(m_cutFlowScaleFactor);
    }
    processes.push_back(vectors[j][i]);
    titles.push_back(titlesAndFiles[j].first);
      }
      formatCutflow(processes, titles, cutNameSubstitutions);
    }
  }

}
// ---------------------------------------------------------------------- //
std::vector<TH1F*> RetrieveInfo::getCutFlows(TString rootFileName, TString matchingName)
{

  std::vector<TH1F*> vecHists;
  vecHists.clear();

  TFile* file = new TFile(rootFileName,"READ");

  if( !file->IsOpen() ){
    std::cout << "<RetrieveInfo::outputCutflow> ERROR, cannot open root file: " << rootFileName << std::endl;
    return vecHists;
  }

  // Get all cutflow histograms
  TIter nextkey (file->GetListOfKeys());
  TKey *key = 0;
  while ((key = (TKey*)nextkey())) {
    TString name = key->GetName();

    // This proabably shouldn't be here...
    if( !name.Contains(matchingName) ) continue;

    TH1F* hist = (TH1F*)key->ReadObj();
    hist->SetDirectory(0);

    vecHists.push_back(hist);

  }

  file->Close();

  return vecHists;

}
// ---------------------------------------------------------------------- //
void RetrieveInfo::Add(TH1F*& global, TH1F* local)
{

  ///
  /// These histograms should have the same binning
  /// but might differ in there last bins,
  /// i.e. one hist didn't get a bin label, failed cuts
  ///

  if( local->GetNbinsX() > global->GetNbinsX() ){
    std::cout << "  <RetrieveInfo::Add> ERROR cannot add histograms" << std::endl;
    return;
  }

  // Add missing bin labels
  for(int g=1; g<=global->GetNbinsX(); g++){

    TString global_binLabel = global->GetXaxis()->GetBinLabel(g);
    TString local_binLabel  = local->GetXaxis()->GetBinLabel(g);

    // Only local has bin label, add a bin label to global
    if( !local_binLabel.IsWhitespace() && global_binLabel.IsWhitespace() ){
      global->GetXaxis()->SetBinLabel(g,local_binLabel.Data() );
    }
    // Only global has bin label, add a bin label to local
    if (!global_binLabel.IsWhitespace() && local_binLabel.IsWhitespace() ){
      local->GetXaxis()->SetBinLabel(g,global_binLabel.Data() );
    }

  }

  // Merge histograms
  TList * list = new TList;
  list->Add(local);
  global->Merge(list);

  delete list;

}
// ---------------------------------------------------------------------- //
std::vector<TH1F*> RetrieveInfo::getAddedCutFlows(TString cutFlowFiles)
{

  ///
  /// Returns a vector of added cutflow histograms
  ///

  std::cout << "Opening files from " << cutFlowFiles << std::endl;
  ifstream myFile ( cutFlowFiles );
  TString name;

  std::vector<TH1F*> vecHists;
  vecHists.clear();

  if ( myFile.is_open() ){
    while ( myFile.good() ){
      name.ReadLine(myFile);
      if (name.IsWhitespace()) {
    break;
      }
      std::cout << "Reading file: " << name << std::endl;
      std::vector<TH1F*> localVecHists = getCutFlows(name);
      // First iteration, just copy what we read from the first file
      if(vecHists.size()==0){
    vecHists = localVecHists;
      }
      // After add each histogram together
      else{
    // Match each hist
    for(unsigned int g=0; g<vecHists.size(); g++){
      // Find corresponding histogram from the file we are reading
      for(unsigned int l=0; l<localVecHists.size(); l++){
        TString global_name = vecHists[g]->GetName();
        if( !global_name.EqualTo( localVecHists[l]->GetName()) ) continue;
        else{
          Add(vecHists[g],localVecHists[l]);
          break;
        }
      } // local
    } // global
      } // else
    } // while good
  } // if is open

  return vecHists;

}
// ---------------------------------------------------------------------- //
std::vector<TH1F*> RetrieveInfo::getAdditionalProcessCutFlows(TString cutFlowFiles, std::vector<TH1F*>& baseVecHists)
{
  ///
  /// Returns a vector of cut flow histograms
  /// in the same order as baseVecHists
  ///

  std::vector<TH1F*> unsortedVecHists = getAddedCutFlows(cutFlowFiles);

  std::vector<TH1F*> vecHists;
  vecHists.clear();

  // Match each hist - this should be changed to be more robust
  for(unsigned int g=0; g<baseVecHists.size(); g++){
    // Find corresponding histogram in the unsorted vector
    for(unsigned int l=0; l<unsortedVecHists.size(); l++){
      TString global_name = baseVecHists[g]->GetName();
      if( !global_name.EqualTo( unsortedVecHists[l]->GetName()) ) {
    continue;
      }
      else{
    vecHists.push_back(unsortedVecHists[l]);
    break;
      }
    } // unsorted
  } // base

  return vecHists;

}
// ---------------------------------------------------------------------- //
std::vector<std::pair<TString, TString> > RetrieveInfo::parseProcessFile(TString cutFlowProcesses)
{
  ifstream myFile ( cutFlowProcesses.Data() );

  TString name;

  std::vector<std::pair<TString, TString> > titlesAndFiles;

  if ( myFile.is_open() ){
    while ( myFile.good() ){
      name.ReadLine(myFile);
      if (name.Contains(";")) {
    TObjArray *tokens = name.Tokenize(";");
    TObjString* token0 = (TObjString*) tokens->At(0);
    TObjString* token1 = (TObjString*) tokens->At(1);
    TString title = token0->GetString();
    TString txtfile = token1->GetString();
    title = title.Strip(TString::kBoth);
    txtfile = txtfile.Strip(TString::kBoth);
    std::pair<TString, TString> titleAndFile (title, txtfile);
    titlesAndFiles.push_back(titleAndFile);
    delete tokens;
      }
    }
  }

  return titlesAndFiles;
}
// ---------------------------------------------------------------------- //
std::map<TString, TString> RetrieveInfo::parseCutNameFile(TString cutFlowCutNames)
{
  ifstream myFile ( cutFlowCutNames.Data() );

  TString name;

  std::map<TString, TString> cutNameSubstitutions;

  if ( myFile.is_open() ){
    while ( myFile.good() ){
      name.ReadLine(myFile);
      if (name.Contains(";")) {
    TObjArray *tokens = name.Tokenize(";");
    TObjString* token0 = (TObjString*) tokens->At(0);
    TObjString* token1 = (TObjString*) tokens->At(1);
    TString cutname = token0->GetString();
    TString subst = token1->GetString();
    cutname = cutname.Strip(TString::kBoth);
    subst = subst.Strip(TString::kBoth);

    cutNameSubstitutions[cutname] = subst;
    delete tokens;
      }
    }
  }
  return cutNameSubstitutions;
}
// ---------------------------------------------------------------------- //
void RetrieveInfo::formatCutflow(TH1F*& hist)
{

  if( !hist ) return;

  std::cout << "Print cutflow for histogram: " << hist->GetName() << std::endl;
  std::cout << std::endl;

  for(int b = 1; b<=hist->GetNbinsX(); b++){
    if(strlen(hist->GetXaxis()->GetBinLabel(b))==0) continue;
    if (m_cutFlowFormat == "txt") {
      std::cout << std::left << std::setw(70)
        << hist->GetXaxis()->GetBinLabel(b)
        << std::setw(10) <<  std::fixed << hist->GetBinContent(b)
        << std::endl;
    }
    if (m_cutFlowFormat == "tex") {
      std::cout << std::left << std::setw(70)
        << hist->GetXaxis()->GetBinLabel(b)
        << std::setw(10) << std::fixed << "&" <<  hist->GetBinContent(b)
        << "\\\\" << std::endl;
    }
  }

  std::cout << std::endl;

}
// ---------------------------------------------------------------------- //
void RetrieveInfo::formatCutflow(std::vector<TH1F*>& hists,
                 std::vector<TString>& titles,
                 std::map<TString, TString> cutNameSubstitutions)
{

  std::stringstream ss;

  if (m_cutFlowFormat == "txt") {
    std::cout << "Print cutflow for histogram: " << hists[0]->GetName() << std::endl;
    std::cout << std::endl;

    std :: cout << std::setw(30) << "";
    for (unsigned int i = 0; i < titles.size(); i++) {
      std:: cout << std::setw(20) << titles[i];
    }
    std::cout << std::endl << std::endl;

    for(int b = 1; b<=hists[0]->GetNbinsX(); b++) {
      if(strlen(hists[0]->GetXaxis()->GetBinLabel(b))==0) continue;

      std::cout << std::left << std::setw(30) << hists[0]->GetXaxis()->GetBinLabel(b);

      // loop over all processes
      for (unsigned int i = 0; i < hists.size(); i++) {
    std::cout << std::setw(20) <<  hists[i]->GetBinContent(b);
      }
      std::cout << std::endl;
    }

    std::cout << std::endl;
  }
  if (m_cutFlowFormat == "tex") {

    TString histName = hists[0]->GetName();

    std::cout << std::endl << "Writing cutflow for histogram: " << histName << std::endl;
    std::cout << std::endl;

    ss << "\\begin{tabular}{l";

    // Process titles
    for (unsigned int i =0; i < titles.size(); i++) ss << "c";
    ss << "}" << std::endl;
    ss << "\\hline\\hline" << std::endl;
    ss << std::left << std::setw(40) << "";
    for (unsigned int i = 0; i < titles.size(); i++) {
      ss << "& " << std::left << std::setw(28) << titles[i];
    }
    ss << "\\\\" << std::endl;
    ss << "\\hline" << std::endl;

    // Substituted bin labels (cut names)
    for(int b = 1; b<=hists[0]->GetNbinsX(); b++) {
      if(strlen(hists[0]->GetXaxis()->GetBinLabel(b))==0) continue;

      TString binLabel = hists[0]->GetXaxis()->GetBinLabel(b);
      if (cutNameSubstitutions.count(binLabel)) {
    binLabel = cutNameSubstitutions[binLabel];
      } else {
    binLabel = binLabel.ReplaceAll(">","$>$").ReplaceAll("<","$<$");
      }
      ss << std::left << std::setw(40) << binLabel;

      // loop over all processes and write yields
      for (unsigned int i = 0; i < hists.size(); i++) {
    int prec;
    if (histName.Contains("unweighted")) {
      prec = 0;
    } else {
      prec = 2;
    }
    std::stringstream tmpss;
    tmpss << "& " <<  std::fixed << std::setprecision(prec) << hists[i]->GetBinContent(b);

    // don't write errors for the unweighted cutflows
    if (!histName.Contains("unweighted")) {
      tmpss << "$\\pm$" << std::fixed << std::setprecision(2) << hists[i]->GetBinError(b);
    }
    ss << std::left << std::setw(30) << tmpss.str();
      }
      ss << "\\\\" << std::endl;
    }
    ss << "\\hline\\hline" << std::endl;
    ss << "\\end{tabular}" << std::endl;

    if (ofs.is_open()) {
      ofs.close();
    }

    if (!m_outputDirname.IsWhitespace()) {
      TString cutFlowFileName = m_outputDirname.Strip(TString::kTrailing, '/')+"/"+m_cutFlowPrefix+"_"+histName+".tex";
      std::cout << "Writing to file: " << cutFlowFileName << std::endl;
      ofs.open(cutFlowFileName, std::ofstream::out);
      ofs << ss.str();
    } else {
      std::cout << "No outputdir given, dumping to stdout ..." << std::endl << std::endl;
      std::cout << ss.str();
    }

  }
}
// ---------------------------------------------------------------------- //
// ---------------------------------------------------------------------- //
const MetaData* RetrieveInfo::getMetaData(TString rootFileName)
{

  TFile* file = new TFile(rootFileName,"READ");

  if( !file->IsOpen() ){
    std::cout << "<RetrieveInfo::getMetaData> ERROR, cannot open root file: " << rootFileName << std::endl;
    abort();
  }

  MetaData* dummy = new MetaData();

  return dummy;

}
// ---------------------------------------------------------------------- //
bool RetrieveInfo::getHist(TString rootFile,TString histName, TH1*& hist)
{


  // Extract histogram
  TFile* file = TFile::Open( rootFile.Data(), "READ");

  if( !file->IsOpen() ){
    Error("RetrieveInfo::getXsec","Cannot open %s to get weighted number of events!",rootFile.Data() );
    return false;
  }

  if( file->FindKey( histName.Data() ) ){
    hist = dynamic_cast<TH1F*>(file->Get( histName.Data() ));
    hist->SetDirectory(0);
  }
  else{
    Error("RetrieveInfo::getXsec","Cannot open find weighted__AOD hist in merged root file!!" );
    return false;
  }

  // Finished with this rootfile
  file->Close();

  return true;

}
// ---------------------------------------------------------------------- //
void RetrieveInfo::compareNEvents(TString rootFileName)
{

  std::vector<TH1F*> counters;
  counters.clear();

  // Get all instances of event counter in this rootfile, should really be only one
  counters = getCutFlows(rootFileName,"eventCounter");

  if(counters.size()==0 || counters.size()>1){
    std::cout << "<RetrieveInfo::compareNEvents> ERROR Could not find eventCounter histogram in : " << rootFileName << std::endl;
    abort();
  }

  // Checked the vector is size 1.
  TH1* eventCounterHist = counters[0];

  // The title is storing the DSID
  TString dsid = eventCounterHist->GetTitle();

  // We need some file storing this information. Just get it directly from AMI?

}
