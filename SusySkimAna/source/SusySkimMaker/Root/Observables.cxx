#include "SusySkimMaker/Observables.h"
#include "SusySkimMaker/Objects.h"
#include "SusySkimMaker/MT2_ROOT.h"
#include "SusySkimMaker/Sphericity.h"
#include "SusySkimMaker/TopnessTool.h"
#include "SusySkimMaker/TTbar2LTagger.h"
#include "SusySkimMaker/TMctLib.h"
#include "SusySkimMaker/MsgLog.h"

#include <fastjet/PseudoJet.hh>
#include <fastjet/ClusterSequence.hh>

// http://pdg.lbl.gov/2012/listings/rpp2012-list-w-boson.pdf
// http://pdg.lbl.gov/2012/listings/rpp2012-list-z-boson.pdf
const float WBOSONMASS = 80.3850;
const float ZBOSONMASS = 91.1876;

Observables::Observables() :
  m_dbg(0)
{

}
// ------------------------------------------------------------------------- //
double Observables::getLifetimeWeight(const Objects* obj, double genTau, double rwtTau)
{

  double weight = 1.0;
  const double c = TMath::C() / (1e-3)*(1e-9);// mm/ns

  for( auto& chargino : obj->charginos ){
    double bFactor_t = chargino->Pt()  / chargino->M();
    if( chargino->decayVtxPerp==0 ) continue;
    double ptime = chargino->decayVtxPerp / bFactor_t / c;
    weight *= (genTau / rwtTau) * exp (-ptime * ( (1.0 / rwtTau) - (1.0 / genTau) ));
  }

  return weight;

}
// ------------------------------------------------------------------------- //
bool Observables::reconstructSSWW(const Objects* obj)
{

  // Exactly two leptons
  if( obj->signalLeptons.size() != 2 ) return false;

  // Require leptons to be same sign
  //if( (obj->signalLeptons[0]->q*obj->signalLeptons[1]->q) < 0 ) return false;

  //
  return true;

}
// ------------------------------------------------------------------------- //
bool Observables::reconstructWZ(const Objects* obj,RecoBoson& Wboson, RecoBoson& Zboson, bool useBaseline)
{

  LeptonVector leptons = useBaseline ? obj->baseLeptons : obj->signalLeptons;

  if( leptons.size() != 3 ) return false;

  float bestMass = -1.0;

  // Find a Z boson
  for( auto& lep1 : leptons ){
    for( auto& lep2 : leptons ){

      //
      if( lep1==lep2 ) continue;

      // Require SFOS pairs
      if( !isSFOS(lep1,lep2) ) continue;

      float mass = (*lep1+*lep2).M();

      if( bestMass<0.0 || fabs(mass-ZBOSONMASS)<fabs(bestMass-ZBOSONMASS) ){

    bestMass = mass;

    // Update parent information
    Zboson.parent.SetPtEtaPhiM( (*lep1+*lep2).Pt(),
                    (*lep1+*lep2).Eta(),
                    (*lep1+*lep2).Phi(),
                    (*lep1+*lep2).M() );

    // New best children
    Zboson.children.clear();
    Zboson.children.push_back(lep1);
    Zboson.children.push_back(lep2);

      }

    }
  }

  // Require a successfully reconstructed Z boson
  if( Zboson.children.size() !=2  ) return false;

  // Now reconstruct the W boson from the only remaining lepton
  // in this event
  for( auto& Wlep : leptons ){
    if( Wlep==Zboson.children[0] || Wlep==Zboson.children[1] ) continue;
    else{

      // W boson candidate
      Wboson.parent.SetPxPyPzE( (*Wlep+obj->met).Px(),
                                (*Wlep+obj->met).Py(),
                                (*Wlep+obj->met).Pz(),
                                (*Wlep+obj->met).E() );

      // Only lepton left
      Wboson.children.push_back(Wlep);

    }
  }

  // Should never happen, since this function exactly requires 3 leptons
  if( Wboson.children.size() != 1 ) return false;

  return true;

}
// ------------------------------------------------------------------------- //
bool Observables::reconstructZZ(const Objects* obj,RecoBoson& Zleading,RecoBoson& Zsubleading, bool useBaseline)
{

  //
  // Returns true when is a ZZ event
  //

  LeptonVector leptons = useBaseline ? obj->baseLeptons : obj->signalLeptons;

  // Require exactly four leptons
  if( leptons.size() !=4 ) return false;

  float bestMass = -1.0;

  // Find a Z boson
  for( auto& lep1 : leptons ){
    for( auto& lep2 : leptons ){

      //
      if( lep1==lep2 ) continue;

      // Require SFOS pairs
      if( !isSFOS(lep1,lep2) ) continue;

      float mass = (*lep1+*lep2).M();

      if( bestMass<0.0 || fabs(mass-ZBOSONMASS)<fabs(bestMass-ZBOSONMASS) ){

	bestMass = mass;

	// Update parent information
	Zleading.parent.SetPtEtaPhiM( (*lep1+*lep2).Pt(),
				      (*lep1+*lep2).Eta(),
				      (*lep1+*lep2).Phi(),
				      (*lep1+*lep2).M() );

	// New best children
	Zleading.children.clear();
	Zleading.children.push_back(lep1);
	Zleading.children.push_back(lep2);

      }
    }
  }

  // Didn't find any Z candidates
  if( Zleading.children.size() != 2 ) return false;

  // Search for the subleading Z
  for( auto& lep1 : leptons ){

    // Check if used?
    if( lep1==Zleading.children[0] || lep1==Zleading.children[1] ) continue;

    for( auto& lep2 : leptons ){

      //
      if( lep1==lep2 ) continue;

      // Require SFOS pairs
      if( !isSFOS(lep1,lep2) ) continue;

      //
      if( lep2==Zleading.children[0] || lep2==Zleading.children[1] ) continue;

      // Should be the only two leptons left

      // Update parent information
      Zsubleading.parent.SetPtEtaPhiM( (*lep1+*lep2).Pt(),
				       (*lep1+*lep2).Eta(),
				       (*lep1+*lep2).Phi(),
				       (*lep1+*lep2).M() );

      // New best children
      Zsubleading.children.clear();
      Zsubleading.children.push_back(lep1);
      Zsubleading.children.push_back(lep2);

    }
  }

  if( Zsubleading.children.size() !=2 ) return false;

  return true;

}

// -----------------------------------------------------------------------
std::vector <TLorentzVector> Observables::solveInvisiblePz_2Bdecay(const TLorentzVector &p1, TLorentzVector p2_pt, const double &mX, const double &m1, const double &m2){

  //
  // Kinematic reconstruction of 2-body decays where one of the child is invisible
  // Topology: X->1 2 (W->lv type)
  //
  // - Known (provided by the arguments): 1's 4-mom, 2's pT vector, masses of parent/children
  // - Calculate the pz of the invisible particle using the mass constraint equation on the parent mass
  // - Output: possible set of invisible particle's 4-mom.
  //   The solution is always two-fold since the mass constraint is given byquadrutic equation.
  //   No solution is found when mT(1,2)>mX, in which case the singular approximated solution assuming mX=mT(1,2) is returned.
  //

  std::vector <TLorentzVector> solutions;

  TLorentzVector tmp_pt1, tmp_pt2;
  tmp_pt1.SetXYZM(p1.Px()   , p1.Py()   , 0., m1);
  tmp_pt2.SetXYZM(p2_pt.Px(), p2_pt.Py(), 0., m2);

  double MTsq = (tmp_pt1+tmp_pt2).M2();
  double dm2 = ( pow(mX,2) - MTsq ) / 2.;
  double ET1 = tmp_pt1.E();
  double ET2 = tmp_pt2.E();
  double Dterm = (dm2 + 2*ET1*ET2) * dm2;

  if(Dterm>0){
    TLorentzVector sol;

    double p2_pz = (  (dm2+ET1*ET2)*p1.Pz() + p1.E()*sqrt(Dterm) ) / pow(ET1,2) ;
    sol.SetXYZM(p2_pt.Px(), p2_pt.Py(), p2_pz, m2);
    solutions.push_back(sol);

    p2_pz = ( (dm2+ET1*ET2)*p1.Pz() - p1.E()*sqrt(Dterm) ) / pow(ET1,2);
    sol.SetXYZM(p2_pt.Px(), p2_pt.Py(), p2_pz, m2);
    solutions.push_back(sol);

  } else{
    double p2_pz = ET2*p1.Pz() / ET1;

    TLorentzVector sol; sol.SetXYZM(p2_pt.Px(), p2_pt.Py(), p2_pz, m2);
    solutions.push_back(sol);
  };
  return solutions;

}
// ------------------------------------------------------------------------- //
TLorentzVector Observables::reconstructNeutrino(const Objects* obj, const LeptonVariable* lep, float WMass, int solution_bit){

  ///
  /// Reconstruct the neurino 4-momentum assuming the event is W->lv, using the leading lepton 4-mom, MET and the W-mass contraint as the input
  /// The solution is generally two-fold (except no solution is found and the approximated solution is quoted. See the description of solveInvisiblePz_2Bdecay above),
  /// which can be by specified using solution_bit (0->W's 4-momentum with larger |neutraino_pz|, 1->smaller one).
  ///

  if(WMass<0) WMass = WBOSONMASS;

  TLorentzVector pMis; pMis.SetPtEtaPhiM(obj->met.Et, 0., obj->met.phi, 0.);

  std::vector <TLorentzVector> v = solveInvisiblePz_2Bdecay(*lep, pMis, WMass, 0., 0.);
  if(solution_bit==0 || v.size()==1) return v[0];
  else                               return v[1];

}
// ------------------------------------------------------------------------- //
bool Observables::isSFOS(LeptonVariable* lep1,LeptonVariable* lep2)
{

  if( !isOS(lep1,lep2) ) return false;
  if( !isSF(lep1,lep2)  ) return false;

  return true;

}
// ------------------------------------------------------------------------- //
bool Observables::isOS(LeptonVariable* lep1,LeptonVariable* lep2)
{

  if( lep1->q*lep2->q > 0 ) return false;

  // Leptons are OS
  return true;

}
// ------------------------------------------------------------------------- //
bool Observables::isSF(LeptonVariable* lep1,LeptonVariable* lep2)
{

  if( lep1->isMu() && lep2->isMu() ) return true;
  if( lep1->isEle() && lep2->isEle() ) return true;

  return false;


}
// ------------------------------------------------------------------------- //
MuonVariable* Observables::cast_to_muon(LeptonVariable* lep)
{

  //
  if( !lep->isMu() ) abort();

  MuonVariable* mu = dynamic_cast<MuonVariable*>( lep ) ;
  return mu;

}
// ------------------------------------------------------------------------- //
ElectronVariable* cast_to_electron(LeptonVariable* lep)
{

  if( !lep->isEle() ) abort();

  ElectronVariable* ele = dynamic_cast<ElectronVariable*>( lep ) ;
  return ele;

}
// ------------------------------------------------------------------------- //
int Observables::getNJets(const Objects*obj, float jet_threshold)
{

  int nJets = 0;

  //
  for(auto&j : obj->cJets){
    if( j->Pt() > jet_threshold ) nJets++;
  }

  return nJets;

}
// ------------------------------------------------------------------------- //
int Observables::getNTruthJets(const Objects*obj, float jet_threshold)
{

  int nTruthJets = 0;

  //
  for(auto&j : obj->truthJets ){
    if( j->Pt() < jet_threshold  ) continue;
    nTruthJets++;
  }

  return nTruthJets;

}
// ------------------------------------------------------------------------- //
int Observables::getNBJets(const Objects*obj, float jet_threshold)
{

  int nBJets = 0;

  //
  for(auto&j : obj->cJets){

    //
    if( !j->bjet ) continue;

    //
    if( j->Pt() > jet_threshold ) nBJets++;

  }

  return nBJets;

}
// ------------------------------------------------------------------------- //
int Observables::getNTruthBJets(const Objects*obj, float jet_threshold)
{

  int nTruthBJets = 0;
  for(auto&j : obj->truthJets ){
    if( j->Pt() < jet_threshold  ) continue;
    if( !j->bjet ) continue;
    nTruthBJets++;
  }

  return nTruthBJets;

}
// ------------------------------------------------------------------------- //
float Observables::getHadWMass(const Objects* obj, float& wPt)
{

  /*
    Reconstruct the hadronic W-mass from central jets
    When a candidate is not found, return -1.0
  */

  float wMass = -1.0;

  for( auto& jet1 : obj->cJets ){
    for( auto& jet2 : obj->cJets ){

      if(jet1==jet2) continue;

      // GeV
      float _wMass = (*jet1+*jet2).M();

      // Choose the mass closest to the W-pole
      if( wMass < 0 ){
    wMass = _wMass;
        wPt    = (*jet1+*jet2).Pt();
    continue;
      }

      // Update best candidate
      if( fabs( _wMass-WBOSONMASS ) < fabs( wMass-WBOSONMASS ) ){
        wMass  = _wMass;
        wPt    = (*jet1+*jet2).Pt();
      }
    }
  }

  return wMass;

}
// ------------------------------------------------------------------------- //
float Observables::getAplanarity(const Objects* obj, unsigned int njet, unsigned int nlep, float minjetpt, bool useBaseline)
{
  LeptonVector leptons    = obj->signalLeptons;
  if(useBaseline) leptons = obj->baseLeptons;

  if( njet == 0 || njet > obj->cJets.size() ) njet = obj->cJets.size();
  if( nlep > leptons.size() )                 nlep = leptons.size();
  int OverThreshold=0;
  vector<TLorentzVector> v_tlv;

  int NlepOverThreshold=0;
  for (unsigned int lep=0; lep<nlep; lep++){
    NlepOverThreshold+=1;
    TLorentzVector lepTemp;
    lepTemp.SetPtEtaPhiM(leptons.at(lep)->Pt(),
                         leptons.at(lep)->Eta(),
                         leptons.at(lep)->Phi(),
                         leptons.at(lep)->M());
    v_tlv.push_back(lepTemp);

  }

  double Ap=-1;

  int NjetsOverThreshold=0;

  for(unsigned int jet=0; jet<njet;jet++){
    if(obj->cJets.at(jet)->Pt() < minjetpt) continue;
    NjetsOverThreshold+=1;

    TLorentzVector jetTemp;
    jetTemp.SetPtEtaPhiM(obj->cJets.at(jet)->Pt(),
                 obj->cJets.at(jet)->Eta(),
                 obj->cJets.at(jet)->Phi(),
                 obj->cJets.at(jet)->M());
    v_tlv.push_back(jetTemp);

  }
  OverThreshold=NlepOverThreshold+NjetsOverThreshold;
  Sphericity ap;
  ap.SetTLV(v_tlv, OverThreshold);
  ap.GetAplanarity(Ap);
  return Ap;

}
// ------------------------------------------------------------------------- //
float Observables::getTopness(const Objects* obj)
{

  float Topness =0;
  LeptonVector lep = obj->signalLeptons;

  if((obj->cJets.size() < 2) || (obj->cJets.size() < 3 && obj->bJets.size() >0)){
    return Topness;
  }

  if(lep.size() != 1){ //if(lep.size()>1 || lep.size()<1){
    if(m_dbg>=2) std::cout << " <WARNING::Observables> Request to get Topness with LeptonVector size: " << lep.size() << std::endl;
    if(m_dbg>=2) std::cout << " <WARNING::Observables> Function needs exactly one lepton, please choose one before calling this function." << std::endl;
    return Topness;
  }

  const TLorentzVector* lepton = dynamic_cast<const TLorentzVector*>(obj->signalLeptons.at(0));
  const TLorentzVector* met = dynamic_cast<const TLorentzVector*>(&(obj->met));
  TLorentzVector* jet1 = NULL;
  TLorentzVector* jet2 = NULL;
  TLorentzVector* jet3 = NULL;

  // 1 b-jet
  if (obj->bJets.size() == 1) {
    // jet1: b-jet
    jet1 = dynamic_cast<TLorentzVector*>(obj->bJets.at(0));
    // jet2, jet3: first and second non-
    for (JetVariable* jet: obj->cJets) {
      if (!jet->bjet && !jet2) { // found jet2
        jet2 = dynamic_cast<TLorentzVector*>(jet);
        continue;
      }
      if (!jet->bjet && (jet2 && !jet3)) { // jet2 already found -> found jet3
        jet3 = dynamic_cast<TLorentzVector*>(jet);
        break; // we're done
      }
    }
    if (!jet2 || !jet3) {
      std::cout << "<WARNING::Observables::getTopness> Couldn't find 2 non-bjets - Something went wrong" << std::endl;
      return Topness;
    }
  }
  // 2+ b-jets
  else if (obj->bJets.size() >= 2) {
    jet1 = dynamic_cast<TLorentzVector*>(obj->bJets.at(0));
    jet2 = dynamic_cast<TLorentzVector*>(obj->bJets.at(1));
  }
  // no b-jets
  else {
    jet1 = dynamic_cast<TLorentzVector*>(obj->cJets.at(0));
    jet2 = dynamic_cast<TLorentzVector*>(obj->cJets.at(1));
  }

  if (jet3) {
    TopnessTool calculator(*lepton, TVector2(met->Px(), met->Py()), *jet1, *jet2, *jet3, 1);
    Topness = calculator.minimize();
  } else {
    TopnessTool calculator(*lepton, TVector2(met->Px(), met->Py()), *jet1, *jet2, 1);
    Topness = calculator.minimize();
  }

  return Topness;

}

// ------------------------------------------------------------------------- //
float Observables::getMinDrJet(const Objects* obj, const TLorentzVector* ref, float minjetpt)
{
  float mindr = 99;
  for( auto& j : obj->cJets ){
    if(  j->Pt() < minjetpt ) continue;
    float dr =  j->DeltaR(*ref, m_useRapidity);
    if (dr < mindr && dr > 0.0001)
      mindr = dr;
  }
  return mindr;

}
// ------------------------------------------------------------------------- //
float Observables::getMinDrBJet(const Objects* obj, const TLorentzVector* ref, float minjetpt)
{
  float mindr = 99;
  for( auto& j : obj->cJets ){
    if(! j->bjet) continue;
    if( j->Pt() < minjetpt ) continue;
    float dr =  j->DeltaR(*ref, m_useRapidity);
    if (dr < mindr && dr > 0.0001)
      mindr = dr;
  }
  return mindr;

}
// ------------------------------------------------------------------------- //
float Observables::getHt(const Objects* obj,unsigned int njet, float minjetpt)
{

  if (njet == 0 || njet > obj->cJets.size()) njet = obj->cJets.size();

  float Ht = 0;

  for(unsigned int jet=0; jet<njet;jet++){
    if(obj->cJets.at(jet)->Pt() < minjetpt) continue;
    Ht += obj->cJets.at(jet)->Pt();
  }

  return Ht;

}
// ------------------------------------------------------------------------- //
float Observables::getTruthHt(const Objects* obj,unsigned int njet, float minjetpt)
{

  if (njet == 0 || njet > obj->truthJets.size()) njet = obj->truthJets.size();

  float Ht = 0;

  for(unsigned int jet=0; jet<njet;jet++){
    if(obj->truthJets.at(jet)->Pt() < minjetpt) continue;
    Ht += obj->truthJets.at(jet)->Pt();
  }

  return Ht;

}
// ------------------------------------------------------------------------- //
float Observables::getMeff(const Objects* obj, unsigned int njet, float minjetpt)
// Calcualtes effective mass (sum of all objects' transverse momenta + MET)
// Jets: multiplicity and pt threshold can be specified, defaults: njet=0, minjetpt=30.0
//       e.g. for MeffInc30 specify minjetpt=30.0, for MeffExcl3 specify njet=3
{

  if (njet == 0 || njet > obj->cJets.size()) njet = obj->cJets.size();

  float meff = 0;

  // Leptons
  for(unsigned int mu=0; mu<obj->baseMuons.size();mu++){
    meff += obj->baseMuons.at(mu)->Pt();
  }
  for(unsigned int ele=0; ele<obj->baseElectrons.size();ele++){
    meff += obj->baseElectrons.at(ele)->Pt();
  }

  //  Jets
  for(unsigned int jet=0; jet<njet;jet++){
    if(obj->cJets.at(jet)->Pt() < minjetpt) continue;
    meff += obj->cJets.at(jet)->Pt();
  }

  // mET
  meff += obj->met.Et;

  return meff;

}
// ------------------------------------------------------------------------- //
float Observables::getTruthMeff(const Objects* obj, unsigned int njet, float minjetpt)
{

  if (njet == 0 || njet > obj->truthJets.size()) njet = obj->truthJets.size();

  float meff = 0;

  // Leptons
  for(unsigned int l=0;l<obj->truthLeptons.size();l++){
    meff += obj->truthLeptons.at(l)->Pt();
  }

  // Jets
  for(unsigned int jet=0; jet<njet;jet++){
    if(obj->truthJets.at(jet)->Pt() < minjetpt) continue;
    meff += obj->truthJets.at(jet)->Pt();
  }

  // mET
  meff += obj->met.getTruthTLV().Et();

  return meff;

}
// ------------------------------------------------------------------------- //
float Observables::getMeffRel(const Objects* obj, unsigned int njet, float minjetpt)
// Calculates MET / effective mass
// Jets: multiplicity and pt threshold can be specified, defaults: njet=0, minjetpt=30.0
//       e.g. for MeffExcl3Rel specify njet=3
{

  return obj->met.Et / getMeff(obj, njet, minjetpt);

}

// ------------------------------------------------------------------- //

float Observables::getMetRel(const Objects* obj, float DeltaPhi)
{
  //
  //Relative Met, weighted by angular distance to nearest object if DeltaPhi < Pi/2
  //

  float MetRel=-999.0;

  // TLorentzVector* met_vec = dynamic_cast<TLorentzVector*>(&met);
  if(DeltaPhi <0){cout << "WARNING: DELTAPHI VALUE NEGATIVE" << endl;}
  if(DeltaPhi >= M_PI/2){MetRel = obj->met.Et;}
  else if(DeltaPhi < M_PI/2){MetRel = (obj->met.Et)*sin(DeltaPhi);}
  else cout << "WARNING: DELTAPHI VALUE OUT OF SCOPE" << endl;

  return MetRel;
}

// ------------------------------------------------------------------------- //
float Observables::getMll(const Objects* obj, bool useBaseline)
{

  float mll = 0.0;
  LeptonVector lep = obj->signalLeptons;
  if(useBaseline) lep = obj->baseLeptons;

  if( lep.size() < 2 ){
    if(m_dbg>=2) std::cout << " <WARNING::Observables> Request to get mll with LeptonVector size: " << lep.size() << std::endl;
    return mll;
  }


  mll = ((*lep.at(0)) + (*lep.at(1))).M();

  return mll;


}

// ------------------------------------------------------------------------- //
float Observables::getMt(const Objects* obj, bool useBaseline, unsigned int lepindex, bool useTrackMET)
{

  float mT = 0.0;

  LeptonVector lep;

  if(useBaseline) lep = obj->baseLeptons;
  else            lep = obj->signalLeptons;

  if(lep.size() < lepindex+1){ //if(lep.size()>1 || lep.size()<1){
    if(m_dbg>=2) std::cout << " <WARNING::Observables> Request to get mT with LeptonVector size: " << lep.size() << std::endl;
    if(m_dbg>=2) std::cout << " <WARNING::Observables> Function needs exactly one lepton, please choose one before calling this function." << std::endl;
    return mT;
  }

  if(!useTrackMET){
    mT = TMath::Sqrt(2.0 * lep.at(lepindex)->Pt() * obj->met.Et * (1.0 - TMath::Cos(lep.at(lepindex)->DeltaPhi(obj->met) ) ));
  }
  else{
    mT = TMath::Sqrt(2.0 * lep.at(lepindex)->Pt() * obj->trackMet.Et * (1.0 - TMath::Cos(lep.at(lepindex)->DeltaPhi( obj->trackMet ) ) ));
  }

  return mT;


}
// ------------------------------------------------------------------------- //
float Observables::getMt(const MetVariable& met, LeptonVariable* lep)
{
  return TMath::Sqrt(2.0 * lep->Pt() * met.Et * (1.0 - TMath::Cos(lep->DeltaPhi(met) ) ));
}
// ------------------------------------------------------------------------- //
float Observables::getTruthMt(const Objects* obj)
{


  float mT = 0.0;

  // Check a lepton exists
  if( obj->truthLeptons.size()==0 ) return mT;

  // Calculate mT with truth lepton and truth met
  mT = TMath::Sqrt(2.0 * obj->truthLeptons[0]->Pt() * obj->met.getTruthTLV().Et() * (1.0 - TMath::Cos(obj->truthLeptons[0]->DeltaPhi(obj->met.getTruthTLV()) ) ));

  return mT;

}
// ------------------------------------------------------------------------- //
float Observables::getMtWg(const Objects* obj, unsigned int photon_idx)
{

  float mtwg = 0.0;

  // Photon checks
  if( photon_idx>obj->basePhotons.size() ){
    MsgLog::WARNING("Observables::getMtWg","Photon index %i is larger than the number of baseline photons %i",photon_idx,obj->basePhotons.size() );
    return mtwg;
  }

  // Lepton checks
  if( obj->baseLeptons.size()==0 ){
    MsgLog::WARNING("Observables::getMtWg","No base lepton in the event, impossible to calculate getMtWg" );
    return mtwg;
  }

  auto p = obj->signalPhotons[photon_idx];
  auto l = obj->baseLeptons[0];

  // Each term separately
  float gmterm = 2.0 * p->Et() * obj->met.Et * ( 1.0 - TMath::Cos(p->DeltaPhi(obj->met)) );
  float lmterm = 2.0 * l->Et() * obj->met.Et * ( 1.0 - TMath::Cos(l->DeltaPhi(obj->met)) );
  float glterm = 2.0 * p->Et() * l->Et() * ( 1.0 - TMath::Cos(p->DeltaPhi(*l)) );

  //
  mtwg = gmterm + lmterm + glterm;

  // Return!
  return TMath::Sqrt(mtwg);

}
// ------------------------------------------------------------------- //
float Observables::getMct(const Objects* obj)
{

  if(obj->cJets.size() < 2 ) return 0;

  TVector2 m1;
  m1.TVector2::SetMagPhi(obj->met.Et,obj->met.phi);

  TMctLib mctcalc;

  float mymct  = 0;
  if(obj->baseLeptons.size() > 0 && obj->baseLeptons[0]->isMu()){
    mymct = mctcalc.mctcorr(*(obj->cJets[0]),*(obj->cJets[1]),*(obj->baseMuons[0]),m1,13000,0);
  }
  else if(obj->baseLeptons.size() > 0 && !obj->baseLeptons[0]->isMu()){
    mymct = mctcalc.mctcorr(*(obj->cJets[0]),*(obj->cJets[1]),*(obj->baseElectrons[0]),m1,13000,0);
  }
  else cout << "WARNGING: NO LEPTONS FOR MCT CALCULATION!!!" << endl;

  return mymct;

}

// ------------------------------------------------------------------- //
float Observables::getMct2(const Objects* obj)
{
  float mymct  = 0;
  if(obj->bJets.size() < 2 ) return 0;

  TVector2 m1;
  m1.TVector2::SetMagPhi(obj->met.Et,obj->met.phi);
  TLorentzVector* vds  = new TLorentzVector();
  vds->SetPtEtaPhiM(0,0,0,0);//GeV

  TMctLib mctcalc;

  mymct = mctcalc.mctcorr(*(obj->bJets[0]),*(obj->bJets[1]),(*vds),m1,13000,0);


  return mymct;

}

// ------------------------------------------------------------------------- //
float Observables::getMinMtb(const Objects* obj, float minjetpt)
{

  if(obj->bJets.size()){

    float min_mtb = 1e10;

    for(auto bJet : obj->bJets){
      if(bJet->Pt()<minjetpt) continue;
      float mt = TMath::Sqrt(2.0 * obj->met.Et * bJet->Pt() * (1.0 - TMath::Cos( obj->met.phi-bJet->Phi() ) ) );
      min_mtb = min(min_mtb, mt);
    }
    return min_mtb;
  }

  return 0;
}

// ------------------------------------------------------------------------- //
float Observables::getMt2(const Objects* obj, mt2type type, float trial_invis, bool useBaseline, bool useTrackMET)
{

  // Default value if mt2 can't be defined
  float mt2 = -999.0;

  // ------------------------------------------
  // Case 2 leptons, user input trial mass
  // ------------------------------------------
  const TLorentzVector* met = dynamic_cast<const TLorentzVector*>(&(obj->met));

  // NOTE that the met_track is only usable for MT2LL currently!
  const TLorentzVector* met_track = dynamic_cast<const TLorentzVector*>(&(obj->trackMet));

  if (type == MT2LL) {

    LeptonVector lep;

    if(useBaseline) lep = obj->baseLeptons;
    else            lep = obj->signalLeptons;

    // require exactly two leptons for calculation
    if(lep.size() != 2){
      return mt2;
    }
    // Compute MT2
    TLorentzVector* lep1 = dynamic_cast<TLorentzVector*>(lep[0]);
    TLorentzVector* lep2 = dynamic_cast<TLorentzVector*>(lep[1]);

    if(!useTrackMET){
      ComputeMT2 mycalc1 = ComputeMT2(*lep1, *lep2, *met, trial_invis, trial_invis);
      mt2 = mycalc1.Compute();
      return mt2;
    }
    else{
      ComputeMT2 mycalc1 = ComputeMT2(*lep1, *lep2, *met_track, trial_invis, trial_invis);
      mt2 = mycalc1.Compute();
      return mt2;
    }
  }

  /***************************************************
    MT2LBLB: Use 2 bJets and 2 leptons if available
  ***************************************************/
  else if (type == MT2LBLB) {

    /// finding b-jets
    TLorentzVector* jet1 = NULL;
    TLorentzVector* jet2 = NULL;

    // 2+ b-jets
    if (obj->bJets.size() >= 2) {
      jet1 = dynamic_cast<TLorentzVector*>(obj->bJets.at(0));
      jet2 = dynamic_cast<TLorentzVector*>(obj->bJets.at(1));
    }
    // no b-jets
    else {
      std::cout << "<WARNING::Observables::getMt2> Couldn't find two bjets in this event\
                      ." << std::endl;
        return mt2;
    }

    /// finding leptons
    LeptonVector lep;

    if(useBaseline) lep = obj->baseLeptons;
    else            lep = obj->signalLeptons;

    // require exactly two leptons for calculation
    if(lep.size() < 2){
      std::cout << "<WARNING::Observables::getMt2> Couldn't find two leptons in this event\
                      ." << std::endl;
      return mt2;
    }
    // Compute MT2
    TLorentzVector* lep1 = dynamic_cast<TLorentzVector*>(lep[0]);
    TLorentzVector* lep2 = dynamic_cast<TLorentzVector*>(lep[1]);



    // assume W boson and neutrino to be the MET
    ComputeMT2 mycalc1 = ComputeMT2(*jet1 + *lep1, *jet2 + *lep2, *met, 0., 80.);
    float mt2_1 = mycalc1.Compute();
    ComputeMT2 mycalc2 = ComputeMT2(*jet2 + *lep1, *jet1 + *lep2, *met, 0., 80.);
    float mt2_2 = mycalc2.Compute();

    // return the smaller one
    return mt2_1 > mt2_2 ? mt2_2 : mt2_1;
  }

  // Case exactly one lepton
  if(obj->signalLeptons.size() != 1){
    return mt2;
  }
  // need 2 jets
  if(obj->cJets.size() < 2){
    return mt2;
  }

  const TLorentzVector* lep = dynamic_cast<const TLorentzVector*>(obj->signalLeptons.at(0));

  /***************************************************
    AMT2B: Use bJets if available, otherwise cJets
  ***************************************************/
  if (type == AMT2B) {

    TLorentzVector* jet1 = NULL;
    TLorentzVector* jet2 = NULL;

    // 1 b-jet
    if (obj->bJets.size() == 1) {
      // jet1: b-jet
      jet1 = dynamic_cast<TLorentzVector*>(obj->bJets.at(0));
      // jet2: first non-b jet
      for (const auto& jet: obj->cJets) {
        if (!jet->bjet) {
          jet2 = dynamic_cast<TLorentzVector*>(jet);
          break;
        }
      }
      if (!jet2) {
        std::cout << "<WARNING::Observables::getMt2> Couldn't find non-bjet\
                      in >=2 jet event with exactly one b-jet. Something went wrong" << std::endl;
        return mt2;
      }
    }
    // 2+ b-jets
    else if (obj->bJets.size() >= 2) {
      jet1 = dynamic_cast<TLorentzVector*>(obj->bJets.at(0));
      jet2 = dynamic_cast<TLorentzVector*>(obj->bJets.at(1));
    }
    // no b-jets
    else {
      jet1 = dynamic_cast<TLorentzVector*>(obj->cJets.at(0));
      jet2 = dynamic_cast<TLorentzVector*>(obj->cJets.at(1));
    }

    // assume W boson and neutrino to be the MET
    ComputeMT2 mycalc1 = ComputeMT2(*jet1, *jet2 + *lep, *met, 0., 80.);
    float mt2_1 = mycalc1.Compute();
    ComputeMT2 mycalc2 = ComputeMT2(*jet2, *jet1 + *lep, *met, 0., 80.);
    float mt2_2 = mycalc2.Compute();

    // return the smaller one
    return mt2_1 > mt2_2 ? mt2_2 : mt2_1;
  }


  /***************************************************
    AMT2BWEIGHT: Use bJets (defined by highest b-tag weight)
  ***************************************************/
  else if (type == AMT2BWEIGHT) {

    JetVariable* bjet1 = NULL;
    JetVariable* bjet2 = NULL;

    // find 2 highest b-tag weight jets
    float mv2_1 = -1;
    float mv2_2 = -1;
    // first
    for (JetVariable* jet: obj->cJets) {
      if (jet->mv2c10 > mv2_1) {
        mv2_1 = jet->mv2c10;
        bjet1 = jet;
      }
    }
    // second
    for (JetVariable* jet: obj->cJets) {
      if (jet->mv2c10 > mv2_2 && jet->mv2c10 < mv2_1) {
        mv2_2 = jet->mv2c10;
        bjet2 = jet;
      }
    }

    if (!bjet1 || !bjet2) {
      //std::cout << "<WARNING::Observables::getMt2> AMT2BWEIGHT Couldn't find 2 jets with meaningful b-tag weights" << std::endl;
      return mt2;
    }

    auto jet1 = dynamic_cast<TLorentzVector*>(bjet1);
    auto jet2 = dynamic_cast<TLorentzVector*>(bjet2);

    // assume W boson and neutrino to be the MET
    ComputeMT2 mycalc1 = ComputeMT2(*jet1, *jet2 + *lep, *met, 0., 80.);
    float mt2_1 = mycalc1.Compute();
    ComputeMT2 mycalc2 = ComputeMT2(*jet2, *jet1 + *lep, *met, 0., 80.);
    float mt2_2 = mycalc2.Compute();

    // return the smaller one
    return mt2_1 > mt2_2 ? mt2_2 : mt2_1;
  }

  /***************************************************
    AMT2: Always use cJets
  ***************************************************/
  else if (type == AMT2) {

    TLorentzVector* jet1 = dynamic_cast<TLorentzVector*>(obj->cJets.at(0));
    TLorentzVector* jet2 = dynamic_cast<TLorentzVector*>(obj->cJets.at(1));

    // assume W boson and neutrino to be the MET
    ComputeMT2 mycalc1 = ComputeMT2(*jet1, *jet2 + *lep, *met, 0.,80.);
    float mt2_1 = mycalc1.Compute();
    ComputeMT2 mycalc2 = ComputeMT2(*jet2, *jet1 + *lep, *met, 0.,80.);
    float mt2_2 = mycalc2.Compute();

    // return the smaller one
    return mt2_1 > mt2_2 ? mt2_2 : mt2_1;
  }

  /***************************************************
    MT2TAU: Use highest pt jet, excluding 2 jets with highest b-tag weights
  ***************************************************/
  else if (type == MT2TAU) {

    // need 3 jets
    if (obj->cJets.size() < 3) {
      return mt2;
    }

    JetVariable* bjet1 = NULL;
    JetVariable* bjet2 = NULL;

    // find 2 highest b-tag weight jets
    float mv2_1 = -1;
    float mv2_2 = -1;
    // first
    for (JetVariable* jet: obj->cJets) {
      if (jet->mv2c10 > mv2_1) {
        mv2_1 = jet->mv2c10;
        bjet1 = jet;
      }
    }
    // second
    for (JetVariable* jet: obj->cJets) {
      if (jet->mv2c10 > mv2_2 && jet->mv2c10 < mv2_1) {
        mv2_2 = jet->mv2c10;
        bjet2 = jet;
      }
    }

    if (!bjet1 || !bjet2) {
      //std::cout << "<WARNING::Observables::getMt2> MT2TAU Couldn't find 2 jets with meaningful b-tag weights" << std::endl;
      return mt2;
    }

    // find highest pt jet excluding 2 "b-jets"
    JetVariable* taujet = NULL;
    float maxpt = -1;
    for (JetVariable* jet: obj->cJets) {
      if (jet == bjet1 || jet == bjet2) {
        continue;
      }
      if (jet->Pt() > maxpt) {
        maxpt = jet->Pt();
        taujet = jet;
      }
    }
    if (!taujet) {
      std::cout << "<WARNING::Observables::getMt2> Couldn't find tau jet" << std::endl;
      return mt2;
    }

    // Compute MT2
    TLorentzVector* taujet_lv = dynamic_cast<TLorentzVector*>(taujet);
    ComputeMT2 mycalc1 = ComputeMT2(*taujet_lv, *lep, *met, 0., 0.);
    mt2 = mycalc1.Compute();

    return mt2;
  }

  /***************************************************
    MT2LJ: Simply use highest pt jet and lepton
  ***************************************************/
  else if (type == MT2LJ) {

    // Compute MT2
    TLorentzVector* jet = dynamic_cast<TLorentzVector*>(obj->cJets.at(0));
    ComputeMT2 mycalc1 = ComputeMT2(*jet, *lep, *met, 0., 0.);
    mt2 = mycalc1.Compute();

    return mt2;
  }
  else {
    std::cout << "<WARNING::Observables::getMt2> Given MT2 type not implemented" << std::endl;
    return mt2;
  }
}

// ------------------------------------------------------------------- //
float Observables::getMlj(const Objects*obj, unsigned int njet, bool useBaseline)
{
//Calculates invariant mass of the dijet system and the nearest lepton (njet =2) OR the invariant mass
//of the highest p_T jet and the nearest lepton
//If useBaseline is true, baseline leptons are chosen for calulation
//Default: useBaseline=false, njet=1

  float mLJ = -999.0;

  LeptonVector lep;
  TLorentzVector* Lepton = 0;
  TLorentzVector* Jet = 0;
  TLorentzVector* NearestLepton = 0;

  float deltaR = 1000.0;

  if(useBaseline) lep = obj->baseLeptons;
  else            lep = obj->signalLeptons;

  if(njet == 1 && obj->cJets.size() > 0){
    Jet = dynamic_cast<TLorentzVector*>(obj->cJets[0]);
  }
  else if(njet == 2 && obj->cJets.size() > 1){
    TLorentzVector Jet1 = *dynamic_cast<TLorentzVector*>(obj->cJets[0]);
    TLorentzVector Jet2 = *dynamic_cast<TLorentzVector*>(obj->cJets[1]);
    TLorentzVector DiJet = Jet1 + Jet2;
    Jet = &DiJet;
  }
  else {
    if(njet > 2){
      std::cout<< "<WARNING::Observables> Request to get mLJ from more than two Jets, njets should not be >2, Highest p_t Jet taken" << std::endl;
    }
    else return mLJ;
  }

  for(unsigned int leps=0; leps < lep.size();leps++){
    Lepton = dynamic_cast<TLorentzVector*>(lep[leps]);
    if(fabs(Jet->DeltaR(*Lepton)) <= deltaR){
      deltaR = fabs(Jet->DeltaR(*Lepton));
      NearestLepton = Lepton;
    }
    else continue;
  }

  mLJ = (*NearestLepton + *Jet).M();

  return mLJ;

}
// ------------------------------------------------------------------- //
float Observables::getDeltaPhiNearest(const Objects* obj, const TLorentzVector* ref, bool useBaseline)
{
  //Calculates the Angular distance from the nearest Object to ref, consideres Jets and Leptons

  float DeltaPhi = -999.0;

  LeptonVector leps;
  JetVector jets;

  TLorentzVector* Lepton;
  TLorentzVector* Jet = 0;
  TLorentzVector* NearestObject = 0;

  float deltaR = 1000.0;

  if(useBaseline){
    leps = obj->baseLeptons;
    jets = obj->baselineJets;
  }
  else{
    leps = obj->signalLeptons;
    jets = obj->cJets;
  }
  //Lepton distance loop
  for(unsigned int lepind=0; lepind < leps.size(); lepind++){
    Lepton =  dynamic_cast<TLorentzVector*>(leps[lepind]);
    if(fabs(ref->DeltaR(*Lepton)) <= deltaR){
      deltaR = fabs(ref->DeltaR(*Lepton));
      NearestObject = Lepton;
    }
    else continue;
  }

  //Jet distance loop
  for(unsigned int jetind=0; jetind < jets.size(); jetind++){
    Jet = dynamic_cast<TLorentzVector*>(jets[jetind]);
    if(fabs(ref->DeltaR(*Jet)) <= deltaR){
      deltaR = fabs(ref->DeltaR(*Jet));
      NearestObject = Jet;
    }
    else continue;
  }

  DeltaPhi = fabs(ref->DeltaPhi(*NearestObject));
  return DeltaPhi;
}

// ------------------------------------------------------------------------- //
int Observables::getTTbar2LTagger(const Objects* obj, float& WMass1, float& WMass2, float& TopMass1, float& TopMass2, float& ttReso)
{

  int nsol1=0;
  int nsol2=0;
  int nsoll=0;
  int npairs=0;
  float metx = obj->met.Et*cos(obj->met.phi);
  float mety = obj->met.Et*sin(obj->met.phi);

  double TopMass11tmp = 0.;
  double TopMass12tmp = 0.;
  double WMass11tmp = 0.;
  double WMass12tmp = 0.;
  double ttResotmp1 = 0.;

  double TopMass21tmp = 0.;
  double TopMass22tmp = 0.;
  double WMass21tmp = 0.;
  double WMass22tmp = 0.;
  double ttResotmp2 = 0.;

  double deltaMtmp11 = 9999999999.9;
  double deltaMtmp12 = 9999999999.9;
  double deltaMtmp21 = 9999999999.9;
  double deltaMtmp22 = 9999999999.9;
  double deltaM1    = 999999999.9;
  double deltaM2    = 999999999.9;

  //// variables for solutions of ttbar constrains
  double pl1[4];
  double pl2[4];
  double pb1[4];
  double pb2[4];
  //double pxa[4];
  //double pxb[4];
  //double pya[4];
  //double pyb[4];
  //double pza[4];
  //double pzb[4];
  //int nsol=0;

  if( obj->signalLeptons.size() < 1){
    std::cout << " <WARNING::Observables> There is no signal lepton to be reconstructed." << std::endl;
    return -1;
  }

  float lepPt  = obj->signalLeptons.size()>=1 ?  obj->signalLeptons[0]->Pt()  : 0.0;
  float lepEta = obj->signalLeptons.size()>=1 ?  obj->signalLeptons[0]->Eta()  : 0.0;
  float lepPhi = obj->signalLeptons.size()>=1 ?  obj->signalLeptons[0]->Phi()  : 0.0;

  //if(signalTaus.size() < 1){
  //std::cout << " <WARNING::Observables> There is no signal tau to be reconstructed." << std::endl;
  //return -1;
  //Other test will be performed later.
  //}

  // Tau information
  float t1Pt  = obj->signalTaus.size()>=1 ?  obj->signalTaus[0]->Pt()  : 0.0;
  float t1Eta = obj->signalTaus.size()>=1 ?  obj->signalTaus[0]->Eta() : 0.0;
  float t1Phi = obj->signalTaus.size()>=1 ?  obj->signalTaus[0]->Phi() : 0.0;


  for (int i=0;i<4;i++) {
    pb1[i]=0.;
    pb2[i]=0.;
    pl1[i]=0.;
    pl2[i]=0.;
  }
  // loop all the bjets
  //for(unsigned int i = 0; i<obj->bJets.size(); i++){
  //if(obj->bJets[i]->Pt()<25 ) continue;
  //for(unsigned int j = i+1; j<obj->bJets.size(); j++){
  //if(obj->bJets[j]->Pt()<25 ) continue;

  // loop all the cjets
  for(unsigned int i = 0; i<obj->cJets.size(); i++){
    if(obj->cJets[i]->Pt()<25 ) continue;
    for(unsigned int j = i+1; j<obj->cJets.size(); j++){
      if(obj->cJets[j]->Pt()<25 ) continue;

      TLorentzVector* jet11 = new TLorentzVector();
      TLorentzVector* jet12 = new TLorentzVector();
      TLorentzVector* lep11 = new TLorentzVector();
      TLorentzVector* lep12 = new TLorentzVector();
      TLorentzVector* nu11  = new TLorentzVector();
      TLorentzVector* nu12  = new TLorentzVector();

      lep11->SetPtEtaPhiM(lepPt,lepEta,lepPhi,0.0 );//GeV
      lep12->SetPtEtaPhiM(t1Pt,t1Eta,t1Phi,0.0 );//GeV
      pl1[0]=lep11->Px();
      pl1[1]=lep11->Py();
      pl1[2]=lep11->Pz();
      pl1[3]=lep11->E();
      pl2[0]=lep12->Px();
      pl2[1]=lep12->Py();
      pl2[2]=lep12->Pz();
      pl2[3]=lep12->E();

      // use bjets
      //jet11->SetPtEtaPhiM(obj->bJets[i]->Pt(),obj->bJets[i]->Eta(),obj->bJets[i]->Phi(),0.0 );
      //jet12->SetPtEtaPhiM(obj->bJets[j]->Pt(),obj->bJets[j]->Eta(),obj->bJets[j]->Phi(),0.0 );
      // use jets
      jet11->SetPtEtaPhiM(obj->cJets[i]->Pt(),obj->cJets[i]->Eta(),obj->cJets[i]->Phi(),0.0 );
      jet12->SetPtEtaPhiM(obj->cJets[j]->Pt(),obj->cJets[j]->Eta(),obj->cJets[j]->Phi(),0.0 );

      pb1[0]= jet11->Px();
      pb1[1]= jet11->Py();
      pb1[2]= jet11->Pz();
      pb1[3]= jet11->E();
      pb2[0]= jet12->Px();
      pb2[1]= jet12->Py();
      pb2[2]= jet12->Pz();
      pb2[3]= jet12->E();

      // First lepton and bjet combination
      // pl1+pb1   pl2+pb2
      TTbar2LTagger tt2lTagger;
      tt2lTagger.solve2l(pl1, pb1, pl2, pb2, metx, mety);
      //nsol1 = nsol;
      nsol1 = tt2lTagger.nsol;
      //std::cout << "nsol1="  << nsol1 << endl;

      // reconstruct topmass and ttbar reso2nance
      for(Int_t i=0;i<tt2lTagger.nsol;i++) {
        nu11->SetPxPyPzE(tt2lTagger.pxa[i], tt2lTagger.pya[i], tt2lTagger.pza[i], sqrt(tt2lTagger.pxa[i]*tt2lTagger.pxa[i]+tt2lTagger.pya[i]*tt2lTagger.pya[i]+tt2lTagger.pza[i]*tt2lTagger.pza[i]));  //pxa[_i] from solve2l();//jjj
        nu12->SetPxPyPzE(tt2lTagger.pxb[i], tt2lTagger.pyb[i], tt2lTagger.pzb[i], sqrt(tt2lTagger.pxb[i]*tt2lTagger.pxb[i]+tt2lTagger.pyb[i]*tt2lTagger.pyb[i]+tt2lTagger.pzb[i]*tt2lTagger.pzb[i]));
        if( nu11->Pz() > 1e9 || nu12->Pz() > 1e9) continue;
        if( nu11->Pz() < -1e9 || nu12->Pz() < -1e9) continue;
  // if there is any solution, calculate top, w mass
        TopMass11tmp = ((*jet11) + (*lep11) + (*nu11)).M();
        TopMass12tmp = ((*jet12) + (*lep12) + (*nu12)).M();
        WMass11tmp   = ((*lep11) + (*nu11)).M();
        WMass12tmp   = ((*lep12) + (*nu12)).M();
        ttResotmp1 = ((*jet11) + (*lep11) + (*nu11) + (*jet12) + (*lep12) + (*nu12)).M();

        deltaMtmp11 = ( (*lep11) + (*nu11) ).M() - WBOSONMASS;
        deltaMtmp12 = ( (*lep12) + (*nu12) ).M() - WBOSONMASS;

    //        std::cout << "i=" << i << "   W1=" << ( (*lep11) + (*nu11) ).M()  << "    W2=" << ( (*lep12) + (*nu12) ).M()
    //       << "   Top1=" << TopMass11tmp << "   Top2=" << TopMass12tmp << " ttResotmp2="<< ttResotmp1
    //       << "  nu1.pz()=" << nu11->Pz() << "  nu2.pz()=" << nu12->Pz() << endl;


  // get the top1,w1 when deltaMW1 is the smallest
  // get the top2,w2 when deltaMW2 is the smallest
        if(deltaMtmp11 < deltaM1) {
          deltaM1   = deltaMtmp11;
          TopMass1  = TopMass11tmp;//to keep
          WMass1    = WMass11tmp;//to keep
        }
        if(deltaMtmp12 < deltaM2) {
          deltaM2   = deltaMtmp12;
          TopMass2  = TopMass12tmp;//to keep
          WMass2    = WMass12tmp;//to keep
        }
  // when both deltaMW1 and deltaMW2 is the smallest, keep ttReso; else ttReso=0 by default
        if(deltaMtmp11 < deltaM1 && deltaMtmp12 < deltaM2 )    ttReso = ttResotmp1;//to keep
        }

     // Second lepton and bjet combination
      // pl1+pb2   pl2+pb1
      TTbar2LTagger tt2lTagger2;
      tt2lTagger2.solve2l(pl1, pb1, pl2, pb2, metx, mety);
      nsol2 = tt2lTagger2.nsol;
      //std::cout << "nsol2=" << nsol2 << endl;

      // reconstruct topmass and ttbar reso2nance
      for(Int_t i=0;i<tt2lTagger2.nsol;i++) {
        nu11->SetPxPyPzE(tt2lTagger2.pxa[i], tt2lTagger2.pya[i], tt2lTagger2.pza[i], sqrt(tt2lTagger2.pxa[i]*tt2lTagger2.pxa[i]+tt2lTagger2.pya[i]*tt2lTagger2.pya[i]+tt2lTagger2.pza[i]*tt2lTagger2.pza[i]));  //pxa[_i] from solve2l();//jjj
        nu12->SetPxPyPzE(tt2lTagger2.pxb[i], tt2lTagger2.pyb[i], tt2lTagger2.pzb[i], sqrt(tt2lTagger2.pxb[i]*tt2lTagger2.pxb[i]+tt2lTagger2.pyb[i]*tt2lTagger2.pyb[i]+tt2lTagger2.pzb[i]*tt2lTagger2.pzb[i]));
        if( nu11->Pz() > 1e9 || nu12->Pz() > 1e9) continue;
        if( nu11->Pz() < -1e9 || nu12->Pz() < -1e9) continue;

  // if there is any solution, calculate top, w mass
        TopMass21tmp = ((*jet11) + (*lep11) + (*nu11)).M();
        TopMass22tmp = ((*jet12) + (*lep12) + (*nu12)).M();
        WMass21tmp   = ((*lep11) + (*nu11)).M();
        WMass22tmp   = ((*lep12) + (*nu12)).M();
        ttResotmp2= ((*jet11) + (*lep11) + (*nu11) + (*jet12) + (*lep12) + (*nu12)).M();

        deltaMtmp21 = ( (*lep11) + (*nu11) ).M() - WBOSONMASS;
        deltaMtmp22 = ( (*lep12) + (*nu12) ).M() - WBOSONMASS;

        //std::cout << "i=" << i << "   W1=" << ( (*lep11) + (*nu11) ).M()  << "    W2=" << ( (*lep12) + (*nu12) ).M()
    //<< "   Top1=" << TopMass21tmp << "   Top2=" << TopMass22tmp << " ttResotmp2="<< ttResotmp2
    //<< "  nu1.pz()=" << nu11->Pz() << "  nu2.pz()=" << nu12->Pz() << endl;


  // get the top1,w1 when deltaMW1 is the smallest
  // get the top2,w2 when deltaMW2 is the smallest
        if(deltaMtmp21 < deltaM1) {
          deltaM1   = deltaMtmp21;
          TopMass1  = TopMass21tmp;//to keep
          WMass1    = WMass21tmp;//to keep
        }
        if(deltaMtmp22 < deltaM2) {
          deltaM2   = deltaMtmp22;
          TopMass2  = TopMass22tmp;//to keep
          WMass2    = WMass22tmp;//to keep
        }

  // when both deltaMW1 and deltaMW2 is the smallest, keep ttReso; else ttReso=0 by default
        if(deltaMtmp21 < deltaM1 && deltaMtmp22 < deltaM2 )    ttReso = ttResotmp2;//to keep
        }

      //std::cout<< "TopMass1 = "<< TopMass1 << " WMass1 = "<< WMass1<<std::endl;
      //std::cout<< "TopMass2 = "<< TopMass2 << " WMass2 = "<< WMass2<<std::endl;
      //cout<<"nsol1,nsol2="<<nsol1<<","<<nsol2 << "  (" << i <<"," << j<< ")"<<endl;
      if(nsol1>0 || nsol2>0) npairs++;//npairs: looping all the bjets, the number of possible bjet-pair combinations to satisfy the equation.
      if( nsol1>=1 )         nsoll+=nsol1;
      if( nsol2>=1 )         nsoll+=nsol2;//nsoll:  all the solutions: for one bjet-pair, there could be more than one solutions.

      jet11->Delete();
      jet12->Delete();
      nu11->Delete();
      nu12->Delete();
      lep11->Delete();
      lep12->Delete();

    } // loop jet12
  } // loop jet11
    //} //
  //} //

  return npairs;
}
// ------------------------------------------------------------------------------ //
float Observables::passDeadTile(const Objects* obj,float& eta, float& phi)
{

  float minDeltaPhi = 9999.0;

  for(auto& j : obj->cJets){

    if(j->Pt() < 50.0 || fabs(j->Eta())>2.0 ) continue;

    // Get min
    if( fabs(j->DeltaPhi(obj->met)) < minDeltaPhi ){
      eta = j->Eta();
      phi = j->Phi();
      minDeltaPhi = fabs(j->DeltaPhi(obj->met));
    }
  }

 return minDeltaPhi;

}
// ------------------------------------------------------------------------------ //
float Observables::averageTiming(const Objects* obj,unsigned int nJet)
{


  float avgTiming=0.0;

  if( obj->cJets.size() < nJet ) nJet = obj->cJets.size();

  for( unsigned int j=0; j<nJet; j++ ){
    JetVariable* jet = obj->cJets[j];

    avgTiming += jet->Timing;

  }


  avgTiming = avgTiming / float(nJet);


  return avgTiming;

}
// ------------------------------------------------------------------------------ //
float Observables::minDeltaPhiMetPileUpJets(const Objects* obj)
{

  float _deltaPhi = 0;
  float minDeltaPhi = 9999.0;
  for(auto& jet : obj->cJets){

    if( jet->Pt()>=50.0 && jet->Pt() < 70.0 && jet->jvt<0.64 ){
      _deltaPhi = jet->DeltaPhi( obj->met );
      // Take Min
      if(fabs(_deltaPhi)<minDeltaPhi ) minDeltaPhi = fabs(_deltaPhi);
    }

  }

  return minDeltaPhi;

}
// ------------------------------------------------------------------------------ //
float Observables::EnergyWeightedTime(const Objects* obj,unsigned int nJet)
{

  float energyWeightedTime=0.0;

  float num=0;
  float den=0;

  if( obj->cJets.size() < nJet ) nJet = obj->cJets.size();

  for( unsigned int j=0; j<nJet; j++ ){
    JetVariable* jet = obj->cJets[j];
    num += jet->E() * jet->Timing;
    den += jet->E();
  }

  energyWeightedTime = (num / den);

  return energyWeightedTime;

}
// ------------------------------------------------------------------------------ //
std::vector<Observables::FatJet> Observables::getReclusteredTrimmedFatJets(const Objects* obj, float R, float fcut)
{
    //////// Jet reclustering and trimming

  // jet reclustering
  // 1. Need to covert the vector fo jets toa vector of pseudojets
  // only need p4() since we are using them as input

  std::vector<fastjet::PseudoJet> input_jets;
  for( auto& ajet: obj->aJets ){
    if( (ajet->Pt()>20.0) && (TMath::Abs(ajet->Eta())<2.8) ){
      input_jets.push_back(
      fastjet::PseudoJet(
      ajet->Px(),
      ajet->Py(),
      ajet->Pz(),
      ajet->E()));
    }
  }

  // 2. Build up the new jet definitions using input configurations
  // - jet algorithm
  // - radius
  fastjet::JetDefinition jet_def(fastjet::antikt_algorithm, R);

  // 3. Run the Cluster Sequence on pseudijets with right jet definition above
  // cs = cluster sequence
  fastjet::ClusterSequence cs(input_jets, jet_def);

  // 4. Grab the reclustering jets, sorted by pt()
  //rc_jets = reclustered jets
  std::vector<fastjet::PseudoJet> rc_jets = fastjet::sorted_by_pt(cs.inclusive_jets(20.));

  // jet trimming
  // 5. Apply trimming on PJ.constituents() using fcut (trimmimg cut to apply)
  // rc_t_jet = trimmed reclustered jet
  //std::vector<TLorentzVector> rc_t_jets;
  std::vector<Observables::FatJet> rc_t_jets;

  for(auto rc_jet : rc_jets){
    TLorentzVector rc_t_jet = TLorentzVector();
    // loop over subjets
    int nSubJets = 0;
    std::vector<TLorentzVector> pSubJets;
    for(auto rc_jet_subjet : rc_jet.constituents()){
      TLorentzVector subjet = TLorentzVector();
      subjet.SetPtEtaPhiE(
                          rc_jet_subjet.pt(),
                          rc_jet_subjet.eta(),
                          rc_jet_subjet.phi(),
                          rc_jet_subjet.e()
                         );
      if(subjet.Pt() > fcut*rc_jet.pt()){
        rc_t_jet += subjet;
    pSubJets.push_back(subjet);
        nSubJets++;
      }
    }
    Observables::FatJet fatJet;
    fatJet.jet = rc_t_jet;
    fatJet.nSubJets = nSubJets;
    fatJet.pSubJets = pSubJets;
    rc_t_jets.push_back(fatJet);
  }

  std::sort(rc_t_jets.begin(), rc_t_jets.end(),
            [](Observables::FatJet j1, Observables::FatJet j2) {
              return (j1.jet.Pt() > j2.jet.Pt());
            });

  return rc_t_jets;
}

// ------------------------------------------------------------------------------ //
float Observables::getMSqTauTau(const Objects* obj, int version, bool useTrackMET)
{
  // Returns di-tau invariant mass squared calculated from MET and L1pT and L2pT

  // Two definitions exist in the literature - please select carefully
  // They differ in how they treat the xi variable

  // Version 1 : set int version = 1
  // See arXiv:1401.1235, arXiv:1412.0618 for definition and example usage
  // This version allows keeps M^2(tautau) strictly positive
  // The modulus of xi is taken when rescaling the energy-like part of the momentum

  // Version 2 : set int version = 2
  // See arXiv:1409.7058, arXiv:1501.02511 for definition and example usage
  // This version allows M^2(tautau) to be negative
  // The references report slightly better discriminating power than version 1

  // To Do -- Version 3 : set int version = 3
  // See CMS-PAS-SUS-16-025 for definition and example usage

  float MSqTauTau = -99999.;

  const TLorentzVector* met = dynamic_cast<const TLorentzVector*>(&(obj->met));

  // For track-based MET
  const TLorentzVector* met_track = dynamic_cast<const TLorentzVector*>(&(obj->trackMet));

  if(obj->baseLeptons.size() >= 2) {
    // ================================================
    // get components of transverse momenta
    float pmiss_x = met->Px();
    float pmiss_y = met->Py();

    if(useTrackMET){
      pmiss_x = met_track->Px();
      pmiss_y = met_track->Py();
    }

    float pL1_E   = obj->baseLeptons[0]->E();
    float pL1_x   = obj->baseLeptons[0]->Px();
    float pL1_y   = obj->baseLeptons[0]->Py();
    float pL1_z   = obj->baseLeptons[0]->Pz();

    float pL2_E   = obj->baseLeptons[1]->E();
    float pL2_x   = obj->baseLeptons[1]->Px();
    float pL2_y   = obj->baseLeptons[1]->Py();
    float pL2_z   = obj->baseLeptons[1]->Pz();

    // calculate the xi (scale factor of tau daughter neutrinos to observed lepton pT)
    // we solved this simultaneous equation by hand here (matrix inversion)
    float determinant = pL1_x * pL2_y - pL1_y * pL2_x;
    float xi_1 = (pmiss_x * pL2_y - pL2_x * pmiss_y)/determinant;
    float xi_2 = (pmiss_y * pL1_x - pL1_y * pmiss_x)/determinant;

    if ( version == 1 ) {
      // energy part of dot product has modulus around xi variables
      float energyDot   = pL1_E * pL2_E * ( 1. + fabs(xi_1) ) * ( 1. + fabs(xi_2) );
      TVector3 L1_3vec(pL1_x, pL1_y, pL1_z);
      TVector3 L2_3vec(pL2_x, pL2_y, pL2_z);
      // momentum part of dot product has unrestricted xi
      float momentumDot =  L1_3vec.Dot( L2_3vec ) * ( 1. + xi_1 ) * ( 1. + xi_2 );
      // finally perform the 4-vector dot product manually here
      MSqTauTau = 2 * ( energyDot - momentumDot );
    }

    else if ( version == 2 ) {
      // xi remains unrestricted for energy and momentum like parts
      TLorentzVector* sigLep2 = dynamic_cast<TLorentzVector*>( obj->baseLeptons.at(1) );
      MSqTauTau = (1. + xi_1) * (1. + xi_2) * 2 * obj->baseLeptons[0]->Dot( *sigLep2 );
    }


    else {
      MSqTauTau = 0.;
    }
    // end caluclate m_2tau
    // ================================================
  }


  return MSqTauTau;
}

// ------------------------------------------------------------------------------ //
float Observables::getRll(const Objects* obj)
{
  // Distance between two leptons
  float Rll = -1.;

  if(obj->baseLeptons.size() >= 2) {

    //auto sigLep1 = dynamic_cast<TLorentzVector*>( obj->signalLeptons.at(0) );
    TLorentzVector* Lep2 = dynamic_cast<TLorentzVector*>( obj->baseLeptons[1] );

    //Rll = obj->signalLeptons[0]->DeltaR( obj->signalLeptons.at(1) );
    Rll = obj->baseLeptons[0]->DeltaR(*Lep2, m_useRapidity);
  }
  return Rll;
}

// ------------------------------------------------------------------------------ //
float Observables::getDPhiJ1Met(const Objects* obj, bool useTrackMET)
{
  // Azimuthal angle between leading jet and MET
  float DPhiJ1Met = 999.;

  //auto met  = dynamic_cast<const TLorentzVector*>(&(obj->met));

  if(obj->cJets.size() > 0) {
    //auto jet1 = dynamic_cast<TLorentzVector*>( obj->cJets.at(0) );

    if(!useTrackMET){
      DPhiJ1Met = fabs(obj->cJets[0]->DeltaPhi( obj->met ));
    }
    else{
      DPhiJ1Met = fabs(obj->cJets[0]->DeltaPhi( obj->trackMet ));
    }
  }
  return DPhiJ1Met;
}

// JEFF
// ------------------------------------------------------------------------------ //
float Observables::getDPhiTrkMet(int iTrk, const Objects* obj, bool useTrackMET)
{
  // Azimuthal angle between track and MET
  float DPhiTrkMet = 999.;

  //auto met  = dynamic_cast<const TLorentzVector*>(&(obj->met));

  if(!useTrackMET){
    DPhiTrkMet = fabs(obj->tracks[iTrk]->DeltaPhi( obj->met ));
  }
  else{
    DPhiTrkMet = fabs(obj->tracks[iTrk]->DeltaPhi( obj->trackMet ));
  }
  return DPhiTrkMet;
}

// ------------------------------------------------------------------------------ //
float Observables::getDPhiJNMet(const Objects* obj, unsigned int jetIndex, float jet_threshold, bool useTrackMET)
{
  // Azimuthal angle between leading jet and MET
  float DPhiJetMet = 999.;

  if(obj->cJets.size() > jetIndex && obj->cJets[jetIndex]->Pt() > jet_threshold) {

    if(!useTrackMET){
      DPhiJetMet = fabs(obj->cJets[jetIndex]->DeltaPhi( obj->met ));
    }
    else{
      DPhiJetMet = fabs(obj->cJets[jetIndex]->DeltaPhi( obj->trackMet ));
    }
  }
  return DPhiJetMet;
}

// ------------------------------------------------------------------------------ //
float Observables::getDPhibJetMet(const Objects* obj)
{
  // Azimuthal angle between leading jet and MET
  float DPhibJetMet = 999.;

  if(obj->bJets.size()>=1){
     DPhibJetMet = fabs(obj->bJets[0]->DeltaPhi( obj->met ));
  }

  return DPhibJetMet;
}

// ------------------------------------------------------------------------------ //
float Observables::getDPhilMet(const Objects* obj)
{
  // Azimuthal angle between leading jet and MET
  float DPhilMet = 999.;

  if(obj->baseLeptons.size() == 1 ) {
     DPhilMet = fabs(obj->baseLeptons[0]->DeltaPhi( obj->met ));
  }
  return DPhilMet;
}

// ------------------------------------------------------------------------------ //
float Observables::getminDPhiJetMet(const Objects* obj, int maxNJet, float jet_threshold, bool useTrackMET)
{
  float minDPhi = 999.;

  if(maxNJet<0) maxNJet = 9999;

  for(int ijet=0; ijet<min(maxNJet, (int)obj->cJets.size()); ++ijet){
    if(obj->cJets[ijet]->Pt()<jet_threshold) continue;

    minDPhi =useTrackMET ?
      min(minDPhi, acos(cos( obj->trackMet.phi - (float) obj->cJets[ijet]->Phi()  )) ) :
      min(minDPhi, acos(cos( obj->met.phi      - (float) obj->cJets[ijet]->Phi()  )) ) ;
  }

  return minDPhi;
}
// ------------------------------------------------------------------------------ //
float Observables::getminDPhiJetInvisibleMet(const Objects* obj, int maxNJet, float jet_threshold, MetVariable::MetFlavor met_flavor)
{
  float minDPhi = 999.;

  if(maxNJet<0) maxNJet = 9999;

  const MetVariable *met = obj->getMETFlavor(met_flavor,false);
  for(int ijet=0; ijet<min(maxNJet, (int)obj->cJets.size()); ++ijet){
    if(obj->cJets[ijet]->Pt()<jet_threshold) continue;
    minDPhi = min(minDPhi, acos(cos( met->phi      - (float) obj->cJets[ijet]->Phi()  )) ) ;
  }

  return minDPhi;
}
// ------------------------------------------------------------------------------ //
float Observables::getminDPhiJetDileptonMet(const Objects* obj, int maxNJet, float jet_threshold, bool useTrackMET)
{
  float minDPhi = 999.;

  if(maxNJet<0) maxNJet = 9999;

  const LeptonVariable *lep1 = obj->baseLeptons[0];
  const LeptonVariable *lep2 = obj->baseLeptons[1];
  const MetVariable met = useTrackMET ? obj->trackMet : obj->met;
  TLorentzVector dilepton_met = TLorentzVector();
  dilepton_met.SetPx(obj->met.px + lep1->Px() + lep2->Px());
  dilepton_met.SetPy(obj->met.py + lep1->Py() + lep2->Py());

  for(int ijet=0; ijet<min(maxNJet, (int)obj->cJets.size()); ++ijet){
    if(obj->cJets[ijet]->Pt()<jet_threshold) continue;

    minDPhi = min(minDPhi, acos(cos( (float) dilepton_met.Phi() - (float) obj->cJets[ijet]->Phi())) );

  }

  return minDPhi;
}
// ------------------------------------------------------------------------------ //
float Observables::getminDPhiJetPhotonMet(const Objects* obj, int maxNJet, float jet_threshold, bool useTrackMET)
{
  float minDPhi = 999.;

  if(maxNJet<0) maxNJet = 9999;

  const PhotonVariable *photon = obj->basePhotons[0];
  const MetVariable met = useTrackMET ? obj->trackMet : obj->met;
  TLorentzVector photon_met = TLorentzVector();
  photon_met.SetPx(obj->met.px + photon->Px());
  photon_met.SetPy(obj->met.py + photon->Py());

  for(int ijet=0; ijet<min(maxNJet, (int)obj->cJets.size()); ++ijet){
    if(obj->cJets[ijet]->Pt()<jet_threshold) continue;

    minDPhi = min(minDPhi, acos(cos( (float) photon_met.Phi() - (float) obj->cJets[ijet]->Phi())) );

  }

  return minDPhi;
}
// ------------------------------------------------------------------------------ //
float Observables::getminDPhibJetMet(const Objects* obj)
{

  float deltaPhi = 0;
  float minDPhibJetMet = 999.;

     deltaPhi = obj->bJets[0]->DeltaPhi( obj->met );
     if(fabs(deltaPhi)<minDPhibJetMet ) minDPhibJetMet = fabs(deltaPhi);

  return minDPhibJetMet;
}

// ------------------------------------------------------------------------------ //
float Observables::getminDPhilMet(const Objects* obj)
{

  float _deltaPhi = 0;
  float minDPhilMet = 999.;

  if(obj->baseLeptons.size() == 1 ) {
     _deltaPhi = obj->baseLeptons[0]->DeltaPhi( obj->met );
     if(fabs(_deltaPhi)<minDPhilMet ) minDPhilMet = fabs(_deltaPhi);
  }
  return minDPhilMet;
}

// ------------------------------------------------------------------------------ //
float Observables::getRjlOverEl(const Objects* obj)
{
  // Rjl = distance between jet and closest lepton, El is energy of closest lepton to jet
  // can only deal with 0, 1 or 2 leptons

  float RjlOverEl = -1.;

  // require at least 1 jet
  if(obj->cJets.size() == 0) {
    return RjlOverEl;
  }

  // straightforward when there is 1 lepton
  if(obj->baseLeptons.size() == 1 ) {
    TLorentzVector* Lep1 = dynamic_cast<TLorentzVector*>( obj->baseLeptons[0] );
    RjlOverEl = obj->cJets[0]->DeltaR(*Lep1, m_useRapidity) / obj->baseLeptons[0]->E();
  }

  // case when there are two leptons
  if(obj->baseLeptons.size() >= 2 ) {

    TLorentzVector* Lep1 = dynamic_cast<TLorentzVector*>( obj->baseLeptons[0] );
    TLorentzVector* Lep2 = dynamic_cast<TLorentzVector*>( obj->baseLeptons[1] );

    // min distance of jet1 from leptons
    float dR_J1_L1 = obj->cJets[0]->DeltaR(*Lep1, m_useRapidity);
    float dR_J1_L2 = obj->cJets[0]->DeltaR(*Lep2, m_useRapidity);
    float minR_jl = min(dR_J1_L1, dR_J1_L2);

    float E_L1 = obj->baseLeptons[0]->E();
    float E_L2 = obj->baseLeptons[1]->E();

    float E_l  = -999.;
    // now choose E_l to be energy of lepton closest to jet1
    if (dR_J1_L1 == minR_jl) {
      E_l = E_L1;
    }
    if (dR_J1_L2 == minR_jl) {
      E_l = E_L2;
    }

    // calculate R over E
    RjlOverEl = minR_jl / E_l;
  }
  return RjlOverEl;
}


// ------------------------------------------------------------------------------ //
float Observables::getLepCosThetaLab(const Objects* obj)
{
  // Polar angle of one of the two leptons with the beam axis
  // evaluated in the laboratory detector frame (i.e. as measured by ATLAS)
  // See A Barr https://arxiv.org/abs/hep-ph/0511115 for definition

  float absLepCosThetaLab = -1.0;
  // case when there are two leptons
  if(obj->baseLeptons.size() >= 2 ) {
    // obtain pointers to the two leptons' Lorentz vectors
    TLorentzVector* Lep1 = dynamic_cast<TLorentzVector*>( obj->baseLeptons[0] );
    TLorentzVector* Lep2 = dynamic_cast<TLorentzVector*>( obj->baseLeptons[1] );
    float DeltaEta_ll = Lep1->Eta() - Lep2->Eta();
    absLepCosThetaLab = abs( tanh( DeltaEta_ll/2. ) );
  }

  return absLepCosThetaLab;
}

// ------------------------------------------------------------------------------ //
float Observables::getLepCosThetaCoM(const Objects* obj)
{

  // Polar angle of one of the two leptons with the beam axis
  // evaluated in the dilepton centre of momentum frame
  // See T Melia https://arxiv.org/abs/1110.6185 for definition

  float absLepCosThetaCoM = -1.0;
  // case when there are two leptons
  if(obj->baseLeptons.size() >= 2 ) {

    // obtain pointers to the two leptons' Lorentz vectors
    TLorentzVector* Lep1 = dynamic_cast<TLorentzVector*>( obj->baseLeptons[0] );
    TLorentzVector* Lep2 = dynamic_cast<TLorentzVector*>( obj->baseLeptons[1] );

    // construct TLorentzVectors from the pointers
    TLorentzVector lep1P( Lep1->Px(), Lep1->Py(), Lep1->Pz(), Lep1->E() );
    TLorentzVector lep2P( Lep2->Px(), Lep2->Py(), Lep2->Pz(), Lep2->E() );

    // this is so we can add the two vectors and calculate boost of the dilepton CoM frame
    TVector3 boostVec = ( lep1P + lep2P ).BoostVector();
    // now boost each lepton Lorentz vector into the dilepton CoM frame
    lep1P.Boost( -boostVec );
    lep2P.Boost( -boostVec );
    // evaluate the cos(theta ll) in this CoM frame
    absLepCosThetaCoM = abs (tanh( ( lep1P.Eta() - lep2P.Eta() ) / 2. ) );
  }
  return absLepCosThetaCoM;
}

// ------------------------------------------------------------------------------ //
double Observables::getLepPhotonTopoEtclus(const Objects* obj, TrackVariable* track, double deltaR, bool useBaseline, bool useOnlyCore)
{

  double topoetclus = 0.;

  LeptonVector leptons;
  PhotonVector photons;
  TLorentzVector* Track = dynamic_cast<TLorentzVector*>(track);
  TLorentzVector* Lepton;
  TLorentzVector* Photon;
  double LeptonTopoEtclus;
  double PhotonTopoEtclus;

  if(useBaseline){
    leptons = obj->baseLeptons;
    photons = obj->basePhotons;
  } else {
    leptons = obj->signalLeptons;
    photons = obj->signalPhotons;
  }

  // Leptons
  for(unsigned int lepind=0; lepind < leptons.size(); lepind++){
    Lepton =  dynamic_cast<TLorentzVector*>(leptons[lepind]);
    if(useOnlyCore) LeptonTopoEtclus = leptons[lepind]->SUSY20_topoetcone20NonCoreCone - leptons[lepind]->SUSY20_topoetcone20;
    else LeptonTopoEtclus = leptons[lepind]->SUSY20_topoetcone20NonCoreCone;

    if(TMath::Hypot(Lepton->Eta() - Track->Eta(), track->q * Lepton->DeltaPhi(*Track) + TMath::ASin(0.3*1.4/Track->Pt())) <= deltaR) topoetclus += LeptonTopoEtclus;
  }

  // Photons
  for(unsigned int photonind=0; photonind < photons.size(); photonind++){
    Photon =  dynamic_cast<TLorentzVector*>(photons[photonind]);
    if(useOnlyCore) PhotonTopoEtclus = photons[photonind]->SUSY20_topoetcone20NonCoreCone - photons[photonind]->SUSY20_topoetcone20;
    else PhotonTopoEtclus = photons[photonind]->SUSY20_topoetcone20NonCoreCone;

    if(TMath::Hypot(Photon->Eta() - Track->Eta(), track->q * Photon->DeltaPhi(*Track) + TMath::ASin(0.3*1.4/Track->Pt())) <= deltaR) topoetclus += PhotonTopoEtclus;
  }

  return topoetclus;
}

// ------------------------------------------------------------------------------ //
double Observables::getLepPhotonTrackPt(const Objects* obj, TrackVariable* track, double deltaR, bool useBaseline, bool usePhotonConv)
{

  double LepPhotonTrackPt = 0.;

  TLorentzVector* Track = dynamic_cast<TLorentzVector*>(track);

  for( auto& nearby_track : obj->baseTracks ){
    if ( *track == *nearby_track ){ continue; } // Exclude the track itself
    if ( nearby_track->Pt() < 1.0 ){ continue; }
    if ( fabs(nearby_track->z0SinTheta) > 1.5 ) { continue; }
    if ( fabs(nearby_track->d0) > 1.5 ) { continue; }
    if ( !(nearby_track->isTight) ) { continue; }

    TLorentzVector* Nearby_Track = dynamic_cast<TLorentzVector*>(nearby_track);
    if( !(Track->DeltaR(*Nearby_Track) < deltaR) ) continue;

    bool isAssoc =
      isMuonTrk(obj, nearby_track, useBaseline) ||
      isElectronTrk(obj, nearby_track, useBaseline) ||
      isPhotonTrk(obj, nearby_track, useBaseline);

    if( isAssoc ) LepPhotonTrackPt += nearby_track->Pt();
    else if( usePhotonConv && getLepPhotonMinDR(obj, nearby_track, useBaseline) < 0.02 ) LepPhotonTrackPt += nearby_track->Pt();

  }

  return LepPhotonTrackPt;
}

// ------------------------------------------------------------------------------ //
double Observables::getLepPhotonMinDR(const Objects* obj, TrackVariable* track, bool useBaseline)
{

  double deltaR = 100.;
  LeptonVector leptons;
  PhotonVector photons;
  TLorentzVector* Track = dynamic_cast<TLorentzVector*>(track);
  TLorentzVector* Lepton;
  TLorentzVector* Photon;

  if(useBaseline){
    leptons = obj->baseLeptons;
    photons = obj->basePhotons;
  } else {
    leptons = obj->signalLeptons;
    photons = obj->signalPhotons;
  }

  // Leptons
  for(unsigned int lepind=0; lepind < leptons.size(); lepind++){
    Lepton =  dynamic_cast<TLorentzVector*>(leptons[lepind]);
    if(Lepton->DeltaR(*Track) < deltaR) deltaR = fabs(Lepton->DeltaR(*Track));
  }

  // Photons
  for(unsigned int photonind=0; photonind < photons.size(); photonind++){
    Photon =  dynamic_cast<TLorentzVector*>(photons[photonind]);
    if(Photon->DeltaR(*Track) < deltaR) deltaR = fabs(Photon->DeltaR(*Track));
  }

  return deltaR;
}

// ------------------------------------------------------------------------------ //
bool Observables::isMuonTrk(const Objects* obj, TrackVariable* track, bool useBaseline)
{

  bool isMuon = false;
  MuonVector muons;

  if(useBaseline) muons = obj->baseMuons;
  else muons = obj->signalMuons;

  for(auto& muon : muons){
    if(muon->trkLink && (*muon->trkLink == *track) ) isMuon = true;
  }

  return isMuon;
}

// ------------------------------------------------------------------------------ //
bool Observables::isElectronTrk(const Objects* obj, TrackVariable* track, bool useBaseline)
{

  bool isElectron = false;
  ElectronVector electrons;

  if(useBaseline) electrons = obj->baseElectrons;
  else electrons = obj->signalElectrons;

  for(auto& electron : electrons){
    if(electron->trkLink && (*electron->trkLink == *track) ) isElectron = true;
  }

  return isElectron;
}

// ------------------------------------------------------------------------------ //
bool Observables::isPhotonTrk(const Objects* obj, TrackVariable* track, bool useBaseline)
{

  bool isPhoton = false;
  PhotonVector photons;

  if(useBaseline) photons = obj->basePhotons;
  else photons = obj->signalPhotons;

  for(auto& photon : photons){
    for(auto& photontrack : photon->trkLinks){
      if( *photontrack == *track ) isPhoton = true;
    }
  }

  return isPhoton;
}

// ------------------------------------------------------------------------- //
Observables::~Observables()
{
}
