#include "sys/stat.h"
#include "sys/types.h"
#include <cerrno>
#include <unistd.h>
#include <stdlib.h>
#include <memory>

#include <NearbyLepIsoCorrection/NearbyLepIsoCorrection.h>
#include <IsolationSelection/IsolationSelectionTool.h>

#include "xAODEgamma/ElectronContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODBase/IParticleHelpers.h"
#include "xAODCore/ShallowCopy.h"

#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include <TFile.h>

SG::AuxElement::Decorator<char> Dec_Iso("Isolation"); //Isolation decorator of the lepton
SG::AuxElement::Decorator<char> Dec_In("Input"); //Quality criteria of the leptons
SG::AuxElement::Decorator<char> Dec_signal("Signal"); //Final signal decorator
std::string APP_NAME = "IsoCorrectionTester";
NearLep::IsoCorrection* LepIso(0);
CP::IsolationSelectionTool* IsoTool(0);

template<typename Container> StatusCode RetrieveContainer(xAOD::TEvent &Ev, const std::string &Key, Container* &C, xAOD::ShallowAuxContainer* &Aux);
void PromptLeptons(xAOD::IParticleContainer* Container);
int main(int argc, char* argv[]) {
    std::string InputFile;

    //Let's create the Container and ShallowCopyContainer Pointers
    xAOD::ElectronContainer* Electrons(0);
    xAOD::MuonContainer* Muons(0);
    xAOD::ShallowAuxContainer* ShallowElectrons(0);
    xAOD::ShallowAuxContainer* ShallowMuons(0);
    //Reading the Arguments parsed to the executable
    for (int a = 1; a < argc; ++a) {
        if (strcmp(argv[a], "-I") == 0 && (a + 1) != argc) InputFile = argv[a + 1]; //InputFile
    }
    if (APP_NAME.empty()) {
        Error("NearByLepIsoCorrectionTester", "The name is not defined");
        return EXIT_FAILURE;
    }
    Info(APP_NAME.c_str(), "Open input file %s", InputFile.c_str());
    TFile* File = TFile::Open(InputFile.c_str(), "READ");
    if (File == NULL || File->IsOpen() == false) {
        Error(APP_NAME.c_str(), "The File Could not be opened");
        return EXIT_FAILURE;
    }
    if (!xAOD::Init(APP_NAME.c_str()).isSuccess()) {
        Error(APP_NAME.c_str(), "Could not initialize the xAOD enviroment");
        return EXIT_FAILURE;
    }
    xAOD::TEvent m_event;
    if (!m_event.readFrom(File, xAOD::TEvent::kClassAccess).isSuccess()) {
        Error(APP_NAME.c_str(), "Could not read from the File");
        return EXIT_FAILURE;
    }
    Long64_t EventsInFile = m_event.getEntries();
    Info(APP_NAME.c_str(), "File successfully opened. Events in File %lli", EventsInFile);

    // First intialize an instance of the Isolation selection Tool. 
    // If you are using SUSYTools you do not need to initialize an extra tool. The Correction tool will retrieve any instance of the Isolations tool
    // which is named IsoTool from the ToolStore
    IsoTool = new CP::IsolationSelectionTool("IsoTool");
    if (!IsoTool->setProperty("ElectronWP", "GradientLoose").isSuccess()) Warning(APP_NAME.c_str(), "Could not set the ElectronWP of the Isolation tool");
    if (!IsoTool->setProperty("MuonWP", "GradientLoose").isSuccess()) Warning(APP_NAME.c_str(), "Could not set the MuonWP of the Isolation tool");
    if (!IsoTool->initialize()) {
        Error(APP_NAME.c_str(), "The initialization of the IsolationTool failed");
        return EXIT_FAILURE;
    }

    LepIso = new NearLep::IsoCorrection("LeptonIsoCorrection");

    //Now give the isolation tool the needed decorator names

    if (!LepIso->setProperty("InputQualityDecorator", "Input").isSuccess()) Warning(APP_NAME.c_str(), "Could not set the InputQuality decorator"); //The default value is NonIsoSignal
    if (!LepIso->setProperty("IsolationDecorator", "Isolation").isSuccess()) Warning(APP_NAME.c_str(), "Could not set the isolation decorator");  //The default value is isol as used in SUSYTools
    if (!LepIso->setProperty("SignalDecorator", "Signal").isSuccess()) Warning(APP_NAME.c_str(), "Could not set the signal decorator"); //The default value is signal as used in SUSYTools. This decorator must be different from the input one
    //Ask if the tool should update the signal decorator. 
    //It will override then the signal decorator to Input + Isolation. Otherwise the signal decorator will be left unchanged
    if (!LepIso->setProperty("RequireIsoSignal", true).isSuccess()) Warning(APP_NAME.c_str(), "Unkown if the signal decorator will be overwritten");

    //Now we have a set of different options for the correction.

    // The tool reruns the TackIsolation tool in the default configuration.
    //One can also override the primary vertex collection name
    // LepIso->setProperty( "VertexContainerAPP_NAME" ,"PrimaryVertices" );

    //The tool substracts the track pt of the nearby input lepton without destroying the Pt(Var)Cone values and passes afterwards the lepton to the isolation tool
    if (!LepIso->setProperty("ApplyPtConeCorrection", true).isSuccess()) Warning(APP_NAME.c_str(), "Could not switch on the PtCone correction");
    //If that is not enough to pass the isolation. It afterwards substract the Egamma clusters from nearby Input electrons from the EtCone value.
    if (!LepIso->setProperty("ApplyTopoEtConeCorrection", true).isSuccess()) Warning(APP_NAME.c_str(), "Could not switch on the TopoEtCone correction");
    if (LepIso->initialize().isFailure()) {
        Error(APP_NAME.c_str(), "The initialization of the IsolationCorrectionTool failed");
        return EXIT_FAILURE;
    }

    for (Long64_t i = 0; i < EventsInFile; ++i) {
        m_event.getEntry(i); //Reading the i-th Entry
        //Load the leptons with some dummy selection
        if (!RetrieveContainer(m_event, "Electrons", Electrons, ShallowElectrons).isSuccess()) Error(APP_NAME.c_str(), "Could not retrieve the electrons in event %lli", i);
        else if (!RetrieveContainer(m_event, "Muons", Muons, ShallowMuons).isSuccess()) Error(APP_NAME.c_str(), "Could not retrieve the muons in event %lli", i);
        //Now we have everything in place. Let's substract the signal leptons from the Isolation Cones
        else {
            if (LepIso->CorrectIsolation(Electrons, Muons).isSuccess()) {
                Info(APP_NAME.c_str(), "##############################################################");
                Info(APP_NAME.c_str(), "                Prompt event %lli", i);
                Info(APP_NAME.c_str(), "##############################################################");
                PromptLeptons(Electrons);
                PromptLeptons(Muons);
            }
        }
    }
    return EXIT_SUCCESS;
}

template<typename Container> StatusCode RetrieveContainer(xAOD::TEvent &Ev, const std::string &Key, Container* &C, xAOD::ShallowAuxContainer* &Aux) {
    if (Aux) delete Aux;
    const Container* InCont(0);
    
    if (!Ev.retrieve(InCont, Key).isSuccess()) {
        Error(APP_NAME.c_str(), "Could not retrieve %s", Key.c_str());
        return StatusCode::FAILURE;
    }
    typename std::pair<Container*, xAOD::ShallowAuxContainer*> shallowcopy = xAOD::shallowCopyContainer(*InCont);
    Aux = shallowcopy.second;
    C = shallowcopy.first;
    if (!xAOD::setOriginalObjectLink(*InCont, *C)) {
        Error(APP_NAME.c_str(), "Failed to set Object links to %s", Key.c_str());
        return StatusCode::FAILURE;
    }
    for (typename Container::const_iterator p = C->begin(); p != C->end(); ++p) {
        // Let's skip the calibration and identification steps and just apply a simple kinematic selection
        Dec_In(**p) = ((*p)->pt() > 1000. && fabs((*p)->eta()) < 2.);
        //Actually LepIso checks the isolation requirement as well in the SaveIsolation step. So this line is not really needed
        Dec_Iso(**p) = IsoTool->accept(**p);
        //Finally we define everything passing Input + Isolation as our signal
        Dec_signal(**p) = Dec_Iso(**p) && Dec_In(**p);
    }
    return StatusCode::SUCCESS;
}
void PromptLeptons(xAOD::IParticleContainer* Container) {
    for (xAOD::IParticleContainer::const_iterator L = Container->begin(); L != Container->end(); ++L) {
        //First let's load the original PtConeValue
        float OriginalPtCone = LepIso->GetIsoVar(*L, xAOD::Iso::IsolationType::ptvarcone30, NearLep::StoredCone::Original);
        // Then the Corrected one
        float CorrectedPtCone = LepIso->GetIsoVar(*L, xAOD::Iso::IsolationType::ptvarcone30, NearLep::StoredCone::Corrected);
        //If you have reran the Track IsolationTool then you can also ask for the Recalculated Cone value.
        if (!(*L)->auxdata<char>("Input") || fabs(OriginalPtCone - CorrectedPtCone) < 500.) continue; // If there was nothing to correct why should we prompt it
        //~ if ( ! (*L)->auxdata<char>("Input") || CorrectedPtCone > -50. ) continue; // If there was nothing to correct why should we prompt it
        std::string PartType;
        if ((*L)->type() == xAOD::Type::ObjectType::Electron) PartType = "Electron";
        else if ((*L)->type() == xAOD::Type::ObjectType::Muon) PartType = "Muon";
        Info(PartType.c_str(), "pt : %.2f GeV        eta: %.2f        phi: %.2f     Isolated: %i    PtVarCone (Original): %.2f GeV        PtVarCone (Corrected): %.2f GeV", (*L)->pt() / 1000., (*L)->eta(), (*L)->phi(), (*L)->auxdata<char>("Isolation"), OriginalPtCone / 1000., CorrectedPtCone / 1000.);
    }
}
