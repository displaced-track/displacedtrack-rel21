#ifndef NEARBYLEPISOCORRECTION_h
#define NEARBYLEPISOCORRECTION_h

#include "AsgTools/AsgTool.h"
#include "AsgTools/ToolHandle.h"
#include "AsgTools/AnaToolHandle.h"

#include <vector>
#include <map>

#include <NearbyLepIsoCorrection/INearbyLepIsoCorrection.h>
#include <IsolationSelection/IIsolationSelectionTool.h>

#include <xAODPrimitives/IsolationType.h>
#include <xAODPrimitives/IsolationFlavour.h>
#include <xAODPrimitives/IsolationConeSize.h>

namespace CP {
    class IIsolationSelectionTool;
    class IsolationWP;
}
namespace InDet {
    class IInDetTrackSelectionTool;
}
namespace NearLep {
    /// Class to remove the contributions of close-by leptons from the lepton isolation cone variables.
    ///
    /// @author Johannes Junggeburth <johannes.josef.junggeburth@cern.ch>
    class IsoCorrection: public virtual IIsoCorrection, public asg::AsgTool {
        public:
            ASG_TOOL_CLASS( IsoCorrection, NearLep::IIsoCorrection)
            IsoCorrection ( const std::string& Name);
            ~IsoCorrection();
            /// Initialize the tool
            virtual StatusCode initialize();

            /// These functions copy the Standard isolation values and the uncalibrated pt of a lepton into an separate set of decorators.
            /// Also the functions restore the original isolation variables in the case that the current selection is a systematic variation
            /// They must be called during the baseline/signal selection of the leptons i.e. before the isolation correction is applied.
            /// The decorators are named like the original variables with an Prefix in front. The prefix can be changed via the 'PreFixOrignalCones' property of the tool

            virtual StatusCode SaveIsolation(const xAOD::Electron* E);
            virtual StatusCode SaveIsolation(const xAOD::Muon* M);

            virtual StatusCode LoadIsolation(const xAOD::Electron* E, StoredCone T = StoredCone::Original);
            virtual StatusCode LoadIsolation(const xAOD::Muon* M, StoredCone T = StoredCone::Original);

            /// These functions apply the isolation correction. Leptons that satify the 'InputQualityDecorator' selection criteria are checked whether they
            /// pass the isolation criteria and if it is neccessary the isolation cone variables are then corrected by the contributions from close-by leptons that also
            /// satisfy the 'InputQualityDecorator'
            /// If the 'ApplyPtConeCorrection' is switched on all Pt(Var)Cone variables are corrected using the primary ID tracks associated with muons and electrons in the first place.
            /// if the property 'UseSecondaryTracks' is also switched on then in addition secondary tracks associated with close-by electrons are also used for the correction
            /// Leptons that still fail the isolation requirements are then corrected by the Et() of the CaloClusters associated with close-by electrons
            /// if the 'ApplyTopoEtConeCorrection' property is set.
            virtual StatusCode CorrectIsolation(xAOD::ElectronContainer* Electrons, xAOD::MuonContainer* Muons);

            /// Corrected isolation cones can be saved separately for each applied systematic variation
            virtual StatusCode SetSystematic(const CP::SystematicSet& Set);

            ///Returns the size of the particles isolation cone. In the case of an ptvarconeX  Min ( ptconeX , 10GeV/<uncalibrated pt>) is returned
            virtual double ConeSize(const xAOD::IParticle* P, xAOD::Iso::IsolationType Cone) const;

            ///Returns the value of the particles isolation cone variable. One can choose either between the Standard or the Corrected isolation cone
            virtual float GetIsoVar(const xAOD::IParticle* P, xAOD::Iso::IsolationType Iso, StoredCone T = StoredCone::Original) const;

            /// Retrieves the primary ID track associated with the lepton. If the ID track is not available then the GSF track (Electrons) or the PrimaryTrack(Muons) is returned
            const xAOD::TrackParticle* Track(const xAOD::IParticle* P) const;
            /// Retrieves the calorimeter cluster of the particle
            const xAOD::CaloCluster* Cluster(const xAOD::IParticle* P) const;
            /// The uncalibrated Et of a CaloCluster minus the contribution from the TileGap3 is returned
            float ClusterEtMinusTile(const xAOD::CaloCluster* C) const;

        protected:

            //Copy the Values of the Cones
            virtual StatusCode CopyOriginalValue(const xAOD::IParticle* P);
            virtual StatusCode SaveConeValue(const xAOD::IParticle* P, xAOD::Iso::IsolationType I, float Value, StoredCone T = StoredCone::Corrected);
            virtual bool IsolationAvailable(const xAOD::IParticle* P, xAOD::Iso::IsolationType Iso, StoredCone T = StoredCone::Original) const;

            //Correction of the Cones
            template<typename Cont> StatusCode IsoConeCorrection(const Cont* C);

            //Functions to Correct the PtCone Values
            StatusCode TrackCorrection(const xAOD::IParticle* P); //Corrects the  PtCones of the Particle
            StatusCode TrackCorrection(const xAOD::IParticle* Lep, const xAOD::TrackParticle* CT); //Subtracts the track from the cone
            const xAOD::IParticle* TrackIsoRefPart(const xAOD::IParticle* P) const;
            const xAOD::Vertex* RetrieveBestVtx() const;

            //Determines whether the track satisfies quality criteria or was already used for correction
            bool AcceptTrack(const xAOD::TrackParticle* Track, const xAOD::Vertex* PrimVtx) const;

            template<typename Part> StatusCode UpdateDecorators(const Part* P, StoredCone T = StoredCone::Original);

            //Functions to Correct the TopoEtCone Values
            const xAOD::IParticle* ClusterIsoRefPart(const xAOD::IParticle* P) const;
            StatusCode CaloCorrection(const xAOD::IParticle* NonIso, const xAOD::IParticleContainer* C);
            StatusCode CaloCorrection(const xAOD::IParticle* P);
            float TopoEtCorrection(const xAOD::IParticle* R, const xAOD::IParticle* S, xAOD::Iso::IsolationType I);

            double DeltaR2(const xAOD::IParticle* P, const xAOD::IParticle* P1) const;

            //// Internal variable to check the initialization Status of the Tool
            bool m_Initialized;

            bool m_UseIsoSignal;
            bool m_CopyIsoDecorator;					//The information whether the Particle passes the Isolation or not is stored in a separate decorator
            bool m_UsePassORdecorator;

            bool m_CorrectPtCones;					//The tool simply substract the track pt of the nearby leptons
            bool m_UseSecondaryTracks;

            bool m_CorrectTopoEtCones;				//The tool simply substract the ET of the Cluster associated to the Electrons
            bool m_CorrectTopoEtCore;					//Sets whether clusters within 0.1 should be subtractred from the isolation

            bool m_ResetOriginalCone;					//If this option is switched on the isolation correction overrides the cones with the old values after the iso decorator has been updated
            bool m_AlwaysCorrectCones;

            float m_ISO_ETCONECUT; //If one wants to avoid overcorrection of the TopoEtCone
            float m_MaxDR;

            xAOD::ElectronContainer** m_ElContainer;
            xAOD::MuonContainer** m_MuContainer;
            const xAOD::Vertex* m_PrimVtx;

            ToolHandle<CP::IIsolationSelectionTool> m_isoTool;
            asg::AnaToolHandle<InDet::IInDetTrackSelectionTool> m_TrkSel;

            typedef SG::AuxElement::Decorator<char> CharDecor;
            typedef SG::AuxElement::Decorator<float> FloatDecor;

            std::unique_ptr<CharDecor> m_dec_Isolation;
            std::unique_ptr<CharDecor> m_dec_Signal;
            std::unique_ptr<CharDecor> m_dec_NonIsoSignal;
            std::unique_ptr<CharDecor> m_dec_PassOR;

            std::unique_ptr<CharDecor> m_dec_ConesSaved;
            std::unique_ptr<CharDecor> m_dec_PtConesRecal;
            //~ std::unique_ptr<FloatDecor> m_dec_OriginalPt;

            typedef std::map<xAOD::Iso::IsolationType, std::unique_ptr<FloatDecor>> CorrectionDecorMap;
            typedef std::pair<xAOD::Iso::IsolationType, std::unique_ptr<FloatDecor>> CorrectionDecorator;

            CorrectionDecorMap m_dec_OriginalCones;
            CorrectionDecorMap m_dec_CorrectedCones;
            std::unique_ptr<CharDecor> m_dec_CopyIsolation;

            //Signal Decorator Definitions
            std::string m_NonIsol;
            std::string m_SignalDec;
            std::string m_IsoDec;
            std::string m_ORdec;
            //Definitons to save the cone
            std::string m_PreDecOrig;
            std::string m_PreDecCorr;
            std::string m_VtxContName;

            std::vector<int> m_ISO_PTCONES_int;
            std::vector<int> m_ISO_TOPOETCONES_int;

            std::vector<xAOD::Iso::IsolationType> m_ISO_PTCONES;
            std::vector<xAOD::Iso::IsolationType> m_ISO_TOPOETCONES;
            std::vector<xAOD::Iso::IsolationType> m_ISO_CONES;

            std::set<const xAOD::TrackParticle*> m_ExcludedTracks;

            bool TrackIso(xAOD::Iso::IsolationType Iso) const;
            bool PtIso(xAOD::Iso::IsolationType Iso) const;
            bool PtVarIso(xAOD::Iso::IsolationType Iso) const;

            bool EtIso(xAOD::Iso::IsolationType Iso) const;
            bool TopoEtIso(xAOD::Iso::IsolationType Iso) const;
            bool PassNonIsoSignal(const xAOD::IParticle* P) const;
            bool PassIsolation(const xAOD::IParticle* P) const;

            bool PassSignal(const xAOD::IParticle* P) const;
            bool IsSame(const xAOD::IParticle* P, const xAOD::IParticle* P1) const;

            bool Overlap(const xAOD::IParticle* P, const xAOD::IParticle* P1, double dR) const;
            void PromptParticleInformation(const xAOD::IParticle* Part, std::string AddInfo = "") const;
            std::string BoolToString(bool Boolean) const;

            typedef bool (IsoCorrection::*IsoSelect)(xAOD::Iso::IsolationType) const;
            void FilterIsolationTypes(std::vector<xAOD::Iso::IsolationType> & Cones, IsoSelect Selector);

            void OrderIsolationTypes(std::vector<xAOD::Iso::IsolationType>& Cones, xAOD::Iso::IsolationFlavour Flavour);
            bool IsIsolationTypeInVector(xAOD::Iso::IsolationType Iso, const std::vector<xAOD::Iso::IsolationType>& Vector) const;
            void ExtractIsolationVariables(const std::vector<CP::IsolationWP*> & WPs);
            void TransferIsolationVariables(std::vector<int> &IntVec, std::vector<xAOD::Iso::IsolationType>& IsoVec);
            float GetPtForConeSize(const xAOD::IParticle*) const;

    };
}
#endif // #ifdef NEARBYLEPISOCORRECTION_h
