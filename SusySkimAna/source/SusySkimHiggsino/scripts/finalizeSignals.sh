mkdir outputs
mv *tree_LOOSE_NOMINAL.root outputs/
hadd outputs/AllSignals_SusySkimHiggsino_${GROUP_SET_TAG}_SUSY16_tree_NoSys.root *SusySkimHiggsino*tree_NoSys.root
hadd outputs/AllSignals_SusySkimHiggsino_${GROUP_SET_TAG}_SUSY16_tree_AllSys.root *SusySkimHiggsino*tree*.root
mv *tree_NoSys.root outputs/
