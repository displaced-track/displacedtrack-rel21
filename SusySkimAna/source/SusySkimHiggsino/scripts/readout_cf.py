#!/usr/bin/env python

import argparse, json

import ROOT

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Read-in cutflow from file and save as .json')
    parser.add_argument('-i', '--input', required=True, help="File to read cutflow from.")
    parser.add_argument('-o', '--output', required=True, help="Name of output file")
    args = parser.parse_args()

    f = args.input
    t = ROOT.TFile(f)
    cf = t.Get("cutFlow___unweighted")
    cuts = []
    cutflow = {}
    for i in range(1, cf.GetNbinsX()+1):
        label = cf.GetXaxis().GetBinLabel(i)
        if label:
            cuts.append(label)
            cutflow[label] = cf.GetBinContent(i)
    for cut in cuts:
        print(cut, cutflow[cut])
    
    if not args.output.endswith(".json"):
        args.output += ".json"
    with open(args.output, "w") as w:
        json.dump({"labels" : cuts, "cutflow" : cutflow}, w, indent=4)
