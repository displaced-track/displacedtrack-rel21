mkdir outputs
mv *tree_LOOSE_NOMINAL.root outputs/

# note that "data" is not in this list since
# it would be identical to the NoSys anyway
#top REMOVE FOR NOW 
listOfBkgsForHistFitter="
top
diboson
other
Zjets_all
Zttjets_all
"
#Zgamma
#Zttgamma

# Make sure the below list doesn't have any of the trees above,
# or we'll be wasting time by running more hadds than needed!
#alt_diboson0L_PowPy
#alt_diboson1L_PowPy
#alt_diboson2L_PowPy
#alt_diboson3L_PowPy
#alt_diboson4L_PowPy
#alt_DY_PowPy
#alt_triboson_Sherpa222
#alt_ttbar_aMcAtNlo_dilep
#alt_ttbar_PowhegPythia8_nonallhad_hdamp258p75
#alt_ttbar_PowPy8_dilep_hdamp258p75
#alt_ttbar_PowPy8_nonallhad_hdamp258p75
#alt_ttbar_Sherpa_221_dilepton
#alt_ttbar_Sherpa_221_singleLepton
#alt_ttbar_Sherpa_224_dilepton
#Wjets
listOfOtherBkgs="
diboson0L
diboson1L
diboson2L
diboson3L
diboson4L
higgs
singletop
topOther
triboson
ttbar
"
#Wgamma
#Zvvgamma
#Zvvjets

# loop over both lists of bkg samples
#for listOfBkgs in $listOfBkgsForHistFitter;
for listOfBkgs in $listOfBkgsForHistFitter $listOfOtherBkgs;
do

  # hadd each bkg sample into one root file that covers all systs
  for bkgSample in $listOfBkgs;
  do
    echo ${bkgSample}
    # FIXME once we have systematics
    hadd outputs/${bkgSample}_SusySkimHiggsino_${GROUP_SET_TAG}_hadded_tree.root ${bkgSample}_*SusySkimHiggsino*tree*root
  done

done

# hadd HistFitter output files for NoSys and AllSys
# unsure if we want to hadd data with bkg this time around...
#strHaddNoSys="hadd outputs/AllBkgsButFakes_SusySkimHiggsino_${GROUP_SET_TAG}_SUSY16_tree_NoSys.root data_*SusySkimHiggsino*tree_NoSys.root " 
strHaddNoSys="hadd outputs/AllBkgsButFakes_SusySkimHiggsino_${GROUP_SET_TAG}_SUSY16_tree_NoSys.root "
strHaddAllSys="hadd outputs/AllBkgsButFakes_SusySkimHiggsino_${GROUP_SET_TAG}_SUSY16_tree_AllSys.root " # FIXME once we have systs

# loop over all HistFitter bkgs
for bkgSample in $listOfBkgsForHistFitter;
do
  strHaddNoSys+="${bkgSample}_*SusySkimHiggsino*tree_NoSys.root "
  strHaddAllSys+="${bkgSample}_*SusySkimHiggsino*tree*.root " # FIXME once we have systs
done

# execute the hadd commands
eval $strHaddNoSys
eval $strHaddAllSys # FIXME once we have systs

mv *tree_NoSys.root outputs/

