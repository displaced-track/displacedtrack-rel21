import ROOT,sys,os

'''
Usage: hadd trees with different names together

python haddTreesWithDifferentNames.py diboson diboson2L diboson3L diboson4L; 
python haddTreesWithDifferentNames.py top ttbar singletop; 
python haddTreesWithDifferentNames.py other topOther higgs triboson
'''

listOfSamples = sys.argv

# script name, so who cares
listOfSamples.pop(0)

# output sample name
outSample = listOfSamples.pop(0)

#listOfSys = [
#   	"NoSys"
#]
listOfSys = [
        "NoSys"
        ,"LOOSE_NOMINAL"
	,"EG_RESOLUTION_ALL__1down"
	,"EG_RESOLUTION_ALL__1up"
	,"EG_SCALE_AF2__1down"
	,"EG_SCALE_AF2__1up"
	,"EG_SCALE_ALL__1down"
	,"EG_SCALE_ALL__1up"
	,"JET_EtaIntercalibration_NonClosure_highE__1down"
	,"JET_EtaIntercalibration_NonClosure_highE__1up"
	,"JET_EtaIntercalibration_NonClosure_negEta__1down"
	,"JET_EtaIntercalibration_NonClosure_negEta__1up"
	,"JET_EtaIntercalibration_NonClosure_posEta__1down"
	,"JET_EtaIntercalibration_NonClosure_posEta__1up"
	,"JET_Flavor_Response__1down"
	,"JET_Flavor_Response__1up"
	,"JET_GroupedNP_1__1down"
	,"JET_GroupedNP_1__1up"
	,"JET_GroupedNP_2__1down"
	,"JET_GroupedNP_2__1up"
	,"JET_GroupedNP_3__1down"
	,"JET_GroupedNP_3__1up"
	,"JET_JER_DataVsMC__1down"
	,"JET_JER_DataVsMC__1up"
	,"JET_JER_EffectiveNP_1__1down"
	,"JET_JER_EffectiveNP_1__1up"
	,"JET_JER_EffectiveNP_2__1down"
	,"JET_JER_EffectiveNP_2__1up"
	,"JET_JER_EffectiveNP_3__1down"
	,"JET_JER_EffectiveNP_3__1up"
	,"JET_JER_EffectiveNP_4__1down"
	,"JET_JER_EffectiveNP_4__1up"
	,"JET_JER_EffectiveNP_5__1down"
	,"JET_JER_EffectiveNP_5__1up"
	,"JET_JER_EffectiveNP_6__1down"
	,"JET_JER_EffectiveNP_6__1up"
	,"JET_JER_EffectiveNP_7restTerm__1down"
	,"JET_JER_EffectiveNP_7restTerm__1up"
	,"LOOSE_NOMINAL"
	,"MET_SoftTrk_ResoPara"
	,"MET_SoftTrk_ResoPerp"
	,"MET_SoftTrk_ScaleDown"
	,"MET_SoftTrk_ScaleUp"
	,"MUON_ID__1down"
	,"MUON_ID__1up"
	,"MUON_MS__1down"
	,"MUON_MS__1up"
	,"MUON_SCALE__1down"
	,"MUON_SCALE__1up"
        ]
#R20 systematics
#LOOSE_NOMINAL.root
#EG_RESOLUTION_ALL__1down.root
#EG_RESOLUTION_ALL__1up.root
#EG_SCALE_ALL__1down.root
#EG_SCALE_ALL__1up.root
#JET_EtaIntercalibration_NonClosure__1down.root
#JET_EtaIntercalibration_NonClosure__1up.root
#JET_GroupedNP_1__1down.root
#JET_GroupedNP_1__1up.root
#JET_GroupedNP_2__1down.root
#JET_GroupedNP_2__1up.root
#JET_GroupedNP_3__1down.root
#JET_GroupedNP_3__1up.root
#JET_JER_SINGLE_NP__1up.root
#JET_RelativeNonClosure_AFII__1down.root
#JET_RelativeNonClosure_AFII__1up.root
#MET_SoftTrk_ResoPara.root
#MET_SoftTrk_ResoPerp.root
#MET_SoftTrk_ScaleDown.root
#MET_SoftTrk_ScaleUp.root
#MUON_ID__1down.root
#MUON_ID__1up.root
#MUON_MS__1down.root
#MUON_MS__1up.root
#MUON_SCALE__1down.root
#MUON_SCALE__1up.root

for sys in listOfSys :
    
    print sys
    chain = ROOT.TChain(outSample+"_"+sys)
    print chain

    for sample in listOfSamples :
        tree = sample+"_SusySkimHiggsino_"+os.getenv("GROUP_SET_TAG",None)+"_SUSY16_tree_"+sys+".root/"+sample+"_"+sys
	print tree
        if sys == "NoSys" or sys == "LOOSE_NOMINAL" :
            chain.Add(tree)
        else :
            chain.Add(tree)
            #chain.Add("../"+tree)

    outfile = ROOT.TFile(outSample+"_SusySkimHiggsino_"+os.getenv("GROUP_SET_TAG",None)+"_SUSY16_tree_"+sys+".root","RECREATE")
    newchain = chain.CloneTree()
    newchain.SetName(outSample+"_"+sys)
    newchain.SetTitle(outSample+"_"+sys)
    outfile.Write()
