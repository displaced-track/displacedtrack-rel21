#Functions for making group sets
import subprocess
import glob
import os


#Dictionary of mc r tags
MCtagsDict = {}
MCtagsDict['mc16a'] = 'r9364'
MCtagsDict['mc16d'] = 'r10201'
MCtagsDict['mc16e'] = 'r10724'

def inclusiveRange(a, b):
    return range (a, b+1)

def GetDSID (DID):
    """Get DSID number from DID name"""
    if '.' in DID:
        DSID = DID.split('.')[1]
        return DSID
    else:
        print ("Error in GetDSID for {}".format(DID))
        
def GetAllContainers (MCperiod, ptag):
    """ Gets all container names matching MCperiod and ptag from Rucio.
    Doing this so samples in group sets exist on Rucio """
    
    cmd1 = ['rucio', 'list-dids', '--filter', 'type=CONTAINER', '--short',  'mc16_13TeV:mc16_13TeV*deriv.DAOD_EXOT5*{}_{}'.format(MCtagsDict[MCperiod], ptag) ]
    output1 = subprocess.check_output(cmd1)
    containerlist = output1.split()
    containerlist.sort(key = lambda entry: int(GetDSID(entry))) #Sort container list by DSID
    return containerlist

def MakeGroupSetTextFile (SampleName, Direc, DSIDRange, ContainerList, MCperiod): 
    """ Makes a group set text file

    Parameters
    _________
    SampleName: string
        name of the sample
    Direc : string
        directory to save text file to  
    DSIDRange : range 
        DSID numbers for particular sample
    ContainlerList: list
        List of containers to filter 
    MCperiod: string
       MC period for this groupset 
    """
    
    DesiredContainers = []
    KnownDSIDs = []
    if ContainerList:
        for entry in ContainerList:
            #DesiredEntry = entry.replace('mc16_13TeV:','') #uncomment if want to remove scope
            DSID = int(GetDSID(entry))
            
            if DSID in DSIDRange:
                if DSID in KnownDSIDs:
                    print ('Error trying to add same DSID {} twice!'.format(DSID))
                    return
                    KnownDSIDs.append(DSID)
                DesiredContainers.append(entry)
                #DesiredContainers.append(DesiredEntry)
        del entry
    
    #Check if all are of same time
    if DesiredContainers:
        AF2 = -1
        if all ('s3126' in x for x in DesiredContainers):
            AF2 = 0
            textfilename = Direc +'/' + SampleName + '_' + MCperiod + '.txt'
            writeTextFile(SampleName, textfilename, AF2, DesiredContainers)

        elif all('a875' in x for x in DesiredContainers):
            AF2 = 1
            textfilename = Direc +'/' + SampleName + '_' + MCperiod +'_AF2' '.txt'
            writeTextFile(SampleName+ '_AF2', textfilename, AF2, DesiredContainers)

        else:
            textfilename1 = Direc +'/' + SampleName + '_' + MCperiod + '.txt'
            FullSimContainers = [x for x in DesiredContainers if 's3126' in x]
            writeTextFile(SampleName, textfilename1 , 0, FullSimContainers)
            
            textfilename2 = Direc +'/' + SampleName + '_' + MCperiod +'_AF2' '.txt'
            AF2Containers =  [x for x in DesiredContainers if 'a875' in x]
            writeTextFile(SampleName + '_AF2', textfilename2, 1, AF2Containers)

    else:
        print ('No entries matching in Container list for {}'.format(SampleName))
    return

def writeTextFile(SampleName, textfilename, AF2, DesiredContainers ):
    """Writes the group set text file"""
    with open(textfilename, 'w') as outfile:
        outfile.writelines(['\n', 
                            '# NAME \t\t : {} \n'.format(SampleName),
                            '# DATA \t\t : 0 \n',
                            '# AF2 \t\t : {} \n'.format(AF2),
                            '# PRIORITY \t : 0 \n'])
        for entry in DesiredContainers:
            outfile.write('\n' + entry)
        print ('File {} written'.format(textfilename))
