
########################## READ BEFORE EDITING #########################
# Take note of the following:
#  1) Any line containing a '#' will be ignored
#     - Note that this means you cannot put comments at the end of the line!
#  2) Separate a field and its value by ":"
#  3) For booleans, use an integer: false==0, true==1
#  4) For paths, they will be expanded, so please always specify with respect to $ROOTCOREBIN env
########################################################################

#
# DxAOD Kernel name: This specifies which Bookkeeper data to extract
# and ultimately will be used to normalize the samples to the total sum of weights
#
DxAODKernel : PHYSKernel

#
# Paths specific to SusySkimHiggsino
#
SysFileDir         : SusySkimHiggsino/systematics/
GroupSetDir        : SusySkimHiggsino/samples/
SusyToolsConfigDir : SusySkimHiggsino/SUSYTools/

SusyToolsConfigFile : SusySkimHiggsino/SUSYTools/SUSYTools_Slep.conf

#
# Pile-up reweighting information
#
LumiCalcMC16A : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root
LumiCalcMC16A : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root
LumiCalcMC16CD : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root
LumiCalcMC16E : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root

#To manually set PRW file
#PRWMC16A : /r03/atlas/domjones/CompressedVBF/RootFiles/Ntuples/TestSamples/mc16_13TeV.310502.Pythia8_A14NNPDF23LO_jetjet_Powerlaw_MJJ1000.deriv.NTUP_PILEUP.e6945_s3126_r9364_p3288/NTUP_PILEUP.15778259._000001.pool.root.1

#Special PRW files, used over the AutoMagicPRW tool
PRWSpecial_Ztautau_364221_mc16d : SusySkimMaker/PRW/Ztautau_364221_mc16d_Fixed.root 
PRWSpecial_Znunu_364222_mc16d : SusySkimMaker/PRW/Znunu_364222_mc16d_Fixed.root 
PRWSpecial_Znunu_364223_mc16a : SusySkimMaker/PRW/Znunu_364223_mc16a_Fixed.root 
PRWSpecial_WjetsPTV_364224_mc16e : SusySkimMaker/PRW/WjetsPTV_364224_mc16e_Fixed.root
PRWSpecial_QCDdijet_PowerLaw_mc16a : SusySkimMaker/PRW/pileup_mc16a_dsid310502_FS_fixed.root
PRWSpecial_QCDdijet_Pyth_361027_mc16d : SusySkimMaker/PRW/pileup_mc16d_dsid361027_FS.root
PRWSpecial_QCDdijet_Pyth_361032_mc16d : SusySkimMaker/PRW/pileup_mc16d_dsid361032_FS.root

# Shut off PRW
#DisablePRW : 1
DisablePRW : 0

# Use SUSYTools automagic PRW configuration
UseAutoMagicPRW : 1

# Ignore the GRL
IgnoreGRL : 0

# Good run list information
#
Grl : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml
Grl : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml
Grl : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml
Grl : /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20190318/data18_13TeV.periodAllYear_DetStatus-v102-pro22-04_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml


# Cross sections taken from PMG tool, use default file: dev/PMGTools/PMGxsecDB_mc16.txt
UsePMGXsecTool : 1

#
# MET Rebuilding, true or false
#
ExcludeAdditionalLeptons : 0

#
# Filtering options
#
FilterBaseline     : 1
FilterOR           : 0
#SkimNLeptons       : 2
SkimNLeptons       : 0
DoSampleOR         : 0
DoSampleORZjetZgam : 0

# Options for building MET
UsePhotonsInMET    : 1
UseTausInMET       : 0

# Data specific filtering
RequireTrigInData : 0
RequireGRL : 1

# Misc configurations
#
UnitsFromMEV        : 0.001
SaveTruthInfo       : 1
WriteDetailedSkim   : 1
UseNearbyLepIsoCorr : 0
SaveTracks          : 0
WriteEgammaClusters : 0
PrimaryTrackContainer : InDetTrackParticles

#
# Scale factor control
DoTriggerSFs : 1

TriggerUpstreamMatching : 1
WriteTriggerPrescales : 1

#
# Tool verbosity
# VERBOSE = 1, DEBUG = 2, INFO = 3, WARNING = 4, ERROR = 5
#
TrigMatchToolVerbosity : 5
SUSYToolsVerbosity : 3

# Truth level configurations
#
EmulateTruthBtagging : 1
SaveTruthEvtInfo : 1
SaveTruthEvtInfoFull: 0
SaveTruthParticleInfo: 1

#
# LHE3 Weights (for samples where appropriate)
#
WriteLHE3 : 1

# Rebuild an additional ETmiss with muons flagged as invisible
AddMETWithMuonsInvis : 1
AddMETWithLeptonsInvis : 1

# Save Taus
SaveTaus : 1

# To write out Btag scores
DoBTagScores: 1
SkipMV2c10Scores: 1
DL1rContainerName: AntiKt4EMPFlowJets_BTagging201903
