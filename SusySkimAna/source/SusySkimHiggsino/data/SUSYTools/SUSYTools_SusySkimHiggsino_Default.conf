##################################################
# SUSYTools configuration file
##################################################
EleBaseline.Pt: 4500.
EleBaseline.Eta: 2.47
EleBaseline.Id: VeryLooseLLH
EleBaseline.CrackVeto: false
EleBaseline.z0: 0.5
#
Ele.Et: 4500.
Ele.Eta: 2.47
Ele.CrackVeto: false
#Ele.Iso: Gradient
Ele.Iso: FCLoose
Ele.Id: MediumLLH
Ele.d0sig: 5.
Ele.z0: 0.5
Ele.IsoHighPt: FCHighPtCaloOnly
Ele.DoModifiedId: true
#
MuonBaseline.Pt: 3000.
MuonBaseline.Id: 2 # Low pT WP is 5
MuonBaseline.z0: 0.5
#
Muon.Pt: 3000.
Muon.Eta: 2.5
Muon.Id: 2 # Medium. Low pT WP is 5
Muon.Iso: Loose_VarRad
Muon.d0sig: 3.
Muon.z0: 0.5
#
MuonCosmic.z0: 1.
MuonCosmic.d0: 0.2
#
BadMuon.qoverp: 0.2
#
PhotonBaseline.Pt: 25000.
PhotonBaseline.Id: Tight

#
Photon.Pt: 130000.
Photon.Eta: 2.37
Photon.Id: Tight
Photon.Iso: FixedCutTight
#
Tau.Pt: 20000.
Tau.Eta: 2.50
Tau.Id: Medium
#
#
Jet.Pt: 20000.
Jet.Eta: 4.5
Jet.InputType: 9 # EMPFlow. EMTopo is 1.
#Jet.JESNPSet: 1 # no longer exists
Jet.JVT_WP: Medium
#Jet.UncertConfig: rel21/Fall2018/R4_GlobalReduction_FullJER.config
Jet.UncertConfig: rel21/Summer2019/R4_CategoryReduction_SimpleJER.config
#
FwdJet.doJVT: false
FwdJet.JvtEtaMin: 2.5
FwdJet.JvtPtMax: 50e3
#
Jet.LargeRcollection: AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets
#Jet.LargeRuncConfig: MultiTagging_medium.config
#Jet.WtaggerWP: medium
#Jet.ZtaggerWP: medium
#
BadJet.Cut: LooseBad

Btag.enable: true
##
#Btag.Tagger: MV2c10
Btag.WP: FixedCutBEff_85
Btag.MinPt: 20000.
#Btag.CalibPath: xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2018-10-19_v1.root
BtagTrkJet.MinPt: 20000.
Btag.Tagger: DL1r
Btag.CalibPath: xAODBTaggingEfficiency/13TeV/2020-21-13TeV-MC16-CDI-2021-04-16_v1.root
Btag.TimeStamp: 201903

#
# set the -999. to positive number to override default
OR.DoBoostedElectron: true
OR.BoostedElectronC1: -999.
OR.BoostedElectronC2: -999.
OR.BoostedElectronMaxConeSize: -999.
OR.DoBoostedMuon: true
OR.BoostedMuonC1: -999.
OR.BoostedMuonC2: -999.
OR.BoostedMuonMaxConeSize: -999.
OR.DoMuonJetGhostAssociation: true
OR.DoTau: false
OR.DoPhoton: true
OR.Bjet: true
OR.ElBjet: true
OR.MuBjet: true
OR.MuJetPtRatio: -999.
OR.MuJetTrkPtRatio: -999.
OR.RemoveCaloMuons: true
#OR.ApplyJVT: true
OR.MuJetInnerDR: -999.
OR.BtagWP: FixedCutBEff_85
OR.DoFatJets: false
OR.EleFatJetDR: 1.
OR.JetFatJetDR: 1.
#
SigLep.RequireIso: 0
#
MET.EleTerm: RefEle
MET.GammaTerm: RefGamma
MET.TauTerm: RefTau
MET.JetTerm: RefJet
MET.MuonTerm: Muons
MET.OutputTerm: Final
MET.JetSelection: Tight
MET.RemoveOverlappingCaloTaggedMuons: 1
#MET.DoMuonJetOR: 1
MET.DoTrkSyst: 1
MET.DoCaloSyst: 0
#
#METSys.ConfigPrefix: METUtilities/data17_13TeV/prerec_Jan16
#
#PRW.DefaultChannel: -1
# actual Mu files have to be set in SUSYTools
PRW.ActualMu2017File: GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRW.ActualMu2018File: GoodRunsLists/data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root

#PRW.autoconfigPRWPath: /afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData/dev/SUSYTools/PRW_AUTOCONFIG_SIM/files/
#PRW.MuUncertainty: 0.2 #obsolete Oct9

# Trigger SFs configuration
# Use TrigGlobalEfficiencyCorrectionTool interface from SUSYTools
# SUSYTools did not implement a Singlelep instance of the Tool for
# some reason, so we use the Dilep variant and feed it with single muon expressions
Trig.Dilep2015: mu20_iloose_L1MU15_OR_mu50
Trig.Dilep2016: mu26_ivarmedium_OR_mu50
Trig.Dilep2017: mu26_ivarmedium_OR_mu50
Trig.Dilep2018: mu26_ivarmedium_OR_mu50
# just hijack the multilep variant for our single electron triggers as well
Trig.Multi2015: e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose
Trig.Multi2016: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
Trig.Multi2017: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
Trig.Multi2018: e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0

StrictConfigCheck: true
