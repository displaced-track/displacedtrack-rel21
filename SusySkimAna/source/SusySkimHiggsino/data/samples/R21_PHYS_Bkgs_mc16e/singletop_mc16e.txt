# NAME          : singletop
# Data          : 0
# AF2           : 0
# PRIORITY      : 0

mc16_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_PHYS.e6527_s3126_r10724_p5001
mc16_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_PHYS.e6527_s3126_r10724_p5001
mc16_13TeV.410646.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top.deriv.DAOD_PHYS.e6552_s3126_r10724_p5001
mc16_13TeV.410647.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop.deriv.DAOD_PHYS.e6552_s3126_r10724_p5001
mc16_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_PHYS.e6671_s3126_r10724_p5001
mc16_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_PHYS.e6671_s3126_r10724_p5001
