# NAME          : diboson_fullLep_strong_Sherpa2212
# Data          : 0
# AF2           : 0
# PRIORITY      : 0

mc16_13TeV.700600.Sh_2212_llll.deriv.DAOD_PHYS.e8433_s3126_r9364_p5001
mc16_13TeV.700601.Sh_2212_lllv.deriv.DAOD_PHYS.e8433_s3126_r9364_p5001
mc16_13TeV.700602.Sh_2212_llvv_os.deriv.DAOD_PHYS.e8433_s3126_r9364_p5001
mc16_13TeV.700603.Sh_2212_llvv_ss.deriv.DAOD_PHYS.e8433_s3126_r9364_p5001
mc16_13TeV.700604.Sh_2212_lvvv.deriv.DAOD_PHYS.e8433_s3126_r9364_p5001
mc16_13TeV.700605.Sh_2212_vvvv.deriv.DAOD_PHYS.e8433_s3126_r9364_p5001
