# NAME          : Gjets
# DATA          : 0
# AF2           : 0
# PRIORITY      : 0

mc16_13TeV.364541.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_17_35.deriv.DAOD_SUSY20.e6788_s3126_r10724_p5241
mc16_13TeV.364542.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_35_70.deriv.DAOD_SUSY20.e6788_s3126_r10724_p5241
mc16_13TeV.364543.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_70_140.deriv.DAOD_SUSY20.e5938_s3126_r10724_p5241
mc16_13TeV.364544.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_140_280.deriv.DAOD_SUSY20.e5938_s3126_r10724_p5241
mc16_13TeV.364545.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_280_500.deriv.DAOD_SUSY20.e5938_s3126_r10724_p5241
mc16_13TeV.364546.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_500_1000.deriv.DAOD_SUSY20.e5938_s3126_r10724_p5241
mc16_13TeV.364547.Sherpa_222_NNPDF30NNLO_SinglePhoton_pty_1000_E_CMS.deriv.DAOD_SUSY20.e6068_s3126_r10724_p5241
