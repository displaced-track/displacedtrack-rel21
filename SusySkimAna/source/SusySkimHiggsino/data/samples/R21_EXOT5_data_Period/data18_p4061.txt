
# NAME     : data18
# DATA     : 1
# AF2      : 0
# PRIORITY : 0

data18_13TeV:data18_13TeV.periodQ.physics_Main.PhysCont.DAOD_EXOT5.grp18_v01_p4061
data18_13TeV:data18_13TeV.periodO.physics_Main.PhysCont.DAOD_EXOT5.grp18_v01_p4061
data18_13TeV:data18_13TeV.periodL.physics_Main.PhysCont.DAOD_EXOT5.grp18_v01_p4061
data18_13TeV:data18_13TeV.periodK.physics_Main.PhysCont.DAOD_EXOT5.grp18_v01_p4061
data18_13TeV:data18_13TeV.periodM.physics_Main.PhysCont.DAOD_EXOT5.grp18_v01_p4061
data18_13TeV:data18_13TeV.periodI.physics_Main.PhysCont.DAOD_EXOT5.grp18_v01_p4061
data18_13TeV:data18_13TeV.periodB.physics_Main.PhysCont.DAOD_EXOT5.grp18_v01_p4061
data18_13TeV:data18_13TeV.periodD.physics_Main.PhysCont.DAOD_EXOT5.grp18_v01_p4061
data18_13TeV:data18_13TeV.periodF.physics_Main.PhysCont.DAOD_EXOT5.grp18_v01_p4061
data18_13TeV:data18_13TeV.periodC.physics_Main.PhysCont.DAOD_EXOT5.grp18_v01_p4061
