
# NAME 		: MGPy8EG_A14N23LO_SM_N2C1m_%5_%6_2L2MET75_MadSpin
# DATA 		: 0
# AF2 		: 1
# PRIORITY 	: 0

mc16_13TeV.395046.MGPy8EG_A14N23LO_SM_N2C1m_100_60_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.396909.MGPy8EG_A14N23LO_SM_N2C1m_101p5_100_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7025_a875_r10724_p3654
mc16_13TeV.395057.MGPy8EG_A14N23LO_SM_N2C1m_102_100_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6708_a875_r10724_p3654
mc16_13TeV.395058.MGPy8EG_A14N23LO_SM_N2C1m_103_100_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395059.MGPy8EG_A14N23LO_SM_N2C1m_105_100_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395060.MGPy8EG_A14N23LO_SM_N2C1m_110_100_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395054.MGPy8EG_A14N23LO_SM_N2C1m_110_80_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395061.MGPy8EG_A14N23LO_SM_N2C1m_115_100_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6708_a875_r10724_p3654
mc16_13TeV.395062.MGPy8EG_A14N23LO_SM_N2C1m_120_100_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395047.MGPy8EG_A14N23LO_SM_N2C1m_120_60_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6980_a875_r10724_p3654
mc16_13TeV.395055.MGPy8EG_A14N23LO_SM_N2C1m_120_80_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.396917.MGPy8EG_A14N23LO_SM_N2C1m_126p5_125_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7025_a875_r10724_p3654
mc16_13TeV.395066.MGPy8EG_A14N23LO_SM_N2C1m_127_125_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6708_a875_r10724_p3654
mc16_13TeV.395076.MGPy8EG_A14N23LO_SM_N2C1m_153_150_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395077.MGPy8EG_A14N23LO_SM_N2C1m_155_150_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6708_a875_r10724_p3654
mc16_13TeV.395078.MGPy8EG_A14N23LO_SM_N2C1m_160_150_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395079.MGPy8EG_A14N23LO_SM_N2C1m_165_150_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395080.MGPy8EG_A14N23LO_SM_N2C1m_170_150_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6708_a875_r10724_p3654
mc16_13TeV.395081.MGPy8EG_A14N23LO_SM_N2C1m_180_150_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6708_a875_r10724_p3654
mc16_13TeV.395082.MGPy8EG_A14N23LO_SM_N2C1m_190_150_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395086.MGPy8EG_A14N23LO_SM_N2C1m_205_200_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
#mc16_13TeV.395083.MGPy8EG_A14N23LO_SM_N2C1m_210_150_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395087.MGPy8EG_A14N23LO_SM_N2C1m_210_200_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395088.MGPy8EG_A14N23LO_SM_N2C1m_215_200_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6708_a875_r10724_p3654
mc16_13TeV.395089.MGPy8EG_A14N23LO_SM_N2C1m_220_200_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395096.MGPy8EG_A14N23LO_SM_N2C1m_260_250_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395097.MGPy8EG_A14N23LO_SM_N2C1m_265_250_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.396913.MGPy8EG_A14N23LO_SM_N2C1m_81p5_80_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7025_a875_r10724_p3654
mc16_13TeV.395048.MGPy8EG_A14N23LO_SM_N2C1m_82_80_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6708_a875_r10724_p3654
mc16_13TeV.395049.MGPy8EG_A14N23LO_SM_N2C1m_83_80_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6708_a875_r10724_p3654
mc16_13TeV.395050.MGPy8EG_A14N23LO_SM_N2C1m_85_80_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654
mc16_13TeV.395051.MGPy8EG_A14N23LO_SM_N2C1m_90_80_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e6708_a875_r10724_p3654
mc16_13TeV.395052.MGPy8EG_A14N23LO_SM_N2C1m_95_80_2L2MET75_MadSpin.deriv.DAOD_SUSY16.e7035_a875_r10724_p3654

