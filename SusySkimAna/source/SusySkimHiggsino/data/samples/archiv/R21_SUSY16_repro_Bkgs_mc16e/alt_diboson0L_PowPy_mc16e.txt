# NAME 		: alt_diboson0L_PowPy
# DATA 		: 0
# AF2 		: 0
# PRIORITY 	: 0

mc16_13TeV.361605.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvvv_mll4.deriv.DAOD_SUSY16.e4054_s3126_r10724_p3652
mc16_13TeV.361608.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqvv.deriv.DAOD_SUSY16.e4711_s3126_r10724_p3652
mc16_13TeV.361611.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvqq_mqq20.deriv.DAOD_SUSY16.e4711_s3126_r10724_p3652
