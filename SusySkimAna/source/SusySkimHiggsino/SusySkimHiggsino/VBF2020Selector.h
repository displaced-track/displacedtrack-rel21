#ifndef SusySkimHiggsino_VBF2020Selector_h
#define SusySkimHiggsino_VBF2020Selector_h

//RootCore
#include "SusySkimMaker/BaseUser.h"
#include "SusySkimHiggsino/Common.h"

class VBF2020Selector : public BaseUser
{

 public:
  VBF2020Selector();
  ~VBF2020Selector() {};

  Common com;
  void setup(ConfigMgr*& configMgr);
  bool doAnalysis(ConfigMgr*& configMgr);
  bool passCuts(ConfigMgr*& configMgr);
  void finalize(ConfigMgr*& configMgr);

  bool init_merge(MergeTool*& /*mergeTool*/){return true; } 
  bool execute_merge(MergeTool*& /*mergeTool*/){return true;} 


  // Cut flow processor (when in cutFlowOnly mode)
  void doCutFlow(ConfigMgr*& configMgr);
  void fillCutFlows(ConfigMgr*& configMgr, TString cutFlowName, TString description);
  
  //void computeVBF2020Variables(ConfigMgr*& configMgr);
  //void writeVBF(ConfigMgr*& configMgr);
  //void computeVBFJigSawVariables(ConfigMgr*& configMgr); 

    /*
  JetVector FilterJets(JetVector JETs, double pt_cut, double eta_cut);
  void addJetVariables(const ConfigMgr* configMgr, TString prefix);
  void setJetVariables(const ConfigMgr* configMgr, TString prefix, const JetVariable* jet);
    */


  // Control flags
  bool m_cutFlowOnly;

  //Variables
  //Declared here so can be used in doAnalysis / passCuts / RJR
  /*
  bool validVBFtags; 
  bool validSpecJet;

  TLorentzVector vbftag_J1; 
  TLorentzVector vbftag_J2; 
  TLorentzVector vbftag_J3; 
  TLorentzVector jetC; 
  TLorentzVector jetD; 

  ElectronVector CombiBaselineElectrons;
  */

};

static const BaseUser* VBF2020Selector_instance __attribute__((used)) = new VBF2020Selector();

#endif
