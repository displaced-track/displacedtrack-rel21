#include "SusySkimHiggsino/HiggsinoSelector.h"
#include <algorithm>

// includes needed for emulation of L1Topo based triggers
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTrigger/EnergySumRoI.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODJet/JetContainer.h"

// includes needed for RJR variable computation 
#include "RestFrames/RestFrames.hh"
#include <TLorentzVector.h>
#include <TVector3.h>
#include <vector>
//#include "TruthDecayContainer/TruthEvent_XX.h"
// ------------------------------------------------------------------------------------------ //
HiggsinoSelector::HiggsinoSelector() :
  BaseUser("SusySkimHiggsino", "HiggsinoSelector")
{

}



// ------------------------------------------------------------------------------------------ //
void HiggsinoSelector::setup(ConfigMgr*& configMgr)
{

  com.setup(configMgr, true);

  // Turn off OR for baseline muons for fakes selection
  configMgr->obj->disableORBaselineMuons(false);

}


// ------------------------------------------------------------------------------------------ //
bool HiggsinoSelector::doAnalysis(ConfigMgr*& configMgr)
{
  /*
    This is the main method, which is called for each event
  */

  // Skims events by imposing any cuts you define in this method below
  if( !passCuts(configMgr) ) return false;

  com.doAnalysis(configMgr);

  // Fill the output tree
  configMgr->treeMaker->Fill(configMgr->getSysState(),"tree");

  return true;

}



// ------------------------------------------------------------------------------------------ //
bool HiggsinoSelector::passCuts(ConfigMgr*& configMgr)
{

  //This method is used to apply any cuts you wish before writing
  //the output trees

  double weight = 1; // pb-1
  if(!configMgr->obj->evt.isData()){

    weight = 140000; // pb-1
  }
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::GEN);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::EVT);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::LEP);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::JVT);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::PILEUP);

  bool passMetTrig = configMgr->treeMaker->getBoolVariable("trigMatch_metTrig")  || configMgr->treeMaker->getBoolVariable("IsMETTrigPassed");

  // Fill cutflow histograms
  configMgr->cutflow->bookCut("cutFlow","allEvents",weight );

  ////////////////////////////////////////
  //// begin ntuple preselection cuts ////
  ////////////////////////////////////////

  // Apply all recommended event cleaning cuts
  if( !configMgr->obj->passEventCleaning( configMgr->cutflow, "cutFlow", weight ) ) return false;

  // At least two baseline leptons
  if( configMgr->obj->baseLeptons.size() < 2 ) return false;
  configMgr->cutflow->bookCut("cutFlow","At least two baseline leptons", weight );

  // Skimming for ETmiss-triggered events
  if(passMetTrig) {
    // MET trigger
    configMgr->cutflow->bookCut("cutFlow","MET trigger passed", weight );

    // MET skimming
    if(configMgr->obj->met.Et < 150.0) return false;
    configMgr->cutflow->bookCut("cutFlow","MET > 150 GeV", weight );

    return true;
  }


  // Removing functionality below for now!
  return false;
  
  // Extra cuts for all trees except the nominal with a loose preselection
  // (used for e.g. producing the fake ntuple)
  if(configMgr->treeMaker->getTreeState() != "LOOSE_NOMINAL"){
    LeptonVariable* lep1 = configMgr->obj->baseLeptons[0];
    LeptonVariable* lep2 = configMgr->obj->baseLeptons[1];
    //if(configMgr->obj->cJets.size() >0) JetVariable* jet1 = configMgr->obj->cJets[0];
    Observables obs;

    // Tighter MET skimming
    if(configMgr->obj->met.Et < 120.0) return false;
    configMgr->cutflow->bookCut("cutFlow","Syst trees: MET > 120 GeV", weight );

    // At least one signal jet with pT > 100 GeV
    if( getNJets(configMgr->obj, 100.0) < 1 ) return false;
    configMgr->cutflow->bookCut("cutFlow","Syst trees: >= 1 signal jet with pT > 100 GeV", weight );

    // At least two signal leptons
    if( configMgr->obj->signalLeptons.size() < 2 ) return false;
    configMgr->cutflow->bookCut("cutFlow","Syst trees: At least two signal leptons", weight );

    // lep1 and lep2 Author != 16
    if(lep1->author == 16 || lep2->author == 16) return false;
    configMgr->cutflow->bookCut("cutFlow","Syst trees: lepAuthor != 16", weight );

    //leptons truth matched 
    //if(lep1->isEle() && (lep1->type == 2 || (lep1->type == 4 && lep1->egMotherType == 2))) return false;
    //else if(lep1->isMu() && (lep1->type == 6)) return false;
    //if(lep2->isEle() && (lep2->type == 2 || (lep2->type == 4 && lep2->egMotherType == 2))) return false;
    //else if(lep2->isMu() && (lep2->type == 6)) return false;

    // Require mll < 60 GeV for the leading dilepton pair (baseline leptons)
    if( getMll(configMgr->obj, true) > 60.0 ) return false;
    configMgr->cutflow->bookCut("cutFlow","Syst trees: Dilepton mass below 60 GeV", weight );
    // Require mll > 1 GeV and veto J/psi
    if( getMll(configMgr->obj, true) < 1.0 || (getMll(configMgr->obj, true) > 3.0 && getMll(configMgr->obj, true) < 3.2) ) return false;
    configMgr->cutflow->bookCut("cutFlow","Syst trees: Dilepton mass below 60 GeV", weight );

    // Leading lepton pT > 5 GeV 
    if (lep1->Pt() < 5.0) return false; 
    configMgr->cutflow->bookCut("cutFlow","Syst trees: Leading lepton pT below 5 GeV", weight );	

    // Rll > 0.05 
    if((*lep1).DeltaR(*lep2) < 0.05) return false;
    configMgr->cutflow->bookCut("cutFlow","Syst trees: Rll < 0.05", weight );

    // DPhiJ1Met > 2.0
    //if(fabs(TVector2::Phi_mpi_pi(configMgr->obj->met.phi - jet1->Phi())) < 2.0) return false; 
    //configMgr->cutflow->bookCut("cutFlow","Syst trees: DPhiJ1Met < 2.0", weight );

    // minDPhiAllJetsMet > 0.4
    float minDPhiAllJetsMet = 99999;
    for(unsigned int jetIndex = 0; jetIndex < configMgr->obj->cJets.size(); ++jetIndex){
      minDPhiAllJetsMet = std::min(minDPhiAllJetsMet, obs.getDPhiJNMet(configMgr->obj, jetIndex, 30.0));
    }
    if(minDPhiAllJetsMet < 0.4) return false;
    configMgr->cutflow->bookCut("cutFlow","Syst trees: minDPhiAllJetsMet > 0.4", weight ); 

  }

  return true;

}

// ------------------------------------------------------------------------------------------ //
void HiggsinoSelector::finalize(ConfigMgr*& configMgr)
{

  /*
   This method is called at the very end of the job. Can be used to merge cutflow histograms 
   for example. See CutFlowTool::mergeCutFlows(...)
  */

}
// ------------------------------------------------------------------------------------------ //
bool HiggsinoSelector::init_merge(MergeTool*& mergeTool){return true; }
// ------------------------------------------------------------------------------------------ //
bool HiggsinoSelector::execute_merge(MergeTool*& mergeTool){return true;}
// ------------------------------------------------------------------------------------------ //
