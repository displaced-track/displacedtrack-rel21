#include "SusySkimHiggsino/MonojetSelector.h"
#include "SusySkimMaker/Observables.h"
#include "PathResolver/PathResolver.h"
#include "TruthDecayContainer/ProcClassifier.h"

#include <iostream>
#include <fstream>
#include <algorithm>



// ------------------------------------------------------------------------------------------ //
MonojetSelector::MonojetSelector() :
  BaseUser("SusySkimHiggsino", "MonojetSelector"),
  m_cutFlowOnly(false)
{}

MonojetSelector::~MonojetSelector(){}

// ------------------------------------------------------------------------------------------ //
void MonojetSelector::setup(ConfigMgr*& configMgr)
{
  std::cout << "MonojetSelector::setup  INFO  cutFlowOnly: " << m_cutFlowOnly << std::endl;
  std::cout << "MonojetSelector::setup  INFO  truthOnly  : " << m_truthOnly << std::endl;
  std::cout << "MonojetSelector::setup  INFO  disableCuts: " << m_disableCuts << std::endl;

  ///////////////// Object Selection /////////////////////

  // Use object def from SUSYTools
  configMgr->obj->useSUSYToolsSignalDef(true);

  // Bad muon veto
  configMgr->obj->applyBadMuonVeto(true);

  // Jet cleaning for Pflow jets
  configMgr->obj->applyBadJetVeto(false);
  configMgr->obj->applyEvtLooseBadJetVeto(true);  // or applyEvtTightBadJetVeto

  // Muon cuts
  configMgr->obj->setBaselineMuonPt(3.0);
  configMgr->obj->setBaselineMuonEta(2.7);
  configMgr->obj->setSignalMuonPt(3.0);
  configMgr->obj->setSignalMuonEta(2.7);

  // Electrons
  configMgr->obj->setBaselineElectronEt(4.5);
  configMgr->obj->setBaselineElectronEta(2.47);
  configMgr->obj->setSignalElectronEt(4.5);
  configMgr->obj->setSignalElectronEta(2.47);

  // Taus
  configMgr->obj->setSignalTauPt(20.0);
  configMgr->obj->setSignalTauEta(2.5);

  // Jets
  configMgr->obj->setCJetPt(20.0);
  configMgr->obj->setCJetEta(2.80);
  configMgr->obj->setFJetPt(30.0);
  configMgr->obj->setFJetEtaMin(2.80);
  configMgr->obj->setFJetEtaMax(4.50);


  ///////////////// Triggers /////////////////////

  // Defined by run number <start,end>; -1 means ignore that bound
  // Trigger bit is filled by m_configMgr->doTriggerAna() in SusySkimDriver::xAODEvtLooper::execute

  // Single electron trigger
  configMgr->addTriggerAna("HLT_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose",276262,284484, "singleElectronTrig"); // 2015
  configMgr->addTriggerAna("HLT_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",297730,-1, "singleElectronTrig"); // 2016-

  // Single photon trigger
  configMgr->addTriggerAna("HLT_g120_loose_OR_g200_etcut",276262,284484, "singlePhotonTrig"); // 2015
  configMgr->addTriggerAna("HLT_g140_loose_OR_g300_etcut",297730,-1, "singlePhotonTrig"); // 2016-

  // Single muon trigger
  configMgr->addTriggerAna("HLT_mu20_iloose_L1MU15_OR_HLT_mu40",276262,284484,"singleMuonTrig"); // 2015
  configMgr->addTriggerAna("HLT_mu26_ivarmedium_OR_HLT_mu50",297730,-1,"singleMuonTrig");        // 2016-

  // Met trigger
  configMgr->addTriggerAna("HLT_xe70_mht",                276262, 284484,"metTrig"); // 2015
  configMgr->addTriggerAna("HLT_xe90_mht_L1XE50",         296939, 302872,"metTrig"); // 2016 A-D3
  configMgr->addTriggerAna("HLT_xe100_mht_L1XE50",        302919, 303892,"metTrig"); // 2016 D4-F1
  configMgr->addTriggerAna("HLT_xe110_mht_L1XE50",        303943, 320000,"metTrig"); // 2016 F2-
  configMgr->addTriggerAna("HLT_xe110_pufit_L1XE55",      320000, 348884,"metTrig"); // 2017
  configMgr->addTriggerAna("HLT_xe110_pufit_xe70_L1XE50", 348885, 350013,"metTrig"); // 2018 B-C5
  configMgr->addTriggerAna("HLT_xe110_pufit_xe65_L1XE50", 350067,     -1,"metTrig"); // 2018 C6-

  // MET trigger decision from SUSYTools
  configMgr->treeMaker->addBoolVariable("IsMETTrigPassed",false);


  /////////////////// Output trees /////////////////////////

  // Global vars
  configMgr->treeMaker->addIntVariable   ("year",0);
  configMgr->treeMaker->addFloatVariable ("mu",0.0);
  configMgr->treeMaker->addFloatVariable ("actual_mu",0.0);
  configMgr->treeMaker->addIntVariable   ("nVtx",0);
  configMgr->treeMaker->addTV3Variable   ("privtx",0);
  // configMgr->treeMaker->addVecTV3Variable("pileupvtx");
  configMgr->treeMaker->addTV3Variable   ("beampos",0);
  configMgr->treeMaker->addBoolVariable  ("passBadTileJetVeto",false);
  configMgr->treeMaker->addBoolVariable  ("passLooseBadJetVeto",false);
  configMgr->treeMaker->addBoolVariable  ("passTightBadJetVeto",false);

  // Truth vars
  com.addTruthVariables(configMgr);

  //Jet Cleaning Variables
  configMgr->treeMaker->addBoolVariable("passEmulTightJetCleaning1Jet", 0);
  configMgr->treeMaker->addBoolVariable("passEmulTightJetCleaning2Jet", 0);

  // Lepton vars
  configMgr->treeMaker->addIntVariable("nLep_base",0);
  configMgr->treeMaker->addIntVariable("nMu_base",0);
  configMgr->treeMaker->addIntVariable("nEle_base",0);
  configMgr->treeMaker->addIntVariable("nLep_signal",0);
  configMgr->treeMaker->addIntVariable("nMu_signal",0);
  configMgr->treeMaker->addIntVariable("nEle_signal",0);
  com.addLeptonVariables(configMgr,"lep1");  // Define lep1Pt, lep1Eta etc.
  com.addLeptonVariables(configMgr,"lep2");

  // V0 vars //JEFF
  configMgr->treeMaker->addIntVariable     ("nV0s",0);
  configMgr->treeMaker->addVecFloatVariable("v0_Kshort_mass");
  configMgr->treeMaker->addVecFloatVariable("v0_Lambda_mass");
  configMgr->treeMaker->addVecFloatVariable("v0_Lambdabar_mass");
  configMgr->treeMaker->addVecFloatVariable("v0_Kshort_massError");
  configMgr->treeMaker->addVecFloatVariable("v0_Lambda_massError");
  configMgr->treeMaker->addVecFloatVariable("v0_Lambdabar_massError");
  configMgr->treeMaker->addVecFloatVariable("v0_Rxy");
  configMgr->treeMaker->addVecFloatVariable("v0_RxyError");
  configMgr->treeMaker->addVecFloatVariable("v0_pT");
  configMgr->treeMaker->addVecFloatVariable("v0_pTError");
  configMgr->treeMaker->addVecFloatVariable("v0_px");
  configMgr->treeMaker->addVecFloatVariable("v0_py");
  configMgr->treeMaker->addVecFloatVariable("v0_pz");
  //configMgr->treeMaker->addVecFloatVariable("v0_track1Pt");
  //configMgr->treeMaker->addVecFloatVariable("v0_track2Pt");
  //configMgr->treeMaker->addVecFloatVariable("v0_track1Eta");
  //configMgr->treeMaker->addVecFloatVariable("v0_track2Eta");
  //configMgr->treeMaker->addVecFloatVariable("v0_track1Phi");
  //configMgr->treeMaker->addVecFloatVariable("v0_track2Phi");
  //configMgr->treeMaker->addVecIntVariable  ("v0_track1TightPrimary");
  //configMgr->treeMaker->addVecIntVariable  ("v0_track2TightPrimary");

  // VSI vars // SICONG
  configMgr->treeMaker->addIntVariable     ("nVSIs",0);
  configMgr->treeMaker->addVecFloatVariable("vsi_px");
  configMgr->treeMaker->addVecFloatVariable("vsi_py");
  configMgr->treeMaker->addVecFloatVariable("vsi_pz");
  //configMgr->treeMaker->addVecFloatVariable("vsi_track1Pt");
  //configMgr->treeMaker->addVecFloatVariable("vsi_track2Pt");
  //configMgr->treeMaker->addVecFloatVariable("vsi_track1Eta");
  //configMgr->treeMaker->addVecFloatVariable("vsi_track2Eta");
  //configMgr->treeMaker->addVecFloatVariable("vsi_track1Phi");
  //configMgr->treeMaker->addVecFloatVariable("vsi_track2Phi");
  configMgr->treeMaker->addVecFloatVariable("vsi_mass");
  configMgr->treeMaker->addVecFloatVariable("vsi_vtx_mass");
  //configMgr->treeMaker->addVecIntVariable  ("vsi_track1TightPrimary");
  //configMgr->treeMaker->addVecIntVariable  ("vsi_track2TightPrimary");


  //Photon vars
  configMgr->treeMaker->addIntVariable("nPhoton_base",0);
  configMgr->treeMaker->addIntVariable("nPhoton_signal",0);

//  // Truth vars
//  configMgr->treeMaker->addVecFloatVariable("TruthEta");
//  configMgr->treeMaker->addVecFloatVariable("TruthPhi");
//  configMgr->treeMaker->addVecFloatVariable("TruthPt");
//  configMgr->treeMaker->addVecTV3Variable  ("TruthdecayVtx");
//  configMgr->treeMaker->addVecTV3Variable  ("TruthprodVtx");
//  configMgr->treeMaker->addVecFloatVariable("TruthdecayVtxPerp");
//  configMgr->treeMaker->addVecFloatVariable("TruthprodVtxPerp");
//  configMgr->treeMaker->addVecIntVariable  ("TruthStatus");
//  configMgr->treeMaker->addVecIntVariable  ("TruthPdgId");
//  configMgr->treeMaker->addVecIntVariable  ("TruthParentPdgId");
//  configMgr->treeMaker->addVecIntVariable  ("TruthParentStatus");
//  configMgr->treeMaker->addVecIntVariable  ("TruthBarcode");
//  configMgr->treeMaker->addVecIntVariable  ("TruthParentBarcode");
//  configMgr->treeMaker->addVecBoolVariable ("TruthIsAntiKt4Jet");
//  configMgr->treeMaker->addVecBoolVariable ("TruthIsBJet");

  // Track vars
  //    Track Number
  configMgr->treeMaker->addIntVariable     ("nTrk_base",0);
  configMgr->treeMaker->addIntVariable     ("nTrk_signal",0);
  //    Track Basics
  configMgr->treeMaker->addVecBoolVariable ("trkisTight");
  configMgr->treeMaker->addVecBoolVariable ("trkisLoose");
  configMgr->treeMaker->addVecFloatVariable("trkPt");
  configMgr->treeMaker->addVecFloatVariable("trkEta");
  configMgr->treeMaker->addVecFloatVariable("trkPhi");
  configMgr->treeMaker->addVecIntVariable  ("trkQ");
  configMgr->treeMaker->addVecFloatVariable("trkZ0");
  configMgr->treeMaker->addVecFloatVariable("trkZ0Origin"); //Sicong TODO: Note that we need consistent definition
  configMgr->treeMaker->addVecFloatVariable("trkZ0Err");
  configMgr->treeMaker->addVecFloatVariable("trkZ0SinTheta");
  configMgr->treeMaker->addVecFloatVariable("trkD0");
  configMgr->treeMaker->addVecFloatVariable("trkD0Err");
  configMgr->treeMaker->addVecFloatVariable("trkD0Sig");
  configMgr->treeMaker->addVecFloatVariable("trkchiSquared");
  configMgr->treeMaker->addVecFloatVariable("trknumberDoF");
  configMgr->treeMaker->addVecFloatVariable("trkTRTdEdx");
  configMgr->treeMaker->addVecFloatVariable("trkpixeldEdx");

  //    Track: Vertex Related
  configMgr->treeMaker->addVecFloatVariable("trkProdVtx_x");
  configMgr->treeMaker->addVecFloatVariable("trkProdVtx_y");
  configMgr->treeMaker->addVecFloatVariable("trkProdVtx_z");
  configMgr->treeMaker->addVecIntVariable  ("trkProdVtxType");
  configMgr->treeMaker->addVecIntVariable  ("trkIsHardVtx");
  configMgr->treeMaker->addVecFloatVariable("trkDecayVtx_x");
  configMgr->treeMaker->addVecFloatVariable("trkDecayVtx_y");
  configMgr->treeMaker->addVecFloatVariable("trkDecayVtx_z");
  configMgr->treeMaker->addVecIntVariable  ("trkDecayVtxType");
  configMgr->treeMaker->addVecBoolVariable ("trkisSecTrk");

  //    Track: Link to Muon/Electron/Photon
  configMgr->treeMaker->addVecBoolVariable ("trkisBaseMuon");
  configMgr->treeMaker->addVecBoolVariable ("trkisBaseElectron");
  configMgr->treeMaker->addVecBoolVariable ("trkisBasePhoton");
  configMgr->treeMaker->addVecBoolVariable ("trkisSignalMuon");
  configMgr->treeMaker->addVecBoolVariable ("trkisSignalElectron");
  configMgr->treeMaker->addVecBoolVariable ("trkisSignalPhoton");

  //    Track: Truth Info
  configMgr->treeMaker->addVecIntVariable  ("trkLabel");
  configMgr->treeMaker->addVecIntVariable  ("trkDef");
  configMgr->treeMaker->addVecIntVariable  ("trkpdgId");
  configMgr->treeMaker->addVecIntVariable  ("trknpar");
  configMgr->treeMaker->addVecIntVariable  ("trkparbarcode");
  configMgr->treeMaker->addVecIntVariable  ("trkparpdgId");
  configMgr->treeMaker->addVecIntVariable  ("trkparnchild");
  configMgr->treeMaker->addVecIntVariable  ("trkparchildpdgId");
  configMgr->treeMaker->addVecFloatVariable("trkparchildpt");
  configMgr->treeMaker->addVecIntVariable  ("trkparchildbarcode");
  configMgr->treeMaker->addVecIntVariable  ("trkBarcode");
  configMgr->treeMaker->addVecIntVariable  ("trkStatus");
  configMgr->treeMaker->addVecIntVariable  ("trkType");
  configMgr->treeMaker->addVecIntVariable  ("trkOrigin");
  configMgr->treeMaker->addVecBoolVariable ("trkIsTruthBSM");


  //    Track: Other Fit Variables & Vertex
  configMgr->treeMaker->addVecFloatVariable("trkFitQuality");
  configMgr->treeMaker->addVecFloatVariable("trkVtxQuality");
  configMgr->treeMaker->addVecFloatVariable("trkD0SV");
  configMgr->treeMaker->addVecFloatVariable("trkD0ErrSV");
  configMgr->treeMaker->addVecFloatVariable("trkZ0SV");
  configMgr->treeMaker->addVecFloatVariable("trkZ0ErrSV");
  configMgr->treeMaker->addVecFloatVariable("trkTruthD0SV");
  configMgr->treeMaker->addVecFloatVariable("trkTruthZ0SV");

  //    Track: Isolation
  configMgr->treeMaker->addVecFloatVariable("trkPtcone20");
  configMgr->treeMaker->addVecFloatVariable("trkPtcone30");
  configMgr->treeMaker->addVecFloatVariable("trkPtcone40");
  configMgr->treeMaker->addVecFloatVariable("trkNonBaseAssocPtcone20");
  configMgr->treeMaker->addVecFloatVariable("trkNonBaseAssocPtcone30");
  configMgr->treeMaker->addVecFloatVariable("trkNonBaseAssocPtcone40");
  configMgr->treeMaker->addVecFloatVariable("trkNonBasePhotonConvPtcone20");
  configMgr->treeMaker->addVecFloatVariable("trkNonBasePhotonConvPtcone30");
  configMgr->treeMaker->addVecFloatVariable("trkNonBasePhotonConvPtcone40");
  configMgr->treeMaker->addVecFloatVariable("trkNonSignalAssocPtcone20");
  configMgr->treeMaker->addVecFloatVariable("trkNonSignalAssocPtcone30");
  configMgr->treeMaker->addVecFloatVariable("trkNonSignalAssocPtcone40");
  configMgr->treeMaker->addVecFloatVariable("trkNonSignalPhotonConvPtcone20");
  configMgr->treeMaker->addVecFloatVariable("trkNonSignalPhotonConvPtcone30");
  configMgr->treeMaker->addVecFloatVariable("trkNonSignalPhotonConvPtcone40");

  configMgr->treeMaker->addVecFloatVariable("trkBaseLepPhotonMinDR");
  configMgr->treeMaker->addVecFloatVariable("trkSignalLepPhotonMinDR");
  configMgr->treeMaker->addVecFloatVariable("trkTopoEtcone20");
  configMgr->treeMaker->addVecFloatVariable("trkTopoEtcone30");
  configMgr->treeMaker->addVecFloatVariable("trkTopoEtcone40");
  configMgr->treeMaker->addVecFloatVariable("trkTopoEtclus20");
  configMgr->treeMaker->addVecFloatVariable("trkTopoEtclus30");
  configMgr->treeMaker->addVecFloatVariable("trkTopoEtclus40");
  configMgr->treeMaker->addVecFloatVariable("trkBaseLepPhotonSubtractedTopoEtclus20");
  configMgr->treeMaker->addVecFloatVariable("trkBaseLepPhotonSubtractedTopoEtclus30");
  configMgr->treeMaker->addVecFloatVariable("trkBaseLepPhotonSubtractedTopoEtclus40");
  configMgr->treeMaker->addVecFloatVariable("trkBaseLepPhotonSubtractedCoreConeTopoEtclus20");
  configMgr->treeMaker->addVecFloatVariable("trkBaseLepPhotonSubtractedCoreConeTopoEtclus30");
  configMgr->treeMaker->addVecFloatVariable("trkBaseLepPhotonSubtractedCoreConeTopoEtclus40");
  configMgr->treeMaker->addVecFloatVariable("trkSignalLepPhotonSubtractedTopoEtclus20");
  configMgr->treeMaker->addVecFloatVariable("trkSignalLepPhotonSubtractedTopoEtclus30");
  configMgr->treeMaker->addVecFloatVariable("trkSignalLepPhotonSubtractedTopoEtclus40");
  configMgr->treeMaker->addVecFloatVariable("trkSignalLepPhotonSubtractedCoreConeTopoEtclus20");
  configMgr->treeMaker->addVecFloatVariable("trkSignalLepPhotonSubtractedCoreConeTopoEtclus30");
  configMgr->treeMaker->addVecFloatVariable("trkSignalLepPhotonSubtractedCoreConeTopoEtclus40");

  // Sicong: V0/VSI relevant variables:
  configMgr->treeMaker->addVecFloatVariable("trkDPhiMet");
  configMgr->treeMaker->addVecFloatVariable("trkIso");
  configMgr->treeMaker->addVecFloatVariable("trkM");
  configMgr->treeMaker->addVecBoolVariable ("trkTightPrimary");
  configMgr->treeMaker->addVecFloatVariable("trkIsoTightPrimary");
  configMgr->treeMaker->addVecBoolVariable ("trkMatchedToV0");
  configMgr->treeMaker->addVecBoolVariable ("trkMatchedToVsi");
  configMgr->treeMaker->addVecFloatVariable("trkMatchedVsiMass");

  //    Track: Hit Relevant Information
  configMgr->treeMaker->addVecIntVariable ("trknExpIBLHits");
  configMgr->treeMaker->addVecIntVariable ("trknExpBLayerHits");
  configMgr->treeMaker->addVecIntVariable ("trknIBLHits");
  configMgr->treeMaker->addVecIntVariable ("trknBLayerHits");
  configMgr->treeMaker->addVecIntVariable ("trknPixHits");
  configMgr->treeMaker->addVecIntVariable ("trknPixLayers");
  configMgr->treeMaker->addVecIntVariable ("trknPixDeadSensors");
  configMgr->treeMaker->addVecIntVariable ("trknPixHoles");
  configMgr->treeMaker->addVecIntVariable ("trknPixSharedHits");
  configMgr->treeMaker->addVecIntVariable ("trknPixOutliers");
  configMgr->treeMaker->addVecIntVariable ("trknSCTHits");
  configMgr->treeMaker->addVecIntVariable ("trknSCTDeadSensors");
  configMgr->treeMaker->addVecIntVariable ("trknSCTHoles");
  configMgr->treeMaker->addVecIntVariable ("trknSCTSharedHits");
  configMgr->treeMaker->addVecIntVariable ("trknSCTOutliers");
  configMgr->treeMaker->addVecIntVariable ("trknTRTHits");
  configMgr->treeMaker->addVecIntVariable ("trknPixSpoiltHits");
  configMgr->treeMaker->addVecIntVariable ("trknGangedFlaggedFakes");

  configMgr->treeMaker->addVecVecBoolVariable("trk_SR_Sd0");
  configMgr->treeMaker->addVecVecBoolVariable("trk_VR_Sd0");

  // Tau vars
  configMgr->treeMaker->addIntVariable("nTau_base",0);
  configMgr->treeMaker->addIntVariable("nTau_signal",0);
  com.addTauVariables(configMgr,"tau1");
  com.addTauVariables(configMgr,"tau2");

  // Small-R jets
  configMgr->treeMaker->addIntVariable("nJet20", 0);
  configMgr->treeMaker->addIntVariable("nJet30", 0);
  configMgr->treeMaker->addVecFloatVariable("jetPt");
  configMgr->treeMaker->addVecFloatVariable("jetEta");
  configMgr->treeMaker->addVecFloatVariable("jetPhi");
  configMgr->treeMaker->addVecFloatVariable("jetM");
  configMgr->treeMaker->addVecBoolVariable("jetIsB");
  configMgr->treeMaker->addVecIntVariable("jetTruthLabel");
  configMgr->treeMaker->addVecIntVariable("jetNtrk");
  configMgr->treeMaker->addVecFloatVariable("jetTiming");
  configMgr->treeMaker->addVecFloatVariable("jetJVT");

  // B-jets
  configMgr->treeMaker->addIntVariable("nBJet20",0);
  configMgr->treeMaker->addIntVariable("nBJet30",0);

  // MET, dPhi(jet,met)
  configMgr->treeMaker->addFloatVariable("met_Et",0.0);
  configMgr->treeMaker->addFloatVariable("met_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("metTruth_Et",0.0);
  configMgr->treeMaker->addFloatVariable("metTruth_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("met_track_Et",0.0);
  configMgr->treeMaker->addFloatVariable("met_track_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("met_Signif",0.0);
  //configMgr->treeMaker->addFloatVariable("metTST_Et",0.0);
  //configMgr->treeMaker->addFloatVariable("metTST_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("metMuon_Et",0.0);
  configMgr->treeMaker->addFloatVariable("metMuon_Phi",0.0);

  configMgr->treeMaker->addFloatVariable("met_muons_invis_Et",   0.0);
  configMgr->treeMaker->addFloatVariable("met_muons_invis_Phi",  0.0);
  configMgr->treeMaker->addFloatVariable("met_muons_invis_Signif",  0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_met_muons_invis_allJets20", 0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_met_muons_invis_allJets30", 0.0);
  configMgr->treeMaker->addFloatVariable("met_electrons_invis_Et",   0.0);
  configMgr->treeMaker->addFloatVariable("met_electrons_invis_Phi",  0.0);
  configMgr->treeMaker->addFloatVariable("met_electrons_invis_Signif",  0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_met_electrons_invis_allJets20", 0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_met_electrons_invis_allJets30", 0.0);
  configMgr->treeMaker->addFloatVariable("met_photons_invis_Et",   0.0);
  configMgr->treeMaker->addFloatVariable("met_photons_invis_Phi",  0.0);
  configMgr->treeMaker->addFloatVariable("met_photons_invis_Signif",  0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_met_photons_invis_allJets20", 0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_met_photons_invis_allJets30", 0.0);
  configMgr->treeMaker->addFloatVariable("met_leptons_invis_Et",   0.0);
  configMgr->treeMaker->addFloatVariable("met_leptons_invis_Phi",  0.0);
  configMgr->treeMaker->addFloatVariable("met_leptons_invis_Signif",  0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_met_leptons_invis_allJets20", 0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_met_leptons_invis_allJets30", 0.0);
  configMgr->treeMaker->addFloatVariable("met_Et_LepInvis",   -1.0);
  configMgr->treeMaker->addFloatVariable("met_Phi_LepInvis",   -1.0);
  configMgr->treeMaker->addFloatVariable("met_Signif_LepInvis",   -1.0);


  // ++++++++++ Analysis variables +++++++++++ //
  configMgr->treeMaker->addFloatVariable("minDPhi_met_allJets20", 0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_met_allJets30", 0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_met_4j30", 0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_trackMet_allJets20", 0.0);
  configMgr->treeMaker->addFloatVariable("minDPhi_trackMet_allJets30", 0.0);
  configMgr->treeMaker->addFloatVariable("HtJet30",0.);
  configMgr->treeMaker->addFloatVariable("meffInc30",0.);
  configMgr->treeMaker->addFloatVariable("mt_tau", 0.);
  configMgr->treeMaker->addFloatVariable("mt_taujet", 0.);


  // for 1L
  configMgr->treeMaker->addFloatVariable("L1_mt", 0.0);
  configMgr->treeMaker->addFloatVariable("L1_ptlv", 0.0);
  configMgr->treeMaker->addFloatVariable("L1_philv", 0.0);

  // for 2L
  configMgr->treeMaker->addBoolVariable("L2_SF", false);
  configMgr->treeMaker->addBoolVariable("L2_ELE", false);
  configMgr->treeMaker->addBoolVariable("L2_MU", false);
  configMgr->treeMaker->addBoolVariable("L2_OS", false);
  configMgr->treeMaker->addTLVariable  ("L2_lep1");
  configMgr->treeMaker->addTLVariable  ("L2_lep2");
  configMgr->treeMaker->addTLVariable  ("L2_met");
  configMgr->treeMaker->addFloatVariable("L2_mll", 0.0);
  configMgr->treeMaker->addFloatVariable("L2_ptll", 0.0);
  configMgr->treeMaker->addFloatVariable("L2_phill", 0.0);

  // for dilepton
  configMgr->treeMaker->addFloatVariable("Dilepton_mll",0.0);
  configMgr->treeMaker->addFloatVariable("Dilepton_met_Et",0.0);
  configMgr->treeMaker->addFloatVariable("Dilepton_met_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("Dilepton_minDPhi_met_allJets30", 0.0);

  // for 1Gamma
  configMgr->treeMaker->addTLVariable("Photon");
  configMgr->treeMaker->addFloatVariable("Photon_met_Et",0.0);
  configMgr->treeMaker->addFloatVariable("Photon_met_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("Photon_minDPhi_met_allJets30", 0.0);
  configMgr->treeMaker->addBoolVariable("Photon_IsoFCTightCaloOnly", false);
  configMgr->treeMaker->addBoolVariable("Photon_IsoFixedCutLoose", false);
  configMgr->treeMaker->addBoolVariable("Photon_IsoFixedCutTight", false);
  configMgr->treeMaker->addFloatVariable("Photon_Topoetcone20", -999.0);
  configMgr->treeMaker->addFloatVariable("Photon_Topoetcone30", -999.0);
  configMgr->treeMaker->addFloatVariable("Photon_Topoetcone40", -999.0);
  configMgr->treeMaker->addFloatVariable("Photon_SUSY20_Topoetcone20", -999.0);
  configMgr->treeMaker->addFloatVariable("Photon_SUSY20_Topoetcone30", -999.0);
  configMgr->treeMaker->addFloatVariable("Photon_SUSY20_Topoetcone40", -999.0);
  configMgr->treeMaker->addFloatVariable("Photon_SUSY20_Topoetclus20", -999.0);
  configMgr->treeMaker->addFloatVariable("Photon_SUSY20_Topoetclus30", -999.0);
  configMgr->treeMaker->addFloatVariable("Photon_SUSY20_Topoetclus40", -999.0);
  configMgr->treeMaker->addBoolVariable("Photon_passOR", false);
  configMgr->treeMaker->addIntVariable("Photon_conversionType", 0);
  configMgr->treeMaker->addFloatVariable("Photon_conversionRadius", 0.);

  // Flags for semi-data driven BG estimation
  configMgr->treeMaker->addStringVariable("DecayProcess", "None");
  configMgr->treeMaker->addVecVecBoolVariable("SR_MET");
  configMgr->treeMaker->addVecVecBoolVariable("VR_MET");

  // +++++++++++ Weights +++++++++++++ //
  configMgr->treeMaker->addDoubleVariable("pileupWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("leptonWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("eventWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("bTagWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("trigWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("jvtWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("photonWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightUp",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightDown",1.0);

  //configMgr->treeMaker->addFloatVariable("globalDiLepTrigSF",1.0);
  //configMgr->treeMaker->addFloatVariable("globalMultiLepTrigSF",1.0);

  // PDF variables
  configMgr->treeMaker->addFloatVariable("x1", -1.0);
  configMgr->treeMaker->addFloatVariable("x2", -1.0);
  configMgr->treeMaker->addFloatVariable("pdf1", -1.0);
  configMgr->treeMaker->addFloatVariable("pdf2", -1.0);
  configMgr->treeMaker->addFloatVariable("scalePDF", -1.0);
  configMgr->treeMaker->addIntVariable("id1", 0);
  configMgr->treeMaker->addIntVariable("id2", 0);

  ///////////////////// Cut flow definition //////////////////////

  // Make a cutflow stream
  configMgr->cutflow->defineCutFlow("cutFlow", configMgr->treeMaker->getFile("tree"));

  // Toggle this on if you want to run the cutFlow mode
  // where dedicated cut flows are performed instead of ntuple making
  m_cutFlowOnly = false;

  if(m_cutFlowOnly){

    // Non-sequential cut flows
    configMgr->cutflow->defineCutFlow("cutFlow_genWeight",        configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_eventWeight",      configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_pileupWeight",     configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_leptonWeight",     configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_bTagWeight",       configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_jvtWeight",        configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_triggerWeight",    configMgr->treeMaker->getFile("tree"));

    // Sequential cut flows
    configMgr->cutflow->defineCutFlow("cutFlow_sq",               configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_weight",        configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_genWeight",     configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_eventWeight",   configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_pileupWeight",  configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_leptonWeight",  configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_bTagWeight",    configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_jvtWeight",     configMgr->treeMaker->getFile("tree"));
    configMgr->cutflow->defineCutFlow("cutFlow_sq_triggerWeight", configMgr->treeMaker->getFile("tree"));
  }

  ////////////////// Tool initialization ///////////////////

  // Read in TH1s that contain SFs for ETmiss-Triggers
  com.load_sfs();

  // Read in TH1 that is contains the fit of data and MGP8 Zjets in pT(mu, mu)
  com.load_ISR_fit();

  // Object class contains the definitions of all physics objects, eg muons, electrons, jets
  // See xAODNtupleMaker::Objects for available methods; configMgr->obj
  com.PrepareBDTConfig();
  com.PreparePLTWP();


}
// ------------------------------------------------------------------------------------------ //
bool MonojetSelector::doAnalysis(ConfigMgr*& configMgr)
{
  /*
    This is the main method, which is called for each event
  */

  // Perform a dedicated cut flow and end the event if cutFlowOnly mode.
  if(m_cutFlowOnly){
    doCutFlow(configMgr);
    return false;
  }

  // Skims events by imposing any cuts you define in this method below
  if( !passCuts(configMgr) ) return false;

  //
  // -------- Fill the output tree variables ---------- //
  //
  bool useBaseline = true; // Use baseline leptons for compute the analysis variables

  //
  // Global Vars
  //
  const int runNumber =
    configMgr->obj->evt.isMC ?
    configMgr->obj->evt.randomRunNumber :
    configMgr->obj->evt.runNumber       ;

  configMgr->treeMaker->setIntVariable("year", com.getYear(runNumber) );
  configMgr->treeMaker->setFloatVariable("mu",configMgr->obj->evt.mu);
  configMgr->treeMaker->setFloatVariable("actual_mu",configMgr->obj->evt.actual_mu);
  configMgr->treeMaker->setIntVariable("nVtx",configMgr->obj->evt.nVtx);
  configMgr->treeMaker->setTV3Variable("privtx",configMgr->obj->evt.privtx);
  // for(auto pileupvtx : configMgr->obj->evt.pileupvtx) configMgr->treeMaker->appendVecTV3Variable("pileupvtx",pileupvtx);
  configMgr->treeMaker->setTV3Variable("beampos",configMgr->obj->evt.beampos);

  bool passBadTileJetVeto=true;
  for(auto jet : configMgr->obj->baselineJets){
    if(jet->badTile) {  passBadTileJetVeto=false; break; }
  }

  // Jet Cleaning variables
  configMgr->treeMaker->setBoolVariable("passBadTileJetVeto", passBadTileJetVeto);
  configMgr->treeMaker->setBoolVariable("passLooseBadJetVeto", configMgr->obj->evt.passEvtCleaning( EventVariable::EventCleaning::EvtLooseBadJet) );
  configMgr->treeMaker->setBoolVariable("passTightBadJetVeto", configMgr->obj->evt.passEvtCleaning( EventVariable::EventCleaning::EvtTightBadJet) );

  //Jet Cleaning Variables
  configMgr->treeMaker->setBoolVariable("passEmulTightJetCleaning1Jet", configMgr->obj->evt.passEvtCleaning(EventVariable::EventCleaning::EmulEvtTightBad1Jet));
  configMgr->treeMaker->setBoolVariable("passEmulTightJetCleaning2Jet", configMgr->obj->evt.passEvtCleaning(EventVariable::EventCleaning::EmulEvtTightBad2Jet));

  configMgr->treeMaker->setBoolVariable("IsMETTrigPassed", configMgr->susyObj.IsMETTrigPassed(configMgr->obj->evt.randomRunNumber, true));



  //
  // Truth vars
  //
  com.setTruthVariables(configMgr);

  //
  // Leptons
  //
  int  nLep_base = configMgr->obj->baseLeptons.size();
  LeptonVariable* lep1 = nLep_base>=1 ? configMgr->obj->baseLeptons[0] : 0;
  LeptonVariable* lep2 = nLep_base>=2 ? configMgr->obj->baseLeptons[1] : 0;

  configMgr->treeMaker->setIntVariable("nLep_base", nLep_base);
  configMgr->treeMaker->setIntVariable("nMu_base",configMgr->obj->baseMuons.size());
  configMgr->treeMaker->setIntVariable("nEle_base",configMgr->obj->baseElectrons.size());
  configMgr->treeMaker->setIntVariable("nLep_signal",configMgr->obj->signalLeptons.size());
  configMgr->treeMaker->setIntVariable("nMu_signal",configMgr->obj->signalMuons.size());
  configMgr->treeMaker->setIntVariable("nEle_signal",configMgr->obj->signalElectrons.size());

  if(lep1) com.setLeptonVariables(configMgr,"lep1", lep1);
  if(lep2) com.setLeptonVariables(configMgr,"lep2", lep2);

  //
  // V0s
  //
  configMgr->treeMaker->setIntVariable("nV0s", configMgr->obj->v0s.size());
  for( auto& v0 : configMgr->obj->v0s ){
    configMgr->treeMaker->appendVecFloatVariable("v0_Kshort_mass", v0->Kshort_mass);
    configMgr->treeMaker->appendVecFloatVariable("v0_Lambda_mass", v0->Lambda_mass);
    configMgr->treeMaker->appendVecFloatVariable("v0_Lambdabar_mass", v0->Lambdabar_mass);
    configMgr->treeMaker->appendVecFloatVariable("v0_Kshort_massError", v0->Kshort_massError);
    configMgr->treeMaker->appendVecFloatVariable("v0_Lambda_massError", v0->Lambda_massError);
    configMgr->treeMaker->appendVecFloatVariable("v0_Lambdabar_massError", v0->Lambdabar_massError);
    configMgr->treeMaker->appendVecFloatVariable("v0_Rxy", v0->Rxy);
    configMgr->treeMaker->appendVecFloatVariable("v0_RxyError", v0->RxyError);
    configMgr->treeMaker->appendVecFloatVariable("v0_pT", v0->pT);
    configMgr->treeMaker->appendVecFloatVariable("v0_pTError", v0->pTError);
    configMgr->treeMaker->appendVecFloatVariable("v0_px", v0->px);
    configMgr->treeMaker->appendVecFloatVariable("v0_py", v0->py);
    configMgr->treeMaker->appendVecFloatVariable("v0_pz", v0->pz);
    //configMgr->treeMaker->appendVecFloatVariable("v0_track1Pt", v0->track1Pt);
    //configMgr->treeMaker->appendVecFloatVariable("v0_track2Pt", v0->track2Pt);
    //configMgr->treeMaker->appendVecFloatVariable("v0_track1Eta", v0->track1Eta);
    //configMgr->treeMaker->appendVecFloatVariable("v0_track2Eta", v0->track2Eta);
    //configMgr->treeMaker->appendVecFloatVariable("v0_track1Phi", v0->track1Phi);
    //configMgr->treeMaker->appendVecFloatVariable("v0_track2Phi", v0->track2Phi);
    //configMgr->treeMaker->appendVecIntVariable("v0_track1TightPrimary", v0->track1TightPrimary);
    //configMgr->treeMaker->appendVecIntVariable("v0_track2TightPrimary", v0->track2TightPrimary);
  }

  //
  // VSIs
  //
  configMgr->treeMaker->setIntVariable("nVSIs", configMgr->obj->vsiVec.size());
  for( auto& vsi : configMgr->obj->vsiVec ){
    //std::cout << "VSI Track Indices: mass: " << vsi->mass << "vtx_mass"<< vsi->vtx_mass<<" Track indices:";
    //for (auto index: vsi->trackIndices) std::cout << index << ' ';
    //std::cout << std::endl;
    configMgr->treeMaker->appendVecFloatVariable("vsi_px", vsi->px);
    configMgr->treeMaker->appendVecFloatVariable("vsi_py", vsi->py);
    configMgr->treeMaker->appendVecFloatVariable("vsi_pz", vsi->pz);

    configMgr->treeMaker->appendVecFloatVariable("vsi_mass", vsi->mass);
    configMgr->treeMaker->appendVecFloatVariable("vsi_vtx_mass", vsi->vtx_mass);

    //configMgr->treeMaker->appendVecFloatVariable("vsi_track1Pt", vsi->track1Pt);
    //configMgr->treeMaker->appendVecFloatVariable("vsi_track2Pt", vsi->track2Pt);
    //configMgr->treeMaker->appendVecFloatVariable("vsi_track1Eta", vsi->track1Eta);
    //configMgr->treeMaker->appendVecFloatVariable("vsi_track2Eta", vsi->track2Eta);
    //configMgr->treeMaker->appendVecFloatVariable("vsi_track1Phi", vsi->track1Phi);
    //configMgr->treeMaker->appendVecFloatVariable("vsi_track2Phi", vsi->track2Phi);
    //configMgr->treeMaker->appendVecIntVariable("vsi_track1TightPrimary", vsi->track1TightPrimary);
    //configMgr->treeMaker->appendVecIntVariable("vsi_track2TightPrimary", vsi->track2TightPrimary);
  }

  //
  // Photons
  //
  configMgr->treeMaker->setIntVariable("nPhoton_base",configMgr->obj->basePhotons.size());
  configMgr->treeMaker->setIntVariable("nPhoton_signal",configMgr->obj->signalPhotons.size());

//  // Truth Particles
//  for( auto& truth : configMgr->obj->truth){
//    configMgr->treeMaker->appendVecFloatVariable("TruthEta", truth->Eta());
//    configMgr->treeMaker->appendVecFloatVariable("TruthPhi", truth->Phi());
//    configMgr->treeMaker->appendVecFloatVariable("TruthPt", truth->Pt());
//    configMgr->treeMaker->appendVecTV3Variable("TruthdecayVtx", truth->decayVtx);
//    configMgr->treeMaker->appendVecTV3Variable("TruthprodVtx", truth->prodVtx);
//    configMgr->treeMaker->appendVecFloatVariable("TruthdecayVtxPerp", truth->decayVtxPerp);
//    configMgr->treeMaker->appendVecFloatVariable("TruthprodVtxPerp", truth->prodVtxPerp);
//    configMgr->treeMaker->appendVecIntVariable("TruthPdgId", truth->pdgId);
//    configMgr->treeMaker->appendVecIntVariable("TruthStatus", truth->status);
//    configMgr->treeMaker->appendVecIntVariable("TruthParentPdgId", truth->parentPdgId);
//    configMgr->treeMaker->appendVecIntVariable("TruthParentStatus", truth->parentStatus);
//    configMgr->treeMaker->appendVecIntVariable("TruthBarcode", truth->barcode);
//    configMgr->treeMaker->appendVecIntVariable("TruthParentBarcode", truth->parentBarcode);
//    configMgr->treeMaker->appendVecBoolVariable("TruthIsAntiKt4Jet", truth->isAntiKt4Jet);
//    configMgr->treeMaker->appendVecBoolVariable("TruthIsBJet", truth->bjet);
//  }

  std::vector<int> truthBSMBarcodes;
  for( auto& truth : configMgr->obj->truth){
      if (truth->isTruthBSM) truthBSMBarcodes.push_back(truth->barcode);
  }
  //for (int i: truthBSMBarcodes)
  //    std::cout << i << ' ';
  //
  // Tracks (d0Sig>6)
  //

  //Prepare basic MET direction:
  int  tmp_nPhoton_base = configMgr->obj->basePhotons.size();

  TLorentzVector tmp_met_photon_invis = TLorentzVector();
  if(tmp_nPhoton_base==1){
    PhotonVariable* photon = tmp_nPhoton_base>=1 ? configMgr->obj->basePhotons[0] : 0;
    TLorentzVector Photon = TLorentzVector();
    Photon.SetPtEtaPhiM(photon->Pt(),photon->Eta(),photon->Phi(),photon->M());
    TLorentzVector L2_met = TLorentzVector();
    L2_met.SetE(configMgr->obj->met.Et);
    L2_met.SetPx(configMgr->obj->met.px);
    L2_met.SetPy(configMgr->obj->met.py);
    tmp_met_photon_invis = (Photon + L2_met);

  }


  configMgr->treeMaker->setIntVariable("nTrk_base", configMgr->obj->baseTracks.size());

  int nTrk_signal = 0;
  int iTrk = 0; // JEFF

  TLorentzVector tmp_met_muons_invis = TLorentzVector();
  tmp_met_muons_invis.SetPtEtaPhiM(1,0,configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_MUON)->phi,0);
  TLorentzVector tmp_met_LepInvis = TLorentzVector();
  tmp_met_LepInvis.SetPtEtaPhiM(1,0,configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->phi,0);

  const int nBJet20    = getNBJets ( configMgr->obj, 20.0 );
  const int nLep_signal = configMgr->obj->signalLeptons.size();
  bool IsTTBarVR =  (nBJet20 >= 2 && nLep_signal >= 1);
  const TLorentzVector* met_muons_invis     = dynamic_cast<const TLorentzVector*>(configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_MUON));
  const TLorentzVector* met_electrons_invis = dynamic_cast<const TLorentzVector*>(configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_ELECTRON));
  const TLorentzVector* met_leptons_invis   = dynamic_cast<const TLorentzVector*>(configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP));
  const TLorentzVector* met_photons_invis   = dynamic_cast<const TLorentzVector*>(configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_PHOTON));

  for( auto& trk : configMgr->obj->baseTracks ){
 
    //Remove tracks that are not displaced
    if(fabs(trk->d0/trk->d0Err) < 2.0) continue;

    //Remove track in forward region |eta| > 2.0
    if(!(fabs(trk->Eta()) < 2.0)) continue;

    //Remove track that do not go near the missing ET
    //getDPhiTrkMet(iTrk, configMgr->obj)

    if ( !IsTTBarVR //Do not apply for ttbar VR
         && (fabs(trk->DeltaPhi( *met_muons_invis )) > TMath::PiOver2())
         && (fabs(trk->DeltaPhi( *met_electrons_invis )) > TMath::PiOver2())
         && (fabs(trk->DeltaPhi( *met_leptons_invis )) > TMath::PiOver2())
         && (fabs(trk->DeltaPhi( *met_photons_invis )) > TMath::PiOver2())
         && (fabs(trk->DeltaPhi( configMgr->obj->met )) > 3.1415926/2.)
         && (fabs(trk->DeltaPhi( tmp_met_muons_invis )) > 3.1415926/2.)
         && (fabs(trk->DeltaPhi( tmp_met_LepInvis )) > 3.1415926/2.)
         && ((tmp_nPhoton_base==1 && fabs(trk->DeltaPhi( tmp_met_photon_invis )) > 3.1415926/2. ) ||tmp_nPhoton_base!=1)
    ){
      iTrk++; continue;
    };

    //Add Track Classification
    int trkLabel = 5;
    if(fabs(trk->parpdgId) != 15 && trk->origin == 0 && trk->type == 17)     trkLabel = 0;
    else if(fabs(trk->parpdgId) != 15 && trk->origin == 0 && trk->type == 0) trkLabel = 1;
    else if(fabs(trk->parpdgId) == 15 && abs(trk->pdgId)==11)                trkLabel = 2;
    else if(fabs(trk->parpdgId) == 15 && abs(trk->pdgId)==13)                trkLabel = 3;
    else if(fabs(trk->parpdgId) == 15 && trk->type == 17)                    trkLabel = 4;
    else                                                                     trkLabel = 5;
    configMgr->treeMaker->appendVecIntVariable("trkLabel", trkLabel);

    float d0Sig = fabs(trk->d0/trk->d0Err);
    if(d0Sig>6.) nTrk_signal++;

    configMgr->treeMaker->appendVecFloatVariable("trkDPhiMet", getDPhiTrkMet(iTrk, configMgr->obj));
    iTrk++;

    // Loop over tracks again to calculate isolation
    float ptcone              = 0;
    float ptcone_TightPrimary = 0; // ptcone when requiring nearby tracks to pass TightPrimary
    TVector3 trk_v;
    trk_v.SetPtEtaPhi(trk->Pt(), trk->Eta(), trk->Phi());
    for( auto& nearby_trk : configMgr->obj->baseTracks ){
      if ( trk->Pt() == nearby_trk->Pt() ){ continue; } // Exclude the track itself
      if ( nearby_trk->Pt() < 1.0 ){ continue; }
      if ( fabs(nearby_trk->z0SinTheta) > 1.5 ) { continue; }
      if ( fabs(nearby_trk->d0) > 1.5 ) { continue; }

      TVector3 nearby_trk_v;
      nearby_trk_v.SetPtEtaPhi(nearby_trk->Pt(), nearby_trk->Eta(), nearby_trk->Phi());
      float deltaR = trk_v.DeltaR(nearby_trk_v);
      if ( deltaR < 1.0 ){
      	ptcone += nearby_trk->Pt();
      	if ( nearby_trk->TightPrimary == 1 ){
      	  ptcone_TightPrimary += nearby_trk->Pt();
      	}
      }
    }
    configMgr->treeMaker->appendVecFloatVariable("trkIso", ptcone);
    configMgr->treeMaker->appendVecFloatVariable("trkIsoTightPrimary", ptcone_TightPrimary);

    // Loop over V0s to see if track is matched to V0 vertex
    bool matchedToV0 = false;
    for( auto& v0 : configMgr->obj->v0s ){
      if( (fabs(trk->Pt() - v0->track1Pt) < 2e-4 && fabs(trk->Eta() - v0->track1Eta) < 0.02 && fabs(trk->Phi() - v0->track1Phi) < 0.02) ||
	  (fabs(trk->Pt() - v0->track2Pt) < 2e-4 && fabs(trk->Eta() - v0->track2Eta) < 0.02 && fabs(trk->Phi() - v0->track2Phi) < 0.02) ){
	matchedToV0 = true;
        break;
      }
    }

    if ( matchedToV0 == true ){
      configMgr->treeMaker->appendVecBoolVariable("trkMatchedToV0", 1);
    }
    else{
      configMgr->treeMaker->appendVecBoolVariable("trkMatchedToV0", 0);
    }

    // SICONG: VSI relevant
    bool matchedToVSI = false;
    float matchedVsiMass = -1;
    for( auto& vsi : configMgr->obj->vsiVec )
    {
      if (matchedToVSI) break;
      for (auto index: vsi->trackIndices) {
        if (trk->index == index) {matchedToVSI = true; matchedVsiMass = vsi->mass; break;}
      }
    }
    configMgr->treeMaker->appendVecBoolVariable("trkMatchedToVsi", matchedToVSI);
    configMgr->treeMaker->appendVecFloatVariable("trkMatchedVsiMass", matchedVsiMass);


    configMgr->treeMaker->appendVecBoolVariable("trkisTight", trk->isTight);
    configMgr->treeMaker->appendVecBoolVariable("trkisLoose", trk->isLoose);

    configMgr->treeMaker->appendVecFloatVariable("trkPt", trk->Pt());
    configMgr->treeMaker->appendVecFloatVariable("trkEta", trk->Eta());
    configMgr->treeMaker->appendVecFloatVariable("trkPhi", trk->Phi());
    configMgr->treeMaker->appendVecFloatVariable("trkM", trk->m);
    configMgr->treeMaker->appendVecBoolVariable("trkTightPrimary", trk->TightPrimary);
    configMgr->treeMaker->appendVecFloatVariable("trkProdVtx_x", trk->prodvtx.x());
    configMgr->treeMaker->appendVecFloatVariable("trkProdVtx_y", trk->prodvtx.y());
    configMgr->treeMaker->appendVecFloatVariable("trkProdVtx_z", trk->prodvtx.z());
    configMgr->treeMaker->appendVecIntVariable("trkProdVtxType", trk->prodvtxtype);
    configMgr->treeMaker->appendVecIntVariable("trkIsHardVtx", trk->ishardvtx);
    configMgr->treeMaker->appendVecFloatVariable("trkDecayVtx_x", trk->decayvtx.x());
    configMgr->treeMaker->appendVecFloatVariable("trkDecayVtx_y", trk->decayvtx.y());
    configMgr->treeMaker->appendVecFloatVariable("trkDecayVtx_z", trk->decayvtx.z());
    configMgr->treeMaker->appendVecIntVariable("trkDecayVtxType", trk->decayvtxtype);
    configMgr->treeMaker->appendVecBoolVariable("trkisSecTrk", trk->isSecTrk);

    configMgr->treeMaker->appendVecFloatVariable("trkchiSquared", trk->chiSquared);
    configMgr->treeMaker->appendVecFloatVariable("trknumberDoF", trk->numberDoF);

    configMgr->treeMaker->appendVecIntVariable("trkDef", trk->trackType);

    configMgr->treeMaker->appendVecIntVariable("trkQ", trk->q);
    configMgr->treeMaker->appendVecIntVariable("trkpdgId", trk->pdgId);
    configMgr->treeMaker->appendVecIntVariable("trknpar", trk->npar);
    configMgr->treeMaker->appendVecIntVariable("trkparbarcode", trk->parbarcode);
    configMgr->treeMaker->appendVecIntVariable("trkparpdgId", trk->parpdgId);
    configMgr->treeMaker->appendVecIntVariable("trkparnchild", trk->parnchild);
    configMgr->treeMaker->appendVecIntVariable("trkparchildpdgId", trk->parchildpdgId);
    configMgr->treeMaker->appendVecFloatVariable("trkparchildpt", trk->parchildpt);
    configMgr->treeMaker->appendVecIntVariable("trkparchildbarcode", trk->parchildbarcode);

    if (std::find(truthBSMBarcodes.begin(), truthBSMBarcodes.end(), trk->barcode) != truthBSMBarcodes.end())//(trk->parpdgId!=0)&&
    {
        configMgr->treeMaker->appendVecBoolVariable("trkIsTruthBSM", true);
    } else {
        configMgr->treeMaker->appendVecBoolVariable("trkIsTruthBSM", false);
    }

    configMgr->treeMaker->appendVecFloatVariable("trkZ0", trk->z0);
    configMgr->treeMaker->appendVecFloatVariable("trkZ0Origin", trk->z0Origin);
    configMgr->treeMaker->appendVecFloatVariable("trkZ0Err", trk->z0Err);
    configMgr->treeMaker->appendVecFloatVariable("trkZ0SinTheta", trk->z0SinTheta);
    configMgr->treeMaker->appendVecIntVariable("trkBarcode", trk->barcode);
    configMgr->treeMaker->appendVecFloatVariable("trkD0", trk->d0);
    configMgr->treeMaker->appendVecFloatVariable("trkD0Err", trk->d0Err);
    configMgr->treeMaker->appendVecFloatVariable("trkD0Sig", d0Sig);
    configMgr->treeMaker->appendVecFloatVariable("trkPtcone20", trk->ptcone20);
    configMgr->treeMaker->appendVecFloatVariable("trkPtcone30", trk->ptcone30);
    configMgr->treeMaker->appendVecFloatVariable("trkPtcone40", trk->ptcone40);
    configMgr->treeMaker->appendVecFloatVariable("trkTopoEtcone20", trk->etcone20Topo);
    configMgr->treeMaker->appendVecFloatVariable("trkTopoEtcone30", trk->etcone30Topo);
    configMgr->treeMaker->appendVecFloatVariable("trkTopoEtcone40", trk->etcone40Topo);
    configMgr->treeMaker->appendVecFloatVariable("trkTopoEtclus20", trk->etclus20Topo);
    configMgr->treeMaker->appendVecFloatVariable("trkTopoEtclus30", trk->etclus30Topo);
    configMgr->treeMaker->appendVecFloatVariable("trkTopoEtclus40", trk->etclus40Topo);
    configMgr->treeMaker->appendVecIntVariable("trkStatus", trk->status);
    configMgr->treeMaker->appendVecIntVariable("trkType", trk->type);
    configMgr->treeMaker->appendVecIntVariable("trkOrigin", trk->origin);
    configMgr->treeMaker->appendVecFloatVariable("trkFitQuality", trk->fitQuality);
    configMgr->treeMaker->appendVecFloatVariable("trkVtxQuality", trk->vtxQuality);
    configMgr->treeMaker->appendVecFloatVariable("trkD0SV", trk->d0SV);
    configMgr->treeMaker->appendVecFloatVariable("trkD0ErrSV", trk->d0ErrSV);
    configMgr->treeMaker->appendVecFloatVariable("trkZ0SV", trk->z0SV);
    configMgr->treeMaker->appendVecFloatVariable("trkZ0ErrSV", trk->z0ErrSV);
    configMgr->treeMaker->appendVecFloatVariable("trkTruthD0SV", trk->truth_d0SV);
    configMgr->treeMaker->appendVecFloatVariable("trkTruthZ0SV", trk->truth_z0SV);

    configMgr->treeMaker->appendVecFloatVariable("trkTRTdEdx", trk->TRTdEdx);
    configMgr->treeMaker->appendVecFloatVariable("trkpixeldEdx", trk->pixeldEdx);

    configMgr->treeMaker->appendVecIntVariable("trknExpIBLHits", trk->nExpIBLHits);
    configMgr->treeMaker->appendVecIntVariable("trknExpBLayerHits", trk->nExpBLayerHits);
    configMgr->treeMaker->appendVecIntVariable("trknIBLHits", trk->nIBLHits);
    configMgr->treeMaker->appendVecIntVariable("trknBLayerHits", trk->nBLayerHits);
    configMgr->treeMaker->appendVecIntVariable("trknPixHits", trk->nPixHits);
    configMgr->treeMaker->appendVecIntVariable("trknPixLayers", trk->nPixLayers);
    configMgr->treeMaker->appendVecIntVariable("trknPixDeadSensors", trk->nPixDeadSensors);
    configMgr->treeMaker->appendVecIntVariable("trknPixHoles", trk->nPixHoles);
    configMgr->treeMaker->appendVecIntVariable("trknPixSharedHits", trk->nPixSharedHits);
    configMgr->treeMaker->appendVecIntVariable("trknPixOutliers", trk->nPixOutliers);
    configMgr->treeMaker->appendVecIntVariable("trknSCTHits", trk->nSCTHits);
    configMgr->treeMaker->appendVecIntVariable("trknSCTDeadSensors", trk->nSCTDeadSensors);
    configMgr->treeMaker->appendVecIntVariable("trknSCTHoles", trk->nSCTHoles);
    configMgr->treeMaker->appendVecIntVariable("trknSCTSharedHits", trk->nSCTSharedHits);
    configMgr->treeMaker->appendVecIntVariable("trknSCTOutliers", trk->nSCTOutliers);
    configMgr->treeMaker->appendVecIntVariable("trknTRTHits", trk->nTRTHits);
    configMgr->treeMaker->appendVecIntVariable("trknPixSpoiltHits", trk->nPixSpoiltHits);
    configMgr->treeMaker->appendVecIntVariable("trknGangedFlaggedFakes", trk->nGangedFlaggedFakes);

    // Calculate minimum dR to reconstructed lepton & photon
    configMgr->treeMaker->appendVecFloatVariable("trkBaseLepPhotonMinDR"  , getLepPhotonMinDR(configMgr->obj, trk, true));
    configMgr->treeMaker->appendVecFloatVariable("trkSignalLepPhotonMinDR", getLepPhotonMinDR(configMgr->obj, trk, false));
    // Remove lepton & photon topoetclus values
    configMgr->treeMaker->appendVecFloatVariable("trkBaseLepPhotonSubtractedTopoEtclus20"        , trk->etclus20Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.2, true, false));
    configMgr->treeMaker->appendVecFloatVariable("trkBaseLepPhotonSubtractedTopoEtclus30"        , trk->etclus30Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.3, true, false));
    configMgr->treeMaker->appendVecFloatVariable("trkBaseLepPhotonSubtractedTopoEtclus40"        , trk->etclus40Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.4, true, false));
    configMgr->treeMaker->appendVecFloatVariable("trkBaseLepPhotonSubtractedCoreConeTopoEtclus20", trk->etclus20Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.2, true, true));
    configMgr->treeMaker->appendVecFloatVariable("trkBaseLepPhotonSubtractedCoreConeTopoEtclus30", trk->etclus30Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.3, true, true));
    configMgr->treeMaker->appendVecFloatVariable("trkBaseLepPhotonSubtractedCoreConeTopoEtclus40", trk->etclus40Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.4, true, true));
    configMgr->treeMaker->appendVecFloatVariable("trkSignalLepPhotonSubtractedTopoEtclus20"        , trk->etclus20Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.2, false, false));
    configMgr->treeMaker->appendVecFloatVariable("trkSignalLepPhotonSubtractedTopoEtclus30"        , trk->etclus30Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.3, false, false));
    configMgr->treeMaker->appendVecFloatVariable("trkSignalLepPhotonSubtractedTopoEtclus40"        , trk->etclus40Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.4, false, false));
    configMgr->treeMaker->appendVecFloatVariable("trkSignalLepPhotonSubtractedCoreConeTopoEtclus20", trk->etclus20Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.2, false, true));
    configMgr->treeMaker->appendVecFloatVariable("trkSignalLepPhotonSubtractedCoreConeTopoEtclus30", trk->etclus30Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.3, false, true));
    configMgr->treeMaker->appendVecFloatVariable("trkSignalLepPhotonSubtractedCoreConeTopoEtclus40", trk->etclus40Topo - getLepPhotonTopoEtclus(configMgr->obj, trk, 0.4, false, true));


    //Check track associated to muon and electron
    configMgr->treeMaker->appendVecBoolVariable("trkisBaseMuon", isMuonTrk(configMgr->obj, trk, true));
    configMgr->treeMaker->appendVecBoolVariable("trkisBaseElectron", isElectronTrk(configMgr->obj, trk, true));
    configMgr->treeMaker->appendVecBoolVariable("trkisBasePhoton", isPhotonTrk(configMgr->obj, trk, true));
    configMgr->treeMaker->appendVecBoolVariable("trkisSignalMuon", isMuonTrk(configMgr->obj, trk, false));
    configMgr->treeMaker->appendVecBoolVariable("trkisSignalElectron", isElectronTrk(configMgr->obj, trk, false));
    configMgr->treeMaker->appendVecBoolVariable("trkisSignalPhoton", isPhotonTrk(configMgr->obj, trk, false));


    //Recalculate ptcone variables without tracks associated to leptons
    configMgr->treeMaker->appendVecFloatVariable("trkNonBaseAssocPtcone20", trk->ptcone20 - getLepPhotonTrackPt(configMgr->obj, trk, 0.2, true, false));
    configMgr->treeMaker->appendVecFloatVariable("trkNonBaseAssocPtcone30", trk->ptcone30 - getLepPhotonTrackPt(configMgr->obj, trk, 0.3, true, false));
    configMgr->treeMaker->appendVecFloatVariable("trkNonBaseAssocPtcone40", trk->ptcone40 - getLepPhotonTrackPt(configMgr->obj, trk, 0.4, true, false));
    configMgr->treeMaker->appendVecFloatVariable("trkNonBasePhotonConvPtcone20", trk->ptcone20 - getLepPhotonTrackPt(configMgr->obj, trk, 0.2, true, true));
    configMgr->treeMaker->appendVecFloatVariable("trkNonBasePhotonConvPtcone30", trk->ptcone30 - getLepPhotonTrackPt(configMgr->obj, trk, 0.3, true, true));
    configMgr->treeMaker->appendVecFloatVariable("trkNonBasePhotonConvPtcone40", trk->ptcone40 - getLepPhotonTrackPt(configMgr->obj, trk, 0.4, true, true));
    configMgr->treeMaker->appendVecFloatVariable("trkNonSignalAssocPtcone20", trk->ptcone20 - getLepPhotonTrackPt(configMgr->obj, trk, 0.2, false, false));
    configMgr->treeMaker->appendVecFloatVariable("trkNonSignalAssocPtcone30", trk->ptcone30 - getLepPhotonTrackPt(configMgr->obj, trk, 0.3, false, false));
    configMgr->treeMaker->appendVecFloatVariable("trkNonSignalAssocPtcone40", trk->ptcone40 - getLepPhotonTrackPt(configMgr->obj, trk, 0.4, false, false));
    configMgr->treeMaker->appendVecFloatVariable("trkNonSignalPhotonConvPtcone20", trk->ptcone20 - getLepPhotonTrackPt(configMgr->obj, trk, 0.2, false, true));
    configMgr->treeMaker->appendVecFloatVariable("trkNonSignalPhotonConvPtcone30", trk->ptcone30 - getLepPhotonTrackPt(configMgr->obj, trk, 0.3, false, true));
    configMgr->treeMaker->appendVecFloatVariable("trkNonSignalPhotonConvPtcone40", trk->ptcone40 - getLepPhotonTrackPt(configMgr->obj, trk, 0.4, false, true));

    std::vector<bool> v_SR_Sd0;
    std::vector<bool> v_VR_Sd0;
    for(Int_t i = 1; i < 31; i++){
      bool SR_Sd0 = false;
      bool VR_Sd0 = false;
      if(ProcClassifier::IsZjets_nunu_Sherpa(configMgr->getDSID())){
        if(std::fabs(trk->d0 / trk->d0Err) < i){
          SR_Sd0 = true;
          VR_Sd0 = true;
        }
      }
      else{
        if(std::fabs(trk->d0 / trk->d0Err) >= i) SR_Sd0 = true;
        else VR_Sd0 = true;
      }
      v_SR_Sd0.push_back(SR_Sd0);
      v_VR_Sd0.push_back(VR_Sd0);
    }
    configMgr->treeMaker->appendVecVecBoolVariable("trk_SR_Sd0", v_SR_Sd0);
    configMgr->treeMaker->appendVecVecBoolVariable("trk_VR_Sd0", v_VR_Sd0);

  }
  configMgr->treeMaker->setIntVariable("nTrk_signal", nTrk_signal);


  //
  // Taus
  //
  int nTau_base = configMgr->obj->baseTaus.size();
  const TauVariable* tau1 = nTau_base>=1 ? configMgr->obj->baseTaus[0] : 0;
  const TauVariable* tau2 = nTau_base>=1 ? configMgr->obj->baseTaus[1] : 0;
  configMgr->treeMaker->setIntVariable("nTau_base", nTau_base);
  configMgr->treeMaker->setIntVariable("nTau_signal", configMgr->obj->signalTaus.size());
  if(tau1) com.setTauVariables(configMgr,"tau1", tau1);
  if(tau2) com.setTauVariables(configMgr,"tau2", tau2);

  //
  // Jets
  //
  configMgr->treeMaker->setIntVariable("nJet20", getNJets( configMgr->obj, 20.0 ) );
  configMgr->treeMaker->setIntVariable("nJet30", getNJets( configMgr->obj, 30.0 ) );
  for( auto& jet : configMgr->obj->cJets ){
    configMgr->treeMaker->appendVecFloatVariable("jetPt", jet->Pt());
    configMgr->treeMaker->appendVecFloatVariable("jetEta", jet->Eta());
    configMgr->treeMaker->appendVecFloatVariable("jetPhi", jet->Phi());
    configMgr->treeMaker->appendVecFloatVariable("jetM", jet->M());
    configMgr->treeMaker->appendVecBoolVariable("jetIsB", jet->bjet);
    configMgr->treeMaker->appendVecIntVariable("jetTruthLabel", jet->truthLabel);
    configMgr->treeMaker->appendVecIntVariable("jetNtrk", jet->nTrk);
    configMgr->treeMaker->appendVecFloatVariable("jetTiming", jet->Timing);
    configMgr->treeMaker->appendVecFloatVariable("jetJVT", jet->jvt);
  }

  //
  // B-jets
  //
  configMgr->treeMaker->setIntVariable("nBJet20", getNBJets( configMgr->obj, 20.0 ) );
  configMgr->treeMaker->setIntVariable("nBJet30", getNBJets( configMgr->obj, 30.0 ) );


  //
  // MET
  //
  configMgr->treeMaker->setFloatVariable("met_Et",configMgr->obj->met.Et);
  configMgr->treeMaker->setFloatVariable("met_Phi",configMgr->obj->met.phi );
  configMgr->treeMaker->setFloatVariable("metTruth_Et",configMgr->obj->met.Et_truth);
  configMgr->treeMaker->setFloatVariable("metTruth_Phi", atan2(configMgr->obj->met.py_truth, configMgr->obj->met.px_truth));
  configMgr->treeMaker->setFloatVariable("met_track_Et",configMgr->obj->trackMet.Et);
  configMgr->treeMaker->setFloatVariable("met_track_Phi",configMgr->obj->trackMet.phi );
  configMgr->treeMaker->setFloatVariable("met_Signif",configMgr->obj->met.metSignif);
  //configMgr->treeMaker->setFloatVariable("metTST_Et",configMgr->obj->met.Et_soft );
  //configMgr->treeMaker->setFloatVariable("metTST_Phi",configMgr->obj->met.phi_soft );
  configMgr->treeMaker->setFloatVariable("metMuon_Et",configMgr->obj->met.Et_muon);
  configMgr->treeMaker->setFloatVariable("metMuon_Phi",configMgr->obj->met.phi_muon);

  configMgr->treeMaker->setFloatVariable("met_muons_invis_Et",   configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_MUON)->Et );
  configMgr->treeMaker->setFloatVariable("met_muons_invis_Phi",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_MUON)->phi );
  configMgr->treeMaker->setFloatVariable("met_muons_invis_Signif",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_MUON)->metSignif );
  configMgr->treeMaker->setFloatVariable("minDPhi_met_muons_invis_allJets20", getminDPhiJetInvisibleMet(configMgr->obj, -1, 20., MetVariable::INVISIBLE_MUON));
  configMgr->treeMaker->setFloatVariable("minDPhi_met_muons_invis_allJets30", getminDPhiJetInvisibleMet(configMgr->obj, -1, 30., MetVariable::INVISIBLE_MUON));
  configMgr->treeMaker->setFloatVariable("met_electrons_invis_Et",   configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_ELECTRON)->Et );
  configMgr->treeMaker->setFloatVariable("met_electrons_invis_Phi",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_ELECTRON)->phi );
  configMgr->treeMaker->setFloatVariable("met_electrons_invis_Signif",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_ELECTRON)->metSignif );
  configMgr->treeMaker->setFloatVariable("minDPhi_met_electrons_invis_allJets20", getminDPhiJetInvisibleMet(configMgr->obj, -1, 20., MetVariable::INVISIBLE_ELECTRON));
  configMgr->treeMaker->setFloatVariable("minDPhi_met_electrons_invis_allJets30", getminDPhiJetInvisibleMet(configMgr->obj, -1, 30., MetVariable::INVISIBLE_ELECTRON));
  configMgr->treeMaker->setFloatVariable("met_photons_invis_Et",   configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_PHOTON)->Et );
  configMgr->treeMaker->setFloatVariable("met_photons_invis_Phi",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_PHOTON)->phi );
  configMgr->treeMaker->setFloatVariable("met_photons_invis_Signif",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_PHOTON)->metSignif );
  configMgr->treeMaker->setFloatVariable("minDPhi_met_photons_invis_allJets20", getminDPhiJetInvisibleMet(configMgr->obj, -1, 20., MetVariable::INVISIBLE_PHOTON));
  configMgr->treeMaker->setFloatVariable("minDPhi_met_photons_invis_allJets30", getminDPhiJetInvisibleMet(configMgr->obj, -1, 30., MetVariable::INVISIBLE_PHOTON));
  configMgr->treeMaker->setFloatVariable("met_leptons_invis_Et",   configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->Et );
  configMgr->treeMaker->setFloatVariable("met_leptons_invis_Phi",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->phi );
  configMgr->treeMaker->setFloatVariable("met_leptons_invis_Signif",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->metSignif );
  configMgr->treeMaker->setFloatVariable("minDPhi_met_leptons_invis_allJets20", getminDPhiJetInvisibleMet(configMgr->obj, -1, 20., MetVariable::INVISIBLE_LEP));
  configMgr->treeMaker->setFloatVariable("minDPhi_met_leptons_invis_allJets30", getminDPhiJetInvisibleMet(configMgr->obj, -1, 30., MetVariable::INVISIBLE_LEP));
  configMgr->treeMaker->setFloatVariable("met_Et_LepInvis",      configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->Et );
  configMgr->treeMaker->setFloatVariable("met_Phi_LepInvis",     configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->phi );
  configMgr->treeMaker->setFloatVariable("met_Signif_LepInvis",  configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->metSignif );

  //
  // Analysis variables
  //

  configMgr->treeMaker->setFloatVariable("minDPhi_met_allJets20", getminDPhiJetMet(configMgr->obj, -1, 20.));
  configMgr->treeMaker->setFloatVariable("minDPhi_met_allJets30", getminDPhiJetMet(configMgr->obj, -1, 30.));
  configMgr->treeMaker->setFloatVariable("minDPhi_met_4j30", getminDPhiJetMet(configMgr->obj, 4, 30.));
  configMgr->treeMaker->setFloatVariable("minDPhi_trackMet_allJets20", getminDPhiJetMet(configMgr->obj, -1, 20., true));
  configMgr->treeMaker->setFloatVariable("minDPhi_trackMet_allJets30", getminDPhiJetMet(configMgr->obj, -1, 30., true));
  configMgr->treeMaker->setFloatVariable("HtJet30", getHt(configMgr->obj, configMgr->obj->cJets.size(), 30.0) );
  configMgr->treeMaker->setFloatVariable("meffInc30", getMeff(configMgr->obj, configMgr->obj->cJets.size(), 30.0) );

  // mT_tau
  //   Find the jet closest to met assuming W is boosted

  //tlv taujet(0,0,0,0);
  TLorentzVector taujet(0,0,0,0);
  float minDR_met=999.;
  for( auto& jet : configMgr->obj->cJets ){
    float DR_met=acos(cos(jet->Phi()-configMgr->obj->met.phi));
    if(DR_met < minDR_met){
      minDR_met = DR_met;
      taujet = *jet;
    }
  }
  float mt_taujet = com.getMtVis(configMgr->obj, taujet);
  configMgr->treeMaker->setFloatVariable("mt_taujet", mt_taujet);

  float mt_tau = tau1 ? com.getMtVis(configMgr->obj, *tau1) : mt_taujet;
  configMgr->treeMaker->setFloatVariable("mt_tau", mt_tau);

  // 1L variables
  if(nLep_base==1){
    float mt = getMt(configMgr->obj, true, 0); // leading lepton, useBaseline=true
    configMgr->treeMaker->setFloatVariable("L1_mt", mt);

    TLorentzVector pMis; pMis.SetPtEtaPhiM(configMgr->obj->met.Et, 0., configMgr->obj->met.phi, 0.);
    tlv pW = *lep1 + pMis;
    configMgr->treeMaker->setFloatVariable("L1_ptlv", pW.Pt());
    configMgr->treeMaker->setFloatVariable("L1_philv", pW.Phi());
  }

  // 2L variables
  if(nLep_base==2){
    bool SF = (lep1->isMu()==lep2->isMu());
    bool MuSF = (lep1->isMu() && lep2->isMu());
    bool EleSF = (lep1->isEle() && lep2->isEle());
    bool OS = lep1->q+lep2->q==0;
    configMgr->treeMaker->setBoolVariable("L2_SF", SF);
    configMgr->treeMaker->setBoolVariable("L2_ELE", EleSF);
    configMgr->treeMaker->setBoolVariable("L2_MU", MuSF);
    configMgr->treeMaker->setBoolVariable("L2_OS", OS);

    tlv pZ = *lep1 + *lep2;
    TLorentzVector L2_lep1 = TLorentzVector();
    L2_lep1.SetPtEtaPhiM(lep1->Pt(),lep1->Eta(),lep1->Phi(),lep1->M());
    TLorentzVector L2_lep2 = TLorentzVector();
    L2_lep2.SetPtEtaPhiM(lep2->Pt(),lep2->Eta(),lep2->Phi(),lep2->M());
    TLorentzVector L2_met = TLorentzVector();
    L2_met.SetE(configMgr->obj->met.Et);
    L2_met.SetPx(configMgr->obj->met.px);
    L2_met.SetPy(configMgr->obj->met.py);

    configMgr->treeMaker->setTLVariable("L2_lep1", L2_lep1);
    configMgr->treeMaker->setTLVariable("L2_lep2", L2_lep2);
    configMgr->treeMaker->setTLVariable("L2_met", L2_met);
    configMgr->treeMaker->setFloatVariable("L2_mll", pZ.M());
    configMgr->treeMaker->setFloatVariable("L2_ptll", pZ.Pt());
    configMgr->treeMaker->setFloatVariable("L2_phill", pZ.Phi());

    configMgr->treeMaker->setFloatVariable("Dilepton_mll",(L2_lep1 + L2_lep2).M());
    configMgr->treeMaker->setFloatVariable("Dilepton_met_Et",(L2_lep1 + L2_lep2 + L2_met).Pt());
    configMgr->treeMaker->setFloatVariable("Dilepton_met_Phi",(L2_lep1 + L2_lep2 + L2_met).Phi());
    configMgr->treeMaker->setFloatVariable("Dilepton_minDPhi_met_allJets30",getminDPhiJetDileptonMet(configMgr->obj, -1, 30));
  }

  //
  // Photon
  //
  int  nPhoton_base = configMgr->obj->basePhotons.size();
  PhotonVariable* photon = nPhoton_base>=1 ? configMgr->obj->basePhotons[0] : 0;
  if(nPhoton_base==1){
    TLorentzVector Photon = TLorentzVector();
    Photon.SetPtEtaPhiM(photon->Pt(),photon->Eta(),photon->Phi(),photon->M());
    TLorentzVector L2_met = TLorentzVector();
    L2_met.SetE(configMgr->obj->met.Et);
    L2_met.SetPx(configMgr->obj->met.px);
    L2_met.SetPy(configMgr->obj->met.py);

    configMgr->treeMaker->setTLVariable("Photon", Photon);
    configMgr->treeMaker->setFloatVariable("Photon_met_Et",(Photon + L2_met).Pt());
    configMgr->treeMaker->setFloatVariable("Photon_met_Phi",(Photon + L2_met).Phi());
    configMgr->treeMaker->setFloatVariable("Photon_minDPhi_met_allJets30",getminDPhiJetPhotonMet(configMgr->obj, -1, 30));

    configMgr->treeMaker->setBoolVariable("Photon_IsoFCTightCaloOnly", photon->IsoFCTightCaloOnly);
    configMgr->treeMaker->setBoolVariable("Photon_IsoFixedCutLoose", photon->IsoFixedCutLoose);
    configMgr->treeMaker->setBoolVariable("Photon_IsoFixedCutTight", photon->IsoFixedCutTight);
    configMgr->treeMaker->setFloatVariable("Photon_Topoetcone20", photon->topoetcone20);
    configMgr->treeMaker->setFloatVariable("Photon_Topoetcone30", photon->topoetcone30);
    configMgr->treeMaker->setFloatVariable("Photon_Topoetcone40", photon->topoetcone40);
    configMgr->treeMaker->setFloatVariable("Photon_SUSY20_Topoetcone20", photon->SUSY20_topoetcone20);
    configMgr->treeMaker->setFloatVariable("Photon_SUSY20_Topoetcone30", photon->SUSY20_topoetcone30);
    configMgr->treeMaker->setFloatVariable("Photon_SUSY20_Topoetcone40", photon->SUSY20_topoetcone40);
    configMgr->treeMaker->setFloatVariable("Photon_SUSY20_Topoetclus20", photon->SUSY20_topoetcone20NonCoreCone);
    configMgr->treeMaker->setFloatVariable("Photon_SUSY20_Topoetclus30", photon->SUSY20_topoetcone30NonCoreCone);
    configMgr->treeMaker->setFloatVariable("Photon_SUSY20_Topoetclus40", photon->SUSY20_topoetcone40NonCoreCone);
    configMgr->treeMaker->setBoolVariable("Photon_passOR", photon->passOR);
    configMgr->treeMaker->setIntVariable("Photon_conversionType", photon->conversionType);
    configMgr->treeMaker->setFloatVariable("Photon_conversionRadius", photon->conversionRadius);
  }

  // Flags for semi-data driven BG estimation
  configMgr->treeMaker->setStringVariable("DecayProcess", ProcClassifier::getDecayProcess(configMgr->getDSID()));

  for(Int_t i = 0; i < 9; i++){
    std::vector<bool> v_SR_MET;
    std::vector<bool> v_VR_MET;
    for(Int_t j = 0; j < 11; j++){
      bool SR_MET = false;
      bool VR_MET = false;
      if(ProcClassifier::IsZjets_nunu_Sherpa(configMgr->getDSID())){
        if(50*i < configMgr->obj->met.Et && configMgr->obj->met.Et < 50*j){
          SR_MET = true;
          VR_MET = true;
        }
      }
      else{
        if(configMgr->obj->met.Et > 50*j) SR_MET = true;
        else if (50*i < configMgr->obj->met.Et) VR_MET = true;
      }
      v_SR_MET.push_back(SR_MET);
      v_VR_MET.push_back(VR_MET);
    }
    configMgr->treeMaker->appendVecVecBoolVariable("SR_MET", v_SR_MET);
    configMgr->treeMaker->appendVecVecBoolVariable("VR_MET", v_VR_MET);
  }

  // Weights
  configMgr->treeMaker->setDoubleVariable("pileupWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::PILEUP));
  configMgr->treeMaker->setDoubleVariable("leptonWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::LEP));
  configMgr->treeMaker->setDoubleVariable("eventWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::EVT));
  configMgr->treeMaker->setDoubleVariable("genWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("bTagWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::BTAG));
  configMgr->treeMaker->setDoubleVariable("trigWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::TRIG,0,"singleLep"));
  configMgr->treeMaker->setDoubleVariable("jvtWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::JVT));
  configMgr->treeMaker->setDoubleVariable("photonWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::PHOTON));
  configMgr->treeMaker->setDoubleVariable("genWeightUp",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("genWeightDown",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));

  // configMgr->treeMaker->setFloatVariable("globalDiLepTrigSF",configMgr->obj->evt.globalDiLepTrig);
  // configMgr->treeMaker->setFloatVariable("globalMultiLepTrigSF",configMgr->obj->evt.globalMultiLepTrig);

  // PDF variables (only for nominal trees)
  if (configMgr->treeMaker->getTreeState()=="") {
    configMgr->treeMaker->setFloatVariable("x1", configMgr->obj->evt.x1);
    configMgr->treeMaker->setFloatVariable("x2", configMgr->obj->evt.x2);
    configMgr->treeMaker->setFloatVariable("pdf1", configMgr->obj->evt.pdf1);
    configMgr->treeMaker->setFloatVariable("pdf2", configMgr->obj->evt.pdf2);
    configMgr->treeMaker->setFloatVariable("scalePDF", configMgr->obj->evt.scalePDF);
    configMgr->treeMaker->setIntVariable("id1", configMgr->obj->evt.id1);
    configMgr->treeMaker->setIntVariable("id2", configMgr->obj->evt.id2);
  }

  //
  configMgr->doWeightSystematics(0,"singleLep");

  // Fill the output tree
  configMgr->treeMaker->Fill(configMgr->getSysState(),"tree");

  //if(nLep_base < 4) delete lep4;

  return true;

}
// ------------------------------------------------------------------------------------------ //
bool MonojetSelector::passCuts(ConfigMgr*& configMgr)
{

  // If no further skim is not needed
  if(m_disableCuts) return true;

  /*
   This method is used to apply any cuts you wish before writing
   the output trees
  */
  float weight = 1; // pb-1
  /*if(!configMgr->obj->evt.isData()){

    // Note that Rel20.7 2015+2016 is actually 36097.56 pb-1, but this is close enough
    weight = 36100; // pb-1
  }*/
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::GEN);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::EVT);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::LEP);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::JVT);
  weight *= configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::PILEUP);

  ////////////////////////////////////////
  //// begin ntuple preselection cuts ////
  ////////////////////////////////////////

  // Kinematic variables
  const int nLep_base = configMgr->obj->baseLeptons.size();
  const int nLep_signal = configMgr->obj->signalLeptons.size();
  const int nMu_base = configMgr->obj->baseMuons.size();
  const float lep1Pt  = nLep_base >= 1 ? configMgr->obj->baseLeptons[0]->Pt() : 0.;
  const float lep2Pt  = nLep_base >= 2 ? configMgr->obj->baseLeptons[1]->Pt() : 0.;
  //const float lep3Pt  = nLep_base >= 3 ? configMgr->obj->baseLeptons[2]->Pt() : 0.;
  //const float lep4Pt  = nLep_base >= 4 ? configMgr->obj->baseLeptons[3]->Pt() : 0.;

  const int nJet30    = getNJets ( configMgr->obj, 30.0 );
  const int nBJet20    = getNBJets ( configMgr->obj, 20.0 );
  const float jet1Pt  = configMgr->obj->cJets.size()>=1 ? configMgr->obj->cJets[0]->Pt() : 0.;
  const float jet1Eta = configMgr->obj->cJets.size()>=1 ? configMgr->obj->cJets[0]->Eta() : 0.;

  const float minDPhi_met_allJets30 = getminDPhiJetMet(configMgr->obj, -1, 30);

  bool passMetTrig  = configMgr->treeMaker->getBoolVariable("trigMatch_metTrig");
  bool pass1ElTrig  = configMgr->treeMaker->getBoolVariable("trigMatch_singleElectronTrig");
  bool pass1PhTrig  = configMgr->treeMaker->getBoolVariable("trigMatch_singlePhotonTrig");
  bool pass1MuTrig  = configMgr->treeMaker->getBoolVariable("trigMatch_singleMuonTrig");

  //
  // Start of the cut flow
  //
  configMgr->cutflow->bookCut("cutFlow","allEvents",weight );

  //
  // Sample overlap removal
  //
  const int DSID    = configMgr->getDSID();
  const int TrueMet = configMgr->obj->evt.GenMET;
  if(DSID==410470){
    if(TrueMet>100) return false;
    configMgr->cutflow->bookCut("cutFlow","Pass sample overlap removal (TrueMet<100 for DSID=410470)", weight );
  }

  //
  // Apply all recommended event cleaning cuts
  //
  if( !configMgr->obj->passEventCleaning( configMgr->cutflow, "cutFlow", weight ) ) return false;

  //Missing ET
  const float met                    = configMgr->obj->met.Et;
  const float met_muons_invis_Et     = configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_MUON)->Et;
  const float met_electrons_invis_Et = configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_ELECTRON)->Et;
  const float met_photons_invis_Et    = configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_PHOTON)->Et;
  const float met_leptons_invis_Et    = configMgr->obj->getMETFlavor(MetVariable::INVISIBLE_LEP)->Et;

  if( met > 300 || met_muons_invis_Et > 300 || met_electrons_invis_Et > 300 || met_photons_invis_Et > 300 || met_leptons_invis_Et > 300 ){
    configMgr->cutflow->bookCut("cutFlow","MET>300", weight );
    if(jet1Pt > 250 && fabs(jet1Eta) < 2.4 ){
      configMgr->cutflow->bookCut("cutFlow","jet1Pt > 250 && |jet1Eta| < 2.4", weight );
      return true;
    }
  }

  if(nBJet20 >= 2){
    configMgr->cutflow->bookCut("cutFlow","nBJet20 >= 2", weight );
    if( nLep_signal >= 1 ){
      configMgr->cutflow->bookCut("cutFlow",">= 1 signal leptons", weight );
      return true;
    }
  }

  //
  // Trigger
  //
  /*bool passTrig = m_truthOnly ? (met>200 || lep1Pt>28) :
    passMetTrig || pass1ElTrig || pass1MuTrig;*/

  /*if( !passTrig ) return false;
  configMgr->cutflow->bookCut("cutFlow","Pass Met or 1L Trigger", weight );*/
//  bool passTrig = m_truthOnly ? (met>200) :
//    passMetTrig || pass1MuTrig || pass1ElTrig || pass1PhTrig;
//
//  if( !passTrig ) return false;
//  configMgr->cutflow->bookCut("cutFlow","Pass Met or 1Lepton or 1Photon Trigger", weight );
//
//  if( !(jet1Pt > 250) ) return false;
//  configMgr->cutflow->bookCut("cutFlow","Leading Jet Pt > 250GeV", weight );
//
//  if( !(std::fabs(jet1Eta) < 2.4) ) return false;
//  configMgr->cutflow->bookCut("cutFlow","Leading Jet Eta < 2.4", weight );
//
//  if( !(nJet30 <= 4) ) return false;
//  configMgr->cutflow->bookCut("cutFlow","nJet30 <= 4", weight );

  //
  // 0L
  //
  /*if( nLep_base == 0 ) {
    configMgr->cutflow->bookCut("cutFlow","== 0 baseline leptons", weight );

    if( !(met > 200) ) return false;
    configMgr->cutflow->bookCut("cutFlow","0L: MET>200", weight );

    if( !(minDPhi_met_allJets30 > 0.4) ) return false;
    configMgr->cutflow->bookCut("cutFlow","0L: minDPhi_met_allJets30>0.4", weight );

    return true;

  }
  //std::cout<<"Passed: met "<<met<< " jet1Pt "<<jet1Pt<<" minDPhi_met_allJets30 " << minDPhi_met_allJets30<<std::endl;

  */

  /*

  // 1L
  else if( nLep_base == 1 ) {
    configMgr->cutflow->bookCut("cutFlow","== 1 baseline leptons", weight );

    TLorentzVector pMis; pMis.SetPtEtaPhiM(configMgr->obj->met.Et, 0., configMgr->obj->met.phi, 0.);
    float WPt = (*configMgr->obj->baseLeptons[0] + pMis).Pt();
    if( !( WPt>50 || met>100 ) ) return false;
    configMgr->cutflow->bookCut("cutFlow","1L: MET>100 or WPt>50", weight );
    //if( !( WPt>200 || met>200 ) ) return false;
    //configMgr->cutflow->bookCut("cutFlow","1L: MET>200 or WPt>200", weight );

    //if( !(nJet30>=2) ) return false;
    //configMgr->cutflow->bookCut("cutFlow","1L: >=2j", weight );

    return true;
  }

  // 2L
  else if( nLep_base == 2 ) {
    configMgr->cutflow->bookCut("cutFlow","== 2 baseline leptons", weight );

    if( !(lep2Pt > 15) ) return false;
    configMgr->cutflow->bookCut("cutFlow","2L: lepPt>15", weight );

    //float ptll = (*configMgr->obj->baseLeptons[0] + *configMgr->obj->baseLeptons[1]).Pt();
    //if( !(met>100 || ptll>100) ) return false;
    //configMgr->cutflow->bookCut("cutFlow","2L: MET>100 or pT(ll)>100", weight );

    //if( !(nJet30>=2) ) return false;
    //configMgr->cutflow->bookCut("cutFlow","2L: >=2j", weight );

    return true;
  }
  */

  /*
  // 3L
  else if( nLep_base == 3 ) {
    configMgr->cutflow->bookCut("cutFlow","== 3 baseline leptons", weight );

    if( lep3Pt < 15 ) return false;
    configMgr->cutflow->bookCut("cutFlow","3L: lepPt>15", weight );

    if( met < 100 ) return false;
    configMgr->cutflow->bookCut("cutFlow","3L: MET>100", weight );

    return true;
  }

  // 4L
  else if( nLep_base == 4 ) {
     configMgr->cutflow->bookCut("cutFlow","== 4 baseline leptons", weight );

    if( lep4Pt < 10 ) return false;
    configMgr->cutflow->bookCut("cutFlow","4L: lepPt>10", weight );

    if( met < 100 ) return false;
    configMgr->cutflow->bookCut("cutFlow","4L: MET>100", weight );

    return true;
  }
  */

  return false;
}

// ------------------------------------------------------------------------------------------ //
void MonojetSelector::doCutFlow(ConfigMgr*& configMgr)
{

  /*
   This method fills all cut flow histograms when cutOnlyMode is on.
  */

  // Fill cutflow histograms
  fillCutFlows(configMgr, "cutFlow", "allEvents");
  fillCutFlows(configMgr, "cutFlow_sq", "allEvents");

  // Apply all recommended event cleaning cuts
  fillCutFlows(configMgr, "cutFlow", "passEventCleaning");
  fillCutFlows(configMgr, "cutFlow_sq", "passEventCleaning");
  if( !configMgr->obj->passEventCleaning( configMgr->cutflow, "dummy", 1. ) ) return;

  // ------------------------------------------------------
  // Non-sequential cuts
  // i.e. events not discarded after each cut
  // ------------------------------------------------------

  const int nLeptons_base     = configMgr->obj->baseLeptons.size();
  /*
  const int nLeptons_signal   = configMgr->obj->signalLeptons.size();
  const int nElectrons_base   = configMgr->obj->baseElectrons.size();
  const int nElectrons_signal = configMgr->obj->signalElectrons.size();
  const int nMuons_base       = configMgr->obj->baseMuons.size();
  const int nMuons_signal     = configMgr->obj->signalMuons.size();
  const int nJet30            = getNJets ( configMgr->obj, 30.0 );
  const int nBJet30           = getNBJets( configMgr->obj, 30.0 );
  const int nFatJet           = configMgr->obj->cFatjets.size();
  */
  if(nLeptons_base   == 0) fillCutFlows(configMgr, "cutFlow", "== 0 Baseline lepton");
  if(nLeptons_base   == 1) fillCutFlows(configMgr, "cutFlow", "== 1 Baseline lepton");

  // ------------------------------------------------------
  // Sequential cuts
  // i.e. events discarded after each cut
  // ------------------------------------------------------


  return;

}
// ------------------------------------------------------------------------------------------ //
void MonojetSelector::fillCutFlows(ConfigMgr*& configMgr, TString cutFlowName, TString description)
{
  /*
   This method is used to fill an entry into cut flows defined in setup()
  */

  float genWeight     = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::GEN);
  float eventWeight   = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::EVT);
  float pileupWeight  = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::PILEUP);
  float leptonWeight  = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::LEP);
  float bTagWeight    = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::BTAG);
  float jvtWeight     = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::JVT);
  float triggerWeight = configMgr->objectTools->getWeight(configMgr->obj, "", ObjectTools::WeightType::TRIG);

  // Fill for cleaning cuts
  if(description=="passEventCleaning"){
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName                 , 1.      );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_genWeight"    , genWeight     );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_eventWeight"  , eventWeight   );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_pileupWeight" , pileupWeight  );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_leptonWeight" , leptonWeight  );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_bTagWeight"   , bTagWeight    );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_jvtWeight"    , jvtWeight     );
    configMgr->obj->passEventCleaning( configMgr->cutflow, cutFlowName+"_triggerWeight", triggerWeight );
  }
  // Fill for the other cuts
  else{
    configMgr->cutflow->bookCut(cutFlowName                 , description, 1.      );

    configMgr->cutflow->bookCut(cutFlowName+"_genWeight"    , description, genWeight     );
    configMgr->cutflow->bookCut(cutFlowName+"_eventWeight"  , description, eventWeight   );
    configMgr->cutflow->bookCut(cutFlowName+"_pileupWeight" , description, pileupWeight  );
    configMgr->cutflow->bookCut(cutFlowName+"_leptonWeight" , description, leptonWeight  );
    configMgr->cutflow->bookCut(cutFlowName+"_bTagWeight"   , description, bTagWeight    );
    configMgr->cutflow->bookCut(cutFlowName+"_jvtWeight"    , description, jvtWeight     );
    configMgr->cutflow->bookCut(cutFlowName+"_triggerWeight", description, triggerWeight );
  }

}
// ------------------------------------------------------------------------------------------ //
void MonojetSelector::finalize(ConfigMgr*& configMgr)
{

  /*
   This method is called at the very end of the job. Can be used to merge cutflow histograms
   for example. See CutFlowTool::mergeCutFlows(...)
  */

}
// ------------------------------------------------------------------------------------------ //
bool MonojetSelector::init_merge(MergeTool*& mergeTool){return true; }
// ------------------------------------------------------------------------------------------ //
bool MonojetSelector::execute_merge(MergeTool*& mergeTool){return true;}
// ------------------------------------------------------------------------------------------ //
