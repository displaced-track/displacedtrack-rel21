getRNs(){
    grep -r "${1}" /cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/share/DSID39*/MC* /cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/share/DSID40*/MC* | grep -v svn | cut -d ":" -f 1 | rev | cut -d "/" -f 1 | rev | cut -d "." -f 2 | sort > log

    NGKey=$2
    if [ "${NGKey}" == "" ]; then
	NGKey="cheeseburger_cheeseburger_cheeseburger_cheeseburger_cheeseburger_cheeseburger"
    fi
    echo ${NGKey}

    rm -f log
    for sub in 37 38 39 40; do
	grep -r "${1}" /cvmfs/atlas.cern.ch/repo/sw/Generators/MC15JobOptions/latest/share/DSID${sub}*/MC* | grep -v svn \
	| grep -v ${NGKey} \
	| cut -d ":" -f 1 | rev | cut -d "/" -f 1 | rev | cut -d "." -f 2 | sort >> log 
    done

    echo "// ${1}"
    echo `root -l -b -q squash_consecutive_DSIDs.cc | grep -v "Processing squash_consecutive_DSIDs.cc"`
}

# Gluinos (via 3-body qq+EWKino, upto 1step)
getRNs MadGraphControl_SimplifiedModel_GG_bbn1.py
getRNs MadGraphControl_SimplifiedModel_GG_direct.py
getRNs MadGraphControl_SimplifiedModel_GG_directGtc.py
getRNs MadGraphControl_SimplifiedModel_GG_directGts.py
getRNs MadGraphControl_SimplifiedModel_GG_onestepCC.py
getRNs MadGraphControl_SimplifiedModel_GG_onestepN2C.py
getRNs MadGraphControl_SimplifiedModel_GG_tb.py
getRNs MadGraphControl_SimplifiedModel_GG_ttn1.py
getRNs MadGraphControl_SimplifiedModel_onestepN2.py
getRNs MadGraphControl_SimplifiedModel_onestepN2_N1.py SLN1
getRNs MadGraphControl_pMSSM_GG_M3muM1lsp.py

# EWKinos (via boson, direct only)
getRNs MadGraphControl_mAMSB.py
getRNs MadGraphControl_NUHM2_higgsino_MadSpin.py
getRNs MadGraphControl_NUHM2_weak_MadSpin.py
getRNs MadGraphControl_SimplifiedModel_C1C1_WW.py
getRNs MadGraphControl_SimplifiedModel_C1C1_WW_MadSpin.py
getRNs MadGraphControl_SimplifiedModel_C1N2_WZ.py
getRNs MadGraphControl_SimplifiedModel_C1N2_WZDV.py
getRNs MadGraphControl_SimplifiedModel_C1N2_WZ_MadSpin.py
getRNs MadGraphControl_SimplifiedModel_C1N2_Wh.py
getRNs MadGraphControl_SimplifiedModel_VBFhiggsino_N2ZC1W_dilepmet.py
getRNs MadGraphControl_SimplifiedModel_VBFwino_N2ZC1W_dilepmet.py
getRNs MadGraphControl_SimplifiedModel_higgsino.py
getRNs MadGraphControl_SimplifiedModel_C1N2N1_GGMHino_Filter.py 

