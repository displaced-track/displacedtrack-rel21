#include "TruthDecayContainer/DecayHandle.h"

// System include(s):
#include <memory>
#include <cstdlib>

// ROOT include(s):
#include <TError.h>
#include <TString.h>

// EDM include(s):
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthMetaData.h"
#include "xAODTruth/TruthMetaDataContainer.h"

// Other include(s):
#include "AsgTools/StatusCode.h"
#include "AsgTools/AsgMetadataTool.h"
#include "TruthDecayContainer/ProcClassifier.h"
#include "TruthDecayContainer/TruthDecayUtils.h"

typedef TLorentzVector tlv;

//////////////////////////////////////////////////////////////////////////////////////////////////////
DecayHandle::DecayHandle() : m_convertFromMeV(0.001),
			     m_DSID(0),
			     m_generator("Unknown"),
			     m_truthParticles(0),
			     m_truthElectrons(0),
			     m_truthMuons(0),
			     m_truthTaus(0),
			     m_truthPhotons(0),
			     m_truthNeutrinos(0),
			     m_truthBosonWDP(0),
			     m_truthTopWDP(0),
			     m_truthBSMWDP(0),
			     m_truthTauWDP(0),
			     m_truthJets(0),
			     m_forcedTruthProc("")
{
}
//////////////////////////////////////////////////////////////////////////////////////////////////////
DecayHandle::~DecayHandle()
{
  //if(m_tauTruthMatchingTool) delete m_tauTruthMatchingTool;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
StatusCode DecayHandle::init(bool useGeV)
{
  m_convertFromMeV = useGeV ? 0.001 : 1.;

  // Init asg::MetaDataTool
  m_metaDataTool = new asg::AsgMetadataTool("asgMetaDataTool_in_TruthDecayContainer");

  const xAOD::TruthMetaDataContainer *metaDataContainer = nullptr;
  if ( !m_metaDataTool->inputMetaStore()->contains<xAOD::TruthMetaData>("TruthMetaData")
       || !m_metaDataTool->inputMetaStore()->retrieve(metaDataContainer,"TruthMetaData").isSuccess()){
    ERROR("DecayHandle:init()","No xAOD::TruthMetaDataContainer found in the file. ");
    return StatusCode::FAILURE;
  }

  if( metaDataContainer->size()==0 ) {
    ERROR("DecayHandle:init()","Loaded xAOD::TruthMetaDataContainer is empty.");
    return StatusCode::FAILURE;
  }

  INFO("DecayHandle:init()","Loaded xAOD::TruthMetaDataContainer");
  m_DSID = metaDataContainer->at(0)->mcChannelNumber();
  m_generator = TString(metaDataContainer->at(0)->generators());

  INFO("DecayHandle:init()","DSID=%i, generator=%s", m_DSID, m_generator.Data());

  // Return gracefully
  return StatusCode::SUCCESS;

}

//////////////////////////////////////////////////////////////////////////////////////////////////////
StatusCode DecayHandle::init(int DSID, bool useGeV, TString generator)
{
  m_DSID           = DSID;
  m_convertFromMeV = useGeV ? 0.001 : 1.;
  m_generator      = generator;

  // If generator is not specified, just try to see if this is Sherpa samples based on the DSID
  if(m_generator=="" || m_generator=="Unknown")
    m_generator = ProcClassifier::IsSherpa(m_DSID) ? "Sherpa" : "Unknown";

  INFO("DecayHandle:init()","DSID=%i, generator=%s", m_DSID, m_generator.Data());

  // Return gracefully
  return StatusCode::SUCCESS;

}
//////////////////////////////////////////////////////////////////////////////////////////////////////
StatusCode DecayHandle::RetrieveDocumentLines(std::vector<tlv> &addPartons,  std::vector<int> &addPartons_pdg, float ptThreshInGeV, bool fillOnlyHF){

  //
  // Retrive status=11 partons (quarks, gluon) for Sherpa samples
  //

  if(!m_truthParticles) return StatusCode::FAILURE;

  addPartons.clear();
  addPartons_pdg.clear();
  std::vector<xAOD::TruthParticle*> v_mcp;
  for(int idx=0, n=(int)m_truthParticles->size(); idx<n; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(m_truthParticles->at(idx)) ;

    // Skip if it is not document line
    if     (isSherpa()){
      if(mcp->status()!=11) continue;
    }else if(isPythia()){
      if(!(mcp->status()>=40 && mcp->status()<=49) ) continue;
    }else{
      if(mcp->status()!=3) continue;
    }

    // PDGID cut
    int pdg=mcp->pdgId();
    if(!(std::abs(pdg)==21 || (std::abs(pdg)>=1 && std::abs(pdg)<=5))) continue;

    if(fillOnlyHF && std::abs(pdg)!=4 && std::abs(pdg)!=5) continue;

    // Only consider partons before the showing
    xAOD::TruthParticle* init_mcp = ascend(mcp,0);

    // Are those already added?
    bool isAdded=false;
    for(auto filled_mcp : v_mcp){
      if(filled_mcp == init_mcp){ isAdded=true; break; }
      float DR = hypot(filled_mcp->eta()-init_mcp->eta(),acos(cos(filled_mcp->phi()-init_mcp->phi())));
      if(DR<0.02) { isAdded=true; break; }
    }
    if(isAdded) continue;

    // Pt cut
    if(init_mcp->pt()/1000. < ptThreshInGeV) continue;

    // Fill
    addPartons.push_back(setTLV(init_mcp));
    addPartons_pdg.push_back(pdg);

    v_mcp.push_back(init_mcp);
  }

  return StatusCode::SUCCESS;
}
/*
//////////////////////////////////////////////////////////////////////////////////////////////////////
StatusCode DecayHandle::RetrieveCharmAndBottom(std::vector<tlv> &addPartons,  std::vector<int> &addPartons_pdg, float ptThreshInGeV){

  //
  // Retrive charms/bottoms that do not share the same self-decay chain
  //

  if(!m_truthParticles) return StatusCode::FAILURE;

  std::vector<xAOD::TruthParticle*> init_mcp;

  addPartons.clear();
  addPartons_pdg.clear();

  for(int idx=0, n=(int)m_truthParticles->size(); idx<n; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)m_truthParticles->at(idx) ;

    if((std::abs(mcp->pdgId())==4 || std::abs(mcp->pdgId())==5) && mcp->pt()/1000.>ptThreshInGeV){
      xAOD::TruthParticle *init = ascend(mcp);
      bool isAdded=false;
      for(auto p : init_mcp){ if(p==init){isAdded=true; break;}  };

      if(!isAdded){
	init_mcp.push_back(init);
	addPartons.push_back(setTLV(mcp));
	addPartons_pdg.push_back(mcp->pdgId());
      }
    }

  }

  return StatusCode::SUCCESS;
}
*/
//////////////////////////////////////////////////////////////////////////////////////////////////////
StatusCode DecayHandle::RetrieveTruthJets(std::vector<tlv> &truthJets,  std::vector<int> &truthJets_label){

  if(!m_truthJets) return StatusCode::SUCCESS;

  //
  // Fill truth jets in the events
  //

  truthJets.clear();
  truthJets_label.clear();

  for(auto jet : *m_truthJets) {

    tlv this_jet(0,0,0,0);
    this_jet.SetPtEtaPhiM( jet->pt() * m_convertFromMeV,
			   jet->eta(),
			   jet->phi(),
			   jet->m() * m_convertFromMeV );

    int truthLabel =
      cacc_TDC_PartonTruthLabelID.isAvailable( *jet ) ? cacc_TDC_PartonTruthLabelID( *jet ) :
      cacc_TDC_HadronConeExclTruthLabelID.isAvailable( *jet ) ? cacc_TDC_HadronConeExclTruthLabelID( *jet ) :
      cacc_TDC_ConeTruthLabelID.isAvailable( *jet ) ? cacc_TDC_ConeTruthLabelID( *jet ) :
      0;

    truthJets.push_back(this_jet);
    truthJets_label.push_back(truthLabel);
  }

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
StatusCode DecayHandle::GetTauDecay(xAOD::TruthParticle* mcp_tau, Decay_tau &decay_tau){

  //
  // Retrieve tau decay chain.
  //   Straightforward when the decay record is all trackable,
  //   otherwise try to find the DR-matched status=2 tau that has the record connected to final state particles.
  //

  if(!m_truthTauWDP) return StatusCode::SUCCESS; // ignore the tau decay

  // Sanity checkes
  if( !mcp_tau ) {
    ERROR("DecayHandle::GetTauDecay", "Input xAOD::TruthParticle is null. Exit.");
    return StatusCode::FAILURE;
  }
  if( std::abs(mcp_tau->pdgId())!=15 ){
    ERROR("DecayHandle::GetTauDecay", "Input xAOD::TruthParticle is not tau (pdgId: %i). Exit.", mcp_tau->pdgId());
    return StatusCode::FAILURE;
  }

  // Init.
  decay_tau.P4  = setTLV(mcp_tau);
  decay_tau.pdg = mcp_tau->pdgId();

  // Skip all the self-decay if possible (do not do this for Sherpa since it will enter an eternal loop).
  xAOD::TruthParticle* fin_tau = isSherpa() ? mcp_tau : descend(mcp_tau,0);
  int fin_tau_pdg = fin_tau->pdgId(); // 15 or -15

  // If the descendant is not available (in case of Sherpa, or others with nChildren=0),
  // search over TruthTauContainer for a tau with the closest dR (max: 0.4) that has the same charge and a tau neutrino as chidren ("tau_TTC").
  if(isSherpa() || getNChildren(fin_tau)==0)     {

    xAOD::TruthParticle* tau_TTC=0;
    double minDR=999.;
    for(int idx=0; idx<(int)m_truthTauWDP->size(); idx++) {
      xAOD::TruthParticle* t = (xAOD::TruthParticle*)(m_truthTauWDP->at(idx)) ;
      if(!(t->pdgId()==fin_tau_pdg && t->status()==2)) continue;
      if(getNChildren(t)<2) continue; // skip self-decaying taus
      double DR = TruthDecayUtils::GetDR(setTLV(fin_tau), setTLV(t));
      if(DR<minDR){  tau_TTC = t;   minDR = DR;  }
    }
    if(!tau_TTC){
      WARNING("DecayHandle::GetTauDecay", "Input tau has 0 children! (Disrupted decay history) Try to pickup a status=2 tau that matches to it which usually contains the decay history... >>>> Could not find a status=2 tau in the same charge with nChildren>=2 in TruthTauContainer. Exit.");
      return StatusCode::FAILURE;
    }
    if(minDR>0.4){
      WARNING("DecayHandle::GetTauDecay", "Input tau has 0 children! (Disrupted decay history) Try to pickup a status=2 tau that matches to it which usually contains the decay history... >>>> Could only find status=2 tau(s) in DR>0.4 with respect to the input tau. Does not look like the same one. Exit.");
      return StatusCode::FAILURE;
    }
    // If the same tau is found in TruthTauContainer, use this to retrieve the decay info.
    fin_tau = descend(tau_TTC,0);
  }

  //
  // Retrieve tau children information
  //
  bool decayIntoLepton=false;
  for(int idx=0; idx<(int)getNChildren(fin_tau); idx++){
    xAOD::TruthParticle *tau_child = (xAOD::TruthParticle*)getChild(fin_tau,idx);
    int tau_child_pdg = tau_child->pdgId();

    // If the tau decays leptonically ...
    if(std::abs(tau_child_pdg)==11 || std::abs(tau_child_pdg)==13){
      decay_tau.pchild = setTLV_fin(tau_child); // fill the lepton as tau's child
      decay_tau.child_pdg = tau_child_pdg;
      decay_tau.decayLabel = 8;
      if(std::abs(tau_child_pdg)==11) decay_tau.subdecayLabel = 11;
      else if(std::abs(tau_child_pdg)==13) decay_tau.subdecayLabel = 12;
      decayIntoLepton=true;
    }
    // Electron/muon-neutrinos associated with the leptonic decay of tau (not the taunu)
    else if(std::abs(tau_child_pdg)==12 || std::abs(tau_child_pdg)==14){
      decay_tau.pchild2 = setTLV_fin(tau_child); // fill the lepton as tau's child
      decay_tau.child2_pdg = tau_child_pdg;
    }

    // If there is the tau neutrino from the tau decay
    else if( (fin_tau->pdgId()==15 && tau_child_pdg==16) || (fin_tau->pdgId()==-15 && tau_child_pdg==-16) ){
      decay_tau.ptaunu = setTLV_fin(tau_child);
    }

  } // end of loop over tau children

  // If the tau decays hadronically ...
  if(!decayIntoLepton){

    // Assign the momentum difference between the tau and the tau neutrino as the momentum of the hadronic tau.
    decay_tau.pchild = decay_tau.P4 - decay_tau.ptaunu;
    // Get the decay label defined in TauTruthMatchingTool
    //decay_tau.decayLabel = m_tauTruthMatchingTool->getDecayMode(*fin_tau); // Diable temporarily!
    Int_t nChargedPion = 0;
    Int_t nStrangeHadron = 0;
    Int_t nNeutralPion = 0;
    Int_t nLightMeson = 0;
    Int_t nPhoton = 0;
    Int_t nOther = 0;
    for(int idx=0; idx<(int)getNChildren(fin_tau); idx++){
      xAOD::TruthParticle *tau_child = (xAOD::TruthParticle*)getChild(fin_tau,idx);
      if(fabs(tau_child->pdgId())==211) nChargedPion++;
      else if(fabs(tau_child->pdgId())==130 || fabs(tau_child->pdgId())== 310 ||  fabs(tau_child->pdgId())== 311 ||  fabs(tau_child->pdgId())== 321 ) nStrangeHadron++;
      else if(fabs(tau_child->pdgId())==111) nNeutralPion++;
      else if(fabs(tau_child->pdgId())==22) nPhoton++;
      else if(fabs(tau_child->pdgId())!=16) nOther++;
    }

    // Subtract photon momentum for single pion decay
    if(nChargedPion==1 && nNeutralPion ==0 && nStrangeHadron == 0){
      for(int idx=0; idx<(int)getNChildren(fin_tau); idx++){
	xAOD::TruthParticle *tau_child = (xAOD::TruthParticle*)getChild(fin_tau,idx);
	if(fabs(tau_child->pdgId())==22) decay_tau.pchild -= setTLV_fin(tau_child);
      }
    }

    // rho meson resonance
    if(nStrangeHadron == 0 && nChargedPion==1 && nNeutralPion ==1){
      for(int idx=0; idx<(int)getNChildren(fin_tau); idx++){
	xAOD::TruthParticle *tau_child = (xAOD::TruthParticle*)getChild(fin_tau,idx);
	if(fabs(tau_child->pdgId())==211) decay_tau.prhochild  = setTLV_fin(tau_child);
	if(fabs(tau_child->pdgId())==111) decay_tau.prhochild2 = setTLV_fin(tau_child);
      }
    }

    if(nChargedPion==1 && nNeutralPion ==0) decay_tau.decayLabel = 0;
    else if(nChargedPion==1 && nNeutralPion ==1) decay_tau.decayLabel = 1;
    else if(nChargedPion==1 && nNeutralPion >=2) decay_tau.decayLabel = 2;
    else if(nChargedPion==3 && nNeutralPion ==0) decay_tau.decayLabel = 3;
    else if(nChargedPion==3 && nNeutralPion >=1) decay_tau.decayLabel = 4;
    else if(nChargedPion==2 || nChargedPion==4 || nChargedPion==5) decay_tau.decayLabel = 5;
    else if(nChargedPion==0 || nChargedPion==6) decay_tau.decayLabel = 6;

    if(nOther >= 1) decay_tau.subdecayLabel = 10;
    else if(nStrangeHadron>=1) decay_tau.subdecayLabel = 9;
    else if(nChargedPion==1 && nNeutralPion ==0) decay_tau.subdecayLabel = 0;
    else if(nChargedPion==1 && nNeutralPion ==1 && nPhoton == 0) decay_tau.subdecayLabel = 1;
    else if(nChargedPion==1 && nNeutralPion ==2) decay_tau.subdecayLabel = 2;
    else if(nChargedPion==1 && nNeutralPion ==3) decay_tau.subdecayLabel = 3;
    else if(nChargedPion==1 && nNeutralPion >=4) decay_tau.subdecayLabel = 4;
    else if(nChargedPion==3 && nNeutralPion ==0) decay_tau.subdecayLabel = 5;
    else if(nChargedPion==3 && nNeutralPion ==1) decay_tau.subdecayLabel = 6;
    else if(nChargedPion==3 && nNeutralPion ==2) decay_tau.subdecayLabel = 7;
    else if(nChargedPion==3 && nNeutralPion >=3) decay_tau.subdecayLabel = 8;
    else {
      INFO("DecayHandle::GetTauDecay", "Tau decay label not filled");
      for(int idx=0; idx<(int)getNChildren(fin_tau); idx++){
	xAOD::TruthParticle *tau_child = (xAOD::TruthParticle*)getChild(fin_tau,idx);
	INFO("DecayHandle::GetTauDecay", "child_pdg: %i", tau_child->pdgId());
      }
      decay_tau.subdecayLabel = -1;
    }

    // Fill the first hadron as the child's pdgId
    if(getNChildren(fin_tau)==2){
      decay_tau.child_pdg = std::abs(getChild(fin_tau,0)->pdgId())==16 ?
	getChild(fin_tau,1)->pdgId() : getChild(fin_tau,0)->pdgId();
    }
  }

  //
  // Check if proper children are found
  //
  if(decay_tau.pchild.E()==0 || decay_tau.ptaunu.E()==0){
    ERROR("DecayHandle::GetTauDecay", "Tau decay info is not fully filled. child_pdg: %i", decay_tau.child_pdg);
    decay_tau.print();
    return StatusCode::FAILURE;
  }

  //
  // Finalize
  //

  decay_tau.finalize();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetWDecay_Impl(const std::vector<xAOD::TruthParticle*> &Wchildren, Decay_boson &decay_W){

  //
  // Retrive W decay information.
  //   Input is given by a vector of W's decay product
  //   (for flexibility against situation where W is not explicitly left in the record).
  //   If you do have a xAOD::TruthParticle that has pdgId=24,-24, use DecayHandle::GetWDecay.
  //


  // Check the input
  if(Wchildren.size()<2){
    ERROR("DecayHandle::GetWDecay_Impl",  "Number of W's children is %i which is <2. Input W's children are as follow: ", Wchildren.size());

    for(int j=0; j<(int)Wchildren.size(); j++){
      xAOD::TruthParticle* child = (xAOD::TruthParticle*)Wchildren[j];
      int child_pdg = child->pdgId();
      ERROR("DecayHandle::GetWDecay_Impl",  "   child %i:   pdgId=%i,  m=%f GeV", j, child_pdg, child->m()/1000.);
    }
    return StatusCode::FAILURE;
  }

  // Init.
  decay_W.clear();

  //
  // Fill the decay info based on the vector of W's children.
  // Note that the notation is:
  //   q1: lepton,tau or quark
  //   q2: netrino    or anti-quark
  //

  for(int j=0; j<(int)Wchildren.size(); j++){
    xAOD::TruthParticle *W_child = Wchildren[j];
    int W_child_pdg = W_child->pdgId();

    // e/mu
    if(std::abs(W_child_pdg)==11 || std::abs(W_child_pdg)==13){
      decay_W.pq1    = setTLV_fin(W_child);
      decay_W.q1_pdg = W_child_pdg;
    }
    // tau
    else if(std::abs(W_child_pdg)==15){
      decay_W.pq1    = setTLV_fin(W_child);
      decay_W.q1_pdg = W_child_pdg;
      GetTauDecay(W_child, decay_W.tau1);
    }
    // ve/vmu/vtau
    else if(std::abs(W_child_pdg)==12 || std::abs(W_child_pdg)==14 || std::abs(W_child_pdg)==16){
      decay_W.pq2    = setTLV_fin(W_child);
      decay_W.q2_pdg = W_child_pdg;
    }
    // hadronic
    else{
      if(std::abs(W_child_pdg)<=6){
	if(W_child_pdg<0) { decay_W.pq1 = setTLV(W_child); decay_W.q1_pdg = W_child_pdg; }
	else {              decay_W.pq2 = setTLV(W_child); decay_W.q2_pdg = W_child_pdg; }
      }
      else if(std::abs(W_child_pdg)==211){
	decay_W.pq1 = setTLV_fin(W_child);
	decay_W.q1_pdg = W_child_pdg;
	break;
      }
    }

    if(decay_W.q1_pdg!=0 && decay_W.q2_pdg!=0) break;  // break if all children are filled

  } // end of loop over W's children

  //
  // Check if proper children are found
  //
  if(fabs(decay_W.q1_pdg) != 211 && (decay_W.q1_pdg==0 || decay_W.q2_pdg==0)){
    ERROR("DecayHandle::GetWDecay", "W decay info is not fully filled.");
    decay_W.print();
    return StatusCode::FAILURE;
  }

  //
  // Finalize
  //

  if(  decay_W.P4.E()==0 ) decay_W.P4 = decay_W.pq1 + decay_W.pq2 ;
  decay_W.pdg = get_charge(decay_W.q1_pdg)+get_charge(decay_W.q2_pdg) > 0 ? 24 : -24;

  decay_W.finalize();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetWDecay(xAOD::TruthParticle *mcp_W, Decay_boson &decay_W){

  //
  // Retrive W decay information.
  //   Input: xAOD::TruthParticle that has pdgId=24,-24
  //

  // Sanity checkes
  if( !mcp_W ) {
    ERROR("DecayHandle::GetWDecay", "Input xAOD::TruthParticle is null. Exit.");
    return StatusCode::FAILURE;
  }
  if( std::abs(mcp_W->pdgId())!=24 ){
    ERROR("DecayHandle::GetWDecay", "Input xAOD::TruthParticle is not W (pdgId: %i). Exit.", mcp_W->pdgId());
    return StatusCode::FAILURE;
  }

  // Skip all the self-decays
  xAOD::TruthParticle* fin_W = descend(mcp_W,0);
  decay_W.P4 = setTLV(fin_W);

  // Find the decay products
  std::vector<xAOD::TruthParticle *>Wchildren; Wchildren.clear();
  for(int j=0; j<(int)getNChildren(fin_W); j++){
    xAOD::TruthParticle *W_child = (xAOD::TruthParticle*)getChild(fin_W,j);
    Wchildren.push_back(W_child);
  }
  // Fill the information
  GetWDecay_Impl(Wchildren,decay_W);

  decay_W.pdg = mcp_W->pdgId();

  return StatusCode::SUCCESS;
}

/////////////////////////////////////////////////////////
StatusCode DecayHandle::GetZDecay_Impl(const std::vector<xAOD::TruthParticle*> &Zchildren, Decay_boson &decay_Z){

  //
  // Retrive Z decay information.
  //   Input is given by a vector of Z's decay product
  //   (for flexibility against situation where Z is not explicitly left in the record).
  //   If you do have a xAOD::TruthParticle that has pdgId=23, use DecayHandle::GetZDecay.
  //

  // Check the input
  if(Zchildren.size()<2){
    ERROR("DecayHandle::GetZDecay_Impl",  "Number of Z's children is %i which is <2. Input Z's children are follow: ", Zchildren.size());
    for(int j=0; j<(int)Zchildren.size(); j++){
      xAOD::TruthParticle* child = (xAOD::TruthParticle*)Zchildren[j];
      int child_pdg = child->pdgId();
      ERROR("DecayHandle::GetZDecay_Impl",  "   child %i:   pdgId=%i,  m=%f GeV", j, child_pdg, child->m()/1000.);
    }
    return StatusCode::FAILURE;
  }

  // Init.
  decay_Z.clear();

  //
  // Fill the decay info based on the vector of Z's children.
  // Note that the notation is:
  //   q1: positively charged e/mu/tau or quark/neutrino
  //   q2: negatively charged e/mu/tau or anti-quark/anti-neutrino
  //

  for(int j=0; j<(int)Zchildren.size(); j++){
    xAOD::TruthParticle *Z_child = Zchildren[j];
    if(!Z_child) continue;
    int Z_child_pdg = Z_child->pdgId();

    // e/mu
    if     (Z_child_pdg==-11 || Z_child_pdg==-13){
      decay_Z.pq1 = setTLV_fin(Z_child);	decay_Z.q1_pdg = Z_child_pdg;
    }
    else if(Z_child_pdg==11 || Z_child_pdg==13){
      decay_Z.pq2 = setTLV_fin(Z_child); decay_Z.q2_pdg = Z_child_pdg;
    }

    // tau
    else if(Z_child_pdg==-15){
      decay_Z.pq1    = setTLV_fin(Z_child);	  decay_Z.q1_pdg = Z_child_pdg;
      GetTauDecay(Z_child, decay_Z.tau1);
    }
    else if(Z_child_pdg==15){
      decay_Z.pq2    = setTLV_fin(Z_child);   decay_Z.q2_pdg = Z_child_pdg;
      GetTauDecay(Z_child, decay_Z.tau2);
    }

    // neutrino
    else if(Z_child_pdg==-12 || Z_child_pdg==-14 || Z_child_pdg==-16){
      decay_Z.pq1 = setTLV_fin(Z_child);            decay_Z.q1_pdg = Z_child_pdg;
    }
    else if(Z_child_pdg==12 || Z_child_pdg==14 || Z_child_pdg==16){
      decay_Z.pq2 = setTLV_fin(Z_child);            decay_Z.q2_pdg = Z_child_pdg;
    }

    // hadronic decay
    else if(std::abs(Z_child_pdg)<=6){
      if(Z_child_pdg<0) { decay_Z.pq1 = setTLV(Z_child); decay_Z.q1_pdg = Z_child_pdg; }
      else {              decay_Z.pq2 = setTLV(Z_child); decay_Z.q2_pdg = Z_child_pdg; }
    }
    else if(std::abs(Z_child_pdg)==111){
      decay_Z.pq1 = setTLV_fin(Z_child);
      decay_Z.q1_pdg = Z_child_pdg;
      break;
    }
    else if(std::abs(Z_child_pdg)==211){
      if(Z_child_pdg<0) { decay_Z.pq1 = setTLV(Z_child); decay_Z.q1_pdg = Z_child_pdg; }
      else {              decay_Z.pq2 = setTLV(Z_child); decay_Z.q2_pdg = Z_child_pdg; }
    }

    // break if all children are filled
    if(decay_Z.q1_pdg!=0 && decay_Z.q2_pdg!=0) break;

  } // end of loop over W's children


  // Check if proper children are found
  if(decay_Z.q1_pdg != 111 && (decay_Z.q1_pdg==0 || decay_Z.q2_pdg==0)){
    ERROR("DecayHandle::GetZDecay", "Z decay info is not fully filled.");
    decay_Z.print();
    return StatusCode::FAILURE;
  }

  // SFOS check
  if(decay_Z.q1_pdg != 111 && (decay_Z.q1_pdg+decay_Z.q2_pdg!=0)){
    ERROR("DecayHandle::GetZDecay", "pdg1+pdg2!=0. pdg1=%i, pdg2=%i. Clear and exit.", decay_Z.q1_pdg, decay_Z.q2_pdg);
    decay_Z.clear();
    return StatusCode::FAILURE;
  }

  //
  // Finalize
  //
  if(  decay_Z.P4.E()==0 ) decay_Z.P4 = decay_Z.pq1 + decay_Z.pq2 ;
  decay_Z.pdg = 23;

  decay_Z.finalize();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetZDecay(xAOD::TruthParticle *mcp_Z, Decay_boson &decay_Z){

  //
  // Retrive Z decay information.
  //   Input: xAOD::TruthParticle that has pdgId=23
  //

  // Sanity checkes
  if( !mcp_Z ) {
    ERROR("DecayHandle::GetZDecay", "Input xAOD::TruthParticle is null. Exit.");
    return StatusCode::FAILURE;
  }
  if( mcp_Z->pdgId()!=23 ){
    ERROR("DecayHandle::GetZDecay", "Input xAOD::TruthParticle is not Z (pdgId: %i). Exit.", mcp_Z->pdgId());
    return StatusCode::FAILURE;
  }

  // Skip all the self-decays
  xAOD::TruthParticle* fin_Z = descend(mcp_Z,23);
  decay_Z.P4 = setTLV(fin_Z);

  // Find the decay products
  std::vector<xAOD::TruthParticle *>Zchildren; Zchildren.clear();
  for(int j=0; j<(int)getNChildren(fin_Z); j++){
    xAOD::TruthParticle *Z_child = (xAOD::TruthParticle*)getChild(fin_Z,j);
    Zchildren.push_back(Z_child);
  }
  // Fill the information
  GetZDecay_Impl(Zchildren, decay_Z);

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetHDecay(xAOD::TruthParticle *mcp_H, Decay_H &decay_H){

  //
  // Retrive H decay information.
  //   Input: xAOD::TruthParticle that has pdgId=25
  //

  // Sanity checkes
  if( !mcp_H ) {
    ERROR("DecayHandle::GetHDecay", "Input xAOD::TruthParticle is null. Exit.");
    return StatusCode::FAILURE;
  }
  if( mcp_H->pdgId()!=25 ){
    ERROR("DecayHandle::GetHDecay", "Input xAOD::TruthParticle is not H (pdgId: %i). Exit.", mcp_H->pdgId());
    return StatusCode::FAILURE;
  }

  // Init.
  decay_H.clear();
  decay_H.P4 = setTLV(mcp_H);
  Decay_boson   W1, W2;      W1.clear();    W2.clear();
  Decay_boson   Z1, Z2;      Z1.clear();    Z2.clear();

  //
  // Fill the decay info based on H's children.
  // Note that the notation is:
  //   q1: positively charged e/mu/tau/W or quark/neutrino, first Z/g/y appearing in the loop
  //   q2: negatively charged e/mu/tau/W or anti-quark/anti-neutrino, second Z/g/y appearing in the loop
  //

  xAOD::TruthParticle* fin_H = descend(mcp_H,0);
  for(int j=0; j<(int)getNChildren(fin_H); j++){
    xAOD::TruthParticle *H_child = (xAOD::TruthParticle*)getChild(fin_H,j);
    int H_child_pdg = H_child->pdgId();

    // WW
    if(H_child_pdg==-24) { // W+
      decay_H.pq1    = setTLV_fin(H_child);  decay_H.q1_pdg = H_child_pdg;
      GetWDecay(H_child, W1);
    }
    else if(H_child_pdg==24) { // W-
      decay_H.pq2    = setTLV_fin(H_child);  decay_H.q2_pdg = H_child_pdg;
      GetWDecay(H_child, W2);
    }

    // ZZ
    else if(H_child_pdg==23){
      if(!Z1.pdg) {
	decay_H.pq1    = setTLV_fin(H_child);  decay_H.q1_pdg = H_child_pdg;
	GetZDecay(H_child, Z1);
      }
      else{
	decay_H.pq2    = setTLV_fin(H_child);  decay_H.q2_pdg = H_child_pdg;
	GetZDecay(H_child, Z2);
      }
    }

    // tautau
    else if(H_child_pdg==-15) { // tau+
      decay_H.pq1    = setTLV_fin(H_child); decay_H.q1_pdg = H_child_pdg;
      GetTauDecay(H_child, decay_H.tau1);
    }
    else if(H_child_pdg==15) { // tau-
      decay_H.pq2    = setTLV_fin(H_child); decay_H.q2_pdg = H_child_pdg;
      GetTauDecay(H_child, decay_H.tau2);
    }

    // ee/mumu
    else if(H_child_pdg==-11 || H_child_pdg==-13) { // l+
      decay_H.pq1    = setTLV_fin(H_child); decay_H.q1_pdg = H_child_pdg;
    }
    else if(H_child_pdg==11 || H_child_pdg==13) { // l-
      decay_H.pq2    = setTLV_fin(H_child); decay_H.q2_pdg = H_child_pdg;
    }

    // gluon-gluon/gamma-gamma. Define the first appearing one as q1.
    else if(H_child_pdg==21 || H_child_pdg==22){
      if(!decay_H.q1_pdg){  decay_H.pq1    = setTLV_fin(H_child);   decay_H.q1_pdg = H_child_pdg; }
      else               {  decay_H.pq2    = setTLV_fin(H_child);   decay_H.q2_pdg = H_child_pdg; }
    }

    // qq (tt,bb,cc,ss,uu,dd) and vv. Define the pdgId>0 one as q1.
    else{
      if(H_child_pdg>0){ decay_H.pq1 = setTLV_fin(H_child); decay_H.q1_pdg = H_child_pdg; }
      else             { decay_H.pq2 = setTLV_fin(H_child); decay_H.q2_pdg = H_child_pdg; }
    }

    // break if all children are filled
    if(decay_H.q1_pdg!=0 && decay_H.q2_pdg!=0) break;

  } // end of loop over H's children

  // Check if proper children are found
  if(decay_H.q1_pdg==0 || decay_H.q2_pdg==0){
    ERROR("DecayHandle::GetHDecay", "H decay info is not fully filled.");
    decay_H.print();
    return StatusCode::FAILURE;
  }

  // finalize
  decay_H.finalize(W1,W2,Z1,Z2);

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
int DecayHandle::GetHiggsDecayLabel(){

  //
  // Retrive H decay label in case there is only one higgs in the event.
  //    To do: warning in case of multiple higgses in the event.
  //

    const xAOD::TruthParticleContainer* cont =
      m_truthParticles ? m_truthParticles :
      m_truthBosonWDP ? m_truthBosonWDP :
      0;
    if(!cont) return StatusCode::FAILURE;

  // +++++ Find higgs ++++++ //
  for(int idx=0, n=(int)cont->size(); idx<n; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
    if(mcp->pdgId()!=25) continue;
    xAOD::TruthParticle * fin_higgs = descend(mcp,0);
    Decay_H H;  GetHDecay(fin_higgs, H);
    return H.decayLabel;
  }

  // No higgs. Exit in tear.
  return 0;
}
//////////////////////////////////////////////////
xAOD::TruthParticle* DecayHandle::GetChildEWG(xAOD::TruthParticle* mcp_susy){

  //
  // Find EW gauginos (C1/C2/N1/N2/N3/N4) from mcp_susy children.
  //

  xAOD::TruthParticle* fin_susy = descend(mcp_susy,0);

  for(int j=0; j<(int)getNChildren(fin_susy); j++){
    xAOD::TruthParticle *child = (xAOD::TruthParticle*)getChild(fin_susy,j);
    int child_pdg = child->pdgId();
    if(mcp_susy->pdgId()==child_pdg){
      WARNING("DecayHandle::GetChildEWG", "Self-decay is still found! (pdgId=%i, pdgId(fin)=%i, status=%i). Something is going wrong.", mcp_susy->pdgId(), fin_susy->pdgId(), fin_susy->status());
      continue;
    }

    if(isEWG(child_pdg)) return child;
  }

  // If nothing's found
  WARNING("DecayHandle::GetChildEWG", "Could not find N1/N2/N3/N4/C1/C2 from decay of input particle (pdgId=%i, nChildren=%i).", mcp_susy->pdgId(), getNChildren(mcp_susy));

  for(int j=0; j<(int)getNChildren(fin_susy); j++){
    xAOD::TruthParticle *child = (xAOD::TruthParticle*)getChild(fin_susy,j);
    WARNING("DecayHandle::GetChildEWG",  "   child %i:   pdgId=%i, status=%i, m=%f GeV, nChildren=%i", j, child->pdgId(), child->status(), child->m()/1000., child->nChildren());
  }

  return 0;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetEWGDecayViaW(xAOD::TruthParticle *mcp_chi, Decay_boson &W){

  //
  // Fill W(*) part of decay information for EWK gaugino that is supposed to emit W(*).
  //  e.g. C1->W N1,  N2->C1 W
  //

  if(!mcp_chi              ){    return StatusCode::FAILURE;  }
  if(!isEWG(mcp_chi->pdgId())){    return StatusCode::FAILURE;  }

  // Fill W
  W.clear();

  xAOD::TruthParticle* fin_W = descend(mcp_chi,24);
  //cout << "DecayHandle::GetEWGDecayViaW  DEBUG  pdgId of fin_W: " << fin_W->pdgId() << std::endl;

  std::vector<xAOD::TruthParticle *>Wchildren; Wchildren.clear();
  for(int j=0; j<(int)getNChildren(fin_W); j++){
    xAOD::TruthParticle *W_child = (xAOD::TruthParticle*)getChild(fin_W,j);
    Wchildren.push_back(W_child);
  }
  GetWDecay_Impl(Wchildren,W);

  //std::cout << "DecayHandle::GetEWGDecayViaW  DEBUG  " << std::endl;  W.print();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetEWGDecayViaZ(xAOD::TruthParticle *mcp_chi, Decay_boson &Z){

  //
  // Fill Z(*) part of decay information for EWK gaugino that is supposed to emit Z(*).
  //  e.g. N2->Z N1,  C2->Z C1
  //

  if(!mcp_chi              ){    return StatusCode::FAILURE;  }
  if(!isEWG(mcp_chi->pdgId())){    return StatusCode::FAILURE;  }

  // Fill Z
  Z.clear();

  xAOD::TruthParticle* fin_Z = descend(mcp_chi,23);
  //cout << "DecayHandle::GetEWGDecayViaZ  DEBUG  pdgId of fin_Z: " << fin_Z->pdgId() << std::endl;

  std::vector<xAOD::TruthParticle *>Zchildren; Zchildren.clear();
  for(int j=0; j<(int)getNChildren(fin_Z); j++){
    xAOD::TruthParticle *Z_child = (xAOD::TruthParticle*)getChild(fin_Z,j);
    Zchildren.push_back(Z_child);
  }
  GetZDecay_Impl(Zchildren,Z);

  //std::cout << "DecayHandle::GetEWGDecayViaZ  DEBUG  " << std::endl;  Z.print();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetEWGDecayViaH(xAOD::TruthParticle *mcp_chi, Decay_H &H){

  //
  // Fill H part of decay information for EWK gaugino that is supposed to emit H.
  //  e.g. N2->H N1,  C2->H C1
  //

  if(!mcp_chi              ){    return StatusCode::FAILURE;  }
  if(!isEWG(mcp_chi->pdgId())){    return StatusCode::FAILURE;  }

  // Fill H
  H.clear();

  bool foundH=false;
  xAOD::TruthParticle* fin_chi = descend(mcp_chi,0);
  for(int j=0; j<(int)getNChildren(fin_chi); j++){
    xAOD::TruthParticle *chi_child = (xAOD::TruthParticle*)getChild(fin_chi,j);
    if(chi_child->pdgId()==25){
      xAOD::TruthParticle* fin_H = descend(chi_child,0);
      GetHDecay(fin_H,H);
      foundH=true;
      break;
    }
  }

  if(!foundH) return StatusCode::FAILURE;

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetEWGDecay(xAOD::TruthParticle *mcp_chi, tlv &pchi_child, int &chi_child_pdg, Decay_boson &boson, tlv &pN1){

  //
  // Retrieve EW gaugino decay information.
  //  Slepton mediated decay is not supported yet.
  //
  // Input:
  //    mcp_chi   : parent gaugino
  // Output:
  //    pchi_child   : 4-momentum of child gaugino.
  //    chi_child_pdg: pdgId of child gaugino.
  //    boson        : W/Z/H associated with the 1st decay.
  //    pN1          : 4-momentum of final state N1.
  //                   Will differ from pchi_child in case of multi-step decay.
  //
  //  To be supported (low priority though):
  //    - Slepton mediated decay.
  //    - Associated bosons in the >=2-step decays.
  //


  // Init.
  pchi_child.SetXYZM(0,0,0,0);
  chi_child_pdg=0;
  boson.clear();
  pN1.SetXYZM(0,0,0,0);

  // If already N1 -> finish.
  int chi_pdg = mcp_chi->pdgId();
  if(chi_pdg==1000022) {  pchi_child = setTLV_fin(mcp_chi); return StatusCode::SUCCESS; }

  // ------- Retrieve child EW gaugino ---- //
  xAOD::TruthParticle* fin_chi = descend(mcp_chi,0);
  xAOD::TruthParticle* chi_child = GetChildEWG(fin_chi);

  if(!chi_child){
    ERROR("DecayHandle::GetEWGDecay", "Could not retrieve child gaugino. Exit.");
    return StatusCode::FAILURE;
  }

  pchi_child    = setTLV(chi_child);
  chi_child_pdg = chi_child->pdgId();
  // ------------

  // Check if the decay chain to N1 ends here.
  if(chi_child_pdg==1000022) pN1 = pchi_child;
  else{
    // >=2-step decay.
    INFO("DecayHandle::GetEWGDecay", ">=2-step decaying EW gaugino found! (%i->%i->...)", chi_pdg, chi_child_pdg);

    // --- Retrieve info for of next decay
    xAOD::TruthParticle* fin_chi_child = descend(chi_child,0);
    xAOD::TruthParticle* chi_grandchild = GetChildEWG(fin_chi_child);
    if(!chi_grandchild){
      ERROR("DecayHandle::GetEWGDecay", "Could not retrieve child gaugino. Exit.");
      return StatusCode::FAILURE;
    }
    tlv pchi_grandchild    = setTLV(chi_grandchild);
    int chi_grandchild_pdg = chi_grandchild->pdgId();
    // -----
    if(chi_grandchild_pdg==1000022) pN1 = pchi_grandchild;
    else{
      // >=3-step decay. Currently not supported.....
      INFO("DecayHandle::GetEWGDecay", ">=3-step decaying EW gaugino found! (%i->%i->%i->...). Currently not supported... pN1 will be left empty.", chi_pdg, chi_child_pdg, chi_grandchild_pdg);

    } // end of if(>=3-step decay)
  } // end of if(>=2-step decay)


  // Guess the species of associated boson based on the types of parent/child gaugino.
  bool isC = isChargino(chi_pdg);
  bool isN = isNeutralino(chi_pdg);
  bool isChildC = isChargino(chi_child_pdg);
  bool isChildN = isNeutralino(chi_child_pdg);

  //std::cout << "DecayHandle::GetEWGDecay  DEBUG  chi_pdg: "  << chi_pdg << "  isC: " << isC << "  isN: " << isN << "  isChildC: " << isChildC << "  isChildN: " << isChildN << std::endl;
  // -------- via W ----------
  if( (isC && isChildN) || (isN && isChildC)){
    Decay_boson W;
    if(GetEWGDecayViaW(fin_chi, W))  boson = W;
    else
      ERROR("DecayHandle::GetEWGDecay", "Could not find associated W decay. Parent pdg: %i, chidl_pdg: %i.", chi_pdg, chi_child_pdg);
  }

  // -------- via Z/H ----------
  else if( (isN && isChildN) || (isC && isChildC)){
    Decay_H H;    Decay_boson Z;
    if     ( GetEWGDecayViaH(fin_chi, H) ) boson = (Decay_boson) H;
    else if( GetEWGDecayViaZ(fin_chi, Z) ) boson = Z;
    else
      ERROR("DecayHandle::GetEWGDecay", "Could not find associated Z/H decay. Parent pdg: %i, chidl_pdg: %i.", chi_pdg, chi_child_pdg);

  }
  // ----- unknown ----------- //
  else{
    ERROR("DecayHandle::GetEWGDecay", "Weird gaugino-gauge boson combination. Parent pdg: %i, chidl_pdg: %i.", chi_pdg, chi_child_pdg);
    ERROR("DecayHandle::GetEWGDecay", "isC: %i, isN: %i, isChildC: %i, isChildN: %i. Exit.", isC, isN, isChildC, isChildN);
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetEWGDecayGGMZh(xAOD::TruthParticle *mcp_chi, tlv &pG, int &chi_child_pdg, Decay_boson &boson){

  //
  // Retrieve EW gaugino decay into a W/Z/h + a gravitino
  //
  // Input:
  //    mcp_chi   : parent gaugino
  // Output:
  //    pG        : 4-momentum of child gravitino
  //    boson     : W/Z/H associated with the 1st decay.
  //

  // Init.
  pG.SetXYZM(0,0,0,0);
  boson.clear();

  // ------- Retrieve child N1 decaying gravitino ---- //
  xAOD::TruthParticle* fin_chi = descend(mcp_chi,0);
  xAOD::TruthParticle* fin_N1 = 0;

  // If the input is already N1
  if(fin_chi->pdgId()==1000022){
    fin_N1 = fin_chi;
  }

  // Otherwise trace the decay chain
  else {
    xAOD::TruthParticle* current = fin_chi;
    while(current->pdgId()!=1000022){
      xAOD::TruthParticle* child = GetChildEWG(current);
      if(!child)                  break; // current has no EWG children

      current = descend(child,0);
      if(getNChildren(current)==0) break; // current is stable but not N1
    }

    if(current->pdgId()!=1000022){
      ERROR("DecayHandle::GetEWGDecayGGMZh", "Could not retrieve child N1 from parent (pdgId=%i). Exit.", mcp_chi->pdgId());
      return StatusCode::FAILURE;
    }
    fin_N1 = current;
  }

  // ------- Retrieve child gravitino ---- //
  xAOD::TruthParticle* gravitino = 0;
  for(int j=0; j<(int)getNChildren(fin_N1); j++){
    xAOD::TruthParticle *child = (xAOD::TruthParticle*)getChild(fin_N1,j);
    int child_pdg = child->pdgId();
    if(child_pdg==1000039) gravitino = (xAOD::TruthParticle*) child;      // Gravitino = 1000039
  }

  if(!gravitino){
    ERROR("DecayHandle::GetEWGDecayGGMZh", "Could not retrieve child gravitino. Exit.");
    return StatusCode::FAILURE;
  }

  chi_child_pdg = 1000039;
  pG = setTLV(gravitino);

  // -------- Retrieve decay via Z/H ----------
  Decay_H H;    Decay_boson Z;
  if     ( GetEWGDecayViaH(fin_chi, H) ) boson = (Decay_boson) H;
  else if( GetEWGDecayViaZ(fin_chi, Z) ) boson = Z;
  else
    ERROR("DecayHandle::GetEWGDecayGGNZh", "Could not find associated Z/H decay.");


  return StatusCode::SUCCESS;
}


//////////////////////////////////////////////////
StatusCode DecayHandle::GetGluinoDecay(xAOD::TruthParticle *mcp_go,
				       tlv &pq1, int &q1_pdg, tlv &pq2, int &q2_pdg,
				       tlv &pchi, int &chi_pdg, Decay_boson &boson, tlv &pN1){

  //
  // Retrieve gluino decay information.
  //  Consider g~ -> q1 q2 chi   where q1, q1 are (anti-)quarks and chi is EW gaugino.
  //
  // Input:
  //    mcp_go   : parent gluino
  // Output:
  //    pq1(q2)      : 4-momentum of q1 (q2).
  //    q1(q2)_pdg   : pdgId of q1 (q2).
  //    pchi         : 4-momentum of the EW gaugino.
  //    chi_pdg      : pdgId of the EW gaugino.
  //    boson        : W/Z/H associated to the EW gaugino decay.
  //    pN1          : 4-momentum of final state N1 from the EW gaugino decay chain.
  //                   Will differ from pchi in case of multi-step EW gaugino decay.
  //

  pchi.SetXYZM(0,0,0,0);
  chi_pdg=0;
  boson.clear();
  pN1.SetXYZM(0,0,0,0);

  // ++++ Retrieve gluino's children +++++ //
  for(int j=0; j<(int)getNChildren(mcp_go); j++){
    xAOD::TruthParticle *go_child = (xAOD::TruthParticle*) getChild(mcp_go,j);
    int go_child_pdg = go_child->pdgId();

    // -------- quarks from gluino
    if(std::abs(go_child_pdg)>=1 && std::abs(go_child_pdg)<=6){
      if(go_child_pdg>0) {
	pq1 = setTLV_fin(go_child);   q1_pdg=go_child_pdg;
      }
      else{
	pq2 = setTLV_fin(go_child);  q2_pdg=go_child_pdg;
      }
    }

    // ------ EW gaugino from gluino -------
    else if(isEWG(go_child_pdg)){
      tlv pchi_child(0,0,0,0);
      int chi_child_pdg=0;
      GetEWGDecay(go_child, pchi_child, chi_child_pdg, boson, pN1);

      if(chi_child_pdg!=1000022)
	WARNING("DecayHandle::GetGluinoDecay","EW gaugino decaying from gluino (pdgId: %i) decays into pdgId=%i istead N1 (pdgId=1000022). Decay info will be only fully stored upto the first decay.", go_child->pdgId(), chi_child_pdg);
    }

  } // end of loop over gluino's children

  if(!pN1.E())
    WARNING("DecayHandle::GetGluinoDecay", "Could not retrieve pN1!");
  if(!pq1.E())
    WARNING("DecayHandle::GetGluinoDecay", "Could not retrieve pq1!");
  if(!pq2.E())
    WARNING("DecayHandle::GetGluinoDecay", "Could not retrieve pq2!");

  return StatusCode::SUCCESS;
}


/////////////////////////////////////////////////////////////////////
//
// Methods to retrieve full decay chains of pair produced particles
//
/////////////////////////////////////////////////////////////////////

StatusCode DecayHandle::GetDecayChain_VV(TruthEvent_VV *evt){

  bool IsVVLep =
    ProcClassifier::IsZZ0L4Nu(m_DSID)
    ||ProcClassifier::IsWZ1L3Nu(m_DSID)
    ||ProcClassifier::IsWW2L2Nu(m_DSID)
    ||ProcClassifier::IsZZ2L2Nu(m_DSID)
    ||ProcClassifier::IsWZ3L1Nu(m_DSID)
    ||ProcClassifier::IsZZ4L0Nu(m_DSID);

  if(m_truthParticles && IsVVLep)  return GetDecayChain_VV_FullLep(evt);

  if      (ProcClassifier::IsWW(m_DSID) || ProcClassifier::IsWt(m_DSID))  return GetDecayChain_WW(evt);
  else if (ProcClassifier::IsWZ(m_DSID) || ProcClassifier::IsZt(m_DSID))  return GetDecayChain_WZ(evt);
  else if (ProcClassifier::IsZZ(m_DSID))                                  return GetDecayChain_ZZ(evt);
  else if (ProcClassifier::IsWH(m_DSID))                                  return GetDecayChain_WH(evt);
  else if (ProcClassifier::IsZH(m_DSID))                                  return GetDecayChain_ZH(evt);
  else if (ProcClassifier::IsHH(m_DSID))                                  return GetDecayChain_HH(evt);
  else {
    ERROR("DecayHandle::GetDecayChain_VV","DSID %i is not registered as either WW/tW/WZ/tZ/ZZ/WH/ZH/HH ProcClassifier.h. Exit.", m_DSID);
  }
  return StatusCode::SUCCESS;
}

///////////////////////////////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_WW(TruthEvent_VV *evt){

  //
  // Retrive decay chain of WW. Applicable also for t+W.
  //

  if( isSherpa() && ProcClassifier::IsWW2L2Nu(m_DSID) ) GetDecayChain_WW2L2Nu_Sherpa(evt);
  else{

    evt->clear();

    const xAOD::TruthParticleContainer* cont =
      m_truthParticles ? m_truthParticles :
      m_truthBosonWDP ? m_truthBosonWDP :
      0;
    if(!cont) return StatusCode::FAILURE;

    // +++++ find Ws ++++++ //
    xAOD::TruthParticle * fin_Wplus=0, *fin_Wminus=0;
    for(int idx=0, nmcp=(int)cont->size(); idx<nmcp; idx++) {
      xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
      int pdg = mcp->pdgId();
      // get W+,W-
      if(pdg==24 && fin_Wplus==0) fin_Wplus = descend(mcp,0);
      if(pdg==-24 && fin_Wminus==0) fin_Wminus = descend(mcp,0);
      if(fin_Wplus!=0 && fin_Wminus!=0) break;
    }

    // +++++ fill decays ++++++ //
    Decay_boson Wplus;   GetWDecay(fin_Wplus, Wplus);
    Decay_boson Wminus;  GetWDecay(fin_Wminus, Wminus);

    evt->set_evt(Wplus, Wminus);
    evt->finalize();
  }

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_WW2L2Nu_Sherpa(TruthEvent_VV *evt){

  //
  // Retrive decay chain of WW for Sherpa llvv samples.
  // lv and the parent W can be uniquely associated based on the charge and lepton number (nu or nubar)
  //

  evt->clear();

  // Retrive taus from WW
  std::vector<xAOD::TruthParticle*> v_taus;
  std::vector<Decay_tau> v_tauDecays;
  GetTaus_SherpaBoson(v_taus, v_tauDecays);

  // Retrive e/mu/nu from WW but not from taus
  std::vector<xAOD::TruthParticle*> v_eles, v_mus, v_nus;
  GetNonTauElectrons_SherpaBoson(v_eles, v_tauDecays);
  GetNonTauMuons_SherpaBoson(v_mus, v_tauDecays);
  GetNonTauNeutrlinos_SherpaBoson(v_nus, v_tauDecays);

  // Sanity checks
  const int nEles = v_eles.size();
  const int nMus = v_mus.size();
  const int nTaus = v_taus.size();
  const int nNus = v_nus.size();
  if( !( nEles+nMus+nTaus==2 && nNus==2 ) ){
    ERROR("DecayHandle::GetDecayChain_WW2L_Sherpa", "Illegal (nEles,nMus,nTaus,nNu)=(%i,%i,%i,%i) found (supposed to be nEles==2+nMus+nTaus==2 && nNu==2). Exit.", nEles, nMus, nTaus, nNus);
    return StatusCode::FAILURE;
  }

  //
  // Pairing WW
  //

  std::vector<xAOD::TruthParticle*> v_lepnus;
  for(auto ele : v_eles) v_lepnus.push_back(ele);
  for(auto mu : v_mus)   v_lepnus.push_back(mu);
  for(auto tau : v_taus) v_lepnus.push_back(tau);
  for(auto nu : v_nus)   v_lepnus.push_back(nu);

  Decay_boson   Wp, Wm;      Wp.clear(); Wm.clear();
  for(auto lepnu : v_lepnus){
    int pdg = lepnu->pdgId();

    if(pdg==-11 || pdg==-13){
      Wp.pq1 = setTLV_fin(lepnu);  Wp.q1_pdg = pdg;
    }
    else if(pdg==-15){
      Wp.pq1 = setTLV_fin(lepnu);  Wp.q1_pdg = pdg;
      GetTauDecay(lepnu, Wp.tau1);
    }
    else if(pdg==12 || pdg==14 || pdg==16){
      Wp.pq2 = setTLV_fin(lepnu);  Wp.q2_pdg = pdg;
    }

      // --------
    else if(pdg==11 || pdg==13){
      Wm.pq1 = setTLV_fin(lepnu);  Wm.q1_pdg = pdg;
    }
    else if(pdg==15){
      Wm.pq1 = setTLV_fin(lepnu);      Wm.q1_pdg = pdg;
      GetTauDecay(lepnu, Wm.tau1);
    }
    else if(pdg==-12 || pdg==-14 || pdg==-16){
      Wm.pq2 = setTLV_fin(lepnu);  Wm.q2_pdg = pdg;
    }

  }
  Wp.P4 = Wp.pq1+Wp.pq2;
  Wp.pdg = 24;
  Wp.finalize();

  Wm.P4 = Wm.pq1+Wm.pq2;
  Wm.pdg = -24;
  Wm.finalize();

  evt->set_evt(Wp, Wm);
  evt->finalize();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_WZ(TruthEvent_VV *evt){

  //
  // Retrieve decay chain of WZ. Applicable also for t+Z.
  //

  if     ( isSherpa() && ProcClassifier::IsWZ3L1Nu(m_DSID) ) GetDecayChain_WZ3L1Nu_Sherpa(evt);
  else if( isSherpa() && ProcClassifier::IsWZ1L3Nu(m_DSID) ) GetDecayChain_WZ1L3Nu_Sherpa(evt);
  else{

    const xAOD::TruthParticleContainer* cont =
      m_truthParticles ? m_truthParticles :
      m_truthBosonWDP ? m_truthBosonWDP :
      0;
    if(!cont) return StatusCode::FAILURE;

    evt->clear();

    Decay_boson W; W.clear();
    Decay_boson Z; Z.clear();
    for(int idx=0, nmcp=(int)cont->size(); idx<nmcp; idx++) {
      xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
      int pdg = mcp->pdgId();

      if(std::abs(pdg)==24){
	xAOD::TruthParticle* fin_W = descend(mcp,0);
	GetWDecay(fin_W, W);
      }
      else if(pdg==23){
	xAOD::TruthParticle* fin_Z = descend(mcp,0);
	GetZDecay(fin_Z, Z);
      }
      else continue;
    } // end of mcp loop

    // pMis_mes = W.pMis+Z.pMis:: ad hoc approximation. to be fixed.
    evt->set_evt(W, Z);
  }

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_WZ3L1Nu_Sherpa(TruthEvent_VV *evt){

  //
  // Retrive decay chain of WZ for Sherpa lllv samples.
  // lv <-> W association can be done uniquely in case of flavor combination of e+mumu/mu+ee.
  // It is not possible in case of eee/mmm, where the assignment that satisfied the on-shell WZ condition the most is taken.
  //

  evt->clear();

  // Retrive taus
  std::vector<xAOD::TruthParticle*> v_taus;
  std::vector<Decay_tau> v_tauDecays;
  GetTaus_SherpaBoson(v_taus, v_tauDecays);

  // Retrive e/mu/nu not from taus
  std::vector<xAOD::TruthParticle*> v_eles, v_mus, v_nus;
  GetNonTauElectrons_SherpaBoson(v_eles, v_tauDecays);
  GetNonTauMuons_SherpaBoson(v_mus, v_tauDecays);
  GetNonTauNeutrlinos_SherpaBoson(v_nus, v_tauDecays);

  std::vector<xAOD::TruthParticle*> v_leptau;
  v_leptau.insert(v_leptau.end(), v_eles.begin(), v_eles.end());
  v_leptau.insert(v_leptau.end(),  v_mus.begin(),  v_mus.end());
  v_leptau.insert(v_leptau.end(), v_taus.begin(), v_taus.end());

  // apply requirement on number of objects (3 leptons and 1 neutrino)
  if(v_nus.size()!=1) {
    WARNING("DecayHandle::GetDecayChain_WZ3L1Nu_Sherpa", "Wrong number of neutrions : %i (should be 1). Exit.", v_nus.size());
    return 0;
  }
  if(v_leptau.size()!=3) {
    WARNING("DecayHandle::GetDecayChain_WZ3L1Nu_Sherpa", "Wrong number of lep+tau %i.(should be 3). Exit.", v_leptau.size());
    return 0;
  }

  if( v_leptau[0]->pdgId()*v_leptau[1]->pdgId()*v_leptau[2]->pdgId()*v_nus[0]->pdgId()<0 ){
    WARNING("DecayHandle::GetDecayChain_WZ3L1Nu_Sherpa", "Tripply charged 3 leptons found: ");
    std::cout << " lep/tau1 pdg: " << v_leptau[0]->pdgId() << std::endl;
    std::cout << " lep/tau2 pdg: " << v_leptau[1]->pdgId() << std::endl;
    std::cout << " lep/tau3 pdg: " << v_leptau[2]->pdgId() << std::endl;
    std::cout << " nu pdg:       " << v_nus[0]->pdgId() << std::endl;
  }

  // Find the most plausible pairing based on the invariant mass of lnu and ll.
  double min_chisq=1e10;
  double mW=0., mZ=0.;
  xAOD::TruthParticle *Zl1=0, *Zl2=0, *Wl=0;
  for(int il=0, nl=v_leptau.size(); il<nl; ++il){
    tlv ptmp_Zl1 = setTLV(v_leptau[il]);
    tlv ptmp_Zl2 = setTLV(v_leptau[(il+1)%3]);
    tlv ptmp_Wl  = setTLV(v_leptau[(il+2)%3]);

    // pickup W,Z-like pairing only
    if(v_leptau[il]->pdgId()+v_leptau[(il+1)%3]->pdgId()!=0)  continue;
    if(! (std::abs(v_leptau[(il+2)%3]->pdgId()+v_nus[0]->pdgId())==1 && std::abs(v_nus[0]->pdgId())-std::abs(v_leptau[(il+2)%3]->pdgId())==1)  ) continue;

    double tmp_mW = (ptmp_Wl+setTLV(v_nus[0])).M();
    double tmp_mZ = (ptmp_Zl1+ptmp_Zl2).M();
    double chisq = pow(mW-80.4,2)/pow(2.0,2) + pow(mZ-91.2,2)/pow(2.4,2); // (m-mean)^2/width^2

    if(chisq<min_chisq){
      min_chisq = chisq;
      Zl1 = v_leptau[il];
      Zl2 = v_leptau[(il+1)%3];
      Wl  = v_leptau[(il+2)%3];
      mW  = tmp_mW;
      mZ  = tmp_mZ;
    }
  }

  if(Wl==0 || Zl1==0 || Zl2==0){
    ERROR("DecayHandle::GetDecayChain_WZ3L1Nu_Sherpa", "Association fails. No SFOS or W->lnu paring in the event:");
    std::cout << " lep/tau1 pdg: " << v_leptau[0]->pdgId() << std::endl;
    std::cout << " lep/tau2 pdg: " << v_leptau[1]->pdgId() << std::endl;
    std::cout << " lep/tau3 pdg: " << v_leptau[2]->pdgId() << std::endl;
    std::cout << " nu pdg:       " << v_nus[0]->pdgId() << std::endl;
    std::cout << " mW:           " << mW << std::endl;
    std::cout << " mZ:           " << mZ << std::endl;
    std::cout << " min_chisq:    " << min_chisq << std::endl;
    std::cout << "Exit." << std::endl;
    return StatusCode::FAILURE;
  }

  // Fill
  Decay_boson W;  W.pq2 = setTLV(v_nus[0]);  W.q2_pdg = v_nus[0]->pdgId();
  if(std::abs(Wl->pdgId())==15){
    W.pq1 = setTLV_fin(Wl);  W.q1_pdg = Wl->pdgId();
    GetTauDecay(Wl,W.tau1);
  }
  else { W.pq1 = setTLV_fin(Wl);  W.q1_pdg = Wl->pdgId(); }
  W.P4 = (W.pq1+W.pq2);
  W.pdg = W.q1_pdg > 0 ? -24 : 24;
  W.finalize();

  Decay_boson Z;  Z.clear();
  if     (Zl1->pdgId()==-15){ Z.pq1 = setTLV_fin(Zl1);  Z.q1_pdg = Zl1->pdgId();  GetTauDecay(Zl1,Z.tau1); } //tau+
  else if(Zl1->pdgId()==15) { Z.pq2 = setTLV_fin(Zl1);  Z.q2_pdg = Zl1->pdgId();  GetTauDecay(Zl1,Z.tau2); } //tau-
  else if(Zl1->pdgId()<0)   { Z.pq1 = setTLV_fin(Zl1);  Z.q1_pdg = Zl1->pdgId(); }  // l+
  else                      { Z.pq2 = setTLV_fin(Zl1);  Z.q2_pdg = Zl1->pdgId(); }  // l-

  if     (Zl2->pdgId()==-15){ Z.pq1 = setTLV_fin(Zl2);  Z.q1_pdg = Zl2->pdgId();  GetTauDecay(Zl2,Z.tau1); } //tau+
  else if(Zl2->pdgId()==15) { Z.pq2 = setTLV_fin(Zl2);  Z.q2_pdg = Zl2->pdgId();  GetTauDecay(Zl2,Z.tau2); } //tau-
  else if(Zl2->pdgId()<0)   { Z.pq1 = setTLV_fin(Zl2);  Z.q1_pdg = Zl2->pdgId(); }  // l+
  else                      { Z.pq2 = setTLV_fin(Zl2);  Z.q2_pdg = Zl2->pdgId(); }  // l-

  Z.P4 = (Z.pq1+Z.pq2);
  Z.pdg = 23;
  Z.finalize();

  evt->set_evt(W, Z);

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_WZ1L3Nu_Sherpa(TruthEvent_VV *evt){

  //
  // Retrive decay chain of WZ for Sherpa lvvv samples.
  //

  evt->clear();

  // Retrive taus from WZ
  std::vector<xAOD::TruthParticle*> v_taus;
  std::vector<Decay_tau> v_tauDecays;
  GetTaus_SherpaBoson(v_taus, v_tauDecays);

  // Retrive e/mu/nu from WZ but not from taus
  std::vector<xAOD::TruthParticle*> v_eles, v_mus, v_nus;
  GetNonTauElectrons_SherpaBoson(v_eles, v_tauDecays);
  GetNonTauMuons_SherpaBoson(v_mus, v_tauDecays);
  GetNonTauNeutrlinos_SherpaBoson(v_nus, v_tauDecays);

  // Sanity checks
  const int nLeps = v_eles.size()+v_mus.size()+v_taus.size();
  const int nNus = v_nus.size();
  if( !( nLeps==1 && nNus==3 ) ){
    ERROR("DecayHandle::GetDecayChain_WZ1L3Nu_Sherpa", "Illegal nEles+nMus+nTaus=%i/nNu=%i found (supposed to be 1/3). Exit.", nLeps, nNus);
    return StatusCode::FAILURE;
  }

  //
  // lvvv->WZ paring
  //

  xAOD::TruthParticle* lep = v_eles.size()==1 ? v_eles[0] : v_mus.size()==1 ? v_mus[0] : v_taus[0];

  // Find the most plausible pairing based on the invariant mass of lnu and ll.
  xAOD::TruthParticle* mcp_Wv=0;
  xAOD::TruthParticle* mcp_Zv1=0;
  xAOD::TruthParticle* mcp_Zv2=0;

  double min_chisq=1e10;
  double mW=0., mZ=0.;
  for(int il=0, nl=v_nus.size(); il<nl; ++il){

    // pickup W,Z-like pairing only
    if(v_nus[il]->pdgId()+v_nus[(il+1)%3]->pdgId()!=0)  continue;
    if(! (std::abs(v_nus[(il+2)%3]->pdgId()+lep->pdgId())==1 && std::abs(lep->pdgId())-std::abs(v_nus[(il+2)%3]->pdgId())==-1)  ) continue;

    double tmp_mW = (setTLV(v_nus[(il+2)%3])+setTLV(lep)).M();
    double tmp_mZ = (setTLV(v_nus[il])+setTLV(v_nus[(il+1)%3])).M();
    double chisq = pow(mW-80.4,2)/pow(2.0,2) + pow(mZ-91.2,2)/pow(2.4,2); // (m-mean)^2/width^2

    if(chisq<min_chisq){
      min_chisq = chisq;
      mcp_Zv1 = v_nus[il];
      mcp_Zv2 = v_nus[(il+1)%3];
      mcp_Wv  = v_nus[(il+2)%3];
      mW  = tmp_mW;
      mZ  = tmp_mZ;
    }
  }

  if(mcp_Wv==0 || mcp_Zv1==0 || mcp_Zv2==0){
    ERROR("DecayHandle::GetDecayChain_WZ1L3Nu_Sherpa", "Association fails. No SFOS or W->lnu paring in the event:");
    std::cout << " lep pdg:      " << lep->pdgId() << std::endl;
    std::cout << " nu1 pdg:      " << v_nus[0]->pdgId() << std::endl;
    std::cout << " nu2 pdg:      " << v_nus[1]->pdgId() << std::endl;
    std::cout << " nu3 pdg:      " << v_nus[2]->pdgId() << std::endl;
    std::cout << " mW:           " << mW << std::endl;
    std::cout << " mZ:           " << mZ << std::endl;
    std::cout << " min_chisq:    " << min_chisq << std::endl;
    std::cout << "Exit." << std::endl;
    return StatusCode::FAILURE;
  }

  // Fill
  Decay_boson W;  W.pq1 = setTLV(lep);  W.q1_pdg = lep->pdgId();
  if(std::abs(lep->pdgId())==15) GetTauDecay(lep,W.tau1);

  W.pq2 = setTLV_fin(mcp_Wv);  W.q2_pdg = mcp_Wv->pdgId();

  W.P4 = (W.pq1+W.pq2);
  W.pdg = W.q1_pdg > 0 ? -24 : 24;
  W.finalize();

  Decay_boson Z;  Z.clear();
  for(auto mcp_Zv : {mcp_Zv1, mcp_Zv2} ){
    int pdg = mcp_Zv->pdgId();
    if(pdg<0)  { Z.pq1 = setTLV_fin(mcp_Zv);  Z.q1_pdg = pdg; }  // nubar
    else       { Z.pq2 = setTLV_fin(mcp_Zv);  Z.q2_pdg = pdg; }  // nu
  }
  Z.P4 = (Z.pq1+Z.pq2);
  Z.pdg = 23;
  Z.finalize();

  evt->set_evt(W, Z);


  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_ZZ(TruthEvent_VV *evt){
  //
  // Retrive decay chain of ZZ.
  //

  if( isSherpa() && (ProcClassifier::IsZZ4L0Nu(m_DSID) || ProcClassifier::IsZZ2L2Nu(m_DSID) || ProcClassifier::IsZZ0L4Nu(m_DSID)) )
    GetDecayChain_ZZ4L_Sherpa(evt);
  else   {

    const xAOD::TruthParticleContainer* cont =
      m_truthParticles ? m_truthParticles :
      m_truthBosonWDP ? m_truthBosonWDP :
      0;
    if(!cont) return StatusCode::FAILURE;

    evt->clear();

    // +++++ find Zs ++++++ //
    xAOD::TruthParticle * fin_Z1=0, *fin_Z2=0;
    for(int idx=0, nmcp=(int)cont->size(); idx<nmcp; idx++) {
      xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
      if(mcp->pdgId()!=23) continue;

      // get Z1,Z2
      xAOD::TruthParticle* fin_mcp = descend(mcp,0);
      if(fin_Z1==0                                 && getNChildren(fin_mcp)>0) fin_Z1 = fin_mcp;
      if(fin_Z1!=0 && fin_Z2==0 && fin_Z1!=fin_mcp && getNChildren(fin_mcp)>0) fin_Z2 = fin_mcp;
      if(fin_Z1!=0 && fin_Z2!=0)            break;
    }

    // +++++ fill decays ++++++ //
    Decay_boson Z1;  GetZDecay(fin_Z1, Z1);
    Decay_boson Z2;  GetZDecay(fin_Z2, Z2);

    evt->set_evt(Z1, Z2);
    evt->finalize();
  }

  return StatusCode::SUCCESS;
}


//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_ZZ4L_Sherpa(TruthEvent_VV *evt){

  //
  // Retrive decay chain of ZZ for Sherpa llll samples.
  //

  evt->clear();

  // Retrive taus from ZZ
  std::vector<xAOD::TruthParticle*> v_taus;
  std::vector<Decay_tau> v_tauDecays;
  if(!ProcClassifier::IsZZ0L4Nu(m_DSID)) GetTaus_SherpaBoson(v_taus, v_tauDecays);

  const int nTaus = v_taus.size();
  if( !( nTaus==4 || nTaus==2 || nTaus==0) ){
    ERROR("DecayHandle::GetDecayChain_ZZ4L_Sherpa", "Illegal nTaus=%i found (supposed to be 0 or 2 or 4). Exit.", nTaus);
    return StatusCode::FAILURE;
  }

  // Retrive e/mu from ZZ but not from taus
  std::vector<xAOD::TruthParticle*> v_eles, v_mus;
  if(!ProcClassifier::IsZZ0L4Nu(m_DSID) && nTaus!=4){
    GetNonTauElectrons_SherpaBoson(v_eles, v_tauDecays);
    GetNonTauMuons_SherpaBoson(v_mus, v_tauDecays);
  }
  const int nEles = v_eles.size();
  const int nMus  = v_mus.size();
  if( !( nTaus+nEles+nMus==4 || nTaus+nEles+nMus==2 || nTaus+nEles+nMus==0) ){
    ERROR("DecayHandle::GetDecayChain_ZZ4L_Sherpa", "Illegal nTaus+nEles+nMus=%i found (supposed to be 0 or 2 or 4). Exit.", nTaus+nEles+nMus);
    return StatusCode::FAILURE;
  }

  // Retrive neutrinos from ZZ but not from taus
  std::vector<xAOD::TruthParticle*> v_nus;
  if(nTaus+nEles+nMus!=4) GetNonTauNeutrlinos_SherpaBoson(v_nus, v_tauDecays);

  const int nNus = v_nus.size();

  // Sanity checks
  if(ProcClassifier::IsZZ4L0Nu(m_DSID) && !(nTaus+nEles+nMus==4 && nNus==0) ){
    ERROR("DecayHandle::GetDecayChain_ZZ4L_Sherpa", "Illegal nTaus+nEles+nMus=%i / nNus=%i pair found. (4/0 expected) Exit.", nTaus+nEles+nMus, nNus);
    return StatusCode::FAILURE;
  }
  if(ProcClassifier::IsZZ2L2Nu(m_DSID) && !(nTaus+nEles+nMus==2 && nNus==2) ){
    ERROR("DecayHandle::GetDecayChain_ZZ4L_Sherpa", "Illegal nTaus+nEles+nMus=%i / nNus=%i pair found. (2/2 expected) Exit.", nTaus+nEles+nMus, nNus);
    return StatusCode::FAILURE;
  }
  if(ProcClassifier::IsZZ0L4Nu(m_DSID) && !(nTaus+nEles+nMus==0 && nNus==4) ){
    ERROR("DecayHandle::GetDecayChain_ZZ4L_Sherpa", "Illegal nTaus+nEles+nMus=%i / nNus=%i pair found. (0/4 expected) Exit.", nTaus+nEles+nMus, nNus);
    return StatusCode::FAILURE;
  }

  // Z-pairing
  Decay_boson Z1, Z2;
  Z1.clear(); Z2.clear();
  if     (nEles==2 && nMus==2) {  GetZDecay_Impl(v_eles,Z1);  GetZDecay_Impl(v_mus,Z2);  }
  else if(nEles==2 && nTaus==2){  GetZDecay_Impl(v_eles,Z1);  GetZDecay_Impl(v_taus,Z2); }
  else if(nEles==2 && nNus==2) {  GetZDecay_Impl(v_eles,Z1);  GetZDecay_Impl(v_nus,Z2);  }
  else if(nMus==2  && nTaus==2){  GetZDecay_Impl(v_mus,Z1);   GetZDecay_Impl(v_taus,Z2); }
  else if(nMus==2  && nNus==2) {  GetZDecay_Impl(v_mus,Z1);   GetZDecay_Impl(v_nus,Z2);  }
  else if(nTaus==2 && nNus==2) {  GetZDecay_Impl(v_taus,Z1);  GetZDecay_Impl(v_nus,Z2);  }
  else if(nEles==4 || nMus==4 || nTaus==4 || nNus==4){

    std::vector<xAOD::TruthParticle*> v_leps =
      nEles==4 ? v_eles :
      nMus==4  ?  v_mus :
      nTaus==4 ? v_taus :
      v_nus;

    std::vector<xAOD::TruthParticle*> v_Zll1, v_Zll2;
    float optZMass=9999.;
    for(int il1=0; il1<(int)v_leps.size(); ++il1){
      for(int il2=il1+1; il2<(int)v_leps.size(); ++il2){
	if( v_leps[il1]->pdgId()+v_leps[il2]->pdgId() != 0 ) continue; // require SFOS

	// identify the idx of the remnants
	std::vector<int> idx_rest;
	for(int il=0; il<(int)v_leps.size(); ++il){
	  if(il==il1 || il==il2) continue;
	  idx_rest.push_back(il);
	}
	int il3 = idx_rest[0];
	int il4 = idx_rest[1];

	float mll = (setTLV((xAOD::TruthParticle*)v_leps[il1])+setTLV((xAOD::TruthParticle*)v_leps[il2])).M();
	if( fabs(mll-91.2) < fabs(optZMass-91.2) ){
	  optZMass = mll;
	  v_Zll1 = {v_leps[il1], v_leps[il2]};
	  v_Zll2 = {v_leps[il3], v_leps[il4]};
	}

      }
    }

    GetZDecay_Impl(v_Zll1,Z1);  GetZDecay_Impl(v_Zll2,Z2);
  }

  else{
    ERROR("DecayHandle::GetDecayChain_ZZ4L_Sherpa", "Illegal lepton multiplicity found: nEles=%i / nMus=%i / nTaus=%i / nNus=%i. Exit.", nEles, nMus, nTaus, nNus);
    return StatusCode::FAILURE;
  }

  evt->set_evt(Z1, Z2);
  evt->finalize();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_WH(TruthEvent_VV *evt){

  //
  // Retrive decay chain of WH.
  //

  const xAOD::TruthParticleContainer* cont =
    m_truthParticles ? m_truthParticles :
    m_truthBosonWDP ? m_truthBosonWDP :
    0;
  if(!cont) return StatusCode::FAILURE;

  evt->clear();

  // +++++ find W/H ++++++ //
  xAOD::TruthParticle * fin_W=0, *fin_H=0;
  for(int idx=0, nmcp=(int)cont->size(); idx<nmcp; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
    int pdg = mcp->pdgId();
    xAOD::TruthParticle* fin_mcp = descend(mcp,0);

    // get W,H
    if(fin_W==0 && std::abs(pdg)==24 && getNChildren(fin_mcp)>0) fin_W = fin_mcp;
    if(fin_H==0 &&           pdg==25 && getNChildren(fin_mcp)>0) fin_H = fin_mcp;
    if(fin_W!=0 && fin_H!=0)          break;
  }

  // +++++ fill decays ++++++ //
  Decay_boson W;  GetWDecay(fin_W, W);
  Decay_H     H;  GetHDecay(fin_H, H);

  evt->set_evt(W, H);
  evt->finalize();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_ZH(TruthEvent_VV *evt){

  //
  // Retrive decay chain of ZH.
  //

  const xAOD::TruthParticleContainer* cont =
    m_truthParticles ? m_truthParticles :
    m_truthBosonWDP ? m_truthBosonWDP :
    0;
  if(!cont) return StatusCode::FAILURE;

  evt->clear();

  // +++++ find Z/H ++++++ //
  xAOD::TruthParticle * fin_Z=0, *fin_H=0;
  for(int idx=0, nmcp=(int)cont->size(); idx<nmcp; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
    int pdg = mcp->pdgId();
    xAOD::TruthParticle* fin_mcp = descend(mcp,0);

    // get Z,H
    if(fin_Z==0 && pdg==23 && getNChildren(fin_mcp)>0)  fin_Z = descend(mcp,0);
    if(fin_H==0 && pdg==25 && getNChildren(fin_mcp)>0)  fin_H = descend(mcp,0);
    if(fin_Z!=0 && fin_H!=0) break;
  }

  // Check if two gauginos are found
  if(!fin_Z || !fin_H){
    ERROR("DecayHandle::GetDecayChain_ZH", "Could not find Z or H in the TruthParticle container. Pointer address for fin_Z: %i, fin_H %i. Exit.", fin_Z, fin_H);
    return StatusCode::FAILURE;
  }

  // +++++ fill decays ++++++ //
  Decay_boson Z;  GetZDecay(fin_Z, Z);
  Decay_H     H;  GetHDecay(fin_H, H);

  evt->set_evt(Z, H);
  evt->finalize();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_HH(TruthEvent_VV *evt){

  //
  // Retrive decay chain of HH.
  //

  const xAOD::TruthParticleContainer* cont =
    m_truthParticles ? m_truthParticles :
    m_truthBosonWDP ? m_truthBosonWDP :
    0;
  if(!cont) return StatusCode::FAILURE;

  evt->clear();

  // +++++ find Hs ++++++ //
  xAOD::TruthParticle * fin_H1=0, *fin_H2=0;
  for(int idx=0, nmcp=(int)cont->size(); idx<nmcp; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
    if(mcp->pdgId()!=25) continue;

    // get H1,H2
    xAOD::TruthParticle* fin_mcp = descend(mcp,0);
    if(fin_H1==0 &&                                 getNChildren(fin_mcp)>0)  fin_H1 = fin_mcp;
    if(fin_H1!=0 && fin_H2==0 && fin_H1!=fin_mcp && getNChildren(fin_mcp)>0)  fin_H2 = fin_mcp;
    if(fin_H1!=0 && fin_H2!=0)                        break;
  }

  // +++++ fill decays ++++++ //
  Decay_H H1;  GetHDecay(fin_H1, H1);
  Decay_H H2;  GetHDecay(fin_H2, H2);

  evt->set_evt(H1, H2);
  evt->finalize();

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_VV_FullLep(TruthEvent_VV *evt){

  //
  // Retrive leptons/neutralinos and pairing them into 2-bosons
  // Only events where the boson assignment is unique are dealt with at the moment.
  //

  evt->clear();

  // Retrive taus
  std::vector<xAOD::TruthParticle*> v_taus;
  std::vector<Decay_tau> v_tauDecays;
  GetTaus_SherpaBoson(v_taus, v_tauDecays);

  // Retrive e/mu/nu not from taus
  std::vector<xAOD::TruthParticle*> v_eles, v_mus, v_nus;
  GetNonTauElectrons_SherpaBoson(v_eles, v_tauDecays);
  GetNonTauMuons_SherpaBoson(v_mus, v_tauDecays);
  GetNonTauNeutrlinos_SherpaBoson(v_nus, v_tauDecays);

  // Sanity checks
  const int nEle=v_eles.size();
  const int nMu=v_mus.size();
  const int nTau=v_taus.size();
  const int nNu=v_nus.size();
  if( nEle+nMu+nTau+nNu != 4 ){
    ERROR("DecayHandle::GetDecayChain_VV_FullLep", "Illegal nEles+nMus+nTaus+nNu=%i found (supposed to be 4). nEle=%i nMu=%i nTau=%i nNu=%i. Exit.", nEle+nMu+nTau+nNu, nEle,nMu,nTau,nNu);
    return StatusCode::FAILURE;
  }

  //
  // Boson paring
  //
  std::vector<xAOD::TruthParticle*> v_lepnu;
  v_lepnu.insert(v_lepnu.end(), v_eles.begin(), v_eles.end());
  v_lepnu.insert(v_lepnu.end(),  v_mus.begin(),  v_mus.end());
  v_lepnu.insert(v_lepnu.end(), v_taus.begin(), v_taus.end());
  v_lepnu.insert(v_lepnu.end(),  v_nus.begin(),  v_nus.end());

  // Find the most plausible pairing based on the invariant mass of lnu and ll or VV (in case of h->VV).
  Decay_boson V1; V1.clear();
  Decay_boson V2; V2.clear();

  float min_chisq=1e10;
  const float MW = 80.4;
  const float MZ = 91.2;
  const float MH = 125.;
  const float gW = 2.0;
  const float gZ = 2.2;
  const float gH = 0.5;

  //
  // Pair the first two
  //
  for(int il1=0; il1<4; ++il1){
    for(int il2=il1+1; il2<4; ++il2){
      bool isWcand1 = (v_lepnu[il1]->isNeutrino() || v_lepnu[il2]->isNeutrino()) && std::abs(v_lepnu[il1]->pdgId()+v_lepnu[il2]->pdgId())==1;
      bool isZcand1 = v_lepnu[il1]->pdgId()+v_lepnu[il2]->pdgId()==0;
      //std::cout << "il1/2: " << il1 << " " <<  il2 << " | pdg1/2: " << v_lepnu[il1]->pdgId() << " " << v_lepnu[il2]->pdgId() << " | W/Zcand1: " << isWcand1 << " " << isZcand1 << std::endl;
      if(!isWcand1 && !isZcand1) continue;
      std::vector<xAOD::TruthParticle*> v1 = {v_lepnu[il1], v_lepnu[il2]};

      //
      // Pair the rest
      //
      int il3=-1, il4=-1;
      for(int il=0; il<6; ++il){
	if     (il!=il1 && il!=il2 && il3==-1           && il4==-1) il3 = il;
	else if(il!=il1 && il!=il2 && il!=il3 && il3>=0 && il4==-1) il4 = il;
      }
      bool isWcand2 = (v_lepnu[il3]->isNeutrino() || v_lepnu[il4]->isNeutrino()) && std::abs(v_lepnu[il3]->pdgId()+v_lepnu[il4]->pdgId())==1;
      bool isZcand2 = v_lepnu[il3]->pdgId()+v_lepnu[il4]->pdgId()==0;
      //std::cout << "il3/4: " << il3 << " " <<  il4 << " | pdg3/4: " << v_lepnu[il3]->pdgId() << " " << v_lepnu[il4]->pdgId() << " | W/Zcand2: " << isWcand2 << " " << isZcand2 << std::endl;
      if(!isWcand2 && !isZcand2) continue;
      std::vector<xAOD::TruthParticle*> v2 = {v_lepnu[il3], v_lepnu[il4]};

      //std::cout << il1 << " " <<  il2 << " " <<  il3 << " " <<  il4 << " " <<  il3 << " " <<  il4 << " " << " | " << isWcand1 << " " << isZcand1 << " "  << isWcand2 << " " << isZcand2 << std::endl;

      //
      // Boson 4-vectors
      //
      TLorentzVector p1 = setTLV(v1[0])+setTLV(v1[1]);
      TLorentzVector p2 = setTLV(v2[0])+setTLV(v2[1]);

      float chisq1  = isWcand1 ? pow(p1.M()-MW,2)/gW : pow(p1.M()-MZ,2)/gZ;
      float chisq2  = isWcand2 ? pow(p2.M()-MW,2)/gW : pow(p2.M()-MZ,2)/gZ;
      float chisqVV = sqrt( (chisq1+chisq2)/2. );

      // Test the VH hypothesis as well
      int   q1 = v1[0]->charge()+v1[1]->charge();
      int   q2 = v2[0]->charge()+v2[1]->charge();
      float chisqH =
	q1+q2==0 ? sqrt( (pow((p1+p2).M()-MH,2)/gH)/2. ) :
	-1.;

      // Update
      float chisq = chisqH>0 ? std::min(chisqVV,chisqH) : chisqVV;
      if(chisq < min_chisq){
	min_chisq = chisq;
	V1.clear();
	if(isWcand1) GetWDecay_Impl(v1,V1);
	else         GetZDecay_Impl(v1,V1);
	V2.clear();
	if(isWcand2) GetWDecay_Impl(v2,V2);
	else         GetZDecay_Impl(v2,V2);
      }

      // end of the loop
    }
  }


  if(V1.P4.E()==0 || V2.P4.E()==0){
    ERROR("DecayHandle::GetDecayChain_VV_FullLep", "Association fails:");

    for(int il=0; il<4; ++il) std::cout << il << ": " << v_lepnu[il]->pdgId() << " " << v_lepnu[il]->pt()/1000. << " " << v_lepnu[il]->phi() << std::endl;
    std::cout << " V1: " << std::endl;
    V1.print();
    std::cout << " V2: " << std::endl;
    V2.print();
    std::cout << " min_chisq:    " << min_chisq << std::endl;
    std::cout << "Exit." << std::endl;

    return StatusCode::FAILURE;
  }

  evt->set_evt(V1, V2);
  evt->finalize();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_VVV(TruthEvent_VVV *evt){

  if(m_truthParticles && ProcClassifier::IsVVVLep(m_DSID))  return GetDecayChain_VVV_FullLep(evt);

  //
  // Retrive decay chain of VVV
  //

  const xAOD::TruthParticleContainer* cont =
    m_truthParticles ? m_truthParticles :
    m_truthBosonWDP ? m_truthBosonWDP :
    0;
  if(!cont) return StatusCode::FAILURE;

  evt->clear();

  // +++++ find W/Z/H ++++++ //
  xAOD::TruthParticle *fin_V[3]={0};
  for(int idx=0, nmcp=(int)cont->size(); idx<nmcp; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
    int pdg = mcp->pdgId();
    xAOD::TruthParticle* fin_mcp = descend(mcp,0);

    if(!(std::abs(pdg)==24 || pdg==23 || pdg==25)) continue;
    if(getNChildren(fin_mcp)==0) continue;

    if(fin_V[0]==0                                          ) fin_V[0] = fin_mcp;
    if(fin_V[1]==0 && fin_mcp!=fin_V[0]                     ) fin_V[1] = fin_mcp;
    if(fin_V[2]==0 && fin_mcp!=fin_V[0] && fin_mcp!=fin_V[1]) fin_V[2] = fin_mcp;

    if(fin_V[0]!=0 && fin_V[1]!=0 && fin_V[2]!=0)          break;
  }

  if(fin_V[0]==0 || fin_V[1]==0 || fin_V[2]==0){
    if(!ProcClassifier::IsVVVHad(m_DSID)) // hadronic VVV has non-VVV component as well
      ERROR("DecayHandle::GetDecayChain_VVV", "Could not find three bosons in the TruthParticle container. Pointer address for V1: %i, V2 %i. V3 %i. Exit.", fin_V[0], fin_V[1], fin_V[2]);
    return StatusCode::FAILURE;
  }

  // +++++ fill decays ++++++ //
  std::vector<Decay_boson> V;
  for(int iv=0; iv<3; iv++){
    if(std::abs(fin_V[iv]->pdgId())==24){
      Decay_boson W;  GetWDecay(fin_V[iv], W);
      V.push_back(W);
    }
    else if(fin_V[iv]->pdgId()==23){
      Decay_boson Z;  GetZDecay(fin_V[iv], Z);
      V.push_back(Z);
    }
    else if(fin_V[iv]->pdgId()==25){
      Decay_H H;  GetHDecay(fin_V[iv], H);
      V.push_back((Decay_boson)H);
    }
  }

  evt->set_evt(V[0], V[1], V[2]);
  evt->finalize();

  return StatusCode::SUCCESS;
}


//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_VVV_FullLep(TruthEvent_VVV *evt){

  //
  // Retrive leptons/neutralinos and pairing them into 3-bosons
  // Only events where the boson assignment is unique are dealt with at the moment.
  //

  evt->clear();

  // Retrive taus
  std::vector<xAOD::TruthParticle*> v_taus;
  std::vector<Decay_tau> v_tauDecays;
  GetTaus_SherpaBoson(v_taus, v_tauDecays);

  // Retrive e/mu/nu not from taus
  std::vector<xAOD::TruthParticle*> v_eles, v_mus, v_nus;
  GetNonTauElectrons_SherpaBoson(v_eles, v_tauDecays);
  GetNonTauMuons_SherpaBoson(v_mus, v_tauDecays);
  GetNonTauNeutrlinos_SherpaBoson(v_nus, v_tauDecays);

  // Sanity checks
  const int nEle=v_eles.size();
  const int nMu=v_mus.size();
  const int nTau=v_taus.size();
  const int nNu=v_nus.size();
  if( nEle+nMu+nTau+nNu != 6 ){
    ERROR("DecayHandle::GetDecayChain_VVV_FullLep", "Illegal nEles+nMus+nTaus+nNu=%i found (supposed to be 6). nEle=%i nMu=%i nTau=%i nNu=%i. Exit.", nEle+nMu+nTau+nNu, nEle,nMu,nTau,nNu);
    return StatusCode::FAILURE;
  }

  //
  // Boson paring
  //
  std::vector<xAOD::TruthParticle*> v_lepnu;
  v_lepnu.insert(v_lepnu.end(), v_eles.begin(), v_eles.end());
  v_lepnu.insert(v_lepnu.end(),  v_mus.begin(),  v_mus.end());
  v_lepnu.insert(v_lepnu.end(), v_taus.begin(), v_taus.end());
  v_lepnu.insert(v_lepnu.end(),  v_nus.begin(),  v_nus.end());

  // Find the most plausible pairing based on the invariant mass of lnu and ll or VV (in case of h->VV).
  Decay_boson V1; V1.clear();
  Decay_boson V2; V2.clear();
  Decay_boson V3; V3.clear();

  float min_chisq=1e10;
  const float MW = 80.4;
  const float MZ = 91.2;
  const float MH = 125.;
  const float gW = 2.0;
  const float gZ = 2.2;
  const float gH = 0.5;

  // Pair the first two
  for(int il1=0; il1<6; ++il1){
    for(int il2=il1+1; il2<6; ++il2){
      bool isWcand1 = (v_lepnu[il1]->isNeutrino() || v_lepnu[il2]->isNeutrino()) && std::abs(v_lepnu[il1]->pdgId()+v_lepnu[il2]->pdgId())==1;
      bool isZcand1 = v_lepnu[il1]->pdgId()+v_lepnu[il2]->pdgId()==0;
      //std::cout << "il1/2: " << il1 << " " <<  il2 << " | pdg1/2: " << v_lepnu[il1]->pdgId() << " " << v_lepnu[il2]->pdgId() << " | W/Zcand1: " << isWcand1 << " " << isZcand1 << std::endl;

      if(!isWcand1 && !isZcand1) continue;
      std::vector<xAOD::TruthParticle*> v1 = {v_lepnu[il1], v_lepnu[il2]};

      // Pair the next two
      for(int il3=0; il3<6; ++il3){
	for(int il4=il3+1; il4<6; ++il4){
	  if(il3==il1 || il3==il2 || il4==il1 || il4==il2) continue;
	  bool isWcand2 = (v_lepnu[il3]->isNeutrino() || v_lepnu[il4]->isNeutrino()) && std::abs(v_lepnu[il3]->pdgId()+v_lepnu[il4]->pdgId())==1;
	  bool isZcand2 = v_lepnu[il3]->pdgId()+v_lepnu[il4]->pdgId()==0;
	  //std::cout << "il3/4: " << il3 << " " <<  il4 << " | pdg3/4: " << v_lepnu[il3]->pdgId() << " " << v_lepnu[il4]->pdgId() << " | W/Zcand2: " << isWcand2 << " " << isZcand2 << std::endl;
	  if(!isWcand2 && !isZcand2) continue;
	  std::vector<xAOD::TruthParticle*> v2 = {v_lepnu[il3], v_lepnu[il4]};

	  // Pair the rest
	  int il5=-1, il6=-1;
	  for(int il=0; il<6; ++il){
	    if     (il!=il1 && il!=il2 && il!=il3 && il!=il4 && il5==-1 && il6==-1) il5 = il;
	    else if(il!=il1 && il!=il2 && il!=il3 && il!=il4 && il!=il5 && il5>=0 && il6==-1) il6 = il;
	  }

	  bool isWcand3 = (v_lepnu[il5]->isNeutrino() || v_lepnu[il6]->isNeutrino()) && std::abs(v_lepnu[il5]->pdgId()+v_lepnu[il6]->pdgId())==1;
	  bool isZcand3 = v_lepnu[il5]->pdgId()+v_lepnu[il6]->pdgId()==0;
	  //std::cout << "il5/6: " << il5 << " " <<  il6 << " | pdg5/6: " << v_lepnu[il5]->pdgId() << " " << v_lepnu[il6]->pdgId() << " | W/Zcand3: " << isWcand3 << " " << isZcand3 << std::endl;
	  if(!isWcand3 && !isZcand3) continue;
	  std::vector<xAOD::TruthParticle*> v3 = {v_lepnu[il5], v_lepnu[il6]};

	  //std::cout << il1 << " " <<  il2 << " " <<  il3 << " " <<  il4 << " " <<  il5 << " " <<  il6 << " " << " | " << isWcand1 << " " << isZcand1 << " "  << isWcand2 << " " << isZcand2 << " "  << isWcand3 << " " << isZcand3 << " " << std::endl;

	  // Boson 4-vectors
	  TLorentzVector p1 = setTLV(v1[0])+setTLV(v1[1]);
	  TLorentzVector p2 = setTLV(v2[0])+setTLV(v2[1]);
	  TLorentzVector p3 = setTLV(v3[0])+setTLV(v3[1]);

	  float chisq1 = isWcand1 ? pow(p1.M()-MW,2)/gW : pow(p1.M()-MZ,2)/gZ;
	  float chisq2 = isWcand2 ? pow(p2.M()-MW,2)/gW : pow(p2.M()-MZ,2)/gZ;
	  float chisq3 = isWcand3 ? pow(p3.M()-MW,2)/gW : pow(p3.M()-MZ,2)/gZ;
	  float chisqVVV = sqrt( (chisq1+chisq2+chisq3)/3. );

	  // Test the VH hypothesis as well
	  int q1 = v1[0]->charge()+v1[1]->charge();
	  int q2 = v2[0]->charge()+v2[1]->charge();
	  int q3 = v3[0]->charge()+v3[1]->charge();
	  float chisqVH =
	    q1+q2==0 ? sqrt((pow((p1+p2).M()-MH,2)/gH + chisq3)/2. ) :
	    q2+q3==0 ? sqrt((pow((p2+p3).M()-MH,2)/gH + chisq1)/2. ) :
	    q3+q1==0 ? sqrt((pow((p3+p1).M()-MH,2)/gH + chisq2)/2. ) :
	    -1.;

	  // Update
	  float chisq = chisqVH>0 ? std::min(chisqVVV,chisqVH) : chisqVVV;
	  if(chisq < min_chisq){
	    min_chisq = chisq;

	    V1.clear();
	    if(isWcand1) GetWDecay_Impl(v1,V1);
	    else       GetZDecay_Impl(v1,V1);
	    V2.clear();
	    if(isWcand2) GetWDecay_Impl(v2,V2);
	    else       GetZDecay_Impl(v2,V2);
	    V3.clear();
	    if(isWcand3) GetWDecay_Impl(v3,V3);
	    else       GetZDecay_Impl(v3,V3);
	  }

	  // end of the loop
	}
      }

    }
  }

  if(V1.P4.E()==0 || V2.P4.E()==0 || V3.P4.E()==0){
    ERROR("DecayHandle::GetDecayChain_VVV_FullLep", "Association fails:");

    for(int il=0; il<6; ++il) std::cout << il << ": " << v_lepnu[il]->pdgId() << " " << v_lepnu[il]->pt()/1000. << " " << v_lepnu[il]->phi() << std::endl;
    std::cout << " V1: " << std::endl;
    V1.print();
    std::cout << " V2: " << std::endl;
    V2.print();
    std::cout << " V3: " << std::endl;
    V3.print();
    std::cout << " min_chisq:    " << min_chisq << std::endl;
    std::cout << "Exit." << std::endl;

    return StatusCode::FAILURE;
  }

  evt->set_evt(V1, V2, V3);
  evt->finalize();

  return StatusCode::SUCCESS;
}


//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_Vjets(TruthEvent_Vjets *evt){

  //
  // Retrieve decay chain of W/Z+jets.
  //

  if( isSherpa() ) GetDecayChain_Vjets_Sherpa(evt);
  else{

    const xAOD::TruthParticleContainer* cont =
      m_truthParticles ? m_truthParticles :
      m_truthBosonWDP ? m_truthBosonWDP :
      0;
    if(!cont) return StatusCode::FAILURE;

    evt->clear();

    // Retrieve W/Z decay
    Decay_boson V; V.clear();
    for(int idx=0, nmcp=(int)cont->size(); idx<nmcp; idx++) {
      xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
      int pdg = mcp->pdgId();

      if(std::abs(pdg)==24){
	xAOD::TruthParticle* fin_W = descend(mcp,0);
	GetWDecay(fin_W, V);
	break;
      }
      else if(pdg==23){
	xAOD::TruthParticle* fin_Z = descend(mcp,0);
	GetZDecay(fin_Z, V);
	break;
      }

    } // end of mcp loop

    // Retrieve additional partons
    std::vector<tlv> addPartons;
    std::vector<int> addPartons_pdg;
    RetrieveDocumentLines(addPartons, addPartons_pdg, 15.);

    // Retrieve truth jets
    std::vector<tlv> truthJets;
    std::vector<int> truthJets_label;
    RetrieveTruthJets(truthJets, truthJets_label);

    // Finalize
    evt->set_evt(V, addPartons, addPartons_pdg, truthJets, truthJets_label);
    evt->finalize();
  }

  return StatusCode::SUCCESS;

}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_Vjets_Sherpa(TruthEvent_Vjets *evt){

  if(ProcClassifier::IsWjets_Sherpa(m_DSID))
    return GetDecayChain_Wjets_Sherpa(evt);

  else if(ProcClassifier::IsZjets_Sherpa(m_DSID))
    return GetDecayChain_Zjets_Sherpa(evt);

  else if(ProcClassifier::IsGammaJets_Sherpa(m_DSID))
    return GetDecayChain_GammaJets_Sherpa(evt);

  else{
    ERROR("DecayHandle::GetDecayChain_Vjets_Sherpa","DSID %i is not registered as either W+jets or Z+jets in ProcClassifier.h. Exit.", m_DSID);
  }
  return StatusCode::FAILURE;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_Wjets_Sherpa(TruthEvent_Vjets *evt){

  //
  // Fill decay info of W and the additional jets associated.
  // Retrive leptons from TruthElectron/MuonContainer, and neutrinos from TruthParticleContainer.
  //

  xAOD::TruthParticleContainer* cont =
    ProcClassifier::IsWjets_enu_Sherpa(m_DSID)   ? m_truthElectrons :
    ProcClassifier::IsWjets_munu_Sherpa(m_DSID)  ? m_truthMuons     :
    ProcClassifier::IsWjets_taunu_Sherpa(m_DSID) ? m_truthTaus      :
    0 ;

  if(!cont) {
    ERROR("DecayHandle::GetDecayChain_Wjets_Sherpa","DSID %i is not registered as W+jets ProcClassifier.h. Exit.", m_DSID);
    return StatusCode::FAILURE;
  }

  evt->clear();

  Decay_boson W; W.clear();

  // Retrive e/mu/tau from W
  for (const auto &mcp: *cont) {
    if(cacc_TDC_truthOrigin.isAvailable(*mcp)){
      int origin = cacc_TDC_truthOrigin(*mcp) ;
      int pdg = mcp->pdgId();
      if(origin==12 && (std::abs(pdg)==11||std::abs(pdg)==13||std::abs(pdg)==15)) {  // from W boson
	W.pq1 = setTLV((xAOD::TruthParticle*) mcp);
	W.q1_pdg = mcp->pdgId();

	if(std::abs(W.q1_pdg)==15){
	  GetTauDecay((xAOD::TruthParticle*)mcp,W.tau1);
	}

	break;
      }
    }
    else{
      WARNING("DecayHandle::GetDecayChain_Wjets", "Truth origin is not available. No clue to know if the lepton is from W or not ... Skip this lepton.");
    }

  }

  if(!W.pq1.E()){
      WARNING("DecayHandle::GetDecayChain_Wjets", "No truth_origin==12 lepton is found. Exit.");
    return StatusCode::FAILURE;
  }

  //
  // Retrive neutrino from W
  //
  //  If truth neutrino container is available -> look for origin=12 and status=1 one
  if(m_truthNeutrinos){
    for (const auto &mcp: *m_truthNeutrinos) {
      if(cacc_TDC_truthOrigin.isAvailable(*mcp)){
	int origin = cacc_TDC_truthOrigin(*mcp) ;
	int pdg = mcp->pdgId();
	if(origin==12 && (std::abs(pdg)==12||std::abs(pdg)==14||std::abs(pdg)==16)) {  // from W boson
	  W.pq2 = setTLV((xAOD::TruthParticle*) mcp);
	  W.q2_pdg = mcp->pdgId();
	  break;
	}
      }
    } // end of loop over truthNeutrinos
  }

  // If truth neutrino container is not available -> look in truth particle container with status=1
  if(!W.pq2.E()){
    if(m_truthParticles){
      for (const auto &mcp: *m_truthParticles) {
	if(mcp->status()==1 && (std::abs(mcp->pdgId())==12 || std::abs(mcp->pdgId())==14 || std::abs(mcp->pdgId())==16)){
	  W.pq2=setTLV(mcp);
	  W.q2_pdg=mcp->pdgId();
	  break;
	}
      }
    }
  }

  // If status=1 is not available -> look for status=3
  if(!W.pq2.E()){
    if(m_truthParticles){
      for (const auto &mcp: *m_truthParticles) {
	if(mcp->status()==3 && (std::abs(mcp->pdgId())==12 || std::abs(mcp->pdgId())==14 || std::abs(mcp->pdgId())==16)){
	  W.pq2=setTLV(mcp);
	  W.q2_pdg=mcp->pdgId();
	  break;
	}
      }
    }
  }

  if(!W.pq2.E()){
    WARNING("DecayHandle::GetDecayChain_Wjets", "No truth neutrino is found. Exit.");
    return StatusCode::FAILURE;
  }

  W.P4  = W.pq1 + W.pq2 ;
  W.pdg = W.q1_pdg > 0 ? -24 : 24;

  // Retrieve additional partons
  std::vector<tlv> addPartons;
  std::vector<int> addPartons_pdg;
  RetrieveDocumentLines(addPartons, addPartons_pdg, 15.);

  // Retrieve truth jets
  std::vector<tlv> truthJets;
  std::vector<int> truthJets_label;
  RetrieveTruthJets(truthJets, truthJets_label);

  // Finalize
  evt->set_evt(W, addPartons, addPartons_pdg, truthJets, truthJets_label);
  evt->finalize();

  //evt->print();
  return StatusCode::SUCCESS;

}
//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_Zjets_Sherpa(TruthEvent_Vjets *evt){

  //
  // Fill decay info of Z and the additional jets associated.
  // Retrive leptons from TruthElectron/MuonContainer, and neutrinos from TruthParticleContainer.
  //

  if(ProcClassifier::IsZjets_nunu_Sherpa(m_DSID)) return DecayHandle::GetDecayChain_Znunu_Sherpa(evt);

  xAOD::TruthParticleContainer* cont =
    ProcClassifier::IsZjets_ee_Sherpa(m_DSID) ? m_truthElectrons :
    ProcClassifier::IsZjets_mumu_Sherpa(m_DSID) ?  m_truthMuons     :
    ProcClassifier::IsZjets_tautau_Sherpa(m_DSID) ? m_truthTaus      :
    0;

  if(!cont) {
    ERROR("DecayHandle::GetDecayChain_Zjets_Sherpa","DSID %i is not registered as Z+jets in ProcClassifier.h. Exit.", m_DSID);
    return StatusCode::FAILURE;
  }

  evt->clear();

  // Retrive e/mu/tau from Z
  Decay_boson Z; Z.clear();
  for (const auto &mcp: *cont) {
    if(cacc_TDC_truthOrigin.isAvailable(*mcp)){
      int origin = cacc_TDC_truthOrigin(*mcp) ;
      if(origin!=13) continue;       // origin=13: from Z

      int pdg = mcp->pdgId();
      if(pdg==-11 || pdg==-13 || pdg==-15) {
	Z.pq1    = setTLV((xAOD::TruthParticle*) mcp);
	Z.q1_pdg = mcp->pdgId();
	if(Z.q1_pdg==-15)
	  GetTauDecay((xAOD::TruthParticle*)mcp, Z.tau1);
      }
      else if(pdg==11 || pdg==13 || pdg==15) {
	Z.pq2    = setTLV((xAOD::TruthParticle*) mcp);
	Z.q2_pdg = mcp->pdgId();
	if(Z.q2_pdg==15)
	  GetTauDecay((xAOD::TruthParticle*)mcp, Z.tau2);
      }

    }
    else{
      WARNING("DecayHandle::GetDecayChain_Zjets_Sherpa", "Truth origin is not available. No clue to know if the lepton is from W or not ... Skip this lepton.");
    }

  }

  if(!Z.pq1.E()){
      WARNING("DecayHandle::GetDecayChain_Zjets_Sherpa", "No truth_origin==13 pdg<0 lepton is found. Exit.");
    return StatusCode::FAILURE;
  }
  if(!Z.pq2.E()){
      WARNING("DecayHandle::GetDecayChain_Zjets_Sherpa", "No truth_origin==13 pdg>0 lepton is found. Exit.");
    return StatusCode::FAILURE;
  }

  Z.P4  = Z.pq1 + Z.pq2 ;
  Z.pdg = 23;

  // Retrieve additional partons
  std::vector<tlv> addPartons;
  std::vector<int> addPartons_pdg;
  RetrieveDocumentLines(addPartons, addPartons_pdg, 15.);

  // Retrieve truth jets
  std::vector<tlv> truthJets;
  std::vector<int> truthJets_label;
  RetrieveTruthJets(truthJets, truthJets_label);

  // Finalize
  evt->set_evt(Z, addPartons, addPartons_pdg, truthJets, truthJets_label);
  evt->finalize();

  //evt->print();
  return StatusCode::SUCCESS;

}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_Znunu_Sherpa(TruthEvent_Vjets *evt){

  evt->clear();

  xAOD::TruthParticleContainer* cont =
    m_truthNeutrinos ? m_truthNeutrinos :
    0;
  if(!cont) return StatusCode::FAILURE;

  Decay_boson Z;  Z.clear();
  for (const auto &mcp: *cont) {
    if(mcp->status()!=1) continue;

    int pdg = mcp->pdgId();
    if( !(std::abs(pdg)==12 || std::abs(pdg)==14 || std::abs(pdg)==16) ) continue;

    // If mcParticle classifier is available, require the neutrino is from Z
    if(cacc_TDC_truthOrigin.isAvailable(*mcp)){
      if(cacc_TDC_truthOrigin(*mcp)!=13) continue;
    }

    if(pdg>0){
      Z.pq1 = setTLV_fin(mcp);  Z.q1_pdg = pdg;
    }
    else{
      Z.pq2 = setTLV_fin(mcp);  Z.q2_pdg = pdg;
    }

  }
  Z.P4 = Z.pq1+Z.pq2;
  Z.pdg = 23;
  Z.finalize();

  // Retrieve additional partons
  std::vector<tlv> addPartons;
  std::vector<int> addPartons_pdg;
  RetrieveDocumentLines(addPartons, addPartons_pdg, 15.);

  // Retrieve truth jets
  std::vector<tlv> truthJets;
  std::vector<int> truthJets_label;
  RetrieveTruthJets(truthJets, truthJets_label);

  // Finalize
  evt->set_evt(Z, addPartons, addPartons_pdg, truthJets, truthJets_label);
  evt->finalize();

  return StatusCode::SUCCESS;
}

//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_GammaJets_Sherpa(TruthEvent_Vjets *evt){

  //
  // Retrive decay chain of ttbar and associated boson (W/Z/H).
  //

  const xAOD::TruthParticleContainer* cont =
    m_truthParticles ? m_truthParticles :
    m_truthPhotons ? m_truthPhotons :
    0;
  if(!cont) return StatusCode::FAILURE;

  evt->clear();
  Decay_boson photon;  photon.clear();
  for (const auto &mcp: *cont) {
    if(mcp->status()!=1) continue;
    photon.P4 = setTLV((xAOD::TruthParticle*)mcp);
    break;
  }
  photon.pdg = 22;
  photon.finalize();

  // Retrieve additional partons
  std::vector<tlv> addPartons;
  std::vector<int> addPartons_pdg;
  RetrieveDocumentLines(addPartons, addPartons_pdg, 15.);

  // Retrieve truth jets
  std::vector<tlv> truthJets;
  std::vector<int> truthJets_label;
  RetrieveTruthJets(truthJets, truthJets_label);

  // Finalize
  evt->set_evt(photon, addPartons, addPartons_pdg, truthJets, truthJets_label);
  evt->finalize();

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_TT(TruthEvent_TT *evt){

  //
  // Retrive decay chain of ttbar and associated boson (W/Z/H).
  //

  const xAOD::TruthParticleContainer* cont =
    m_truthTopWDP ? m_truthTopWDP :
    m_truthParticles ? m_truthParticles :
    0;
  if(!cont) return StatusCode::FAILURE;

  evt->clear();

  int nWlep=0, nWtaulep=0, nWtauhad=0, nWhad=0;
  Decay_boson W1, W2;

  // +++++ find tops ++++++ //
  xAOD::TruthParticle * fin_top=0, *fin_antitop=0;
  for(int idx=0, n=(int)cont->size(); idx<n; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
    int pdg = mcp->pdgId();

    // get top, anti-top
    xAOD::TruthParticle* fin_mcp = descend(mcp,0);
    if(pdg==6  && fin_top==0     && getNChildren(fin_mcp)>0) fin_top     = fin_mcp;
    if(pdg==-6 && fin_antitop==0 && getNChildren(fin_mcp)>0) fin_antitop = fin_mcp;
    if(fin_top!=0 && fin_antitop!=0) break;
  }
  if(!fin_top || !fin_antitop){
    ERROR("DecayHandle::GetDecayChain_TT", "Could not find either the top or antitop in the TruthParticle container. Pointer address for top: %i, antitop %i. Exit.", fin_top, fin_antitop);
    return StatusCode::FAILURE;
  }

  evt->pt1 = setTLV(fin_top);
  evt->pt2 = setTLV(fin_antitop);

  // ++++ Retrieve top's children +++++ //

  // t-> bW+ -> bl+nu
  for(int j=0; j<(int)getNChildren(fin_top); j++){
    xAOD::TruthParticle *top_child = (xAOD::TruthParticle*)getChild(fin_top,j);
    if(!top_child) continue;

    int top_child_pdg = top_child->pdgId();
    if(top_child_pdg==5) // b
      evt->pb1 = setTLV_fin(top_child);

    else if(top_child_pdg==24){ // W+
      xAOD::TruthParticle* fin_W = descend(top_child,0);
      GetWDecay(fin_W, W1);

    } // end of W's block
  } // end of loop over top's children

  if(W1.decayLabel==0)      nWhad++;
  else if(W1.decayLabel==1) nWlep++;
  else if(W1.decayLabel==2) nWtaulep++;
  else if(W1.decayLabel==3) nWtauhad++;


  // ++++ Retrieve anti-top's children +++++ //
  for(int j=0; j<(int)getNChildren(fin_antitop); j++){
    xAOD::TruthParticle *top_child = (xAOD::TruthParticle*)getChild(fin_antitop,j);
    if(!top_child) continue;

    int top_child_pdg = top_child->pdgId();
    if(top_child_pdg==-5)
      evt->pb2 = setTLV_fin(top_child);

    else if(top_child_pdg==-24){
      xAOD::TruthParticle* fin_W = descend(top_child,0);
      GetWDecay(fin_W, W2);

    } // end of W's block
  } // end of loop over top's children

  if(W2.decayLabel==0)      nWhad++;
  else if(W2.decayLabel==1) nWlep++;
  else if(W2.decayLabel==2) nWtaulep++;
  else if(W2.decayLabel==3) nWtauhad++;

  if(nWlep+nWtaulep+nWtauhad+nWhad!=2) {
    ERROR("DecayHandle::GetDecayChain_TT", "Number of chidrens are illegal. Exit: ");
    std::cout << " nWlep: " << nWlep << std::endl;
    std::cout << " nWtaulep: " << nWtaulep << std::endl;
    std::cout << " nWtauhad: " <<  nWtauhad << std::endl;
    std::cout << " nWhad: " << nWhad << std::endl;
    return StatusCode::FAILURE;
  }

  // ++++++ set_evt
  evt->set_evt(W1,W2);

  // ++++++++++++++ Fill additional boson ++++++++++++++++ //

  // tt + W/Z/H
  if(ProcClassifier::IsTTPlusW(m_DSID) || ProcClassifier::IsTTPlusOnshellZ(m_DSID) || ProcClassifier::IsTTPlusH(m_DSID)){

    const xAOD::TruthParticleContainer* contV =
      m_truthParticles ? m_truthParticles :
      m_truthBosonWDP ? m_truthBosonWDP :
      0;

    if(contV){

      for(int idx=0, n=(int)contV->size(); idx<n; idx++) {
	xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(contV->at(idx)) ;
	int pdg = mcp->pdgId();
	if( !(std::abs(pdg)==24 || pdg==23 || pdg==25  )) continue;

	xAOD::TruthParticle * fin = descend(mcp,0);
	if(ProcClassifier::IsTTPlusW(m_DSID) && std::abs(pdg)==24) {
	  Decay_boson this_W; GetWDecay(fin, this_W);
	  if(TruthDecayUtils::GetDR(this_W.P4,evt->B1.P4)>0.02 && TruthDecayUtils::GetDR(this_W.P4,evt->B2.P4)>0.02) evt->add_W(this_W);
	  break;
	}
	else if(ProcClassifier::IsTTPlusOnshellZ(m_DSID) && pdg==23) {
	  Decay_boson this_Z; GetZDecay(fin, this_Z);
	  evt->add_Z(this_Z);
	  break;
	}
	else if(ProcClassifier::IsTTPlusH(m_DSID) && pdg==25) {
	  Decay_H this_H; GetHDecay(fin, this_H);
	  evt->add_H(this_H);
	  break;
	}
      }
    }
  }

  // tt+ll (including off-shell Z)
  else if(ProcClassifier::IsTTPlusLL(m_DSID)) {
    xAOD::TruthParticleContainer* contV =
      ProcClassifier::IsTTPlusEE(m_DSID)     ? m_truthElectrons :  // tt+ee
      ProcClassifier::IsTTPlusMuMu(m_DSID)   ? m_truthMuons :      // tt+mumu
      ProcClassifier::IsTTPlusTauTau(m_DSID) ? m_truthTaus :       // tt+tautau
      0;

    int lep_pdg =
      ProcClassifier::IsTTPlusEE(m_DSID)     ? 11 :  // tt+ee
      ProcClassifier::IsTTPlusMuMu(m_DSID)   ? 13 :  // tt+mumu
      ProcClassifier::IsTTPlusTauTau(m_DSID) ? 15 :  // tt+tautau
      0;

    if(contV) {
      std::vector<xAOD::TruthParticle*> v_ll; // storing lepton candidates from Z deacy
      for(auto mcp : *contV) {
	if(mcp->pt()/1000.<1 || mcp->status()!=1) continue; // ignore too soft (pt<1GeV) leptons presumably coming from converted FSR photons

	tlv this_tlv = setTLV_fin(mcp);

	// Ignore if the lepton matchs with prompt leptons from ttbar
	if( std::abs(evt->B1.q1_pdg)==lep_pdg ){
	  if(TruthDecayUtils::GetDR(evt->B1.pq1,this_tlv)<0.05  ) continue;
	}

	if( std::abs(evt->B2.q1_pdg)==lep_pdg ){
	  if(TruthDecayUtils::GetDR(evt->B2.pq1,this_tlv)<0.05  ) continue;
	}


	xAOD::TruthParticle* fin = descend(mcp,0);
	if(v_ll.size()==0)
	  v_ll.push_back(fin);

	else if(v_ll.size()==1) {
	  if(v_ll.at(0)->pdgId()+fin->pdgId()==0)
	    v_ll.push_back(fin);
	}

	if(v_ll.size()==2) break;
      }
      Decay_boson Z;  evt->add_Z(Z);
    }
  }

  return StatusCode::SUCCESS;
}


//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_XX(TruthEvent_XX *evt){

  //
  // Retrive decay chain of pair produced C1/C2/N1/N2/N3/N4.
  //

  bool isGGMZh = ProcClassifier::IsXXGGMZh(m_DSID, m_forcedTruthProc);

  evt->clear();

  const xAOD::TruthParticleContainer* cont =
    m_truthBSMWDP ? m_truthBSMWDP :
    m_truthParticles ? m_truthParticles :
    0;

  if(!cont) return StatusCode::FAILURE;

  // +++++ Find prompt EW gauginos ++++++ //
  xAOD::TruthParticle * fin_chi1=0, *fin_chi2=0;
  for(int idx=0, n=(int)cont->size(); idx<n; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
    int pdg = mcp->pdgId();

    // if C1/C2/N1/N2/N3/N4
    if( (isGGMZh && pdg==1000022) || (!isGGMZh && isEWG(pdg) && pdg!=1000022) ){
      xAOD::TruthParticle* fin_mcp = descend(mcp,0);
      if     (fin_chi1==0 && fin_chi2==0 &&                      getNChildren(fin_mcp)>0) fin_chi1 = fin_mcp;
      else if(fin_chi1!=0 && fin_chi2==0 && fin_chi1!=fin_mcp && getNChildren(fin_mcp)>0) fin_chi2 = fin_mcp;
      else if(fin_chi1!=0 && fin_chi2!=0)  break;
    }
  }

  /*
  std::cout << "DecayHandle::GetDecayChain_XX  DEBUG  "
	    << " chi1: pdgId=" << fin_chi1->pdgId() << ", m=" << fin_chi1->m()/1000. << " GeV "
	    << " chi2: pdgId=" << fin_chi2->pdgId() << ", m=" << fin_chi2->m()/1000. << " GeV "
	    << std::endl;

  for(int j=0; j<(int)getNChildren(fin_chi1); ++j)
    std::cout << "fin_chi1 child " << j << " " << getChild(fin_chi1,j)->pdgId() << " " << getNChilren(getChild(fin_chi1,j)) << std::endl;

  for(int j=0; j<(int)getNChildren(fin_chi2); ++j)
    std::cout << "fin_chi2 child " << j << " " << getChild(fin_chi2,j)->pdgId() << " " << getNChilren(getChild(fin_chi2,j)) << std::endl;
  */


  // get decay info
  int chi1_child_pdg=0, chi2_child_pdg=0;
  if(fin_chi1){
    tlv dummy(0,0,0,0);
    evt->pchi1 = setTLV(fin_chi1);
    evt->chi1_pdg = fin_chi1->pdgId();
    if(isGGMZh) GetEWGDecayGGMZh(fin_chi1, evt->pN1A, chi1_child_pdg, evt->B1);
    else
      if(evt->chi1_pdg!=1000022) GetEWGDecay(fin_chi1, dummy, chi1_child_pdg, evt->B1, evt->pN1A);
  }

  if(fin_chi2){
    tlv dummy(0,0,0,0);
    evt->pchi2 = setTLV(fin_chi2);
    evt->chi2_pdg = fin_chi2->pdgId();
    if(isGGMZh) GetEWGDecayGGMZh(fin_chi2, evt->pN1B, chi2_child_pdg, evt->B2);
    else
      if(evt->chi2_pdg!=1000022) GetEWGDecay(fin_chi2, dummy, chi2_child_pdg, evt->B2, evt->pN1B);
  }

  // Check if two gauginos are found
  if(!fin_chi1 || !fin_chi2){
    ERROR("DecayHandle::GetDecayChain_XX", "Could not find two gauginos in the TruthParticle container. Pointer address for chi1: %i, chi2 %i. Exit.", fin_chi1, fin_chi2);
    return StatusCode::FAILURE;
  }

  // Check if the gaugino decays are filled
  if(!chi1_child_pdg){
    ERROR("DecayHandle::GetDecayChain_XX", "Couldn't retrieve chi1 decay. Exit.");
    return StatusCode::FAILURE;
  }
  if(!chi2_child_pdg){
    ERROR("DecayHandle::GetDecayChain_XX", "Couldn't retrieve chi2 decay. Exit.");
    return StatusCode::FAILURE;
  }

  // Notify user if the children of the gauginos are not chi10
  if(!isGGMZh){
    if(chi1_child_pdg!=1000022)
      INFO("DecayHandle::GetDecayChain_XX", "chi1 undergoes multi-step decay. Child_pdg: %i", chi1_child_pdg);
    if(chi2_child_pdg!=1000022)
      INFO("DecayHandle::GetDecayChain_XX", "chi2 undergoes multi-step decay. Child_pdg: %i", chi2_child_pdg);
  }

  // Finalize
  evt->finalize();

  return StatusCode::SUCCESS;

}


//////////////////////////////////////////////////
StatusCode DecayHandle::GetDecayChain_GG(TruthEvent_GG *evt){

  //
  // Retrive decay chain of pair produced gluino.
  //

  const xAOD::TruthParticleContainer* cont =
    m_truthBSMWDP ? m_truthBSMWDP :
    m_truthParticles ? m_truthParticles :
    0;
  if(!cont) return StatusCode::FAILURE;

  evt->clear();

  // +++++ find gluinos ++++++ //
  xAOD::TruthParticle * fin_go1=0, *fin_go2=0;
  for(int idx=0, n=(int)cont->size(); idx<n; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(cont->at(idx)) ;
    int pdg = mcp->pdgId();

    if(pdg==1000021){
      // look at the state just before its decay
      xAOD::TruthParticle* fin_mcp = descend(mcp,0);
      // if go1/2 are both empty, identify this gluino as go1
      if     (fin_go1==0 && fin_go2==0 && getNChildren(fin_mcp)>0)                     fin_go1 = fin_mcp;
      // if go1 is filled, and the current gluino is different from go1, identify this as go2
      else if(fin_go1!=0 && fin_go2==0 && fin_go1!=fin_mcp && getNChildren(fin_mcp)>0) fin_go2 = fin_mcp;
      // exit the loop once both gluinos are found.
      else if(fin_go1!=0 && fin_go2!=0)  break;
    }

  }

  // Check if two gluino are found
  if(!fin_go1 || !fin_go2){
    ERROR("DecayHandle::GetDecayChain_XX", "Could not find two gauginos in the TruthParticle container. Pointer address for go1: %i, go2 %i. Exit.", fin_go1, fin_go2);
    return StatusCode::FAILURE;
  }

  // Fill the gluino decays
  tlv dummy(0,0,0,0);

  evt->pgo1 = setTLV(fin_go1);
  GetGluinoDecay(fin_go1, evt->pq11, evt->q11_pdg, evt->pq12, evt->q12_pdg,
		 dummy, evt->chi1_pdg, evt->B1, evt->pN1A);

  evt->pgo2 = setTLV(fin_go2);
  GetGluinoDecay(fin_go2, evt->pq21, evt->q21_pdg, evt->pq22, evt->q22_pdg,
		 dummy, evt->chi2_pdg, evt->B2, evt->pN1B);

  evt->finalize();
  return StatusCode::SUCCESS;

}
/////////////////////////////////////////////////////////////////
StatusCode DecayHandle::GetFinalMuons(std::vector<xAOD::TruthParticle*> &particles,
				      std::vector<int> &parentPdgId,
				      std::vector<int> &parentBarcode){

  // Get status=1 muons
  if(!m_truthMuons) return StatusCode::FAILURE;

  particles.clear();
  parentPdgId.clear();
  parentBarcode.clear();

  for(auto mu : *m_truthMuons){
    if(mu->status()!=1) continue;
    particles.push_back((xAOD::TruthParticle*)mu);

    xAOD::TruthParticle *init_mu = ascend(mu);
    xAOD::TruthParticle *parent = getParent(init_mu);
    int ppdg = 0;
    int pbarcode = 0;
    if(parent){
      ppdg = parent->pdgId();
      pbarcode = parent->barcode();
    }
    parentPdgId.push_back(ppdg);
    parentBarcode.push_back(pbarcode);
  }

  return StatusCode::SUCCESS;

}
/////////////////////////////////////////////////////////////////
StatusCode DecayHandle::GetFinalNeutrinos(std::vector<xAOD::TruthParticle*> &particles,
					  std::vector<int> &parentPdgId,
					  std::vector<int> &parentBarcode){

  // Get status=1 neutrinos
  if(!m_truthNeutrinos) return StatusCode::FAILURE;

  particles.clear();
  parentPdgId.clear();
  parentBarcode.clear();

  for(auto nu : *m_truthNeutrinos){
    if(nu->status()!=1) continue;
    particles.push_back((xAOD::TruthParticle*)nu);

    xAOD::TruthParticle *init_nu = ascend(nu);
    xAOD::TruthParticle *parent = getParent(init_nu);
    int ppdg = 0;
    int pbarcode = 0;
    if(parent){
      ppdg = parent->pdgId();
      pbarcode = parent->barcode();
    }
    parentPdgId.push_back(ppdg);
    parentBarcode.push_back(pbarcode);
  }

  return StatusCode::SUCCESS;

}

/////////////////////////////////////////////////////////////////
StatusCode DecayHandle::GetFakeLeptons(){

  if(!m_truthParticles) return StatusCode::FAILURE;

  for(int idx=0, n=(int)m_truthParticles->size(); idx<n; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(m_truthParticles->at(idx)) ;
    if(mcp->status()!=1) continue;
    if( std::abs(mcp->pdgId())!=11 && std::abs(mcp->pdgId())!=13 ) continue;

    if( isHF_nonPrompt_B(mcp) ){
      xAOD::TruthParticle* parent = getParent(mcp);
      if(!parent) continue;

      xAOD::TruthParticle* grandParent = getParent(parent);
      if(!grandParent) continue;
      std::cout << "NPL grandParent: " << grandParent->pdgId() << std::endl;


      xAOD::TruthParticle* grandGrandParent = getParent(parent);
      if(!grandGrandParent) continue;
      std::cout << "NPL grandGrandParent: " << grandGrandParent->pdgId() << std::endl;

      xAOD::TruthParticle* grandGrandGrandParent = getParent(parent);
      if(!grandGrandGrandParent) continue;
      std::cout << "NPL grandGrandGrandParent: " << grandGrandGrandParent->pdgId() << std::endl;


    }
  }
  return StatusCode::SUCCESS;
}

/////////////////////////////////////////////////////////////////
StatusCode DecayHandle::GetTaus_SherpaBoson(std::vector<xAOD::TruthParticle*> &v_taus, std::vector<Decay_tau> &v_tauDecays){

  v_taus.clear();
  v_tauDecays.clear();
  for(int idx=0, n=(int)m_truthTaus->size(); idx<n; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(m_truthTaus->at(idx)) ;

    if(!cacc_TDC_truthOrigin.isAvailable(*mcp)) continue;
    if(cacc_TDC_truthOrigin(*mcp)!=43 && cacc_TDC_truthOrigin(*mcp)!=47) continue;         // require to be from W/Z or diboson or triboson

    v_taus.push_back(mcp);

    Decay_tau tauDecay;
    GetTauDecay(mcp, tauDecay);
    v_tauDecays.push_back(tauDecay);
  }

  return StatusCode::SUCCESS;

}
/////////////////////////////////////////////////////////////////
StatusCode DecayHandle::GetNonTauElectrons_SherpaBoson(std::vector<xAOD::TruthParticle*> &v_eles, const std::vector<Decay_tau> &v_tauDecays){

  if(!m_truthElectrons) return StatusCode::SUCCESS;

  v_eles.clear();

  for(int idx=0, n=(int)m_truthElectrons->size(); idx<n; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(m_truthElectrons->at(idx)) ;
    if(!cacc_TDC_truthOrigin.isAvailable(*mcp)) continue;
    if(cacc_TDC_truthOrigin(*mcp)!=43 && cacc_TDC_truthOrigin(*mcp)!=47) continue;    // require to be from diboson or triboson
    if( mcp->status()!=1 ) continue;

    bool decayFromTaus=false;
    for(auto tauDecay : v_tauDecays){
      if(isFromTau((xAOD::TruthParticle*)mcp, tauDecay)) { decayFromTaus=true; break; }
    }
    if(decayFromTaus) continue;

    v_eles.push_back(mcp);
  }

  return StatusCode::SUCCESS;
}
/////////////////////////////////////////////////////////////////
StatusCode DecayHandle::GetNonTauMuons_SherpaBoson(std::vector<xAOD::TruthParticle*> &v_mus, const std::vector<Decay_tau> &v_tauDecays){

  if(!m_truthMuons) return StatusCode::SUCCESS;

  v_mus.clear();

  for(int idx=0, n=(int)m_truthMuons->size(); idx<n; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(m_truthMuons->at(idx)) ;
    if(!cacc_TDC_truthOrigin.isAvailable(*mcp)) continue;
    if(cacc_TDC_truthOrigin(*mcp)!=43 && cacc_TDC_truthOrigin(*mcp)!=47) continue;    // require to be from diboson or triboson
    if( mcp->status()!=1 ) continue;

    bool decayFromTaus=false;
    for(auto tauDecay : v_tauDecays){
      if(isFromTau((xAOD::TruthParticle*)mcp, tauDecay)) { decayFromTaus=true; break; }
    }
    if(decayFromTaus) continue;

    v_mus.push_back(mcp);
  }

  return StatusCode::SUCCESS;
}
/////////////////////////////////////////////////////////////////
StatusCode DecayHandle::GetNonTauNeutrlinos_SherpaBoson(std::vector<xAOD::TruthParticle*> &v_nus, const std::vector<Decay_tau> &v_tauDecays){

  if(!m_truthNeutrinos) return StatusCode::SUCCESS;

  v_nus.clear();

  for(int idx=0, n=(int)m_truthNeutrinos->size(); idx<n; idx++) {
    xAOD::TruthParticle* mcp = (xAOD::TruthParticle*)(m_truthNeutrinos->at(idx)) ;
    if(!cacc_TDC_truthOrigin.isAvailable(*mcp)) continue;
    if(cacc_TDC_truthOrigin(*mcp)!=43 && cacc_TDC_truthOrigin(*mcp)!=47) continue;    // require to be from diboson or triboson
    if( mcp->status()!=1 ) continue;

    //auto vtx = mcp->prodVtx();
    //if(vtx) std::cout << "aaa2 " << idx << ": " << vtx->x() << " " << vtx->y() << " " << vtx->z() << std::endl;

    bool decayFromTaus=false;
    for(auto tauDecay : v_tauDecays){
      if(isFromTau((xAOD::TruthParticle*)mcp, tauDecay)) { decayFromTaus=true; break; }
    }
    if(decayFromTaus) continue;

    v_nus.push_back(mcp);
  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////
