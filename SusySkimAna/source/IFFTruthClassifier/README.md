# IFFTruthClassifier

This repo contains the IFFTruthClassifier, a tool for classifying 
electrons and muons based on their type and origin. The definition of the 
classifications can be found in a variety of resources 
([here](http://cern.ch/go/8Qnf), [here](http://cern.ch/go/g6DR)
and [here](http://cern.ch/go/9fBD)).

The current version of the tool is: **1.0** (released on 2019-12-03, [CHANGELOG.md](CHANGELOG.md))

After creating and initializing your tool, there is only one function to 
worry about, `classify`, which can be run on `xAOD::Electron`s or `xAOD::Muon`s.
This will give you back an `IFF::Type` which is one of:

+ Unknown
+ KnownUnknown
+ IsoElectron
+ ChargeFlipIsoElectron
+ PromptMuon
+ PromptPhotonConversion
+ ElectronFromMuon
+ TauDecay
+ BHadronDecay
+ CHadronDecay
+ LightFlavorDecay

The types that deserve particular attention are the `Unknown` and `KnownUnknown`
types. We want there to be no `Unknown` electrons or muons and so will print
a warning if the classifier returns that type. If this happens please contact the 
IFF group to let them know! `KnownUnknown`s are things that we can't classify 
because of missing information. This doesn't nessesarily guarantee there isn't a 
problem, but we don't print out a lot of warnings for these. Other types hopefully
have somewhat intuitive names. If this isn't the case please get in touch.

## Testing the classifier 
### AnalysisBase / EventLoop
In the non-athena releases, a test executable, `testIFFClassifier`, is compiled for you when building the package. 
It can be run via 
```
testIFFClassifier <my xAOD file> 
```
The corresponding source file, `util/test_iff_class.cxx` can be referred to for an example on tool setup in standalone jobs. 
### Athena 
When running in an athena environment, for example `AthAnalysis`, a test equivalent to the above is provided in form of the `IFFTruthClassifierTestAlg` algorithm. 
A set of premade job options is included with the packaged that allow you to run this alg on an input file. 
To do so, call 
```
athena --filesInput <my xAOD file>   IFFTruthClassifier/IFFTruthClassifierTest_jobOptions.py
```
Again, the corresponding sources (`src/IFFTruthClassifierTestAlg.cxx/h`) can be referred to as setup examples for athena. 
