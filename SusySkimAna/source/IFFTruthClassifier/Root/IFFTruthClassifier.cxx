#include <set>
#include <string>

#include "IFFTruthClassifier/IFFTruthClassifier.h"
#include "MCTruthClassifier/MCTruthClassifierDefs.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"

IFFTruthClassifier::IFFTruthClassifier(const std::string& type)
    : asg::AsgTool(type),
      truth_type("truthType"),
      truth_origin("truthOrigin"),
      mother_truth_type("firstEgMotherTruthType"),
      mother_truth_origin("firstEgMotherTruthOrigin"),
      mother_pdg_id("firstEgMotherPdgId") {}

StatusCode IFFTruthClassifier::initialize() {
  ATH_MSG_INFO("Initialize IFFTruthClassifier");
  return StatusCode::SUCCESS;
}

StatusCode IFFTruthClassifier::finalize() {
  ATH_MSG_INFO("Finalize IFFTruthClassifier");
  return StatusCode::SUCCESS;
}

IFF::Type IFFTruthClassifier::classify(const xAOD::Electron& electron) const {
  namespace MC = MCTruthPartClassifier;

  if (is_prompt_electron(electron)) {
    if (is_charge_flip(electron)) {
      return IFF::Type::ChargeFlipIsoElectron;
    } else {
      return IFF::Type::IsoElectron;
    }
  }

  int type = truth_type(electron);
  int origin = truth_origin(electron);
  int mum_type = mother_truth_type(electron);
  int mum_origin = mother_truth_origin(electron);

  // Tau decays
  // Non-isolated electron/photon from tau decay
  if ((type == MC::NonIsoElectron || type == MC::NonIsoPhoton) &&
      origin == MC::TauLep) {
    return IFF::Type::TauDecay;
  }

  // tau -> tau gamma, gamma -> e+ e-
  if (type == MC::BkgElectron && origin == MC::PhotonConv &&
      (mum_type == MC::NonIsoPhoton || mum_type == MC::NonIsoElectron) &&
      mum_origin == MC::TauLep) {
    return IFF::Type::TauDecay;
  }

  // Prompt Photon Conversions

  // gamma -> e+ e-
  if (type == MC::BkgElectron &&
      (origin == MC::PhotonConv || origin == MC::ElMagProc) &&
      mum_type == MC::IsoPhoton && mum_origin == MC::PromptPhot) {
    return IFF::Type::PromptPhotonConversion;
  }

  // H -> gamma gamma, gamma -> e+ e-
  if (type == MC::BkgElectron && origin == MC::PhotonConv &&
      mum_type == MC::IsoPhoton && mum_origin == MC::Higgs) {
    return IFF::Type::PromptPhotonConversion;
  }

  if (type == MC::BkgElectron && origin == MC::PhotonConv &&
      mum_type == MC::BkgPhoton && mum_origin == MC::UndrPhot) {
    return IFF::Type::PromptPhotonConversion;
  }

  // Is a photon?
  if (type == MC::IsoPhoton && origin == MC::PromptPhot) {
    return IFF::Type::PromptPhotonConversion;
  }

  // type = 16 and origin = 38 (again, this is a photon)
  if (type == MC::BkgPhoton && origin == MC::UndrPhot) {
    return IFF::Type::PromptPhotonConversion;
  }

  if (type == MC::BkgElectron && origin == MC::PhotonConv &&
      mum_type == MC::BkgPhoton && mum_origin == MC::UndrPhot &&
      !is_charge_flip(electron)) {
    return IFF::Type::PromptPhotonConversion;
  }

  // TODO: Message from Otilia: """
  // but it's not clear if these electrons are really
  // "fakes" or they should go in the real category (we don't know from where
  // this photon is coming...). I would say more truth studies should be done.
  // """
  // Hence the warning message...
  if (type == MC::BkgElectron && origin == MC::PhotonConv &&
      mum_type == MC::Unknown && mum_origin == MC::ZBoson) {
    ATH_MSG_WARNING(
        "Electron identified as from a PromptPhotonConversion, "
        "but this type of electron needs further study!");
    return IFF::Type::PromptPhotonConversion;
  }

  // Is muon reco as electron
  if ((type == MC::NonIsoElectron || type == MC::NonIsoPhoton) &&
      origin == MC::Mu) {
    return IFF::Type::ElectronFromMuon;
  }

  if (type == MC::BkgElectron && mum_origin == MC::Mu) {
    if ((origin == MC::ElMagProc && mum_type == MC::NonIsoElectron) ||
        (origin == MC::PhotonConv && mum_type == MC::NonIsoPhoton)) {
      return IFF::Type::ElectronFromMuon;
    }
  }

  if (type == MC::IsoMuon || type == MC::BkgMuon) {
    return IFF::Type::ElectronFromMuon;
  }

  // Light hadron decays
  if (type == MC::Hadron) {
    return IFF::Type::LightFlavorDecay;
  }

  if (type == MC::BkgElectron) {
    if (origin == MC::DalitzDec || origin == MC::LightMeson ||
        origin == MC::StrangeMeson) {
      return IFF::Type::LightFlavorDecay;
    }
    if (origin == MC::PhotonConv && mum_type == MC::BkgPhoton &&
        (mum_origin == MC::PiZero || mum_origin == MC::LightMeson ||
         mum_origin == MC::StrangeMeson)) {
      return IFF::Type::LightFlavorDecay;
    }
    if (origin == MC::ElMagProc &&
        ((mum_type == MC::BkgElectron && mum_origin == MC::StrangeMeson) ||
         (mum_type == MC::BkgPhoton &&
          (mum_origin == MC::LightMeson || mum_origin == MC::PiZero)))) {
      return IFF::Type::LightFlavorDecay;
    }
  }

  if (type == MC::BkgPhoton &&
      (origin == MC::PiZero || origin == MC::LightMeson ||
       origin == MC::StrangeBaryon || origin == MC::StrangeMeson)) {
    return IFF::Type::LightFlavorDecay;
  }

  // From B hadron
  if (has_b_hadron_origin(origin) || has_b_hadron_origin(mum_origin)) {
    return IFF::Type::BHadronDecay;
  }

  // From C hadron
  if (has_c_hadron_origin(origin) || has_c_hadron_origin(mum_origin)) {
    return IFF::Type::CHadronDecay;
  }

  // TODO: See if we want this or not. Now we check if this is something we are
  // able to classify or not. Note that this might be a bit dangerous because
  // the reasons for not having origin and status codes might be complex. The
  // main idea is to weed out things we don't have a hope of classifying due to
  // missing or unknown information.
  const xAOD::TruthParticle* truth_particle =
      xAOD::TruthHelpers::getTruthParticle(electron);
  int status = truth_particle ? truth_particle->status() : 0;
  bool stable = (status == 1);

  if (!stable && origin == MC::NonDefined && type == MC::Unknown) {
    return IFF::Type::KnownUnknown;
  }
  if (stable && origin == MC::NonDefined && mum_origin == MC::NonDefined) {
    if ((type == MC::Unknown && mum_type == MC::Unknown) ||
        (type == MC::UnknownElectron && mum_type == MC::UnknownElectron)) {
      return IFF::Type::KnownUnknown;
    }

    if (type == MC::UnknownPhoton) {
      return IFF::Type::KnownUnknown;
    }
  }

  if (truth_particle) {
    const xAOD::TruthParticle* parent = truth_particle;
    ATH_MSG_INFO("Searching for truth particle chain...");
    std::string out;
    while (parent) {
      out += std::to_string(parent->pdgId());
      // std::cout << parent->pdgId();
      parent = parent->parent();
      if (parent) out += " -> ";  // std::cout << " -> ";
    }
    ATH_MSG_INFO(out);
  }

  return IFF::Type::Unknown;
}

IFF::Type IFFTruthClassifier::classify(const xAOD::Muon& muon) const {
  namespace MC = MCTruthPartClassifier;

  int type = truth_type(muon);
  int origin = truth_origin(muon);

  // Check if the type of muon is IsoMuon(6) and whether the origin
  // of the muon is from a prompt source
  static const std::set<int> prompt_source({
      MC::SingleMuon,  // Single muon (origin = 2) from muon twiki
      MC::top,
      MC::WBoson,
      MC::ZBoson,
      MC::Higgs,
      MC::HiggsMSSM,
      MC::SUSY,
      MC::DiBoson,
  });
  if ((type == MC::IsoMuon) && is_in_set(origin, prompt_source)) {
    return IFF::Type::PromptMuon;
  }

  if (((type == MC::IsoMuon) || (type == MC::NonIsoMuon)) &&
      has_b_hadron_origin(origin)) {
    return IFF::Type::BHadronDecay;
  }

  if (type == MC::NonIsoMuon && has_c_hadron_origin(origin)) {
    return IFF::Type::CHadronDecay;
  }

  // TODO:: There is a comment in the example code about J/psi but there is a
  // separate origin code for that: `MC::JPsi == 28.`
  if ((type == MC::IsoMuon || type == MC::BkgMuon) &&
      origin == MC::CCbarMeson) {
    return IFF::Type::CHadronDecay;
  }

  // TODO: The muon twiki includes Light/Strange Baryons but these are not
  // included in the example code
  static const std::set<int> light_source({
      MC::LightMeson,
      MC::StrangeMeson,
      MC::LightBaryon,
      MC::StrangeBaryon,
      MC::PionDecay,
      MC::KaonDecay,
  });
  if (type == MC::BkgMuon && is_in_set(origin, light_source)) {
    return IFF::Type::LightFlavorDecay;
  }

  if (type == MC::Hadron) {
    return IFF::Type::LightFlavorDecay;
  }

  if (type == MC::NonIsoMuon && origin == MC::TauLep) {
    return IFF::Type::TauDecay;
  }

  // TODO:: See comment above about known unknowns for electrons
  if (type == MC::UnknownMuon && origin == MC::NonDefined) {
    return IFF::Type::KnownUnknown;
  }

  const xAOD::TruthParticle* truth_particle =
      xAOD::TruthHelpers::getTruthParticle(muon);
  int status = truth_particle ? truth_particle->status() : 0;
  bool stable = (status == 1);
  if (!stable && type == MC::Unknown && origin == MC::NonDefined) {
    // Data
    return IFF::Type::KnownUnknown;
  } else if (!stable && type == -99999 && origin == -99999) {
    // MC - no status=1 truth particle associated with the primary track
    return IFF::Type::KnownUnknown;
  }

  ATH_MSG_WARNING("Muon type unknown: type = " << type << ", origin = " << origin);

  return IFF::Type::Unknown;
}

bool IFFTruthClassifier::is_prompt_electron(
    const xAOD::Electron& electron) const {
  namespace MC = MCTruthPartClassifier;

  // 1. Electron is IsoElectron - return true
  int type = truth_type(electron);
  if (type == MC::IsoElectron) {
    return true;
  }

  int origin = truth_origin(electron);
  int mum_type = mother_truth_type(electron);
  int mum_origin = mother_truth_origin(electron);

  // Adding these cases from ElectronEfficiencyHelpers
  if (mum_type == MC::IsoElectron) {
    return true;
  }

  // FSR photons from electrons
  if (origin == MC::FSRPhot && type == MC::NonIsoPhoton) {
    return true;
  }

  if (mum_origin == MC::FSRPhot && mum_type == MC::NonIsoPhoton) {
    return true;
  }

  // If we reach here then it is not a prompt electron
  return false;
}

bool IFFTruthClassifier::is_charge_flip(const xAOD::Electron& electron) const {
  return (mother_pdg_id(electron) * electron.charge()) > 0;
}

bool IFFTruthClassifier::has_b_hadron_origin(int origin) const {
  namespace MC = MCTruthPartClassifier;
  static const std::set<int> b_hadrons({
      MC::BottomMeson,
      MC::BBbarMeson,
      MC::BottomBaryon,
  });
  return is_in_set(origin, b_hadrons);
}

bool IFFTruthClassifier::has_c_hadron_origin(int origin) const {
  namespace MC = MCTruthPartClassifier;
  static const std::set<int> c_hadrons({
      MC::CharmedMeson,
      MC::CCbarMeson,
      MC::CharmedBaryon,
  });
  return is_in_set(origin, c_hadrons);
}
