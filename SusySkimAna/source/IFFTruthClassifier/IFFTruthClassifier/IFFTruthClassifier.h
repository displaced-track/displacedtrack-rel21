#ifndef IFFTRUTHCLASSIFIER_IFFTRUTHCLASSIFIER_H_
#define IFFTRUTHCLASSIFIER_IFFTRUTHCLASSIFIER_H_

#include <string>
#include <set>

#include "AsgTools/AsgTool.h"
#include "IFFTruthClassifier/IIFFTruthClassifier.h"
#include "IFFTruthClassifier/IFFTruthClassifierDefs.h"
#include "xAODEgamma/Electron.h"
#include "xAODMuon/Muon.h"

#ifndef XAOD_ANALYSIS
#include "GaudiKernel/ToolHandle.h"
#endif


class IFFTruthClassifier :
virtual public IIFFTruthClassifier, public asg::AsgTool {
  ASG_TOOL_CLASS(IFFTruthClassifier, IIFFTruthClassifier)

 public:
  explicit IFFTruthClassifier(const std::string& type);
  virtual ~IFFTruthClassifier() {}

  StatusCode initialize() override;
  StatusCode finalize()
#ifndef XAOD_ANALYSIS
    override
#endif
    ;

  virtual IFF::Type classify(const xAOD::Electron& electron) const override;
  virtual IFF::Type classify(const xAOD::Muon& muon) const override;

 private:
  const SG::AuxElement::Accessor<int> truth_type;
  const SG::AuxElement::Accessor<int> truth_origin;
  const SG::AuxElement::Accessor<int> mother_truth_type;
  const SG::AuxElement::Accessor<int> mother_truth_origin;
  const SG::AuxElement::Accessor<int> mother_pdg_id;

  bool is_prompt_electron(const xAOD::Electron& electron) const;
  bool is_charge_flip(const xAOD::Electron& electron) const;

  // Helpers to see if the origin is a b/c hadron
  bool has_b_hadron_origin(int origin) const;
  bool has_c_hadron_origin(int origin) const;

  inline bool is_in_set(int origin, const std::set<int>& s) const {
    return s.find(origin) != s.end();
  }
};

#endif  // IFFTRUTHCLASSIFIER_IFFTRUTHCLASSIFIER_H_
