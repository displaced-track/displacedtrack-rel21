# The name of the package
atlas_subdir (IFFTruthClassifier)

atlas_depends_on_subdirs(
  PUBLIC
  MCTruthClassifier
  MuonAnalysisInterfaces
  xAODMuon
  xAODEgamma
  xAODTruth
  xAODEventInfo
  )

# Add the shared library
atlas_add_library (IFFTruthClassifierLib
  IFFTruthClassifier/*.h Root/*.cxx
  PUBLIC_HEADERS IFFTruthClassifier
  LINK_LIBRARIES AsgTools xAODEgamma xAODMuon xAODEventInfo)

if (NOT XAOD_STANDALONE)
  # Add a component library for AthAnalysis only:
  atlas_add_component (IFFTruthClassifier
      src/*.h src/*.cxx src/components/*.cxx
    LINK_LIBRARIES IFFTruthClassifierLib)
else()
# Add the dictionary:
atlas_add_dictionary (IFFTruthClassifier
  IFFTruthClassifier/IFFTruthClassifierDict.h
  IFFTruthClassifier/selection.xml
  LINK_LIBRARIES IFFTruthClassifierLib)
endif ()

# Executable(s) in the package:
if( XAOD_STANDALONE )
   atlas_add_executable( testIFFClassifier
      util/test_iff_class.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} MuonSelectorTools ElectronEfficiencyCorrection
      LINK_LIBRARIES ${ROOT_LIBRARIES} xAODRootAccess xAODEgamma xAODMuon MuonSelectorToolsLib ElectronEfficiencyCorrectionLib
      IFFTruthClassifierLib )
endif()


# Install files from the package:
atlas_install_joboptions( share/*.py )
