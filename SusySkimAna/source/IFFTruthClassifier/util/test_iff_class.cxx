#include <map>

// ROOT include(s):
#include <TError.h>
#include <TFile.h>
#include <TString.h>
// Infrastructure include(s):
#ifdef ROOTCORE
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#endif  // ROOTCORE
// EDM include(s):
#include "IFFTruthClassifier/IFFTruthClassifier.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODMuon/MuonContainer.h"

// For more advanced testing, we do some muon selection
#include "MuonSelectorTools/MuonSelectionTool.h"

// For cross-checking with the electron efficiency helpers
#include "ElectronEfficiencyCorrection/ElectronEfficiencyHelpers.h"

// and some truth particle checks
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/xAODTruthHelpers.h"

#define CHECK(ARG)                                          \
  do {                                                      \
    const bool result = ARG;                                \
    if (!result) {                                          \
      ::Error(APP_NAME, "Failed to execute: \"%s\"", #ARG); \
      return 1;                                             \
    }                                                       \
  } while (false)

int main(int argc, char* argv[]) {
  // The application's name:
  const char* APP_NAME = argv[0];
  // Check if we received a file name:
  if (argc < 2) {
    Error(APP_NAME, "No file name received!");
    Error(APP_NAME, "  Usage: %s [xAOD file name]", APP_NAME);
    return 1;
  }

  // Probably a better way of doing this...
  std::map<int, std::string> IFF_names;
  IFF_names[0] = "Unknown";
  IFF_names[1] = "KnownUnknown";
  IFF_names[2] = "IsoElectron";
  IFF_names[3] = "ChargeFlipIsoElectron";
  IFF_names[4] = "PromptMuon";
  IFF_names[5] = "PromptPhotonConversion";
  IFF_names[6] = "ElectronFromMuon";
  IFF_names[7] = "TauDecay";
  IFF_names[8] = "BHadronDecay";
  IFF_names[9] = "CHadronDecay";
  IFF_names[10] = "LightFlavorDecay";

  // Initialise the application:
  CHECK(xAOD::Init(APP_NAME));

  // Open the input file:
  const TString fileName = argv[1];
  Info(APP_NAME, "Opening file: %s", fileName.Data());
  std::unique_ptr<TFile> ifile(TFile::Open(fileName, "READ"));
  CHECK(ifile.get());

  // Create a TEvent object:
  xAOD::TEvent event;
  CHECK(event.readFrom(ifile.get()));
  Info(APP_NAME, "Number of events in the file: %i",
       static_cast<int>(event.getEntries()));

  // Decide how many events to run over:
  Long64_t entries = event.getEntries();
  if (argc > 2) {
    const Long64_t e = atoll(argv[2]);
    if (e < entries) {
      entries = e;
    }
  }

  bool do_electrons = true;
  bool do_muons = true;

  IFFTruthClassifier myClassifier("IFFClassifier");
  CHECK(myClassifier.initialize());

  static SG::AuxElement::Accessor<int> tT("truthType");
  static SG::AuxElement::Accessor<int> tO("truthOrigin");
  static SG::AuxElement::Accessor<int> mum_type("firstEgMotherTruthType");
  static SG::AuxElement::Accessor<int> mum_origin("firstEgMotherTruthOrigin");
  static SG::AuxElement::Accessor<int> mum_pdgid("firstEgMotherPdgId");
  // Ambiguity flag
  static SG::AuxElement::Accessor<int> ambiguity("DFCommonAddAmbiguity");
  static SG::AuxElement::Accessor<char> pass_DFloose(
      "DFCommonElectronsLHLooseBL");
  static SG::AuxElement::Accessor<char> pass_DFtight("DFCommonElectronsLHTight");

  CP::MuonSelectionTool muon_selector("muon_selector");
  // Tight: 0, Med: 1, Loose: 2, VeryLoose: 3
  CHECK(asg::setProperty(muon_selector, "MuQuality", 3));
  CHECK(asg::setProperty(muon_selector, "MaxEta", 2.4));
  CHECK(muon_selector.initialize());

  std::map<int, int> el_counter;
  std::map<int, int> mu_counter;
  std::map<int, std::map<int, int>> unknown_el_type_origin_counter;
  std::map<int, int> unknown_mu_origin_counter;

  std::map<int, std::map<int, std::map<int, std::map<int, int>>>> ambiguity_map;

  // Loop over the events:
  for (Long64_t entry = 0; entry < entries; ++entry) {
    // Tell the object which entry to look at:
    event.getEntry(entry);

    // Info(APP_NAME, "=================  NEXT EVENT  =================");

    // Electrons
    const xAOD::ElectronContainer* electrons = 0;
    CHECK(event.retrieve(electrons, "Electrons"));

    // unsigned int i = 0;

    if (do_electrons) {
      for (const auto& el : *electrons) {
        // Look at electrons with pT > 1 GeV, |eta| < 2.5, LHLooseBL
        if (el->pt() < 1000 || std::abs(el->eta()) > 2.5 || !pass_DFloose(*el))
          continue;

        int iff_type = static_cast<int>(myClassifier.classify(*el));

        el_counter[iff_type]++;
        if (iff_type == 0) {
          Info(APP_NAME,
               "Unknown electron passing loose selection with type = %d and "
               "origin = %d, and mother type = %d and origin = %d",
               tT(*el), tO(*el), mum_type(*el), mum_origin(*el));
          unknown_el_type_origin_counter[tT(*el)][tO(*el)]++;
        }

        // How ambiguity works (from
        // https://gitlab.cern.ch/atlas/athena/merge_requests/24852/)
        // // cuts to define the various types :
        // -1 : no other track,
        // 0 : other track exists but no good gamma reco,
        // 1 : gamma*,
        // 2 : material conversion
        if (ambiguity.isAvailable(*el)) {
        int amb = ambiguity(*el);
        if (amb >= 0) ambiguity_map[iff_type][amb][tT(*el)][tO(*el)]++;
        }
      }
    }

    // Muons

    if (do_muons) {
      const xAOD::EventInfo* event_info = 0;
      CHECK(event.retrieve(event_info, "EventInfo"));

      const xAOD::MuonContainer* muons = 0;
      CHECK(event.retrieve(muons, "Muons"));

      for (const auto& mu : *muons) {
        if (mu->pt() < 1000 || std::abs(mu->eta()) > 2.5) continue;
        if (!muon_selector.accept(mu)) continue;

        int iff_type = static_cast<int>(myClassifier.classify(*mu));
        mu_counter[iff_type]++;
        // Info(APP_NAME, "Muon type from IFF Classifier = %d", iff_type);
        if (iff_type == 0) {
          unknown_mu_origin_counter[tO(*mu)]++;
          // debug_map[tT(*mu)][tO(*mu)]++;
          const xAOD::TruthParticle* truth_particle =
              xAOD::TruthHelpers::getTruthParticle(*mu);
          if (!truth_particle) {
            Info(APP_NAME, "Unknown muon with NO truth match");
          }
        }
      }
      // Info(APP_NAME, "===>>>  done processing event #%lld ", entry);
    }

  }  // Event loop

  Info(APP_NAME, "Electron summary");
  for (const auto& i : el_counter) {
    Info(APP_NAME, "%23s : %d", IFF_names.at(i.first).c_str(), i.second);
  }
  Info(APP_NAME, "Muon summary");
  for (const auto& i : mu_counter) {
    Info(APP_NAME, "%23s : %d", IFF_names.at(i.first).c_str(), i.second);
  }
  Info(APP_NAME, "Unknown electron origin");
  for (const auto& i : unknown_el_type_origin_counter) {
    for (const auto& j: i.second) {
      Info(APP_NAME, "%d, %d = %d", i.first, j.first, j.second);
    }
  }
  Info(APP_NAME, "Unknown muon origin");
  for (const auto& i : unknown_mu_origin_counter) {
    Info(APP_NAME, "%d : %d", i.first, i.second);
  }

  Info(APP_NAME, "AMBIGUITY MAP");
  Info(APP_NAME, "               IFF Type | Amb | MC Type | MC Origin | Count");
  Info(APP_NAME, "-----------------------------------------------------------");
  for (const auto& i : ambiguity_map) {
    for (const auto& j : i.second) {
      for (const auto& k : j.second) {
        for (const auto& m : k.second) {
          Info(APP_NAME, "%23s | %3d | %7d | %9d | %5d", IFF_names.at(i.first).c_str(), j.first,
               k.first, m.first, m.second);
        }
      }
    }
  }

  return 0;
}
