#include <cmath>
#include "SimpleAnalysisFramework/AnalysisClass.h"
#include <fstream>
#include <iostream>


#define gev 1000
using namespace lwt;

DefineAnalysis(StopOneLepton2018)


void StopOneLepton2018::Init() {
  addRegions({"tN_med", "tN_high"});
  addRegions({"tN_diag_low", "tN_diag_high"});
  addRegions({"bWN"});
  addRegions({"bffN_btag"});
  // addRegions({"bffN_softb"});
  addRegions({"DM"});

  addHistogram("Met",20,0,2000);
  addHistogram("outputScore_RNN",20,0,1);

  //sumofweights = 0.;


  // For bWN RNN: Read in the network file
  std::string in_file_name(FindFile("neural_net_StopOneLepton2018.json"));
  std::ifstream in_file(in_file_name);
  // For the functional model you need to define an output node.
  lwt::GraphConfig config(lwt::parse_json_graph(in_file));
  RNN_graph = std::make_unique<lwt::LightweightGraph>(config);




}

//----------------------------------------------------------------------------//

void StopOneLepton2018::ProcessEvent(AnalysisEvent* event) {
  auto baseElectrons  = event->getElectrons(4.5, 2.47, ELooseBLLH);
  auto baseMuons      = event->getMuons(4,2.7, MuMedium | MuZ05mm);
  auto baseJets       = event->getJets(20., 4.5);
  auto baseTaus       = event->getTaus(20., 2.5, TauLoose);
  auto metVec         = event->getMET();
  float genMet        = event->getGenMET();
  float Met           = metVec.Pt();

  // Reject events with bad jets
  // CHECK: right place?

  if (countObjects(baseJets, 20, 4.5, NOT(LooseBadJet)) != 0) return;

  // overlap removal
  auto radiusCalcLepton = [](const AnalysisObject& lepton, const AnalysisObject&) {
    return std::max(0.1,std::min(0.4, 0.04 + 10 / lepton.Pt()));

  };

  auto muJetSpecial = [](const AnalysisObject& jet, const AnalysisObject& muon) {
    if (jet.pass(NOT(BTag77MV2c10)) && (jet.pass(LessThan3Tracks) || muon.Pt() / jet.Pt() > 0.7))
      return 0.2;
    else
      return 0.;
  };

  /* actually the Delta R requirement is a shared track but here this is simulated by Delta R < 0.01 */
  baseMuons     = overlapRemoval(baseMuons, baseElectrons, 0.01, NOT(MuCaloTaggedOnly));
  baseElectrons = overlapRemoval(baseElectrons, baseMuons, 0.01);

  baseJets      = overlapRemoval(baseJets, baseElectrons, 0.2, NOT(BTag85MV2c10));
  baseElectrons = overlapRemoval(baseElectrons,baseJets, radiusCalcLepton);

  baseJets      = overlapRemoval(baseJets, baseMuons, muJetSpecial);
  baseMuons     = overlapRemoval(baseMuons,baseJets, radiusCalcLepton);

  baseTaus      = overlapRemoval(baseTaus, baseElectrons, 0.1);

  auto baseLeptons     = baseElectrons+baseMuons;
  auto signalJets      = filterObjects(baseJets, 25, 2.5, JVT50Jet);
  if (signalJets.size() > 0) sortObjectsByPt(signalJets); // ensure these are pT sorted
  auto signalBJets     = filterObjects(signalJets, 25, 2.5, BTag77MV2c10);
  auto fatJetsR10 = reclusterJets(signalJets, 1.0, 150, 0.2, 0.05);
  auto fatJetsR12 = reclusterJets(signalJets, 1.2, 150, 0.2, 0.05);

  AnalysisObjects mostBjetLike;
  AnalysisObjects signalNotBjetLike;
  AnalysisObjects signalNotBjet;

  auto signalSoftElectrons = filterObjects(baseElectrons, 4.5, 2.47, ( ETightLH | EIsoFCTight | ED0Sigma5 | EZ05mm ) );
  auto signalElectrons = filterObjects(baseElectrons, 25, 2.47, ( ELooseBLLH | EIsoFixedCutLoose | ED0Sigma5 | EZ05mm ) );
  auto signalSoftMuons = filterObjects(baseMuons, 4, 2.7, ( MuMedium | MuIsoFixedCutTightTrackOnly | MuD0Sigma3 | MuZ05mm ) );
  auto signalMuons = filterObjects(baseMuons, 25, 2.7, ( MuMedium | MuIsoFixedCutLoose | MuD0Sigma3 | MuZ05mm ) );
  auto signalLeptons = signalElectrons + signalMuons;
  auto signalSoftLeptons = signalSoftElectrons + signalSoftMuons;


  int nBjets = signalBJets.size();
  int nJets = signalJets.size();

  ntupVar("n_jet", nJets);
  ntupVar("n_bjet", nBjets);
  ntupVar("n_lep", (int)baseLeptons.size());
  ntupVar("n_lep_soft", (int)signalSoftLeptons.size());  
  ntupVar("n_lep_hard", (int)signalLeptons.size() );
  ntupVar("mc_weight", event->getMCWeights()[0]);
  ntupVar("met", Met);
  ntupVar("genMet", genMet);
  ntupVar("jet",signalJets, true, false, false, true);// saveMass, saveType, saveVtx, saveEnergy
  
  //sumofweights = sumofweights + event->getMCWeights()[0];

  /* bail out for the event if minimal common event selection criteria are not fullfilled */
  if (!(Met > 100 && baseLeptons.size() == 1 &&
        (signalSoftLeptons.size() == 1 || signalLeptons.size() == 1) && nJets > 1))
    return;
  // in the following the baseLepton is always used for calculations, to avoid requiring to check for soft or
  // hard, as only exactly one baseLepton is allowed in the analysis

  // create containers with exactly 2 jets being considered to be b-jets and the inverse
  int bJet1 = -1, bJet2 = -1;

  for (unsigned int i = 0; i < signalJets.size(); ++i) {
    if (signalJets[i].pass(NOT(BTag77MV2c10))) continue;
    if (bJet1 == -1){
      bJet1 = i;
    }
    else if (bJet2 == -1) {
      bJet2 = i;
      break;
    }
  }
  if (bJet2 == -1){
    for (unsigned int i = 0; i < signalJets.size(); ++i) {
      if (signalJets[i].pass(NOT(BTag85MV2c10))) continue;
      if (bJet1 == -1){
        bJet1 = i;
      }
      else if (bJet2 == -1) {
        bJet2 = i;
        break;
      }
    }
    for (unsigned int i = 0; i < signalJets.size(); ++i) {
      if (signalJets[i].pass(NOT(TrueBJet))) continue;
      if (bJet1 == -1){
	bJet1 = i;
      }
      else if (bJet2 == -1) {
	bJet2 = i;
	break;
      }
    }
    for (unsigned int i = 0; i < signalJets.size(); ++i) {  
      if (signalJets[i].pass(NOT(TrueCJet))) continue;
      if (bJet1 == -1){
	bJet1 = i;
      }
      else if (bJet2 == -1) {
	bJet2 = i;
	break;
      }
    }
    for (unsigned int i = 0; i < signalJets.size(); ++i) {
      if (signalJets[i].pass(NOT(TrueTau))) continue;
      if (bJet1 == -1){
	bJet1 = i;
      }
      else if (bJet2 == -1) {
	bJet2 = i;
	break;
      }
    }
    for (unsigned int i = 0; i < signalJets.size(); ++i) {
      if (bJet1 == -1){
        bJet1 = i;
      }
      else if (bJet2 == -1) {
        bJet2 = i;
        break;
      }
    }
  }
  mostBjetLike.push_back(signalJets.at(bJet1));
  mostBjetLike.push_back(signalJets.at(bJet2));
  for (int i = 0; i < (int)signalJets.size(); ++i) {
    if (signalJets[i].pass(NOT(BTag77MV2c10)))
      signalNotBjet.push_back(signalJets.at(i));
    if (i == bJet1 || i == bJet2) continue;
    signalNotBjetLike.push_back(signalJets.at(i));
  }

  /* ensure object collections to be pT sorted */
  sortObjectsByPt(signalJets);
  sortObjectsByPt(signalNotBjetLike);
  if (baseTaus.size() > 0) sortObjectsByPt(baseTaus);

  /* jet resolution */
  auto signalJER = fakeJER(signalJets);

  float sigmaAbsHtMiss = 0;

  /* calculate vecHtMiss */
  AnalysisObject* vecHtMiss = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::MET, 0, 0);
  AnalysisObject* leptonHtMiss = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::MET, 0, 0);
  for (unsigned int i = 0; i < baseLeptons.size(); ++i) {
    *vecHtMiss    -= baseLeptons[i];
    *leptonHtMiss -= baseLeptons[i];
  }

  for (unsigned int i = 0; i < signalJets.size(); ++i)
    *vecHtMiss -= signalJets[i];

  /* calculate Ht and HtSig */
  float Ht = 0;
  for (auto jet : signalJets) Ht += jet.Pt();
  //float HtSig = Met / sqrt(Ht); //not used, avoid confusions

  // variations
  TRandom3 myRandom;
  myRandom.SetSeed(signalJets[0].Pt());  // maybe use event number?
  int PEs = 100;
  double ETmissmean = 0, ETmissRMS = 0;
  for (int j = 0; j < PEs; ++j) {
    double jetHtx = leptonHtMiss->Px();
    double jetHty = leptonHtMiss->Py();
    for (unsigned int i = 0; i < signalJets.size(); ++i) {
      jetHtx -= myRandom.Gaus(signalJets[i].Px(), signalJets[i].Px() * signalJER[i]);
      jetHty -= myRandom.Gaus(signalJets[i].Py(), signalJets[i].Px() * signalJER[i]);
    }
    double ETtemp = sqrt(jetHtx * jetHtx + jetHty * jetHty);
    ETmissmean += ETtemp;
    ETmissRMS  += ETtemp * ETtemp;
  }
  ETmissmean = ETmissmean / PEs;
  sigmaAbsHtMiss = sqrt((ETmissRMS / PEs) - ETmissmean * ETmissmean);

  double HtSigMiss = (ETmissmean - 100.) / sigmaAbsHtMiss;

  double absDPhiJMet[4] = {fabs(signalJets[0].DeltaPhi(metVec)), fabs(signalJets[1].DeltaPhi(metVec)),
			   signalJets.size() > 2 ? fabs(signalJets[2].DeltaPhi(metVec)) : NAN,
			   signalJets.size() > 3 ? fabs(signalJets[3].DeltaPhi(metVec)) : NAN};
  double absDPhiJiMet = absDPhiJMet[0];

  for (int i = 1; i < 4; i++)
    if (absDPhiJMet[i] < absDPhiJiMet) absDPhiJiMet = absDPhiJMet[i];

  float mT = calcMT(baseLeptons[0], metVec);

  float dPhiMetLep = fabs(metVec.DeltaPhi(baseLeptons[0])); // for DM
  float dphiMin = minDphi(metVec, signalJets, 4); // for DM
  float mT2Tau = (baseTaus.size() > 0 ? calcMT2(baseTaus[0], baseLeptons[0], metVec) : 120);
  float pTLepOverMet = baseLeptons[0].Pt() / Met;

  bool preselHighMet = Met > 230 && mT > 30 && baseLeptons[0].Pt() > 25 && absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4;
  bool preselLowMet  =  baseLeptons[0].Pt() > 27 && signalBJets.size() > 0 && signalJets[0].Pt() > 50 && Met > 100 && mT > 90;
  if (baseLeptons[0].type() == ELECTRON)
    preselLowMet = preselLowMet && baseLeptons[0].pass(ETightLH);

  float amT2 = std::min(calcAMT2(mostBjetLike[0] + baseLeptons[0], mostBjetLike[1], metVec, 0., 80),
			calcAMT2(mostBjetLike[1] + baseLeptons[0], mostBjetLike[0], metVec, 0., 80));

  float topness = signalNotBjetLike.empty() ? calcTopness (baseLeptons[0], metVec, mostBjetLike[0], mostBjetLike[1] ):
    calcTopness (baseLeptons[0], metVec, mostBjetLike[0], mostBjetLike[1], signalNotBjetLike[0] ) ;

  float dRbl = baseLeptons[0].DeltaR(mostBjetLike[0]);

  float mbl = -1.;
  if ( signalBJets.size() > 0 ){
    TLorentzVector lepTLV;
    lepTLV.SetPtEtaPhiE(baseLeptons[0].Pt(), baseLeptons[0].Eta(), baseLeptons[0].Phi(), baseLeptons[0].E());  
    TLorentzVector b1TLV; 
    b1TLV.SetPtEtaPhiE(signalBJets[0].Pt(), signalBJets[0].Eta(), signalBJets[0].Phi(), signalBJets[0].E());
    float mbl1 = (b1TLV + lepTLV).M();
    if ( signalBJets.size() > 1 ){
      TLorentzVector b2TLV; 
      b2TLV.SetPtEtaPhiE(signalBJets[1].Pt(), signalBJets[1].Eta(), signalBJets[1].Phi(), signalBJets[1].E());
      float mbl2 = (b2TLV + lepTLV).M();
      mbl = std::min(mbl1, mbl2);
    } else {
      mbl = mbl1;
    }
  }

  float CT2 = 0; // for bffN_btag
  if (Met <= (signalJets[0].Pt()-25)) CT2 = Met;
  else if (Met > (signalJets[0].Pt()-25)) CT2 = (signalJets[0].Pt()-25);


  /* Reconstruct top by a chi2 based method */
  float mW = 80.;
  float mTop = 170.;
  float chi2min = 9e99;
  AnalysisObject* topChi2 = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0);
  int jetComb[3] = {0, 0, 0};
  auto signalBJER = fakeJER(mostBjetLike);
  float f;

  for (int i = 0; i < (int)signalJets.size(); ++i) {
    if (i == bJet1 || i == bJet2) continue;
    for (int j = i + 1; j < (int)signalJets.size(); ++j) {
      if (j == bJet1 || j == bJet2) continue;
      for (unsigned int k = 0; k < mostBjetLike.size() && k < 2; ++k) {
        f = pow((signalJets[i] + signalJets[j] + mostBjetLike[k]).M() - mTop, 2) /
          (pow((signalJets[i] + signalJets[j] + mostBjetLike[k]).M(), 2) *
           (pow(signalJER[i], 2) + pow(signalJER[j], 2) + pow(signalBJER[k], 2))) +
          pow((signalJets[i] + signalJets[j]).M() - mW, 2) /
          (pow((signalJets[i] + signalJets[j]).M(), 2) * (pow(signalJER[i], 2) + pow(signalJER[j], 2)));
        if (f < chi2min) {
          chi2min = f;
          jetComb[0] = i;
          jetComb[1] = j;
          jetComb[2] = k;
        }
      }
    }
  }
  *topChi2 = signalJets[jetComb[0]] + signalJets[jetComb[1]] + mostBjetLike[jetComb[2]];

  AnalysisObject* top1 = new AnalysisObject(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0);
  *top1 = baseLeptons[0] + (jetComb[2] == 0 ? mostBjetLike[1] : mostBjetLike[0]);

  /* calculate MetPerp */
  float MetPerp = NAN;
  AnalysisObject ttbar = *topChi2 + *top1;
  AnalysisObject top1Rest = *top1;
  AnalysisObject metRest = metVec;
  top1Rest.Boost(-ttbar.Px() / ttbar.E(), -ttbar.Py() / ttbar.E(), -ttbar.Pz() / ttbar.E());
  metRest.Boost(-ttbar.Px() / ttbar.E(), -ttbar.Py() / ttbar.E(), -ttbar.Pz() / ttbar.E());
  MetPerp = metRest.Vect().XYvector().Norm(top1Rest.Vect().XYvector()).Mod();

  AnalysisObject WRecl = reclusteredParticle(signalNotBjet, mostBjetLike, mW, false); //signalNotBjet+mostBjetLike is inconsistent but bjets are not used anyway
  AnalysisObject topRecl = reclusteredParticle(signalNotBjetLike, mostBjetLike, 175., true);

  float minDPhiMetBJet = NAN;
  if (nBjets > 0) minDPhiMetBJet = minDphi(metVec, signalBJets, 2);


  fill("Met", Met);


  // --------------------------
  //    tN_med, tN_high, DM
  // --------------------------

  if (signalLeptons.size() == 1) {  // hard lepton
    //tN_med
    if (nJets > 3 && nBjets > 0 && preselHighMet && signalJets[0].Pt() > 100 && signalJets[1].Pt() > 90 &&
        signalJets[2].Pt() > 70 && signalJets[3].Pt() > 50 && MetPerp > 400 && HtSigMiss > 16 && mT > 220 &&
        topness > 9 && topRecl.M() > 150 && dRbl < 2.8 && mT2Tau > 80)
      accept("tN_med");
    //tN_high
    if (nJets > 3 && nBjets > 0 && preselHighMet && signalJets[0].Pt() > 120 && signalJets[1].Pt() > 50 &&
        signalJets[2].Pt() > 50 && signalJets[3].Pt() > 25 && Met > 520 && HtSigMiss > 25 && mT > 380 &&
        topness > 8 && topRecl.M() > 150 && dRbl < 2.6 && mT2Tau > 80)
      accept("tN_high");
    //DM
    if (nJets > 3 && nBjets > 1 && preselHighMet && signalJets[0].Pt() > 80 && signalJets[1].Pt() > 60 &&
        signalJets[2].Pt() > 30 && signalJets[3].Pt() > 25 && signalBJets[0].Pt() > 80 && mT > 180 && HtSigMiss > 15 &&
        topness > 8 && topRecl.M() > 150 && dPhiMetLep > 1.1 && dphiMin > 0.9 && mT2Tau > 80)
      accept("DM");
  }

  // ----------------------
  //       tN_diag 
  // ----------------------

  bool preselHardLeptonNoTauVeto = Met > 230 && mT > 110 && absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4 && nBjets > 0;

  float DMT200 = NAN;
  float DMTdynalpha = NAN;
  float mNeutralino = 27.;
  float mstoplep_200 = NAN;
  float MT2 = NAN;
  float x1 = NAN;

  if (signalLeptons.size() == 1 && signalJets.size() > 3 && preselHardLeptonNoTauVeto) {
    AnalysisObject top0(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0);
    AnalysisObject top1(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0);
    AnalysisObject W0(0., 0., 0., 0., 0, 0, AnalysisObjectType::JET, 0, 0);

    bool flip_b = false;
    float chi2min = FLT_MAX;
    float chi2_1, chi2_2, chi2_3;
    float mStop = 200.;
    float alpha = mNeutralino / mStop;
    const float mW = 80.385;
    const float mTop = 173.21;

    // reconstruct hadronically decaying top
    if (fatJetsR10.size() > 0) {
      chi2_1 = std::pow(fatJetsR10[0].M() - mW, 2) / mW +
	std::pow((fatJetsR10[0] + mostBjetLike[0]).M() - mTop, 2) / mTop;
      chi2_2 = std::pow(fatJetsR10[0].M() - mW, 2) / mW +
	std::pow((fatJetsR10[0] + mostBjetLike[1]).M() - mTop, 2) / mTop;
      chi2_3 = std::pow(fatJetsR10[0].M() - mTop, 2) / mTop;

      if (chi2_1 < chi2_2 && chi2_1 < chi2_3) {
        chi2min = chi2_1;
        W0 = fatJetsR10[0];
        top0 = mostBjetLike[0] + W0;
      } else if (chi2_2 < chi2_1 && chi2_2 < chi2_3) {
        chi2min = chi2_2;
        W0 = fatJetsR10[0];
        top0 = mostBjetLike[1] + W0;
        flip_b = true;
      } else {
        chi2min = chi2_3;
        top0 = fatJetsR10[0];
      }
    }
    if (fatJetsR12.size() > 0) {
      chi2_1 = std::pow(fatJetsR12[0].M() - mW, 2) / mW +
	std::pow((fatJetsR12[0] + mostBjetLike[0]).M() - mTop, 2) / mTop;
      chi2_2 = std::pow(fatJetsR12[0].M() - mW, 2) / mW +
	std::pow((fatJetsR12[0] + mostBjetLike[1]).M() - mTop, 2) / mTop;
      chi2_3 = std::pow(fatJetsR12[0].M() - mTop, 2) / mTop;

      if (chi2_1 < chi2_2 && chi2_1 < chi2_3) {
        chi2min = chi2_1;
        W0 = fatJetsR12[0];
        top0 = mostBjetLike[0] + W0;
      } else if (chi2_2 < chi2_1 && chi2_2 < chi2_3) {
        chi2min = chi2_2;
        W0 = fatJetsR12[0];
        top0 = mostBjetLike[1] + W0;
        flip_b = true;
      } else {
        chi2min = chi2_3;
        top0 = fatJetsR12[0];
      }
    }
    for (unsigned int i = 0; i < signalNotBjetLike.size(); ++i) {
      for (unsigned int j = 0; j < signalNotBjetLike.size(); ++j) {
        if (j == i) continue;

        chi2_1 =
	  std::pow((signalNotBjetLike[i] + signalNotBjetLike[j]).M() - mW, 2) / mW +
	  std::pow((signalNotBjetLike[i] + signalNotBjetLike[j] + mostBjetLike[0]).M() - mTop, 2) /
	  mTop;
        chi2_2 =
	  std::pow((signalNotBjetLike[i] + signalNotBjetLike[j]).M() - mW, 2) / mW +
	  std::pow((signalNotBjetLike[i] + signalNotBjetLike[j] + mostBjetLike[1]).M() - mTop, 2) /
	  mTop;

        if ((chi2_1 < chi2_2) && (chi2_1 < chi2min)) {
          chi2min = chi2_1;
          W0 = signalNotBjetLike[i] + signalNotBjetLike[j];
          top0 = mostBjetLike[0] + W0;
          flip_b = false;
        } else if (chi2_2 < chi2min) {
          chi2min = chi2_2;
          W0 = signalNotBjetLike[i] + signalNotBjetLike[j];
          top0 = mostBjetLike[1] + W0;
          flip_b = true;
        }
      }
    }

    if (!(W0.M() < 0)) {
      if (flip_b) {
        top1 = mostBjetLike[0];
        mostBjetLike[0] = mostBjetLike[1];
        mostBjetLike[1] = top1;
      }
      top1 = mostBjetLike[1] + signalLeptons[0];
    } else {
      float dr0 = mostBjetLike[0].DeltaR(signalLeptons[0]);
      float dr1 = mostBjetLike[1].DeltaR(signalLeptons[0]);
      if (dr0 < dr1) {
        top1 = mostBjetLike[0] + signalLeptons[0];
      } else {
        top1 = mostBjetLike[1] + signalLeptons[0];
      }
    }

    TLorentzVector nu;
    nu.SetPxPyPzE((1. - alpha) * metVec.Px() - alpha * (top0 + top1).Px(),
                  (1. - alpha) * metVec.Py() - alpha * (top0 + top1).Py(), 0, 0);
    nu.SetE(nu.Pt());

    float mT200 = sqrt((1. - cos(signalLeptons[0].DeltaPhi(nu))) * 2. * signalLeptons[0].Pt() * nu.Pt());
    DMT200 = mT - mT200;

    // Calculate mass of leptonically decaying stop
    chi2min = FLT_MAX;
    chi2_1 = FLT_MAX;
    chi2_2 = FLT_MAX;

    TLorentzVector lepTop, metNu;
    lepTop.SetPtEtaPhiM(top1.Pt(), top1.Eta(), top1.Phi(), top1.M());
    metNu.SetPtEtaPhiM(metVec.Pt(), 0, metVec.Phi(), metVec.Pt());
    lepTop += nu;
    metNu += nu;

    float p_mis_1, p_mis_2;
    float m_mis1_2 = pow(mNeutralino, 2);
    float m_mis2_2 = pow(mNeutralino, 2);
    float phi_mis_1 = top0.Phi();
    float phi_mis_2 = lepTop.Phi();
    float theta_mis_1 = top0.Theta();
    float theta_mis_2 = lepTop.Theta();
    std::unique_ptr<TRandom3> uniRand(new TRandom3());

    for (float t = 1.; t > 1e-2; t *= 0.9) {
      double uniRandVals[4] = {0.};
      for (int i = 0; i < 200; ++i) {
        uniRandVals[0] = 3. * (uniRand->Rndm() - 0.5);
        uniRandVals[1] = 3. * (uniRand->Rndm() - 0.5);
        uniRandVals[2] = 1.5 * (uniRand->Rndm() - 0.5);
        uniRandVals[3] = 1.5 * (uniRand->Rndm() - 0.5);

        float phi_mis_i1 = TVector2::Phi_mpi_pi(phi_mis_1 - uniRandVals[0]);
        float phi_mis_i2 = TVector2::Phi_mpi_pi(phi_mis_2 - uniRandVals[1]);
        float theta_mis_i1 = theta_mis_1 - uniRandVals[2];
        float theta_mis_i2 = theta_mis_2 - uniRandVals[3];
        while (theta_mis_i1 > TMath::Pi()) theta_mis_i1 -= TMath::Pi();
        while (theta_mis_i1 < 0) theta_mis_i1 += TMath::Pi();
        while (theta_mis_i2 > TMath::Pi()) theta_mis_i2 -= TMath::Pi();
        while (theta_mis_i2 < 0) theta_mis_i2 += TMath::Pi();

        p_mis_1 = (metNu.Py() * cos(phi_mis_i2) - metNu.Px() * sin(phi_mis_i2)) /
	  (sin(theta_mis_i1) * sin(phi_mis_i1 - phi_mis_i2));
        p_mis_2 = (metNu.Py() - p_mis_1 * sin(theta_mis_i1) * sin(phi_mis_i1)) /
	  (sin(theta_mis_i2) * sin(phi_mis_i2));

        float m_stop1_2 = m_mis1_2 + top0.M2() +
	  2. * sqrt(pow(top0.P(), 2) + top0.M2()) * sqrt(pow(p_mis_1, 2) + m_mis1_2) -
	  2 * top0.P() * p_mis_1 * cos(top0.Theta() - theta_mis_i1);
        float m_stop2_2 =
	  m_mis2_2 + lepTop.M2() +
	  2. * sqrt(pow(lepTop.P(), 2) + lepTop.M2()) * sqrt(pow(p_mis_2, 2) + m_mis2_2) -
	  2 * lepTop.P() * p_mis_2 * cos(lepTop.Theta() - theta_mis_i2);
        chi2_2 = pow(sqrt(m_stop1_2) - mStop, 2) / mStop + pow(sqrt(m_stop2_2) - mStop, 2) / mStop;
        if ((chi2_2 < chi2_1) || uniRand->Rndm() < TMath::Exp(-(chi2_2 - chi2_1) / t)) {
          if (chi2_2 < chi2min) {
            chi2min = chi2_2;
            mstoplep_200 = m_stop2_2;
            phi_mis_1 = phi_mis_i1;
            phi_mis_2 = phi_mis_i2;
            theta_mis_1 = theta_mis_i1;
            theta_mis_2 = theta_mis_i2;
          }
        }
        chi2_1 = chi2_2;
      }
    }
    mstoplep_200 = sqrt(mstoplep_200);

    // Collinear approximation
    p_mis_1 = (metVec.Py() * cos(top1.Phi()) - metVec.Px() * sin(top1.Phi())) /
      (sin(top0.Theta()) * sin(top0.Phi() - top1.Phi()));
    p_mis_2 = (metVec.Py() - p_mis_1 * sin(top0.Theta()) * sin(top0.Phi())) /
      (sin(top1.Theta()) * sin(top1.Phi()));
    x1 = top0.P() / (top0.P() + p_mis_1);

    // chi^2 determination of alpha
    chi2min = FLT_MAX;
    float alpha_i;
    for (mNeutralino = 0; mNeutralino < 600.; mNeutralino += 1.) {
      alpha_i = ((float)mNeutralino) / ((float)mNeutralino + top0.M());
      nu.SetPxPyPzE((1. - alpha_i) * metVec.Px() - alpha_i * (top0 + top1).Px(),
                    (1. - alpha_i) * metVec.Py() - alpha_i * (top0 + top1).Py(), 0, 0);
      nu.SetE(nu.Pt());
      chi2_1 = pow((signalLeptons[0] + nu).M() - mW, 2) / mW + pow(lepTop.M() - mTop, 2) / mTop;
      if (chi2_1 < chi2min) {
        chi2min = chi2_1;
        alpha = alpha_i;
      }
    }
    mNeutralino = (alpha / (1. - alpha)) * top0.M();
    nu.SetPxPyPzE((1. - alpha) * metVec.Px() - alpha * (top0 + top1).Px(),
                  (1. - alpha) * metVec.Py() - alpha * (top0 + top1).Py(), 0, 0);
    nu.SetE(nu.Pt());
    top1 += nu;

    float MTdynalpha =
      sqrt((1. - cos(signalLeptons[0].DeltaPhi(nu))) * 2. * signalLeptons[0].Pt() * nu.Pt());
    DMTdynalpha = mT - MTdynalpha;
    MT2 = calcMT2(top0, top1, metVec);

    // tN_diag_low
    if (preselHardLeptonNoTauVeto && nBjets > 1 && nJets > 4 && signalJets[0].Pt() >= 400. &&
        signalJets[3].Pt() >= 40. && mT >= 150. && DMT200 >= 40. && mstoplep_200 < 600. &&
        mNeutralino >= 5.) {
      accept("tN_diag_low");
    }
    // tN_diag_high
    if (preselHardLeptonNoTauVeto && nBjets > 1 && nJets > 4 && signalJets[0].Pt() >= 400. &&
        signalJets[3].Pt() >= 40. && mT >= 110. && DMTdynalpha >= 60. && Met >= 400. && MT2 < 360. &&
        mNeutralino >= 220. && mNeutralino < 595. && x1 >= -0.2) {
      accept("tN_diag_high");
    }
  } //tN_diag preselection





  // ---------------------------
  //           bffN
  // ---------------------------

  // soft lepton selections
  if (signalSoftLeptons.size() == 1) {
    bool preselSoftLep = Met > 230;
    if (signalSoftLeptons[0].type() == ELECTRON)
      preselSoftLep = preselSoftLep && signalSoftLeptons[0].pass(ETightLH);

    // bffN_btag
    if (preselSoftLep && nJets >= 2  && absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4 &&
	signalJets[0].Pt() > 200 && Met > 300 && mT > 90 && nBjets > 0 && CT2 > 400 &&
	!signalJets[0].pass(BTag77MV2c20) && signalBJets[0].Pt() < 50 && minDPhiMetBJet < 1.5 &&
	pTLepOverMet < 0.05)
      accept("bffN_btag");


    // bffN_softb
    // soft b-tag will be implemented

  }

  ntupVar("met_perp", MetPerp);  
  ntupVar("amt2", amT2);
  ntupVar("topness", topness);
  ntupVar("mt", mT);
  ntupVar("mt2_tau", mT2Tau);
  ntupVar("ht_sig", HtSigMiss);
  ntupVar("m_bl", mbl);
  ntupVar("CT2", CT2);

  ntupVar("lep_pt", baseLeptons[0].Pt());
  ntupVar("lep_eta", baseLeptons[0].Eta());
  ntupVar("lep_phi", baseLeptons[0].Phi());
  ntupVar("lep_e", baseLeptons[0].E());
  ntupVar("met_phi", metVec.Phi());
  ntupVar("met_x", metVec.Px());
  ntupVar("met_y", metVec.Py());
  ntupVar("ht", Ht);

  if (nJets>0){
    ntupVar("jet0_pt", signalJets[0].Pt());
    ntupVar("jet0_eta", signalJets[0].Eta());
    ntupVar("jet0_phi", signalJets[0].Phi());
    ntupVar("jet0_e", signalJets[0].E());
  }
  if (nJets>1){
    ntupVar("jet1_pt", signalJets[1].Pt());
    ntupVar("jet1_eta", signalJets[1].Eta());
    ntupVar("jet1_phi", signalJets[1].Phi());
    ntupVar("jet1_e", signalJets[1].E());
  }
  if (nJets>2){
    ntupVar("jet2_pt", signalJets[2].Pt());
    ntupVar("jet2_eta", signalJets[2].Eta());
    ntupVar("jet2_phi", signalJets[2].Phi());
    ntupVar("jet2_e", signalJets[2].E());
  }
  if (nJets>3){
    ntupVar("jet3_pt", signalJets[3].Pt());
    ntupVar("jet3_eta", signalJets[3].Eta());
    ntupVar("jet3_phi", signalJets[3].Phi());
    ntupVar("jet3_e", signalJets[3].E());
  }
  if (nBjets>0){
    ntupVar("bjet_pt0", signalBJets[0].Pt());
    ntupVar("bjet_eta0", signalBJets[0].Eta());
    ntupVar("bjet_phi0", signalBJets[0].Phi());
    ntupVar("bjet_e0", signalBJets[0].E());
  }
  if (nBjets>1){
    ntupVar("bjet_pt1", signalBJets[1].Pt());
    ntupVar("bjet_eta1", signalBJets[1].Eta());
    ntupVar("bjet_phi1", signalBJets[1].Phi());
    ntupVar("bjet_e1", signalBJets[1].E());
  }

  ntupVar("dr_bjet_lep", dRbl);
  ntupVar("dphi_ttbar", topChi2->DeltaPhi(*top1));
  ntupVar("dphi_hadtop_met", metVec.DeltaPhi(*topChi2));
  ntupVar("dphi_met_lep", dPhiMetLep);
  ntupVar("dphi_min_ptmiss", dphiMin);
  ntupVar("dphi_jet0_ptmiss", absDPhiJMet[0]);
  ntupVar("dphi_jet1_ptmiss", absDPhiJMet[1]);
  ntupVar("minDphiMetBjet", minDPhiMetBJet);

  ntupVar("m_top_chi2", topChi2->M());
  ntupVar("hadw_cand_m", WRecl.M());
  ntupVar("hadtop_cand_m0", topRecl.M());
  ntupVar("lepPt_over_met", pTLepOverMet);

  // tN_diag variables
  ntupVar("DMT200", DMT200);
  ntupVar("DMTdynalpha", DMTdynalpha);
  ntupVar("MT2", MT2);
  ntupVar("mNeutralino", mNeutralino);
  ntupVar("mstoplep_200", mstoplep_200);
  ntupVar("x1", x1);


  // ---------------------------
  //           bWN
  // ---------------------------
//*******RNN********//
 // Define some variables
  std::map<std::string, std::map<std::string, double> > inputs;
  std::map<std::string, std::map<std::string, std::vector<double>> > input_sequences;
  std::vector<double> jet_rnn_pt;
  std::vector<double> jet_rnn_eta;
  std::vector<double> jet_rnn_phi;
  std::vector<double> jet_rnn_e;

  for (auto jet : signalJets){
       jet_rnn_pt.push_back(jet.Pt()*1000);
       jet_rnn_eta.push_back(jet.Eta());
       jet_rnn_phi.push_back(jet.Phi());
       jet_rnn_e.push_back(jet.E()*1000);
   }


// Define some sequences used for the recurrent NN


  input_sequences["node_0"] = {
     {"jet_rnn_pt",  jet_rnn_pt},
     {"jet_rnn_eta", jet_rnn_eta},
     {"jet_rnn_phi", jet_rnn_phi},
     {"jet_rnn_e",   jet_rnn_e},
  };

  // More variables for the other input defined
  float bjets_rnn=0;
  if (nBjets>0){
      bjets_rnn = signalBJets[0].Pt();
   }
  float mbl_rnn=0;
  if (mbl>0){
     mbl_rnn = mbl;
  }

  inputs["node_0"] = {
    {"met",             (Met*1000)},
    {"met_phi",         (metVec.Phi())},
    {"mt",              (mT*1000)},
    {"dphi_met_lep",    (dPhiMetLep)},
    {"m_bl",            (mbl_rnn*1000)},
    {"bjet_pt0",        (bjets_rnn*1000)},
    {"n_jet",           (nJets)},
    {"n_bjet",          (nBjets)},
    {"lep_pt",          (baseLeptons[0].Pt()*1000)},
    {"lep_eta",         (baseLeptons[0].Eta())},
    {"lep_phi",         (baseLeptons[0].Phi())},
    {"lep_e",           (baseLeptons[0].E()*1000)}
  };



  lwt::ValueMap outputs = RNN_graph->compute(inputs, input_sequences);

////////Some eye candy:
//  for (const std::pair<const std::string, double> el: outputs) {
//    std::cout << std::setprecision(7) << el.first << ": " << el.second << std::endl;
//  }

  double NN0;
  NN0 = outputs["out_0"];
  fill("outputScore_RNN", NN0);
  ntupVar("outputScore_RNN", NN0);


  if (signalLeptons.size() == 1) {  // hard lepton
    if (nJets >=4 && nBjets >=1 && preselHighMet && signalJets[0].Pt() > 25 && signalJets[1].Pt() > 25 && signalJets[2].Pt() > 25 && signalJets[3].Pt() > 25 && Met > 230 && mT > 110 && absDPhiJMet[0] > 0.4 && absDPhiJMet[1] > 0.4 && mT2Tau > 80 && NN0>=0.9 && NN0<1.0)
      accept("bWN");
  }






}


