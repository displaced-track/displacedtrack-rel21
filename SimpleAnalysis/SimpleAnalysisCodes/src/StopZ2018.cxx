#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(StopZ2018)

void StopZ2018::Init()
{
  addRegions({"SRS","SRS1","SRS2","SRS3","SRS4","SRL","SRL1","SRL2","SRL3","SRL4","SRH","SRH1","SRH2","SRH3","SRH4"});
}

void StopZ2018::ProcessEvent(AnalysisEvent *event)
{

  const float mZ = 91.2;

  auto electrons = event->getElectrons(5, 2.47, ELooseLH); //filter on pT, eta, ID
  auto muons     = event->getMuons(5, 2.5, MuMedium);  //filter on pT, eta ID
  auto jets      = event->getJets(25., 2.8);  //filter on pT, eta
  auto metVec    = event->getMET();

  // SUSY overlap removal
  jets               = overlapRemoval(jets, electrons, 0.2);
  jets               = overlapRemoval(jets, muons, 0.4, LessThan3Tracks);
  electrons  = overlapRemoval(electrons, jets, 0.4);
  muons      = overlapRemoval(muons, jets, 0.4);

  // object counting
  auto signalElectrons = filterObjects(electrons, 5, 2.47, EMediumLH | EZ05mm | EIsoFixedCutTight);
  auto signalMuons     = filterObjects(muons, 5, 2.4, MuD0Sigma3 | MuZ05mm | MuIsoFixedCutTightTrackOnly);
  auto signalJets      = filterObjects(jets, 30, 2.5);
  auto bjets           = filterObjects(jets, 30., 2.5, BTag77MV2c10);

  auto nbjet            = countObjects(bjets, 30, 2.5);
  auto njet30           = countObjects(signalJets, 30, 2.5);
  //  auto njet60           = countObjects(signalJets, 60, 2.5);

  double MET     = metVec.Et();

  AnalysisObjects signalLeptons = signalElectrons + signalMuons;

  int nlep = signalLeptons.size();
  //  int nel = signalElectrons.size();
  //  int nmu = signalMuons.size();

  

  double jet1pT = -1;
  if (njet30 > 0) jet1pT = signalJets[0].Pt();
  double bjet1pT = -1;
  if (nbjet > 0) bjet1pT = bjets[0].Pt();

  double lep1pT = -1;
  if (nlep > 0) {
    lep1pT = signalLeptons[0].Pt();
  }
  double lep2pT = -1;
  if (nlep > 1) {
    lep2pT = signalLeptons[1].Pt();
  }


  double lep3pT = -1;
  //  double deltaPhiLepMet = -4;
  //  double deltaPhiLepB = -4;
  //  double deltaEtaLepB = -4;
  //  double deltaEtaLepLep = -4;
  //  double deltaPhiTopN1 = -4;
  //  double mT = -1;
  //  double mTmy = -1;

  if (nlep > 2) {
    lep3pT = signalLeptons[2].Pt();
  }
  else return;

  float closest_mll = -1;
  float closest_ptll = -1;

  //  float mll = -1;
  //  float ptll = -1;

  int single_lep_index = -1;
  int Zlep1_index = -1;
  int Zlep2_index = -1;

  //std::cout << "before loop for Z-variables" << std::endl;

  if (signalLeptons.size() > 2) {
    

    
    //index and flavor of selected two leptons

    for (UInt_t ilep = 0; ilep < signalLeptons.size(); ilep++) {

      auto lep1 = signalLeptons[ilep];

      for (UInt_t ilep2 = 0; ilep2 < signalLeptons.size(); ilep2++) {

        if ( ilep == ilep2 ) continue;

        auto lep2 = signalLeptons[ilep2];

        bool isSF = lep1.type()==lep2.type();
        bool isOS = lep1.charge()*lep2.charge()<0;

        double mll_12 = (lep1 + lep2).M();

        if (isOS && isSF && (((fabs(mll_12 - mZ) < fabs(closest_mll - mZ)) || (closest_mll == -1) ))) {
          closest_mll = mll_12;
          closest_ptll = (lep1 + lep2).Perp();
          Zlep1_index = ilep;
          Zlep2_index = ilep2;
        }

        for (UInt_t ilep3 = 0; ilep3 < signalLeptons.size(); ilep3++) {
          if((ilep3 != ilep2) && (ilep3 != ilep)) {
            single_lep_index = ilep3;
            continue;
          }
        }

      }
    }
  }

  if (closest_mll == -1) return;
  double MT2_3l = -1;

  if(nlep>2) {
    //std::cout << "before calc" << std::endl;
    MT2_3l = calcMT2(signalLeptons[Zlep1_index] + signalLeptons[Zlep2_index], signalLeptons[single_lep_index], metVec);
    //std::cout << "MT2_3l " << MT2_3l << std::endl;
  }

  bool pass_SRH_MET = false;
  if(MT2_3l > 100 && lep1pT>40. && lep2pT>20 && lep3pT>20 && jet1pT>30. && nbjet>0 && closest_mll>76.200 && closest_mll<106.200 && bjet1pT >30  && njet30>3) pass_SRH_MET = true;

  if (pass_SRH_MET && MET>250.)  accept("SRH");
  if (pass_SRH_MET && MET>200. && MET <250.)  accept("SRH1");
  if (pass_SRH_MET && MET>250. && MET <300.)  accept("SRH2");
  if (pass_SRH_MET && MET>300. && MET <350.)  accept("SRH3");
  if (pass_SRH_MET && MET>350.)  accept("SRH4");

  bool pass_SRL_ptll = false;
  if(lep1pT > 40 && lep2pT > 20 && lep3pT > 20 && njet30 > 4 && MET > 150 && jet1pT > 100 && bjet1pT > 100 && nbjet>0 && closest_mll>76.200 && closest_mll<106.200) pass_SRL_ptll = true;

  if (pass_SRL_ptll && closest_ptll>150.)  accept("SRL");
  if (pass_SRL_ptll && closest_ptll>150. && closest_ptll <300.)  accept("SRL1");
  if (pass_SRL_ptll && closest_ptll>300. && closest_ptll <450.)  accept("SRL2");
  if (pass_SRL_ptll && closest_ptll>450. && closest_ptll <600.)  accept("SRL3");
  if (pass_SRL_ptll && closest_ptll>600.)  accept("SRL4");


  bool pass_SRS = false;
  if(lep1pT > 40 && lep2pT > 20 && lep3pT < 20 && njet30 > 2 && MET > 200 && closest_ptll < 50 && jet1pT > 150 && closest_mll>76.200 && closest_mll<106.200) pass_SRS = true;
  if (pass_SRS)  accept("SRS");

  bool pass_SRS_partial = false;
  if(lep1pT > 40 && lep2pT > 20 && lep3pT < 60 && njet30 > 2 && jet1pT > 50 && nbjet>0 && closest_mll>76.200 && closest_mll<106.200) pass_SRS_partial = true;

  if (pass_SRS_partial && closest_ptll>50. && closest_ptll <150. && MET > 300 && MET < 350)  accept("SRS1");
  if (pass_SRS_partial && MET > 300 && MET < 350 && closest_ptll > 150)  accept("SRS2");
  if (pass_SRS_partial && MET > 350 && closest_ptll > 50 && closest_ptll < 150)  accept("SRS3");
  if (pass_SRS_partial && MET > 350 && closest_ptll > 150.)  accept("SRS4");


  

  return;
}
