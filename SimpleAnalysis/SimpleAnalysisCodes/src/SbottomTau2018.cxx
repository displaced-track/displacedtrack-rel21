#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(SbottomTau2018)

    void SbottomTau2018::Init() {
  // Define signal/control regions
  addRegions({"SinglebinSR"}); // Single bin discovery SR
  addRegions({"MultibinSR_0_05", "MultibinSR_05_1",
              "MultibinSR_1_plus"}); // Three min theta bins used for exclusion
  addRegions({"CR_Top_mutau_true", "CR_Top_mutau_fake", "CR_Top_tau_true",
              "CR_Top_mu"});
  addRegions({"CR_Z_mumu2b", "CR_Z_mumu0b", "CR_Z_tautau0b"});
  //  addRegion("preSelection"); // Trigger plateau + basis objects selection.
  //  Commented out for pMSSM scan etc.
}

void SbottomTau2018::ProcessEvent(AnalysisEvent *event) {
  // Retrieve basic object lists
  // PLEASE NOTE UNITS ARE ALL IN GEV, AND NOT ATLAS STANDARD MEV!!!
  auto metVec = event->getMET();
  double met = metVec.Et();
  double met_phi = metVec.Phi();

  auto electrons = event->getElectrons(10, 2.47, ELooseBLLH);
  auto muons = event->getMuons(10, 2.7, MuMedium);
  auto candJets = event->getJets(
      20., 2.8); // B-jets not used in OR so don't define them here
  auto taus = event->getTaus(20, 2.5, TauRNNLoose);

  // Overal Removal, should be more or less standard, but with taus included.
  // B-jets treated as normal jets, no special treatment.
  taus = overlapRemoval(taus, electrons, 0.2);
  taus = overlapRemoval(taus, muons, 0.2);
  muons = overlapRemoval(muons, electrons, 0.01, NOT(MuCaloTaggedOnly));
  electrons = overlapRemoval(electrons, muons, 0.01);
  candJets = overlapRemoval(candJets, electrons, 0.2);
  electrons = overlapRemoval(electrons, candJets, 0.4);
  candJets = overlapRemoval(candJets, muons, 0.2);
  muons = overlapRemoval(muons, candJets, 0.4);
  candJets = overlapRemoval(candJets, taus, 0.2);

  // Basic filtering by pT, eta and "ID"
  auto signalJets = filterObjects(candJets, 20);
  auto signalElectrons =
      filterObjects(electrons, 25, 2.47, EIsoLoose | ETightLH);
  auto signalMuons = filterObjects(muons, 25, 2.7, MuIsoLoose | MuMedium);
  auto bjets = filterObjects(signalJets, 20., 2.5, BTag77MV2c10);
  auto signalTaus = filterObjects(taus, 20, 2.5, TauRNNLoose);
  double HT = sumObjectsPt(signalJets) + sumObjectsPt(signalTaus) +
              sumObjectsPt(signalMuons); // This is not used anywhere for the
                                         // signal regions, but to be consistent
                                         // muon pt is included.

  float jet_pt = 0;
  float jet_pt2 = 0;
  float bjet_pt = 0;
  int nBjets = bjets.size();
  int nJets = signalJets.size();
  int nTaus = signalTaus.size();
  int nMuons = signalMuons.size();

  // CR definitions. At least 3 jets are required. Keep in mind that unlike SRs
  // (where at least two taus, two b-jets etc are required) the CRs require
  // exactly 1 tau, exactly 1 muon etc. This is useful in reco samples, but can
  // be somewhat questionable for truth.

  if (signalJets.size() < 3) {
    return;
  }
  if (nJets > 0) {
    jet_pt = signalJets[0].Pt();
  }
  if (nJets > 1) {
    jet_pt2 = signalJets[1].Pt();
  }
  if (nBjets > 0) {
    bjet_pt = bjets[0].Pt();
  }

  // Several tile vetoes are applied in the analysis, this directly affects the
  // acceptance so emulate here. NB: this is period speific, if working with a
  // dataset other than 2015-2018 ATLAS data please disable/modify this.

  int LBA52bad = 0;
  int LBC5bad = 0;
  int LBC63bad = 0;
  int EBA03bad = 0;
  int LBA29bad = 0;
  int LBA30bad = 0;
  int LBA32bad = 0;

  float dphijetmet = 10;
  unsigned int jetcounter = 0;

  // Find the jet closest to met in phi

  for (unsigned int i = 0; i < signalJets.size(); i++) {
    if (fabs(metVec.DeltaPhi(signalJets[i])) < dphijetmet) {
      dphijetmet = fabs(metVec.DeltaPhi(signalJets[i]));
      jetcounter = i;
    }
  }

  // Check whether the jet closest to met in phi hits any of the tiles that are
  // problematic for the analysis:
  // LBA52 in 2015-2016
  if (signalJets[jetcounter].Eta() > (0. - 0.2) &&
      signalJets[jetcounter].Eta() < (0.9 + 0.2) &&
      fabs(TVector2::Phi_mpi_pi(signalJets[jetcounter].Phi() - 5.05)) <
          (0.2 + 9.81e-02 / 2.)) {
    LBA52bad = 1;
  }
  // LBC5 in 2016
  if (signalJets[jetcounter].Eta() > (0 - 0.2) &&
      signalJets[jetcounter].Eta() < (0.9 + 0.2) &&
      fabs(TVector2::Phi_mpi_pi(signalJets[jetcounter].Phi() - 0.44)) <
          (0.2 + 9.81e-02 / 2.)) {
    LBC5bad = 1;
  }
  // LBC63 in 2017:
  if (signalJets[jetcounter].Eta() > (-0.9 - 0.2) &&
      signalJets[jetcounter].Eta() < (0. + 0.2) &&
      fabs(TVector2::Phi_mpi_pi(signalJets[jetcounter].Phi() + 0.147)) <
          (0.2 + 9.81e-02 / 2.)) {
    LBC63bad = 1;
  }
  // EBA03 in 2017
  if (signalJets[jetcounter].Eta() > 0.8 &&
      signalJets[jetcounter].Eta() < 1.7 &&
      fabs(TVector2::Phi_mpi_pi(signalJets[jetcounter].Phi() - 0.245)) <
          9.81e-02) {
    EBA03bad = 1;
  }
  // LBA29 Run numbers 350310 - 352514
  if (signalJets[jetcounter].Eta() > (0. - 0.2) &&
      signalJets[jetcounter].Eta() < (0.9 + 0.2) &&
      fabs(TVector2::Phi_mpi_pi(signalJets[jetcounter].Phi() - 2.798)) <
          (0.2 + 9.81e-02 / 2.)) {
    LBA29bad = 1;
  }
  // LBA30 Run numbers 350361 - 352514
  if (signalJets[jetcounter].Eta() > (0. - 0.2) &&
      signalJets[jetcounter].Eta() < (0.9 + 0.2) &&
      fabs(TVector2::Phi_mpi_pi(signalJets[jetcounter].Phi() - 2.896)) <
          (0.2 + 9.81e-02 / 2.)) {
    LBA30bad = 1;
  }
  // LBA32 Run numbers 355261 - 364292
  if (signalJets[jetcounter].Eta() > (0. - 0.2) &&
      signalJets[jetcounter].Eta() < (0.9 + 0.2) &&
      fabs(TVector2::Phi_mpi_pi(signalJets[jetcounter].Phi() - 3.093)) <
          (0.2 + 9.81e-02 / 2.)) {
    LBA32bad = 1;
  }

  // For all taus that are either closer to met than the closest jet or closer
  // than 0.2 in phi do the same
  for (unsigned int taucounter = 0; taucounter < signalTaus.size(); taucounter++) {
    if (fabs(metVec.DeltaPhi(signalTaus[taucounter])) < 0.2 ||
        fabs(metVec.DeltaPhi(signalTaus[taucounter])) < dphijetmet) {
      if (signalTaus[taucounter].Eta() > (0. - 0.2) &&
          signalTaus[taucounter].Eta() < (0.9 + 0.2) &&
          fabs(TVector2::Phi_mpi_pi(signalTaus[taucounter].Phi() - 5.05)) <
              (0.2 + 9.81e-02 / 2.)) {
        LBA52bad = 1;
      }
      // LBC5 in 2016
      if (signalTaus[taucounter].Eta() > (0 - 0.2) &&
          signalTaus[taucounter].Eta() < (0.9 + 0.2) &&
          fabs(TVector2::Phi_mpi_pi(signalTaus[taucounter].Phi() - 0.44)) <
              (0.2 + 9.81e-02 / 2.)) {
        LBC5bad = 1;
      }
      // LBC63 in 2017:
      if (signalTaus[taucounter].Eta() > (-0.9 - 0.2) &&
          signalTaus[taucounter].Eta() < (0. + 0.2) &&
          fabs(TVector2::Phi_mpi_pi(signalTaus[taucounter].Phi() + 0.147)) <
              (0.2 + 9.81e-02 / 2.)) {
        LBC63bad = 1;
      }
      // EBA03 in 2017
      if (signalTaus[taucounter].Eta() > 0.8 &&
          signalTaus[taucounter].Eta() < 1.7 &&
          fabs(TVector2::Phi_mpi_pi(signalTaus[taucounter].Phi() - 0.245)) <
              9.81e-02) {
        EBA03bad = 1;
      }
      // LBA29 Run numbers 350310 - 352514
      if (signalTaus[taucounter].Eta() > (0. - 0.2) &&
          signalTaus[taucounter].Eta() < (0.9 + 0.2) &&
          fabs(TVector2::Phi_mpi_pi(signalTaus[taucounter].Phi() - 2.798)) <
              (0.2 + 9.81e-02 / 2.)) {
        LBA29bad = 1;
      }
      // LBA30 Run numbers 350361 - 352514
      if (signalTaus[taucounter].Eta() > (0. - 0.2) &&
          signalTaus[taucounter].Eta() < (0.9 + 0.2) &&
          fabs(TVector2::Phi_mpi_pi(signalTaus[taucounter].Phi() - 2.896)) <
              (0.2 + 9.81e-02 / 2.)) {
        LBA30bad = 1;
      }
      // LBA32 Run numbers 355261 - 364292
      if (signalTaus[taucounter].Eta() > (0. - 0.2) &&
          signalTaus[taucounter].Eta() < (0.9 + 0.2) &&
          fabs(TVector2::Phi_mpi_pi(signalTaus[taucounter].Phi() - 3.093)) <
              (0.2 + 9.81e-02 / 2.)) {
        LBA32bad = 1;
      }
    }
  }

  // The tile cleaning is only relevant for fractions of dataset so vetoing
  // events based on them doesn't make much sense. Instead a weight is applied
  // based on fraction of 1 - (number of events in the part of the dataset
  // affected)(total number of events in the dataset). Here integrated
  // luminosity is used, if you are using something other than 2015-2018 ATLAS
  // dataset change the numbers accordingly
  double tile_weight = 1;
  if (LBA52bad) {
    tile_weight = tile_weight * (139.1 - 13.1) / 139.1;
  }
  if (LBC5bad) {
    tile_weight = tile_weight * (139.1 - 17.4) / 139.1;
  }
  if (LBC63bad) {
    tile_weight = tile_weight * (139.1 - 44.3) / 139.1;
  }
  if (EBA03bad) {
    tile_weight = tile_weight * (139.1 - 44.3) / 139.1;
  }
  if (LBA29bad) {
    tile_weight = tile_weight * (139.1 - 11.9) / 139.1;
  }
  if (LBA30bad) {
    tile_weight = tile_weight * (139.1 - 11.8) / 139.1;
  }
  if (LBA32bad) {
    tile_weight = tile_weight * (139.1 - 37.6) / 139.1;
  }

  float dphiMin2 =
      minDphi(metVec, signalJets,
              2); // Smallest Delta Phi between leading 2-jets and MET

  // Top CRs
  // CR_Top_mutau_true
  if (signalTaus.size() == 1 && signalMuons.size() == 1 && bjets.size() == 2 &&
      jet_pt > 140 && jet_pt2 > 100 && bjet_pt > 100 && met > 160 &&
      dphiMin2 > 0.5 && HT > 600 && HT < 1000) {
    if (calcMT(signalTaus[0], metVec) < 80 &&
        signalTaus[0].charge() != signalMuons[0].charge()) {
      accept("CR_Top_mutau_true", tile_weight);
    }
  }

  // CR_Top_mutau_fake. It will likely be empty in truth, tau_mT > 100 is pretty
  // effective...
  else if (signalTaus.size() == 1 && signalMuons.size() == 1 &&
           bjets.size() == 2 && jet_pt > 140 && jet_pt2 > 100 &&
           bjet_pt > 100 && met > 160 && dphiMin2 > 0.5 && HT > 600 &&
           HT < 1000) {
    if (calcMT(signalTaus[0], metVec) > 100 &&
        signalTaus[0].charge() != signalMuons[0].charge()) {
      accept("CR_Top_mutau_fake", tile_weight);
    }
  }

  // CR_Top_tau_true
  else if (signalTaus.size() == 1 && signalMuons.size() == 0 &&
           bjets.size() == 2 && jet_pt > 140 && jet_pt2 > 100 &&
           bjet_pt > 100 && met > 160 && dphiMin2 > 0.5 && HT > 600 &&
           HT < 1000) {
    if (calcMT(signalTaus[0], metVec) < 80) {
      accept("CR_Top_tau_true", tile_weight);
    }
  }

  // CR_Top_mu
  else if (signalTaus.size() == 0 && signalMuons.size() == 1 &&
           bjets.size() == 2 && jet_pt > 140 && jet_pt2 > 100 &&
           bjet_pt > 100 && met > 160 && dphiMin2 > 0.5 && HT > 600 &&
           HT < 1000) {
    accept("CR_Top_mu", tile_weight);
  }

  // CR_Z_mumu2b. Most likely empty in truth unless you have true Z->mumu.
  if (signalTaus.size() == 0 && signalMuons.size() == 2 && bjets.size() == 2 &&
      jet_pt > 140 && jet_pt2 > 100 && met < 100 && dphiMin2 > 0.5 &&
      HT > 600 && HT < 1000) {
    if ((signalMuons[0] + signalMuons[1]).M() > 81 &&
        (signalMuons[0] + signalMuons[1]).M() < 101 &&
        (signalMuons[0] + signalMuons[1]).Pt() > 200 &&
        signalMuons[0].Pt() > 30 &&
        signalMuons[1].charge() != signalMuons[0].charge()) {
      accept("CR_Z_mumu2b", tile_weight);
    }
  }

  // CR_Z_mumu0b. Most likely empty in truth unless you have true Z->mumu.
  else if (signalTaus.size() == 0 && signalMuons.size() == 2 &&
           bjets.size() == 0 && jet_pt > 140 && jet_pt2 > 100 && met < 100 &&
           dphiMin2 > 0.5 && HT > 600 && HT < 1000) {
    if ((signalMuons[0] + signalMuons[1]).M() > 81 &&
        (signalMuons[0] + signalMuons[1]).M() < 101 &&
        (signalMuons[0] + signalMuons[1]).Pt() > 200 &&
        signalMuons[0].Pt() > 30 &&
        signalMuons[1].charge() != signalMuons[0].charge()) {
      accept("CR_Z_mumu0b", tile_weight);
    }
  }

  // CR_Z_tautau0b. 0b kills sbottom multi-b signal effectively.
  else if (signalTaus.size() == 2 && signalMuons.size() == 0 &&
           bjets.size() == 0 && jet_pt > 140 && jet_pt2 > 100 && met > 200 &&
           dphiMin2 > 0.5 && HT > 600 && HT < 1000) {
    if (signalTaus[1].charge() != signalTaus[0].charge() &&
        (calcMT(signalTaus[0], metVec) + calcMT(signalTaus[1], metVec)) < 100) {
      accept("CR_Z_tautau0b", tile_weight);
    }
  }

  // The SRs are all required to have at least 3 jets, 2 b-jets (which also
  // count as jets) and at least 2 taus of opposite charge. A muon veto is also
  // applied.
  if (signalTaus.size() < 2) {
    return;
  }
  if (bjets.size() < 2) {
    return;
  }
  if (signalMuons.size() > 1) {
    return;
  }
  if (signalTaus[0].charge() == signalTaus[1].charge()) {
    return;
  }

  // Trigger Plateau cuts, apply here so that the mt2 computation isn't running
  // for every event.
  if (!(jet_pt > 140 && jet_pt2 > 100 && bjet_pt > 100 && met > 160 &&
        dphiMin2 > 0.5)) {
    return;
  }

  // Stransverse mass mt2 only defined for 2 tau regions. The mass of invisible
  // particles is set to 120 GeV.
  double mt2 = -1;
  if (signalTaus.size() >= 2) {
    mt2 = calcAMT2(signalTaus[0], signalTaus[1], metVec, 120, 120);
  }

  // Take the two leading b-jets and taus, select the minimal b-jet - tau angle
  // between them.
  double mintheta = -1;
  if (signalTaus.size() >= 2 && bjets.size() >= 2) {
    TLorentzVector bjet1, bjet2, tau1, tau2;
    bjet1.SetPtEtaPhiM(bjets[0].Pt(), bjets[0].Eta(), bjets[0].Phi(),
                       bjets[0].M());
    bjet2.SetPtEtaPhiM(bjets[1].Pt(), bjets[1].Eta(), bjets[1].Phi(),
                       bjets[1].M());
    tau1.SetPtEtaPhiM(signalTaus[0].Pt(), signalTaus[0].Eta(),
                      signalTaus[0].Phi(), signalTaus[0].M());
    tau2.SetPtEtaPhiM(signalTaus[1].Pt(), signalTaus[1].Eta(),
                      signalTaus[1].Phi(), signalTaus[1].M());
    mintheta =
        std::min(std::min(bjet1.Angle(tau1.Vect()), bjet1.Angle(tau2.Vect())),
                 std::min(bjet2.Angle(tau1.Vect()), bjet2.Angle(tau2.Vect())));
  }

  double ditaum = -1;
  if (signalTaus.size() >= 2) {
    ditaum = (signalTaus[0] + signalTaus[1]).M();
  }

  if (HT > 1100 && mt2 > 140 && mintheta > 0.5 && ditaum > 55 && ditaum < 120) {
    accept("SinglebinSR", tile_weight);
  }

  if (HT > 1100 && mt2 > 140 && mintheta > 0.0 && mintheta < 0.5 &&
      ditaum > 55 && ditaum < 120) {
    accept("MultibinSR_0_05", tile_weight);
  } else if (HT > 1100 && mt2 > 140 && mintheta > 0.5 && mintheta < 1.0 &&
             ditaum > 55 && ditaum < 120) {
    accept("MultibinSR_05_1", tile_weight);
  } else if (HT > 1100 && mt2 > 140 && mintheta > 1.0 && ditaum > 55 &&
             ditaum < 120) {
    accept("MultibinSR_1_plus", tile_weight);
  }

  std::vector<int> n_prong;

  for (unsigned int i = 0; i < signalTaus.size(); i++) {
    if (signalTaus[i].pass(TauOneProng)) {
      n_prong.push_back(1);
    } else {
      n_prong.push_back(3);
    }
  }

  // Fill in optional ntuple variables
  ntupVar("met", met); // can be simple variables
  ntupVar("tile_weight", tile_weight);
  ntupVar("mt2", mt2);
  ntupVar("nJets", nJets);
  ntupVar("nBjets", nBjets);
  ntupVar("nMuons", nMuons);
  ntupVar("nTaus", nTaus);
  ntupVar("signalTaus", signalTaus);
  ntupVar("signalMuons", signalMuons);
  ntupVar("signalElectrons", signalElectrons);
  ntupVar("signalJets", signalJets);
  ntupVar("signalBjets", bjets);
  if (nBjets > 0)
    ntupVar("leadingBjet", bjets[0]);
  if (nJets > 0)
    ntupVar("leadingJet", signalJets[0]);
  ntupVar("susyProcess", event->getSUSYChannel());
  ntupVar("mcDSID", event->getMCNumber());
  ntupVar("mcWeights", event->getMCWeights());
  ntupVar("jet_pt", jet_pt);
  ntupVar("ht", HT);
  ntupVar("deltaphi", dphiMin2);
  ntupVar("met_phi", met_phi);
  ntupVar("tau_n_prong", n_prong);

  return;
}
