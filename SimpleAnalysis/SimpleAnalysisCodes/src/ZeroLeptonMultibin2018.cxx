#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(ZeroLeptonMultibin2018)

void ZeroLeptonMultibin2018::Init()
{
  // Multibin based SR's
  addRegions({"MB_SSd_SR_2_1000_10","MB_SSd_SR_2_1000_16","MB_SSd_SR_2_1000_22","MB_SSd_SR_2_1600_10","MB_SSd_SR_2_1600_16","MB_SSd_SR_2_1600_22","MB_SSd_SR_2_2200_16","MB_SSd_SR_2_2200_22","MB_SSd_SR_2_2800_16","MB_SSd_SR_2_2800_22","MB_SSd_SR_2_3400_22","MB_SSd_SR_2_3400_28","MB_SSd_SR_2_4000_22","MB_SSd_SR_2_4000_28","MB_SSd_SR_4_1000_10","MB_SSd_SR_4_1000_16","MB_SSd_SR_4_1000_22","MB_SSd_SR_4_1600_10","MB_SSd_SR_4_1600_16","MB_SSd_SR_4_1600_22","MB_SSd_SR_4_2200_16","MB_SSd_SR_4_2200_22","MB_SSd_SR_4_2800_16","MB_SSd_SR_4_2800_22"});
  addRegions({"MB_GGd_SR_4_1000_10","MB_GGd_SR_4_1000_16","MB_GGd_SR_4_1000_22","MB_GGd_SR_4_1600_10","MB_GGd_SR_4_1600_16","MB_GGd_SR_4_1600_22","MB_GGd_SR_4_2200_10","MB_GGd_SR_4_2200_16","MB_GGd_SR_4_2200_22","MB_GGd_SR_4_2800_10","MB_GGd_SR_4_2800_16","MB_GGd_SR_4_2800_22","MB_GGd_SR_4_3400_10","MB_GGd_SR_4_3400_16","MB_GGd_SR_4_3400_22","MB_GGd_SR_4_4000_10","MB_GGd_SR_4_4000_16","MB_GGd_SR_4_4000_22"});
  addRegions({"MB_C_SR_2_1600_16","MB_C_SR_2_1600_22","MB_C_SR_2_2200_16","MB_C_SR_2_2200_22","MB_C_SR_2_2800_16","MB_C_SR_2_2800_22","MB_C_SR_4_1600_16","MB_C_SR_4_1600_22","MB_C_SR_4_2200_16","MB_C_SR_4_2200_22","MB_C_SR_4_2800_16","MB_C_SR_4_2800_22","MB_C_SR_5_1600_16","MB_C_SR_5_1600_22","MB_C_SR_5_2200_16","MB_C_SR_5_2200_22","MB_C_SR_5_2800_16","MB_C_SR_5_2800_22"});

  //addRegions({"MB_SSd_CRT_2_1000","MB_SSd_CRT_2_1600","MB_SSd_CRT_2_2200","MB_SSd_CRT_2_2800","MB_SSd_CRT_2_3400","MB_SSd_CRT_2_4000","MB_SSd_CRT_4_1000","MB_SSd_CRT_4_1600","MB_SSd_CRT_4_2200","MB_SSd_CRT_4_2800"});
  //addRegions({"MB_GGd_CRT_4_1000","MB_GGd_CRT_4_1600","MB_GGd_CRT_4_2200","MB_GGd_CRT_4_2800","MB_GGd_CRT_4_3400","MB_GGd_CRT_4_4000"});
  //addRegions({"MB_C_CRT_2_1600","MB_C_CRT_2_2200","MB_C_CRT_2_2800","MB_C_CRT_4_1600","MB_C_CRT_4_2200","MB_C_CRT_4_2800","MB_C_CRT_5_1600","MB_C_CRT_5_2200","MB_C_CRT_5_2800"});


}

void ZeroLeptonMultibin2018::ProcessEvent(AnalysisEvent *event)
{
  auto electrons  = event->getElectrons(7, 2.47, ELooseLH);
  auto muons      = event->getMuons(6, 2.7, MuMedium);
  auto jets       = event->getJets(20., 2.8);
  auto metVec     = event->getMET();
  double met      = metVec.Et();
  int OverlapVeto = event->getMCVeto();
  //sample overlap removal
  //xAOD::TStore* store = xAOD::TActiveStore::store();
  //unsigned int* pveto = 0;
  //int OverlapVeto=0;
  //if ( !store->retrieve<unsigned int>(pveto,"mcVetoCode").isSuccess() ) throw std::runtime_error("could not retrieve mcVetoCode");
  //  OverlapVeto = *pveto;

  // Reject events with bad jets
  if (countObjects(jets, 20, 2.8, NOT(LooseBadJet))!=0) return;


  // Standard SUSY overlap removal
  jets       = overlapRemoval(jets, electrons, 0.2);
  electrons  = overlapRemoval(electrons, jets, 0.4);
  muons      = overlapRemoval(muons, jets, 0.4);

  // Define signal lepton and bjet
  auto signalMuons      = filterObjects(muons, 6, 2.7, MuD0Sigma3 | MuZ05mm | MuIsoFixedCutTightTrackOnly);
  auto signalElectrons  = filterObjects(electrons, 7, 2.47, ETightLH | ED0Sigma5 | EZ05mm | EIsoFixedCutTight);
  auto signalMuons1     = filterObjects(muons, 50, 2.7, MuD0Sigma3 | MuZ05mm | MuIsoFixedCutTightTrackOnly);
  auto signalElectrons1 = filterObjects(electrons, 50, 2.47, ETightLH | ED0Sigma5 | EZ05mm | EIsoFixedCutTight);

  auto goodJets        = filterObjects(jets, 50);
  auto bjets           = filterObjects(goodJets, 50, 2.5, BTag77MV2c20);


  // baseline lepton veto
  auto leptons        = electrons + muons;
  auto signalleptons  = signalElectrons + signalMuons;
  auto signalleptons1 = signalElectrons1 + signalMuons1;

  // preselection: SR and CRT
  if ((leptons.size() == 0 || signalleptons.size() == 1)){}
  else
    return;

  auto corrected_jets = (leptons.size() == 0) ? goodJets : goodJets + signalleptons1;

  // Define MeffIncl
  float meffIncl = sumObjectsPt(corrected_jets) + met;

  // Define Njets
  int Njets = corrected_jets.size();


  // preselection
  if(met < 300) return;
  if(corrected_jets.size() < 2) return;
  if(corrected_jets[0].Pt() < 200.) return;
  if(corrected_jets[1].Pt() < 50.) return;
  if(meffIncl < 800) return;

  //std::cout << OverlapVeto << std::endl;

  // Meff based analysis regions and selections
  float meff[7] = {};
  for(int nJet=2; nJet<=6; nJet++)
    meff[nJet] = sumObjectsPt(corrected_jets, nJet) + met;

  float dphiMin3    = minDphi(metVec, corrected_jets, 3);
  float dphiMinRest = minDphi(metVec, corrected_jets);
  float Ap          = aplanarity(corrected_jets);
  //  float Sp          = sphericity(corrected_jets);

  float mT = 0.0;
  if ( signalleptons.size() == 1 )
    mT = calcMT(signalleptons[0], metVec);

  int NLeps = leptons.size();
  int NSigLeps = signalleptons.size();
  int Nbjets = bjets.size();

  ntupVar("NLeps", NLeps);
  ntupVar("NSigLeps", NSigLeps);
  ntupVar("met", met);
  ntupVar("meff", meffIncl);
  ntupVar("Ap", Ap);
  ntupVar("nJet",Njets);
  ntupVar("mT", mT);
  if (Njets >= 1 )
    ntupVar("jetPt0", corrected_jets[0].Pt());
  if (Njets >= 2 )
    ntupVar("jetPt1", corrected_jets[1].Pt());
  if (Njets >= 3 )
    ntupVar("jetPt2", corrected_jets[2].Pt());
  if (Njets >= 4 )
    ntupVar("jetPt3", corrected_jets[3].Pt());
  if (Njets >= 5 )
    ntupVar("jetPt4", corrected_jets[4].Pt());
  if (Njets >= 6 )
    ntupVar("jetPt5", corrected_jets[5].Pt());

  if (Njets >= 1 )
    ntupVar("jetEta0",corrected_jets[0].Eta());
  else
    ntupVar("jetEta0",-999.0);
  if (Njets >= 2 )
    ntupVar("jetEta1",corrected_jets[1].Eta());
  else
    ntupVar("jetEta1",-999.0);
  if (Njets >= 3 )
    ntupVar("jetEta2",corrected_jets[2].Eta());
  else
    ntupVar("jetEta2",-999.0);
  if (Njets >= 4 )
    ntupVar("jetEta3",corrected_jets[3].Eta());
  else
    ntupVar("jetEta3",-999.0);
  if (Njets >= 5 )
    ntupVar("jetEta4",corrected_jets[4].Eta());
  else
    ntupVar("jetEta4",-999.0);
  if (Njets >= 6 )
    ntupVar("jetEta5",corrected_jets[5].Eta());
  else
    ntupVar("jetEta5",-999.0);

  ntupVar("dPhi",dphiMin3);
  ntupVar("dPhiR",dphiMinRest);
  ntupVar("metovermeff3",met/meff[4]);
  ntupVar("veto",OverlapVeto);
  ntupVar("nBJet",Nbjets);

  // MB_SSd SR definition
  if ( OverlapVeto==0 && leptons.size()  == 0 && Njets >= 2 && met >= 300.0 && corrected_jets[0].Pt() >= 200.0 && corrected_jets[1].Pt() >= 100.0 && abs(corrected_jets[0].Eta()) <= 2.0 && abs(corrected_jets[1].Eta()) <= 2.0 && dphiMin3 >= 0.8 && (dphiMinRest >= 0.4||Njets<=3) && met/sqrt(meffIncl - met) >= 10 && meffIncl >= 1000.0){
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 1000.0 && meffIncl < 1600.0 && met/sqrt(meffIncl - met) >= 10.0 && met/sqrt(meffIncl - met) < 16.0 && corrected_jets[1].Pt()>250 )
      accept("MB_SSd_SR_2_1000_10");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 1000.0 && meffIncl < 1600.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 && corrected_jets[1].Pt()>250)
      accept("MB_SSd_SR_2_1000_16");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 1000.0 && meffIncl < 1600.0 && met/sqrt(meffIncl - met) >= 22.0  && corrected_jets[1].Pt()>250)
      accept("MB_SSd_SR_2_1000_22");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 1600.0 && met/sqrt(meffIncl - met) >= 10.0 && met/sqrt(meffIncl - met) < 16.0 && corrected_jets[1].Pt()>250 )
      accept("MB_SSd_SR_2_1600_10");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 && corrected_jets[1].Pt()>250)
      accept("MB_SSd_SR_2_1600_16");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 22.0 && corrected_jets[1].Pt()>250 )
      accept("MB_SSd_SR_2_1600_22");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 && corrected_jets[1].Pt()>250)
      accept("MB_SSd_SR_2_2200_16");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 22.0 && corrected_jets[1].Pt()>250 )
      accept("MB_SSd_SR_2_2200_22");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2800.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 && corrected_jets[1].Pt()>250)
      accept("MB_SSd_SR_2_2800_16");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2800.0 && meffIncl < 3400.0 && met/sqrt(meffIncl - met) >= 22.0 && corrected_jets[1].Pt()>250 )
      accept("MB_SSd_SR_2_2800_22");
    if ( Njets >= 2 &&  meffIncl >= 3400.0 && meffIncl < 4000.0 && met/sqrt(meffIncl - met) >= 22.0 && met/sqrt(meffIncl - met) < 28.0 && corrected_jets[1].Pt()>250)
      accept("MB_SSd_SR_2_3400_22");
    if ( Njets >= 2 &&  meffIncl >= 3400.0 && meffIncl < 4000.0 && met/sqrt(meffIncl - met) >= 28.0 && corrected_jets[1].Pt()>250 )
      accept("MB_SSd_SR_2_3400_28");
    if ( Njets >= 2 &&  meffIncl >= 4000.0 && met/sqrt(meffIncl - met) >= 22.0 && met/sqrt(meffIncl - met) < 28.0 && corrected_jets[1].Pt()>250 )
      accept("MB_SSd_SR_2_4000_22");
    if ( Njets >= 2 &&  meffIncl >= 4000.0 && met/sqrt(meffIncl - met) >= 28.0 && corrected_jets[1].Pt()>250 )
      accept("MB_SSd_SR_2_4000_28");
    if ( Njets >= 4 &&  meffIncl >= 1000.0 && meffIncl < 1600.0 && met/sqrt(meffIncl - met) >= 10.0 && met/sqrt(meffIncl - met) < 16.0 )
      accept("MB_SSd_SR_4_1000_10");
    if ( Njets >= 4 &&  meffIncl >= 1000.0 && meffIncl < 1600.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_SSd_SR_4_1000_16");
    if ( Njets >= 4 &&  meffIncl >= 1000.0 && meffIncl < 1600.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_SSd_SR_4_1000_22");
    if ( Njets >= 4 &&  meffIncl >= 1600.0 && met/sqrt(meffIncl - met) >= 10.0 && met/sqrt(meffIncl - met) < 16.0 )
      accept("MB_SSd_SR_4_1600_10");
    if ( Njets >= 4 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_SSd_SR_4_1600_16");
    if ( Njets >= 4 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_SSd_SR_4_1600_22");
    if ( Njets >= 4 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_SSd_SR_4_2200_16");
    if ( Njets >= 4 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_SSd_SR_4_2200_22");
    if ( Njets >= 4 &&  meffIncl >= 2800.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_SSd_SR_4_2800_16");
    if ( Njets >= 4 &&  meffIncl >= 2800.0 && meffIncl < 3400.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_SSd_SR_4_2800_22");
  }
  // MB_GGd SR definition
  if ( OverlapVeto==0 && leptons.size()  == 0 && Njets >= 4 && met >= 300.0 && corrected_jets[0].Pt() >= 200.0 && corrected_jets[1].Pt() >= 100.0 && corrected_jets[2].Pt() >= 100.0 && corrected_jets[3].Pt() >= 100.0 && abs(corrected_jets[0].Eta()) <= 2.0 && abs(corrected_jets[1].Eta()) <= 2.0 && abs(corrected_jets[2].Eta()) <= 2.0 && abs(corrected_jets[3].Eta()) <= 2.0 && dphiMin3 >= 0.4 && (dphiMinRest >= 0.4||Njets<=3) && Ap >= 0.04 && met/sqrt(meffIncl - met) >= 10 && meffIncl >= 1000.0){
    if ( meffIncl >= 1000.0 && meffIncl < 1600.0 && met/sqrt(meffIncl - met) >= 10.0 && met/sqrt(meffIncl - met) < 16.0 )
      accept("MB_GGd_SR_4_1000_10");
    if ( meffIncl >= 1000.0 && meffIncl < 1600.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_GGd_SR_4_1000_16");
    if ( meffIncl >= 1000.0 && meffIncl < 1600.0 && met/sqrt(meffIncl - met) >= 22.0 )
      accept("MB_GGd_SR_4_1000_22");
    if ( meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 10.0 && met/sqrt(meffIncl - met) < 16.0 )
      accept("MB_GGd_SR_4_1600_10");
    if ( meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_GGd_SR_4_1600_16");
    if ( meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 22.0 )
      accept("MB_GGd_SR_4_1600_22");
    if ( meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 10.0 && met/sqrt(meffIncl - met) < 16.0 )
      accept("MB_GGd_SR_4_2200_10");
    if ( meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_GGd_SR_4_2200_16");
    if ( meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 22.0 )
      accept("MB_GGd_SR_4_2200_22");
    if ( meffIncl >= 2800.0 && meffIncl < 3400.0 && met/sqrt(meffIncl - met) >= 10.0 && met/sqrt(meffIncl - met) < 16.0 )
      accept("MB_GGd_SR_4_2800_10");
    if ( meffIncl >= 2800.0 && meffIncl < 3400.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_GGd_SR_4_2800_16");
    if ( meffIncl >= 2800.0 && meffIncl < 3400.0 && met/sqrt(meffIncl - met) >= 22.0 )
      accept("MB_GGd_SR_4_2800_22");
    if ( meffIncl >= 3400.0 && meffIncl < 4000.0 && met/sqrt(meffIncl - met) >= 10.0 && met/sqrt(meffIncl - met) < 16.0 )
      accept("MB_GGd_SR_4_3400_10");
    if ( meffIncl >= 3400.0 && meffIncl < 4000.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_GGd_SR_4_3400_16");
    if ( meffIncl >= 3400.0 && meffIncl < 4000.0 && met/sqrt(meffIncl - met) >= 22.0 )
      accept("MB_GGd_SR_4_3400_22");
    if ( meffIncl >= 4000.0 && met/sqrt(meffIncl - met) >= 10.0 && met/sqrt(meffIncl - met) < 16.0 )
      accept("MB_GGd_SR_4_4000_10");
    if ( meffIncl >= 4000.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_GGd_SR_4_4000_16");
    if ( meffIncl >= 4000.0 && met/sqrt(meffIncl - met) >= 22.0 )
      accept("MB_GGd_SR_4_4000_22");

  }
  // MB_C SR definition
  if ( OverlapVeto==0 && leptons.size()  == 0 && Njets >= 2 && met >= 300.0 && corrected_jets[0].Pt() >= 600.0 && corrected_jets[1].Pt() >= 50.0 && abs(corrected_jets[0].Eta()) <= 2.8 && abs(corrected_jets[1].Eta()) <= 2.8 && dphiMin3 >= 0.4 && (dphiMinRest >= 0.2||Njets<=3) && met/sqrt(meffIncl - met) >= 10 && meffIncl >= 1600.0){
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_C_SR_2_1600_16");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_C_SR_2_1600_22");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_C_SR_2_2200_16");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_C_SR_2_2200_22");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2800.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_C_SR_2_2800_16");
    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2800.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_C_SR_2_2800_22");
    if ( Njets >= 4 && Njets < 5 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_C_SR_4_1600_16");
    if ( Njets >= 4 && Njets < 5 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_C_SR_4_1600_22");
    if ( Njets >= 4 && Njets < 5 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_C_SR_4_2200_16");
    if ( Njets >= 4 && Njets < 5 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_C_SR_4_2200_22");
    if ( Njets >= 4 && Njets < 5 &&  meffIncl >= 2800.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_C_SR_4_2800_16");
    if ( Njets >= 4 && Njets < 5 &&  meffIncl >= 2800.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_C_SR_4_2800_22");
    if ( Njets >= 5 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_C_SR_5_1600_16");
    if ( Njets >= 5 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_C_SR_5_1600_22");
    if ( Njets >= 5 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_C_SR_5_2200_16");
    if ( Njets >= 5 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_C_SR_5_2200_22");
    if ( Njets >= 5 &&  meffIncl >= 2800.0 && met/sqrt(meffIncl - met) >= 16.0 && met/sqrt(meffIncl - met) < 22.0 )
      accept("MB_C_SR_5_2800_16");
    if ( Njets >= 5 &&  meffIncl >= 2800.0 && met/sqrt(meffIncl - met) >= 22.0  )
      accept("MB_C_SR_5_2800_22");
  }


//  // MB_SSd CRT definition
//  if ( OverlapVeto==0 && signalleptons.size()  == 1 && mT > 30.0 && mT < 100.0 && bjets.size() >= 1 && Njets >= 2 && met > 300.0 && corrected_jets[0].Pt() > 200.0 && corrected_jets[1].Pt() > 100.0 && abs(corrected_jets[0].Eta()) < 2.0 && abs(corrected_jets[1].Eta()) < 2.0 && met/sqrt(meffIncl - met) >= 10 && meffIncl >= 1000.0 ){
//    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 1000.0 && meffIncl < 1600.0 && corrected_jets[1].Pt()>250 )
//      accept("MB_SSd_CRT_2_1000");
//    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && corrected_jets[1].Pt()>250)
//      accept("MB_SSd_CRT_2_1600");
//    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && corrected_jets[1].Pt()>250)
//      accept("MB_SSd_CRT_2_2200");
//    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2800.0 && meffIncl < 3400.0 && corrected_jets[1].Pt()>250 )
//      accept("MB_SSd_CRT_2_2800");
//    if ( Njets >= 2 &&  meffIncl >= 3400.0 && meffIncl < 4000.0 && corrected_jets[1].Pt()>250 )
//      accept("MB_SSd_CRT_2_3400");
//    if ( Njets >= 2 &&  meffIncl >= 4000.0 && corrected_jets[1].Pt()>250 )
//      accept("MB_SSd_CRT_2_4000");
//    if ( Njets >= 4 &&  meffIncl >= 1000.0 && meffIncl < 1600.0 )
//      accept("MB_SSd_CRT_4_1000");
//    if ( Njets >= 4 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 )
//      accept("MB_SSd_CRT_4_1600");
//    if ( Njets >= 4 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 )
//      accept("MB_SSd_CRT_4_2200");
//    if ( Njets >= 4 &&  meffIncl >= 2800.0 && meffIncl < 3400.0 )
//      accept("MB_SSd_CRT_4_2800");
//  }
//  // MB_GGd CRT definition
//  if ( OverlapVeto==0 && signalleptons.size()  == 1 && mT > 30.0 && mT < 100.0 && bjets.size() >= 1 && Njets >= 4 && met > 300.0 && corrected_jets[0].Pt() > 200.0 && corrected_jets[1].Pt() > 100.0 && corrected_jets[2].Pt() > 100.0 && corrected_jets[3].Pt() > 100.0 && abs(corrected_jets[0].Eta()) < 2.0 && abs(corrected_jets[1].Eta()) < 2.0 && abs(corrected_jets[2].Eta()) < 2.0 && abs(corrected_jets[3].Eta()) < 2.0 && met/sqrt(meffIncl - met) >= 10 && meffIncl >= 1000.0){
//
//    if ( meffIncl >= 1000.0 && meffIncl < 1600.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_GGd_CRT_4_1000");
//    if ( meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_GGd_CRT_4_1600");
//    if ( meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_GGd_CRT_4_2200");
//    if ( meffIncl >= 2800.0 && meffIncl < 3400.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_GGd_CRT_4_2800");
//    if ( meffIncl >= 3400.0 && meffIncl < 4000.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_GGd_CRT_4_3400");
//    if ( meffIncl >= 4000.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_GGd_CRT_4_4000");
//  }
//  // MB_C CRT definition
//  if ( OverlapVeto==0 && signalleptons.size()  == 1 && mT > 30.0 && mT < 100.0 && bjets.size() >= 1 && Njets >= 2 && met > 300.0 && corrected_jets[0].Pt() > 600.0 && corrected_jets[1].Pt() > 50.0 && abs(corrected_jets[0].Eta()) < 2.8 && abs(corrected_jets[1].Eta()) < 2.8 && met/sqrt(meffIncl - met) >= 10 && meffIncl >= 1600.0){
//    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_C_CRT_2_1600");
//    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_C_CRT_2_2200");
//    if ( Njets >= 2 && Njets < 4 &&  meffIncl >= 2800.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_C_CRT_2_2800");
//    if ( Njets >= 4 && Njets < 5 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_C_CRT_4_1600");
//    if ( Njets >= 4 && Njets < 5 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_C_CRT_4_2200");
//    if ( Njets >= 4 && Njets < 5 &&  meffIncl >= 2800.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_C_CRT_4_2800");
//    if ( Njets >= 5 &&  meffIncl >= 1600.0 && meffIncl < 2200.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_C_CRT_5_1600");
//    if ( Njets >= 5 &&  meffIncl >= 2200.0 && meffIncl < 2800.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_C_CRT_5_2200");
//    if ( Njets >= 5 &&  meffIncl >= 2800.0 && met/sqrt(meffIncl - met) >= 10.0 )
//      accept("MB_C_CRT_5_2800");
//  }


  return;
}
