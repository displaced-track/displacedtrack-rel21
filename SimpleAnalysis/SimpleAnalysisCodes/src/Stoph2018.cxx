#include "SimpleAnalysisFramework/AnalysisClass.h"

DefineAnalysis(Stoph2018)

//Author: G.D'amen

void Stoph2018::Init()
{
  // Define signal/control regions
  addRegion("preselection");
  addRegions({"SRL", "SRH"});
  addRegions({"SRL1", "SRL2", "SRH1", "SRH2", "SRH3"});
}

void Stoph2018::ProcessEvent(AnalysisEvent *event)
{
  //fill("h_SumOfWeights", event->getMCWeights()[0]);
	
  // Soft Objects
  auto  electrons 	= event->getElectrons(5, 2.47, ELooseLH);
  auto  muons     	= event->getMuons(5, 2.5, MuMedium);
  auto  jets    	= event->getJets(25.,2.8);
  auto  metVec   	= event->getMET();

  electrons = overlapRemoval(electrons, jets, 0.4); 
  muons     = overlapRemoval(muons, jets, 0.4);  
  jets      = overlapRemoval(jets, electrons, 0.2);

  auto signal_jets30    = filterObjects(jets, 30, 2.5);
  auto signal_jets60    = filterObjects(jets, 60, 2.5);
  auto signal_bJets     = filterObjects(jets, 30, 2.5, BTag77MV2c10);
  auto signal_electrons = filterObjects(electrons, 20, 2.47, EMediumLH | EZ05mm | EIsoFixedCutTight);
  auto signal_muons     = filterObjects(muons, 20, 2.4, MuD0Sigma3 | MuZ05mm | MuIsoFixedCutTightTrackOnly);


  double MET    = metVec.Et();    
  double METsig = event->getMETSignificance();

  auto signal_leptons = signal_electrons + signal_muons;

  // Count objects
  auto njet30 = signal_jets30.size();
  auto njet60 = signal_jets60.size();
  auto nbjet  = signal_bJets.size();
  auto nlep = signal_leptons.size();

  // Preselection
  if (nlep == 1 && njet30 >= 4 && nbjet >=3) { 
    if (signal_leptons[0].Pt() > 30. || (signal_leptons[0].Pt() <= 30. && MET > 230.)){
      accept("preselection"); 
    }
  } else return;

  auto mT = sqrt(2. * signal_leptons[0].Pt() * MET * ( 1. - cos(signal_leptons[0].DeltaPhi(metVec))));
  
  if (nbjet >=4  && mT > 150.) {
    // Discovery Signal Regions	
    if (METsig > 7 && njet60 >= 6)
      accept("SRL");
    if (METsig > 12 && njet60 >= 4)  
      accept("SRH");

    // Binned Signal Regions	
    if (METsig > 7 && METsig <= 14 && njet60 == 5)
      accept("SRL1");
    if (METsig > 7 && METsig <= 14 && njet60 >= 6)  
      accept("SRL2");
    if (METsig > 10 && METsig <=12 && njet60 == 4)
      accept("SRH1");
    if (METsig > 12 && METsig <=14 && njet60 == 4)  
      accept("SRH2");
    if (METsig > 14 && njet60 >= 4)
      accept("SRH3");

  }

  return;
}
