from ROOT import *
import os
import argparse

'''
Script able to make a cutflow on ntuples, by projecting a certain variable to a TH1F.
Define path, sample and variable with respect to you want to perform the cutflow.
Command example: python cutflow.py -s Zll.root -v LeadingJetPt
'''


path = "/storage/amurrone/DisplacedTrack/ntuples_Apr2022_processed/Zee_wo_bkgEl/"

"""cut_list = [
    '(DatasetNumber >= 700320 && DatasetNumber <= 700322)',
    '(TrackPt > 1.5) && (DPhiTrackMET < 0.5) && (MET > 300. && MET < 400.)',
    '(TrackD0Sig > 2. && TrackD0Sig < 30.)'
]"""

cut_list = [
    '(DatasetNumber >= 700320 && DatasetNumber <= 700322)',
    '(TrackPt > 1.5) && (DPhiTrackMET < 0.5) && (MET > 600.)',
    '(TrackD0Sig > 2. && TrackD0Sig < 30.)'
]

"""cut_list = [
    '(DatasetNumber >= 700323 && DatasetNumber <= 700325)',
    '(TrackPt > 1.5) && (DPhiTrackMET < 0.5) && (MET > 300. && MET < 400.)',
    '(TrackD0Sig > 2. && TrackD0Sig < 30.)'
]"""

"""cut_list = [
    '(DatasetNumber >= 700323 && DatasetNumber <= 700325)',
    '(TrackPt > 1.5) && (DPhiTrackMET < 0.5) && (MET > 600.)',
    '(TrackD0Sig > 2. && TrackD0Sig < 30.)'
]"""

"""cut_list = [
    '(TrackPt > 1.5 && TrackPt < 5.) && (DPhiTrackMET < 0.5) && (MET > 300. && MET < 400.)',
    '(TrackD0Sig > 2. && TrackD0Sig < 30.)'
]"""

"""cut_list = [
    '(TrackPt > 1.5 && TrackPt < 5.) && (DPhiTrackMET < 0.5) && (MET > 600.)',
    '(TrackD0Sig > 2. && TrackD0Sig < 30.)'
]"""



weights_list = ['eventWeight', 'genWeight', 'campaignWeight']
lumi = 138965.16




def main():

    parser = argparse.ArgumentParser(description='Setting input directory and selection')
    parser.add_argument('-s','--sample', default='Zll.root', type=str, help='Sample to process')
    parser.add_argument('-v','--variable', default='LeadingJetPt', type=str, help='Variable with respect to perform the cutflow (the name should be the same as in the input tree)')

    args = parser.parse_args()
    sample = args.sample
    var = args.variable


    weights = '*'.join(weights_list)

    file = TFile.Open(path+sample)
    t = file.Get("Tree")

    h_nbins = 10
    h_min = 0
    h_max = 10
    hname_before = "h_{}_before".format(var)
    cuts_before = '({0}) * ({1})'.format(weights, lumi)
    h_before, n_unweighted_before, n_weighted_before = get_histo(t, hname_before, var, h_nbins,h_min, h_max, cuts_before)
    print ("Tree entries - ", "Unweighted entries: ", n_unweighted_before, "Weighted entries: ", n_weighted_before)
    cuts = []
    for cut in cut_list:
        cuts.append(cut)
        add_cut = ' && '.join(cuts)
        final_cuts = '({0}) * ({1}) * ({2})'.format(add_cut, weights, lumi)
        hname_after = "h_{}_after".format(var)
        h_after, n_unweighted_after, n_weighted_after = get_histo(t, hname_after, var, h_nbins,h_min, h_max, final_cuts)
        print (cut, "- ", "Unweighted entries: ", n_unweighted_after, "Weighted entries: ", n_weighted_after)


def get_histo(tree, name, var, nbins, min, max, cuts):
    h = TH1D(name, var, nbins, min, max)
    tree.Project(name, var, cuts)
    n_unweighted = h.GetEntries()
    n_weighted = h.Integral(0, nbins+1)
    return h, n_unweighted, n_weighted


if __name__ == "__main__":
  main()
