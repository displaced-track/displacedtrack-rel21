def read_XS(dsid):
    crossSections_file = open("/users2/amurrone/PMGxsecDB_mc16.txt", "r")
    lines = crossSections_file.readlines()[1:]

    for line in lines:
        id = line.split('\t')[0]
        xs = line.split('\t')[4]
        filter = line.split('\t')[6]
        kfactor = line.split('\t')[8]
        dsid_string = str(dsid)
        if (dsid_string==id):
           return float(xs), float(filter), float(kfactor), float(xs)*float(filter)*float(kfactor)

def main():

    print('user.tlari.skim17may.Zmumu_MAXHTPTV140_280_CVetoBVeto_EXT0')
    print('DSID: 364106',read_XS(364106))

    print('user.tlari.skim17may.Zmumu_MAXHTPTV140_280_CFilterBVeto_EXT0')
    print('DSID: 364107',read_XS(364107))

    print('user.tlari.skim17may.Zmumu_MAXHTPTV140_280_BFilter_EXT0')
    print('DSID: 364108',read_XS(364108))

    print('user.tlari.skim17may.Zmumu_MAXHTPTV280_500_CVetoBVeto_EXT0')
    print('DSID: 364109',read_XS(364109))

    print('user.tlari.skim17may_2f.Zmumu_MAXHTPTV280_500_CFilterBVeto_EXT0')
    print('DSID: 364110',read_XS(364110))

    print('user.tlari.skim17may.Zmumu_MAXHTPTV280_500_BFilter_EXT0')
    print('DSID: 364111',read_XS(364111))

    print('user.tlari.skim17may_2f.Zmumu_MAXHTPTV500_1000_EXT0')
    print('DSID: 364112',read_XS(364112))

    print('user.tlari.skim17may_2f.Zmumu_MAXHTPTV1000_ECMS_EXT0')
    print('DSID: 364113',read_XS(364113))

if __name__ == "__main__":
    main()
