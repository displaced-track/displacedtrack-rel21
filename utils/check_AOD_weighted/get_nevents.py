import ROOT
import sys
from ROOT import *

import argparse

parser = argparse.ArgumentParser(description='Parser')
parser.add_argument('--sample', type=str, help='sample')
args = parser.parse_args()

sample = args.sample

infile1 = TFile.Open("/storage/amurrone/DisplacedTrack/ntuples/"                           +sample+"/data-tree/"+sample+".root")
infile2 = TFile.Open("/storage/amurrone/DisplacedTrack/ntuples_May2021_HitInfo/"+sample+"/data-tree/"+sample+".root")

h_AOD_weighted1 = infile1.Get("weighted__AOD")
h_AOD_weighted2 = infile2.Get("weighted__AOD")

AOD_weighted1 = h_AOD_weighted1.GetBinContent(1)
AOD_weighted2 = h_AOD_weighted2.GetBinContent(1)

print ('{}, AOD weighted events: ntuples/ = {}, ntuples_May2021_HitInfo/ = {}, matched: {}'.format(sample, AOD_weighted1, AOD_weighted2, AOD_weighted1 == AOD_weighted2 ) )
